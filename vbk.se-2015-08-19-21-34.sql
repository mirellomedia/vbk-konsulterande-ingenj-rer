# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# V�rd: 127.0.0.1 (MySQL 5.5.34)
# Databas: vbk.se
# Genereringstid: 2015-08-19 19:34:31 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Tabelldump article
# ------------------------------------------------------------

DROP TABLE IF EXISTS `article`;

CREATE TABLE `article` (
  `articleId` int(11) NOT NULL AUTO_INCREMENT,
  `articleDate` date NOT NULL,
  `visibility` int(1) NOT NULL DEFAULT '1',
  `deletedAt` date DEFAULT NULL,
  PRIMARY KEY (`articleId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `article` WRITE;
/*!40000 ALTER TABLE `article` DISABLE KEYS */;

INSERT INTO `article` (`articleId`, `articleDate`, `visibility`, `deletedAt`)
VALUES
	(2,'2009-12-14',1,NULL),
	(3,'2009-12-14',1,NULL),
	(4,'2009-12-14',1,NULL),
	(5,'2009-12-14',1,NULL),
	(6,'2009-12-14',1,NULL),
	(7,'2009-12-18',1,NULL),
	(8,'2010-05-31',1,NULL),
	(9,'2010-09-01',1,NULL),
	(10,'2010-09-20',1,NULL),
	(11,'2010-09-20',1,NULL),
	(12,'2010-10-22',1,NULL),
	(13,'2010-11-10',1,NULL),
	(14,'2010-12-10',1,NULL),
	(15,'2010-02-15',1,NULL),
	(16,'2011-02-15',1,NULL),
	(17,'2011-03-28',1,NULL),
	(18,'2011-03-31',1,NULL),
	(19,'2011-03-31',1,NULL),
	(20,'2011-06-15',1,NULL),
	(21,'2011-06-16',1,NULL),
	(22,'2011-06-27',1,NULL),
	(23,'2011-07-24',1,NULL),
	(24,'2011-08-25',1,NULL),
	(25,'2011-08-27',1,NULL),
	(26,'2011-09-06',1,NULL),
	(27,'2011-09-30',1,NULL),
	(28,'2011-10-27',1,NULL),
	(29,'2011-12-08',1,NULL),
	(30,'2011-12-14',1,NULL),
	(31,'2011-12-19',1,NULL),
	(32,'2011-12-20',1,NULL),
	(33,'2012-02-01',1,NULL),
	(34,'2013-02-06',1,NULL),
	(35,'2013-03-07',1,NULL),
	(36,'2012-03-21',1,NULL),
	(37,'2012-05-09',1,NULL),
	(38,'2012-05-10',1,NULL),
	(39,'2012-06-05',1,NULL),
	(40,'2012-07-03',1,NULL),
	(41,'2012-09-11',1,NULL),
	(42,'2012-09-17',1,NULL),
	(43,'2012-10-02',1,NULL),
	(44,'2012-10-24',1,NULL),
	(45,'2012-10-31',1,NULL),
	(46,'2012-11-19',1,NULL),
	(47,'2012-11-26',1,NULL),
	(48,'2012-12-20',1,NULL),
	(49,'2013-01-08',1,NULL),
	(50,'2013-03-13',1,NULL),
	(51,'2013-03-27',1,NULL),
	(52,'2013-04-11',1,NULL),
	(53,'2013-06-10',1,NULL),
	(54,'2013-06-20',1,NULL),
	(55,'2013-08-20',1,NULL),
	(56,'2013-08-28',1,NULL),
	(57,'2013-09-13',1,NULL),
	(58,'2013-09-20',1,NULL),
	(59,'2013-10-16',1,NULL),
	(60,'2013-10-23',1,NULL),
	(61,'2013-10-28',1,NULL),
	(62,'2013-11-15',1,NULL),
	(63,'2013-12-16',1,NULL),
	(64,'2013-12-17',1,NULL),
	(65,'2014-01-10',1,NULL),
	(66,'2014-01-31',1,NULL),
	(67,'2014-02-14',1,NULL),
	(68,'2014-03-19',1,NULL),
	(69,'2014-04-15',1,NULL),
	(70,'2014-05-27',1,NULL),
	(71,'2014-06-19',1,NULL),
	(73,'2014-08-18',1,NULL),
	(77,'2014-09-05',1,NULL),
	(78,'2014-09-17',1,NULL),
	(81,'2014-10-02',1,NULL),
	(82,'2014-10-24',1,NULL),
	(83,'2014-11-17',1,NULL),
	(84,'2014-11-26',1,NULL),
	(85,'2014-11-28',1,NULL),
	(86,'2014-12-15',1,NULL),
	(87,'2014-12-17',1,NULL),
	(88,'2015-01-01',1,NULL),
	(90,'2015-02-01',1,NULL),
	(91,'2015-03-01',1,NULL),
	(92,'2015-04-09',1,NULL),
	(95,'2015-04-28',1,NULL),
	(96,'2015-05-27',0,'2015-05-27'),
	(97,'2015-06-02',1,NULL),
	(98,'2015-06-22',1,NULL),
	(99,'2015-06-22',0,'2015-06-22'),
	(100,'2015-06-22',1,NULL),
	(101,'2015-07-01',1,NULL),
	(102,'2015-07-03',1,NULL);

/*!40000 ALTER TABLE `article` ENABLE KEYS */;
UNLOCK TABLES;


# Tabelldump articlecontent
# ------------------------------------------------------------

DROP TABLE IF EXISTS `articlecontent`;

CREATE TABLE `articlecontent` (
  `articleId` int(11) NOT NULL,
  `languageId` int(2) NOT NULL,
  `articleHeader` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `articleLink` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `articleSum` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `articleContent` text COLLATE utf8_unicode_ci NOT NULL,
  `articleDate` date NOT NULL,
  `articleEdit` date NOT NULL,
  PRIMARY KEY (`articleId`,`languageId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `articlecontent` WRITE;
/*!40000 ALTER TABLE `articlecontent` DISABLE KEYS */;

INSERT INTO `articlecontent` (`articleId`, `languageId`, `articleHeader`, `articleLink`, `articleSum`, `articleContent`, `articleDate`, `articleEdit`)
VALUES
	(0,0,'','','','','0000-00-00','0000-00-00'),
	(2,1,'Utbildningslitteratur för Revit','utbildningslitteratur-for-revit','VBK skriver utbildningslitteratur för REVIT Structure 2010 att användas på Chalmers.\nUnder det senaste året har en av våra medarbetare, Caroline Holmqvist, varit hjälplärare inom arkitektur på Chalmers Tekniska Högskola. Tillsammans med Magnus Persson, CTH, har hon lärt ut REVIT Structure.','<p>\n	VBK skriver utbildningslitteratur för REVIT Structure 2010 att användas på Chalmers.\n</p>\n<p>\n	Under det senaste året har en av våra medarbetare, Caroline Holmqvist, varit hjälplärare inom arkitektur på Chalmers Tekniska Högskola. Tillsammans med Magnus Persson, CTH, har hon lärt ut REVIT Structure.\n</p>\n<p>\n	Carolines kunskaper inom REVIT uppmärksammades av WITU AB som bad henne skriva en lärobok för REVIT Structure 2010. Boken kommer att användas som kurslitteratur på bl.a. Chalmers men lämpar sig också för självstudier och som referensmaterial.\n</p>\n<p>\n	VBK har använt REVIT Structure i projekten Lisebergsteatern och NTC, Falkenberg m.fl.\n</p>\n<p>\n	För mer information är du välkommen att kontakta: <a href=\"mailto:caroline.h@vbk.se\">Caroline Holmqvist</a>\n</p>','2014-08-18','2015-01-09'),
	(3,1,'VBKs kontor i \"nya\" omgivningar','vbks-kontor-i-nya-omgivningar','Vi närmar oss nu slutet på den omfattande ombyggnad och hyresgästanpassning som pågår i byggnaden där VBKs kontor finns.\nI snart tre år har vi varit omgivna av en byggarbetsplats, som flera av er har lagt märke till när ni besökt oss.','<p>Vi närmar oss nu slutet på den omfattande ombyggnad och hyresgästanpassning som pågår i byggnaden där VBKs kontor finns.</p><p><img height=\"290\" alt=\"\" width=\"683\" src=\"/uploads/images//mektagonen.jpg\"></p><p>I snart tre år har vi varit omgivna av en byggarbetsplats, som flera av er har lagt märke till när ni besökt oss.</p><p>Veidekke har på uppdrag av Newsec varit entreprenör för ombyggnaden.</p><p>Bland annat har alla fönster bytts, fasaderna har fått nya ytskikt utvändigt, personhissarna bytts ut och ventilationssystemet blivit ombyggt.</p><p>Vi, VBK, har under tiden passat på att bygga om och anpassa våra lokaler.</p><p>När allt är färdigt senare i år kommer fastigheten som numera kallas Mektagonen, </p><p>f.d. Hällungen, att inrymma Hotel StayAt, Sportlife träningscenter, Willys livsmedelsbutik och kontor.</p><p>I den nya, ombyggda, entrén skall också finnas reception, café, restaurang och</p><p>konferenslokaler.</p>','2014-08-18','0000-00-00'),
	(4,1,'VBK agerar för miljön','vbk-agerar-for-miljon123','VBK vill dra sitt strå till stacken och har infört begreppet ?miljöriktig projektering?.','<p>VBK vill dra sitt strå till stacken och har infört begreppet \"miljöriktig projektering\".</p><p>Inom områdena energi, produktval och restproduktminimering, mark samt&nbsp;god inom- och utomhusmiljö har vi tagit fram riktlinjer för ett enhetligt arbetssätt.</p><p>Dessa riktlinjer och andra hjälpmedel har vi samlat i VBKs Miljömanual.</p><p>Miljömanualen skall vara vårt verktyg för miljöriktig projektering i de projekt vi medverkar.</p><p>För mer information är du välkommen att kontakta:<a href=\"mailto:peter.n@vbk.se\"> Peter Norrby</a> eller&nbsp;<a href=\"mailto:claes.s@vbk.se\">Claes Sjöström</a></p><p><a></a></p>','2014-08-18','2014-08-18'),
	(5,1,'Hotel Scandic Opalen','hotel-scandic-opalen','Påbyggnaden av fem extra våningar på hotel Scandic Opalen är ett exempel på att VBK är med när Göteborg förändras.','<p>Påbyggnaden av fem extra våningar på hotel Scandic Opalen är ett exempel på att VBK är med när Göteborg förändras.</p><p><img height=\"579\" alt=\"\" width=\"599\" src=\"/uploads/images/opalen.jpg\"></p><p><em>Fotomontage</em></p><p>De nya våningarna utförs med stålstomme och håldäcksbjälklag för att minska behovet av förstärkning av befintligt byggnad. Befintlig grundläggning har kompletterats och förstärkningar har utförts vid befintliga gavlar.</p><p>Organisation</p><ul><li>Entreprenör &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;PEAB</li><li>Arkitekt &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Arkitekterna Ktook &amp; Tjäder</li><li>Konstruktör &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;VBK</li><li>VVS &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Andersson &amp; Hultmark</li><li>El &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;Rejlers Ingenjörer</li></ul><p>Vill du ha ytterligare information om projektet är du välkommen att kontakta:&nbsp;<a href=\"mailto:lars-olof.h@vbk.se\">Lars-Olof Hellgren</a></p><p>Du kan också läsa mer på: Referensobjekt</p>','2014-08-18','0000-00-00'),
	(6,1,'Shell Raffinaderi AB - Ny kontrollrumsbyggnad','shell-raffinaderi-ab-ny-kontrollrumsbyggnad','När Shell bygger ny kontrollrumsbyggnad är VBK med. För att skapa en bättre och säkrare arbetsmiljö för raffinaderiets driftpersonal samt möta behovet av mer kontorsutrymmen uppför Shell Raffinaderi AB en ny kontrollrumsbyggnad.','<p>När Shell bygger ny kontrollrumsbyggnad är VBK med. För att skapa en bättre och säkrare arbetsmiljö för raffinaderiets driftpersonal samt möta behovet av mer kontorsutrymmen uppför Shell Raffinaderi AB en ny kontrollrumsbyggnad.</p><p>Allmänt</p><p>För att skapa en bättre och säkrare arbetsmiljö för raffinaderiets driftpersonal samt möta behovet av mer kontorsutrymmen uppför Shell Raffinaderi AB en ny kontrollrumsbyggnad. Byggnaden är uppdelad i två våningar på totalt 2500 m2 med våningshöjder på vardera 5m. Kontrollrum och tillhörande utrymmen för driftpersonal återfinns på nedre plan och kontorsutrymmen samt konferenssal på övre plan.</p><p><img height=\"284\" alt=\"\" width=\"577\" src=\"/uploads/images/shell.jpg\"></p><p>Allmänt Projektering</p><p>Projekteringen är indelad i 3 faser: Förstudie, Systemhandlingar och Bygghandlingar, där VBK har en stor del i form av projekt-/projekteringsledning samt A-, K- och markhandlingar. Bygghandlingsskedet påbörjades första veckan i december 2008 med färdigställande i mitten på juni 2009. VBK har även fått uppdraget att ta fram tillverkningsritningar för prefab gällande väggar, pelare och balkar. För modellering och framtagande av ritningar har i huvudsak Revit Structure 2009 men även AutoCAD Architechture 2009 använts. Tekla Structures 15.0 har använts för att modellera och göra tillverkningsritningar  för prefab.</p><p>Mark och Grundläggning</p><p>Bef. markyta består främst av hårdgjorda ytor av asfalt och grus med en mäktighet av ca 0,5 - 1m. Under detta finns ett större lager lera och närmast berg ett friktionsmaterial med en mäktighet av ca 0,1 - 0,5m. Avståndet till berg varierar för övrigt mellan ca 5m till ca 10m. Byggnaden grundläggs således till berg med hjälp av pålning (stålrörspålar). Pålplattor och stag-/grundbalkar utförs platsgjutna och gjuts samman med bottenplattan för att erhålla sidostabilitet. Vad gäller byggnadens vatten och avlopp så kommer dessa att anslutas till raffinaderiets system som bland annat innefattar egna vattenreningsanläggningar.  </p><p>Bottenplatta och stomme</p><p>För att korta ner tiden för stomresning tills dess att tätt hus erhålls, kommer merparten av byggnadens stomme att utföras i förtillverkade betongelement (prefab-betong). Normalelementen är av storleken 3 x 5m med en vikt av ca 10 ton. Tjockleken är 250mm för väggar i söderfasaden och 200 mm för övriga väggar. Montaget sker med hjälp av mobilkran direkt från bil och undviker på så sätt att stora upplagsytor tas i anspråk, då tillgången på dessa är begränsad. Dock platsgjutes bottenplattan (t=250mm), grundbalkar och pålfundament. Dessutom utförs tre stora väggskivor i plan 1 platsgjutna. Även bjälklagselementen, av plattbärlag, kräver pågjutning med ca 200mm platsgjuten betong.</p><p>Stomstabilitet utgörs av horisontella bjälklagsskivor i samtliga plan, vilka fördelar aktuell belastning till väggskivor i betong. Väggskivorna fördelar det stjälpande momentet till pålarna genom grundläggningen och horisontalkrafterna förs via bottenbjälklaget till grundbalkarna i fasaden samt stagbalkarna vilka fördelar krafterna till jorden genom så kallat passivt jordtryck.</p><p>Fasader</p><p>Ytterväggar förses med 200mm tjocka isolerade plåtkassetter, plåt/isolering/plåt, med ljus- och mörksilvermetallic kulör. Orsaken till detta val är att ge byggnaden ett modernt och arkitektoniskt tilltalande utseende, helt i linje med Shells ambition. Runt byggnaden närmast mark fästes pigmenterade prefabricerade bröstningselement av betong.</p><p>Ny trafikplan</p><p>I samband med uppförande av den nya kontrollrumsbyggnaden har beslut tagits att tung trafik ej skall passera huvudporten (norr), som ligger i omedelbar anslutning till den nya byggnaden, utan intas via raffinaderiets östra port. I projektet ingår bl a ny port, komplettering av vägar, vägarrangemang (vägräcken, belysning mm), kabelgravar samt en ny el-station. </p><p>VBK hjälper till med konstruktion och kontroll. För el-byggnaden dessutom med bygglovshandlingar samt kvalitetsansvarig enligt PBL. </p><p>Byggstart</p><p>Bygget påbörjades i februari 2009 och beräknas vara färdigställt våren 2010. </p><p>Organisation</p><p>Byggherre</p><p>Shell Raffinaderi AB</p><p>Projekteringsledare</p><p>VBK      </p><p>Arkitekt </p><p>VBK</p><p>Konstruktör  </p><p>VBK</p><p>Prefabkonstruktör </p><p>VBK</p><p>VVS    </p><p> BDAB</p><p>El </p><p>Rejlers  </p><p>Vill du ha ytterligare information om projektet är du välkommen att kontakta:<a href=\"mailto:ola.k@vbk.se\" style=\"background-color: rgb(253, 253, 253);\">Ola Kjellman</a></p>','2014-08-18','0000-00-00'),
	(7,1,'Välkommen till vår nya hemsida','valkommen-till-var-nya-hemsida','Vi är glada att kunna presentera vår nya hemsida.\n\nMed bättre sökfunktioner och ett modernare gränssnitt hoppas vi att du skall finna vår nya sida intressant. Titta gärna vidare på våra olika tjänster och våra referensprojekt.','<p>Vi är glada att kunna presentera vår nya hemsida.</p><p>Med bättre sökfunktioner och ett modernare gränssnitt hoppas vi att du skall finna vår nya sida intressant. Titta gärna vidare på våra olika tjänster och våra referensprojekt.</p><p>Tjänster</p><p>Byggprojektering</p><p>Projektadministration</p><p>Underhåll</p><p>Projekt</p><p>Referensobjekt</p><p>Pågående uppdrag</p><p>Har du frågor eller synpunkter om vår nya hemsida är du välkommen att kontakta:<a href=\"mailto:emanuel.d@vbk.se\">Emanuel Dagefors</a></p>','2014-08-18','0000-00-00'),
	(8,1,'Bas P och Bas U','bas-p-och-bas-u','VBK har vidareutbildat åtta av våra medarbetare till byggarbetsmiljösamordnare (BAS).\n\nRollen som byggarbetsmiljösamordnare tillkom i och med nya arbetsmiljöregler 1 januari 2009.','<p>VBK har vidareutbildat åtta av våra medarbetare till byggarbetsmiljösamordnare (BAS).</p><p>Rollen som byggarbetsmiljösamordnare tillkom i och med nya arbetsmiljöregler 1 januari 2009.</p><p>De nya reglerna innebär att byggherren är skyldig att utse en byggarbetsmiljösamordnare som skall leda arbetsmiljöarbetet under hela byggprocessen, från planering och projektering (P) till färdigt utförande (U). För att kunna bli utsedd till byggarbetsmiljösamordnare krävs utbildning, kompetens och erfarenhet som skall kunna styrkas.</p><p>Välkommen att kontakta oss:</p><p>Maria Berg, Anna Hallberg, Barbro Norrby, Peter Norrby, Claes Olofsson, Erland Olsson, Håkan Willbro, Jon Örn</p>','2014-08-18','0000-00-00'),
	(9,1,'Studieresor','studieresor','Under september kommer vi att åka på studieresor för att se intressanta byggnader med spännande konstruktionslösningar','<p>Under september kommer vi att åka på studieresor för att se intressanta byggnader med spännande konstruktionslösningar. Vi kommer också att umgås och ha trevligt tillsammans och få möjlighet att bekanta oss med en ny stad. Några av oss kommer att åka till Barcelona, andra till Athen och ett tredje gäng åker till Belgrad.</p>','2014-08-18','0000-00-00'),
	(11,1,'VBK köper PIAB','vbk-koper-piab','Från och med den 1 oktober 2010 tar VBK Konsulterande ingenjörer AB över verksamheten i PI i Göteborg PIAB AB. PIAB har sedan starten 1970 varit ett fristående konsultföretag i byggbranschen med inriktning på byggnadskonstruktion. Man har idag ca 20 medarbetare och bedriver verksamhet i Göteborg och Skövde.','<p>Från och med den 1 oktober 2010 tar VBK Konsulterande ingenjörer AB över verksamheten i PI i Göteborg PIAB AB. PIAB har sedan starten 1970 varit ett fristående konsultföretag i byggbranschen med inriktning på byggnadskonstruktion. Man har idag ca 20 medarbetare och bedriver verksamhet i Göteborg och Skövde.</p><p>Med större resurser, bredare kompetens, men med en fortsatt hög grad av personlighet är vi övertygade om att förändringen kommer att gynna våra kunder och samarbetspartners. Alla på PIAB idag anställda, både i Göteborg och Skövde, kommer att jobba vidare på VBK. Det innebär att du som är gammal PIAB-kund kommer att kunna behålla samma kontaktperson som tidigare och alla pågående PIAB-projekt kommer att drivas vidare, men nu med VBK som leverantör.</p><p>Efter övertagandet av PIAB är VBK ett starkare och mer komplett företag med drygt 75 medarbetare. Vi ser mycket ljust på framtiden och är redo att ta oss an nya spännande utmaningar inom våra verksamhetsområden byggprojektering och projektadministration.</p>','2014-08-18','0000-00-00'),
	(12,1,'Lyckad kundträff på Opalen','lyckad-kundtraff-pa-opalen','Drygt 120 kunder hade hörsammat VBKs inbjudan och samlades torsdagskvällen den 21 oktober på hotell Scandic Opalen.','<p>Drygt 120 kunder hade hörsammat VBKs inbjudan och samlades torsdagskvällen den 21 oktober på hotell Scandic Opalen.</p><p>Efter inledande mingel med öl och snittar bjöds det på faktaspäckade föredrag. Lars-Olof Hellgren redogjorde för Opalens påbyggnad och hur det kunde vara möjligt att bygga på ett tiovåningshus med ytterligare fem våningar. Därefter förklarade Leif Gustafson på ett mycket underhållande sätt för åhörarna att fem decimeter snö ibland kan vara alldeles för mycket last för vissa lätta tak. </p><p>Efter dessa framföranden var det så dags för kvällens höjdpunkt. En verbal match i nio ronder mellan två före detta höjdhoppare. I ena ringhörnan hittade vi Patrik Sjöberg, 2,42 m och i andra ringhörnan Stefan Holm, OS-guld. Kvällens huvuddomare var Maria Akraka. Patrik vann tillslut denna jämna tvekamp med 5-4. Det visade sig som så ofta förr att hemmaplan fällde avgörandet. </p><p>Efter ett par timmar med andlig spis kastade sig de hungriga gästerna därefter över kvällens rejäla buffé. En mycket lyckad kväll innehållande många skratt och kanske en och annan djupare tanke avslutades vid 23-tiden och deltagarna vandrade hemåt i den blåsiga Göteborgsnatten.</p><p>Nedan följer några bilder från kvällen.<img alt=\"\" width=\"683\" height=\"512\" src=\"/uploads/images/kundtraff2010/opalen01.JPG\"><img alt=\"\" width=\"683\" height=\"512\" src=\"/uploads/images/kundtraff2010/opalen02.JPG\"><img alt=\"\" width=\"683\" height=\"512\" src=\"/uploads/images/kundtraff2010/opalen03.JPG\"><img width=\"683\" height=\"512\" alt=\"\" src=\"/uploads/images/kundtraff2010/opalen04.JPG\"><img width=\"683\" height=\"512\" alt=\"\" src=\"/uploads/images/kundtraff2010/opalen05.JPG\"><img width=\"683\" height=\"512\" alt=\"\" src=\"/uploads/images/kundtraff2010/opalen06.JPG\"><img width=\"683\" height=\"512\" alt=\"\" src=\"/uploads/images/kundtraff2010/opalen07.JPG\"><img width=\"683\" height=\"512\" alt=\"\" src=\"/uploads/images/kundtraff2010/opalen08.JPG\"><img width=\"683\" height=\"512\" alt=\"\" src=\"/uploads/images/kundtraff2010/opalen09.JPG\"><img width=\"683\" height=\"512\" alt=\"\" src=\"/uploads/images/kundtraff2010/opalen10.JPG\"><img width=\"683\" height=\"512\" alt=\"\" src=\"/uploads/images/kundtraff2010/opalen11.JPG\"><img width=\"683\" height=\"512\" alt=\"\" src=\"/uploads/images/kundtraff2010/opalen12.JPG\"><img width=\"683\" height=\"512\" alt=\"\" src=\"/uploads/images/kundtraff2010/opalen13.JPG\"><img width=\"683\" height=\"512\" alt=\"\" src=\"/uploads/images/kundtraff2010/opalen14.JPG\"></p>','2014-08-18','0000-00-00'),
	(13,1,'Goda betyg i årets kundenkät','goda-betyg-i-arets-kundenkat','VBK har sedan början av 2000-talet ett kvalitets- och miljöledningssystem certifierat enligt ISO 9001 och ISO 14001.','<p>VBK har sedan början av 2000-talet ett kvalitets- och miljöledningssystem certifierat enligt ISO 9001 och ISO 14001.</p><p>Som ett led i vår strävan efter ständig förbättring genomför vi årligen, genom personliga intervjuer, en uppföljning av vissa genomförda uppdrag. Kunderna har att sätta betyg på hur vi klarat vårt uppdrag inom sju olika kategorier. Betygsskalan är 1-6, där 1 är mycket dåligt och 6 mycket bra. I år har vi blivit betygssatta av tjugo olika kunder i tjugo olika uppdrag.</p><p>Genomsnittsresultatet blev följande:</p><p>Kategori 1: Produktens innehåll:5,32</p><p>Kategori 2: Tider:5,25</p><p>Kategori 3: Prisvärdhet:4,68</p><p>Kategori 4: Kompetens:5,55</p><p>Kategori 5: Samarbete /relation:5,90</p><p>Kategori 6: Tillgänglighet / information:5,60</p><p>Kategori 7: Miljöarbete:4,67</p>','2014-08-18','0000-00-00'),
	(14,1,'God jul & gott nytt år','god-jul-gott-nytt-ar','VBK önskar alla en God Jul & ett Gott Nytt År!','<p>VBK önskar alla en God Jul &amp; ett Gott Nytt År!</p>','2014-08-18','0000-00-00'),
	(15,1,'TEMA-träff','tema-traff','Under våra interna TEMA-träffar informerar vi varandra om nyheter inom våra respektive specialområden.','<p>Under våra interna TEMA-träffar informerar vi varandra om nyheter inom våra respektive specialområden.</p><p>Nästa TEMA-träff den 22:e februari tar vi upp följande ämnen:</p><p>Brand/Akustik</p><p>Den första europeiska handboken om brandsäkerhet i träbyggnader.</p><p>Nyheter med hänsyn till Eurokod</p><p>Betong/Murverk</p><p> Vikten av värme vid härdning</p><p>Betong som tar upp kväveoxid</p><p>Stålprofiler som kombineras med betong</p><p>Stål/TräNyheter med hänsyn till Eurokod</p><p>Projektadministration</p><p>Allmänna bestämmelser, ABK 09, ABT 06, AB 04 &amp; ABS 09</p><p>Allmänt om Lagen om offentlig upphandling, LOU</p><p>Miljömanual</p><p>Genomgång av VBKs-miljömanual</p>','2014-08-18','0000-00-00'),
	(16,1,'TEMA-träff','tema-traff','Under våra interna TEMA-träffar informerar vi varandra om nyheter inom våra respektive specialområden.','<p>\n	Under våra interna TEMA-träffar informerar vi varandra om nyheter inom våra respektive specialområden.\n</p>\n<p>\n	Nästa TEMA-träff den 22:e februari tar vi upp följande ämnen:\n</p>\n<p>\n	<strong>Brand/Akustik</strong>\n</p>\n<p>\n	Den första europeiska handboken om brandsäkerhet i träbyggnader.\n</p>\n<p>\n	Nyheter med hänsyn till Eurokod\n</p>\n<p>\n	<strong>Betong/Murverk</strong>\n</p>\n<p>\n	 Vikten av värme vid härdning\n</p>\n<p>\n	Betong som tar upp kväveoxid\n</p>\n<p>\n	Stålprofiler som kombineras med betong\n</p>\n<p>\n	<strong>Stål/Trä</strong>\n</p>\n<p>\n	Nyheter med hänsyn till Eurokod\n</p>\n<p>\n	<strong>Projektadministration</strong>\n</p>\n<p>\n	Allmänna bestämmelser, ABK 09, ABT 06, AB 04 &amp; ABS 09\n</p>\n<p>\n	Allmänt om Lagen om offentlig upphandling, LOU\n</p>\n<p>\n	<strong>Miljömanual</strong>\n</p>\n<p>\n	Genomgång av VBKs-miljömanual\n</p>','2014-08-18','2015-01-09'),
	(18,1,'Hälsonyckeltest','halsonyckeltest','VBK utför just nu som en del av vårt arbete med hälsofrämjande åtgärder ett hälsonyckeltest för vår personal.','<p>VBK utför just nu som en del av vårt arbete med hälsofrämjande åtgärder ett hälsonyckeltest för vår personal. Testerna utförs i samarbete med Korpen och innefattar bl.a. ett standardiserat frågeformulär, ett konditionstest samt att man tar fram en individuell hälsoplan för varje deltagare.</p>','2014-08-18','0000-00-00'),
	(19,1,'Praktikant på VBK','praktikant-pa-vbk','VBK säger välkommen till Julia Folkesson som läser på Väg och Vatten och som kommer att praktisera här hos oss fram till den 8:e juli 2011.','<p>VBK säger välkommen till Julia Folkesson som läser på Väg och Vatten och som kommer att praktisera här hos oss fram till den 8:e juli 2011.</p>','2014-08-18','0000-00-00'),
	(20,1,'Nyanställd Marie Teike','nyanstalld-marie-teike','Vi hälsar Marie Teike välkommen till VBK som började hos oss den 1 juni.','<p>Vi hälsar Marie Teike välkommen till VBK som började hos oss den 1 juni. Hon kommer att arbeta inom projektadministration. Marie kommer närmast från Skanska där hon främst arbetat med projektledning inom eftermarknad. Hon är utbildad inom kulturvård och har Bas P och Bas U utbildning.</p>','2014-08-18','0000-00-00'),
	(21,1,'AMA-kurs','ama-kurs','Fredagen den 17 juni har vi gemensam AMA-kurs','<p>Fredagen den 17 juni har vi gemensam AMA-kurs. Olle Thåström från EGA-konsult AB presenterar introduktion till AMA Hus och beskrivningsarbete.</p>','2014-08-18','0000-00-00'),
	(23,1,'Semesterstängt','semesterstangt','Vi har semesterstängt 11 juli till 7 augusti.','<p>Vi har semesterstängt 11 juli till 7 augusti.</p><p>Både växel och reception har under denna tid stängt.</p><p>Vi önskar alla en riktigt skön sommar!</p>','2014-08-18','0000-00-00'),
	(24,1,'Fortbildning','fortbildning','För att stå väl rustade inför kommande uppdrag bedriver vi på VBK en kontinuerlig vidareutbildning av våra medarbetare.','<p>För att stå väl rustade inför kommande uppdrag bedriver vi på VBK en kontinuerlig vidareutbildning av våra medarbetare.</p><p>För närvarande utbildas fyra unga medarbetare i projekteringsverktyget Tekla Structures. Nio andra medarbetare deltar i STD-företagens Uppdragsledarutbildning. Uppdragsledarutbildningen ger en bra grund att stå på när det blir dags att ta sig an handläggarrollen.</p>','2014-08-18','0000-00-00'),
	(25,1,'Lediga tjänster','lediga-tjanster','Tack vare en ökad uppdragsvolym behöver vi nu förstärkning med en Erfaren Byggnadskonstruktör till vårt kontor i Skövde.','<p>2011-08-27</p><p>Tack vare en ökad uppdragsvolym behöver vi nu förstärkning med en Erfaren Byggnadskonstruktör till vårt kontor i Skövde.</p><p> För mer information klicka här.</p>','2014-08-18','0000-00-00'),
	(26,1,'Seminarium 2011','seminarium-2011','Den 8-10 september åker VBK på seminarium till Vallåsen, som är beläget på Hallandsåsen.','<p>Den 8-10 september åker VBK på seminarium till Vallåsen, som är beläget på Hallandsåsen.</p><p>På vägen dit stannar vi i Falkenberg för att besöka två av våra uppdrag, Falkhallen respektive NTC-huset. </p><p>Väl på plats i Vallåsen kommer vi att lyssna på två föreläsare: Therese Malm, från WSP som talar om \"Miljöcertifierad projektering och byggande\" samt Per-Ola Bergqvist, från Foyen Advokatfirma som talar om ABK09.</p><p>Vi passar också på att besöka tunnelbygget genom Hallandsåsen på hemvägen.</p><p>Vi tror och hoppas på några inspirerande dagar och att vi kommer hem med nya kunskaper.</p>','2014-08-18','0000-00-00'),
	(28,1,'Gammalt möter nytt','gammalt-moter-nytt','Onsdagen den 26 oktober samlades drygt 120 personer utanför Tyska Kyrkan för att deltaga i vårt kundevent ?Gammalt möter Nytt?.','<p>2011-10-27</p><p>Onsdagen den 26 oktober samlades drygt 120 personer utanför Tyska Kyrkan för att deltaga i vårt kundevent \"Gammalt möter Nytt\".</p><p>För att stilla den värsta hungern bjöds alla vid ankomsten på en röd pölse med bröd. Till denna dracks naturligtvis en Hof. När korven väl var inmundigad var det dags att tåga in i kyrkan.</p><p>Väl inne i kyrkan bjöds det, som sig bör när man befinner sig i just en kyrka, på andlig spis.Marie och Jens redovisade hur vi arbetar med gamla byggnader för att förlänga den tekniska livslängden med bevarat utseende. Två uppdrag där vi arbetat med detta, nämligen just Tyska kyrkan och brf Vasaplatsen 6, gicks igenom.</p><p>Därefter var det dags för BIM-gänget att ta över. Ola inledde med en allmän presentation av vad BIM är. Därefter tog i tur och ordning Jon, Caroline, Anna och Emanuel över och redogjorde för ett antal projekt där vi använt och haft nytta av BIM.</p><p>Även om GAMLA byggnader och NY teknik är spännande så var väl ändå kvällen höjdpunkt när Stefan Andersson med band klev upp på scenen. Stefan tog oss med på en musikalisk resa till det gamla Göteborg och vidare upp till Marstrand och Carlstens fästning. Efter en dryg timme på scenen avtackades bandet med blommor och kraftiga applåder.</p><p>Nu var det tid för församlingen att dra vidare. Färden gick mot Stadsmuseet (för övrigt också ett gammalt VBK-projekt) där kallskuret, soppa och vin serverades. Framåt 22-tiden gav sig ett antal mätta och av kunskap tyngda gäster hemåt i den lite kyliga Göteborgsnatten.<img width=\"683\" height=\"512\" alt=\"\" src=\"/uploads/images/gammalt-nytt-01.jpg\"><img width=\"683\" height=\"512\" alt=\"\" src=\"/uploads/images/gammalt-nytt-02.jpg\"><img width=\"512\" height=\"683\" alt=\"\" src=\"/uploads/images/gammalt-nytt-03.jpg\"><img width=\"683\" height=\"512\" alt=\"\" src=\"/uploads/images/gammalt-nytt-04.jpg\"><img width=\"683\" height=\"512\" alt=\"\" src=\"/uploads/images/gammalt-nytt-05.jpg\"><img width=\"683\" height=\"512\" alt=\"\" src=\"/uploads/images/gammalt-nytt-06.jpg\"><img width=\"683\" height=\"512\" alt=\"\" src=\"/uploads/images/gammalt-nytt-07.jpg\"><img width=\"683\" height=\"512\" alt=\"\" src=\"/uploads/images/gammalt-nytt-08.jpg\"><img width=\"683\" height=\"512\" alt=\"\" src=\"/uploads/images/gammalt-nytt-09.jpg\"><img width=\"683\" height=\"512\" alt=\"\" src=\"/uploads/images/gammalt-nytt-10.jpg\"><img width=\"683\" height=\"512\" alt=\"\" src=\"/uploads/images/gammalt-nytt-11.jpg\"></p>','2014-08-18','0000-00-00'),
	(29,1,'Stort grattis till Björn Johansson','stort-grattis-till-bjorn-johansson','Under VARM-dagen på Chalmers, den 8 november 2011, anordnade VBK en beräkningstävling.','<p>Under VARM-dagen på Chalmers, den 8 november 2011, anordnade VBK en beräkningstävling.</p><p>Frågan löd:</p><p>Om 0,5m3 äpple står längst ut på en konsol av den här profilen, hur lång kan konsolen vara innan den når brott?</p><p>Förutsättningar:</p><p>     1. Stålsort: S355J2H</p><p>     2. Beräkning enligt Eurocode</p><p>     3. Vikten av kärl är försummad</p><p>Profilen som nämns i frågan var en VKR 100x100x10 som fanns i VBKs monter under dagen. Svaret på frågan är 6.12m.</p><p>Den person som kom närmast var Björn Johansson, som i vinst kommer att få 5 stycken luncher i kårrestaurangen på Chalmers.</p>','2014-08-18','0000-00-00'),
	(30,1,'Lediga tjänster','lediga-tjanster','Tack vare en ökad uppdragsvolym behöver vi nu förstärkning med Erfarna Byggnadskonstruktörer till vårt kontor i Göteborg och Skövde.','<p>Tack vare en ökad uppdragsvolym behöver vi nu förstärkning med Erfarna Byggnadskonstruktörer till vårt kontor i Göteborg och Skövde.</p><p> För mer information klicka här.</p>','2014-08-18','0000-00-00'),
	(31,1,'Julstängt','julstangt','Vi tar ledigt för att fira jul och nyår.','<p>Vi tar ledigt för att fira jul och nyår.</p><p>Vår växel och reception stänger kl. 11.45 den 23/12 och öppnar som vanligt igen den 2/1.</p><p>Vi önskar er alla GOD JUL och GOTT NYTT ÅR</p>','2014-08-18','0000-00-00'),
	(32,1,'Bostadspriset 2011','bostadspriset-2011','VBK har tillsammans med bland annat White Arkitekter projekterat Äppelträdgårdens atriumradhus i Göteborg som vunnit Bostadspriset 2011.','<p>VBK har tillsammans med bland annat White Arkitekter projekterat Äppelträdgårdens atriumradhus i Göteborg som vunnit Bostadspriset 2011.</p><p>Jurins motivering:</p><p>\"För ett modernt och nutida projekt som är ett inspirerande exempel på komplettering av ett miljonprogramsområde. Äppelträdgården är ett positivt tillägg som ger ett lyft till kringmiljön och har ett varierat utbud av upplåtelseformer. Med sin tvåvåningsskala och egensinniga identitet kontrasterar området mot, och berikar, stadsdelens i övrigt högresta bebyggelse. Tillskottet av dessa marklägenheter möjliggör en boendekarriär inom närområdet för dem som önskar. Atriumhusen har ett starkt nutida uttryck med internationella influenser. Bostäderna är gestaltade med en hög inlevelseförmåga med fin detaljering och utsuddade gränser mellan ute och inne.</p><p>Kv. Skolmössan, eller Äppelträdgården, ligger alldeles intill Frölunda torg i västra Göteborg. Atriumhusen, ca fyrtio stycken när området blir fullt utbyggt, utgörs av äganderätter. Ett lika stort antal bostäder uppförs som radhus med hyresrätt. Alla byggnader är placerade inom en gemensam äppelträdgård, en lika poetisk som vacker inramning.</p><p>Trädgårdar och hus blandas i en tät struktur som ger goda kvaliteter för bostäder, privata uteplatser och gemensamma rum. Atriumhusen är finurligt vällösta, med vackra rum och god hantering av dagsljus och utblickar. Den fria fönstersättningen utmanar till nytänkande. Det centrala atriet, som med angränsande carport har drag av gamla tiders vagnslider, erbjuder tillsammans med den övre terrassen uteplatser av varierande karaktär och för olika väder.\"</p><p>Bilder ser ni nedan, fotograf Kalle Sanner.</p><p>För mer information klicka här.</p><p><img alt=\"\" width=\"687\" height=\"554\" src=\"/uploads/images/appletradgarden_1.jpg\"><img alt=\"\" width=\"687\" height=\"691\" src=\"/uploads/images/appletradgarden_2.jpg\"><img alt=\"\" width=\"687\" height=\"1047\" src=\"/uploads/images/appletradgarden_3.jpg\"></p>','2014-08-18','0000-00-00'),
	(33,1,'Nyanställda','nyanstallda','Vi hälsar Katarina Olsson och Niklas Marberg välkomna till VBK!','<p>Vi hälsar Katarina Olsson och Niklas Marberg välkomna till VBK!</p><p>Katarina är högskoleingenjör med tio års erfarenhet och kommer närmast från Tellstedt. Niklas är nyutexaminerad civilingenjör från Lunds Tekniska Högskola.<img alt=\"\" width=\"683\" height=\"512\" src=\"/uploads/images/nyanstallda_201201.jpg\"></p>','2014-08-18','0000-00-00'),
	(34,1,'Nyanställd','nyanstalld','Vi hälsar Anders Nordstedt Boix välkomnen till VBK!','<p>Vi hälsar Anders Nordstedt Boix välkomnen till VBK!<img alt=\"\" width=\"687\" height=\"350\" src=\"/uploads/images/Nyanstallda_201301.jpg\"></p>','2014-08-18','0000-00-00'),
	(35,1,'VBK konstruerar Göteborgs första skyskrapa','vbk-konstruerar-goteborgs-forsta-skyskrapa','I söndagens utgåva av GP var det med ett reportage om ny Gothia Triple Towers som VBK är med och projekterar.','<p>I söndagens utgåva av GP var det med ett reportage om ny Gothia Triple Towers som VBK är med och projekterar.</p><p>Nedan ser ni denna artikel, klicka på bilderna för att se dem i större format.<img alt=\"GP Gothia\" width=\"691\" height=\"485\" src=\"/uploads/images/Gothia_Sida_1_mindre.png\"><img alt=\"GP Gothia\" width=\"691\" height=\"485\" src=\"/uploads/images/Gothia_Sida_2_mindre.png\"></p>','2014-08-18','0000-00-00'),
	(36,1,'Kundenkät 2012','kundenkat-2012','Även i år goda betyg i kundenkäten','<p>Även i år goda betyg i kundenkäten</p><p>VBK har sedan början av 2000-talet ett kvalitets- och miljöledningssystem certifierat enligt ISO 9001 och ISO 14001. Som ett led i vår strävan efter ständig förbättring genomför vi årligen, genom personliga intervjuer, en uppföljning av vissa genomförda slumpvis utvalda uppdrag. Kunderna har att sätta betyg på hur vi klarat vårt uppdrag inom sju olika kategorier. Betygsskalan är 1-6, där 1 är mycket dåligt och 6 mycket bra. I år har vi blivit betygssatta av sjutton olika kunder i sjutton olika uppdrag.</p><p>Genomsnittsresultatet blev följande:</p><p>Kategori 1 - Produktens innehåll:5,44</p><p>Kategori 2 - Tider:4,88</p><p>Kategori 3 - Prisvärdhet:5,35</p><p>Kategori 4 - Kompetens:5,71</p><p>Kategori 5 - Samarbete / relation:5,76</p><p>Kategori 6 - Tillgänglighet / information:5,56</p><p>Kategori 7 - Miljöarbete:4,14</p>','2014-08-18','0000-00-00'),
	(37,1,'Nyanställda','nyanstallda','Vi hälsar Jacob Lilljegren, Fredrik Gelander, Sinisa Buha, Katarina Apleryd och Jonatan Bergman välkomna till VBK!','<p>Vi hälsar Jacob Lilljegren, Fredrik Gelander, Sinisa Buha, Katarina Apleryd och Jonatan Bergman välkomna till VBK!<img width=\"687\" height=\"515\" alt=\"\" src=\"/uploads/images/nyanstallda_201204.JPG\"></p>','2014-08-18','0000-00-00'),
	(38,1,'BIM på bryggeri','bim-pa-bryggeri','Vid femtiden onsdagen den 9 maj anlände närmare 40 personer med buss från Skövde till Qvänum Mat & Malt för att deltaga i vårt kundevent ?BIM på Bryggeri?','<p>Vid femtiden onsdagen den 9 maj anlände närmare 40 personer med buss från Skövde till Qvänum Mat &amp; Malt för att deltaga i vårt kundevent \"BIM på Bryggeri\".</p><p>Väl framme tog alla deltagare snabbt plats i föreläsningssalen på andra våningen i den ombyggda ladugården.</p><p>Efter en inledning av Ulf innehållande lite allmän VBK-information var det så dags för BIM-gänget att ta över. Ola inledde med en allmän presentation av vad BIM är. Därefter tog i tur och ordning Caroline, Kristian och Emanuel över och redogjorde för ett antal projekt där vi använt och haft nytta av BIM. Ola återkom sedan på slutet och knöt ihop BIM-säcken.</p><p>Att BIM är ett ämne som engagerar branschen visade om inte annat de många frågorna och synpunkterna under den efterföljande frågestunden. Efter denna, som vi hoppas, matnyttiga föreläsning var det så dags att intaga lite nyttig(?) mat en våning ner.Vi bjöds på sparrissoppa och kalventrecote, fantastiskt gott! Till detta fick vi prova fyra sorter av husets egenbryggda öl. En förevisning av det lilla bryggeriet ingick också som en av kvällens aktiviteter.</p><p>Vid halv tio-tiden lämnade en buss med ett antal mätta och av BIM-kunskap fyllda gäster Qvänum för återfärden mot&nbsp;Skövde.</p><p><img width=\"687\" height=\"515\" alt=\"\" src=\"/uploads/images/Hemsida1.jpg\"><img width=\"687\" height=\"515\" alt=\"\" src=\"/uploads/images/Hemsida2.jpg\"><img width=\"687\" height=\"515\" alt=\"\" src=\"/uploads/images/Hemsida3.jpg\"><img width=\"687\" height=\"515\" alt=\"\" src=\"/uploads/images/Hemsida4.jpg\"></p>','2014-08-18','0000-00-00'),
	(39,1,'LECA-seminarium 2012','leca-seminarium-2012','På vårt årliga seminarium har Vi besökt Weber Saint-Gobain i Linköping, där vi fick se tillverkningsprocessen av LECA-kulor och LECA-block.','<p>På vårt årliga seminarium har Vi besökt Weber Saint-Gobain i Linköping, där vi fick se tillverkningsprocessen av LECA-kulor och LECA-block.</p><p>Vi fick också en intressant förläsning om murning och putsning och fick själva möjlighet att prova på att både mura och putsa. En mycket trevlig och lärorik dag, som vi tackar Weber Saint-Gobain för, följdes av en lika trevlig kväll på Vadstena Klosterhotell.</p><p><img width=\"687\" height=\"515\" alt=\"\" src=\"/uploads/images/bild1.jpg\"><img width=\"687\" height=\"515\" alt=\"\" src=\"/uploads/images/bild2.jpg\"></p>','2014-08-18','0000-00-00'),
	(41,1,'Nya uppdrag','nya-uppdrag','Sandberedning Volvo Powertrain och Ny karossfabrik TA3, Volvo PV','<p>Sandberedning Volvo Powertrain och Ny karossfabrik TA3, Volvo PV</p><ul><li>Sandberedning Volvo Powertrain, Skövde</li></ul><p>Med Volvo Powertrain som beställare skall VBK upprätta handlingar för en ny anläggning för sandberedning vid Volvos fabrik i Skövde. Sanden skall användas vid tillverkningen av motorfabrikens nya gjutformar. Omfattningen för VBK är 6 - 7 manmånader.</p><ul><li>Ny karossfabrik TA3, Volvo PV Torslanda</li></ul><p>Volvo Personvagnar AB bygger en ny karossfabrik, TA3, på totalt 24.000 kvm i Torslanda. I fabriken skall framtidens Volvobilar tillverkas. Den totala investeringskostnaden ligger på 1,6 miljarder kronor. VBK är byggnadskonstruktör och upprättar bygghandlingar för totalentreprenören Tuve Bygg och tillverkningshandlingar för stomleverantören Strängbetong. Total omfattning för VBK är 20 - 22 manmånader.</p>','2014-08-18','0000-00-00'),
	(42,1,'Nyanställda','nyanstallda','Vi hälsar Niklas Alsterhem, Jonas Eriksson, Ralph Frykman, Johanna Törnberg, Isabella Berner och Julia Folkesson välkomna till VBK!','<p>Vi hälsar Niklas Alsterhem, Jonas Eriksson, Ralph Frykman, Johanna Törnberg, Isabella Berner och Julia Folkesson välkomna till VBK!</p><p><img alt=\"\" width=\"687\" height=\"515\" src=\"/uploads/images/Nyanstallda_201208.jpg\"></p>','2014-08-18','0000-00-00'),
	(43,1,'Nya uppdrag','nya-uppdrag','Kraftvärmeverk Marjarp II, Falköping','<p>Kraftvärmeverk Marjarp II, Falköping</p><p>Med Göteborg Energi som beställare och FEAB (Falbygdens energi) som byggherre har VBK fått uppdraget att agera delprojektledare, byggledare och Bas P när ett nytt kraftvärmeverk byggs i Falköping. Verket skall producera både värme- och elenergi. Bränslet är skogsflis, vilket är nytt för denna typ av anläggning. Kraftvärmeverket har en yta på 1.200 kvm och totalkostnaden är ca 120 milj kr. Det beräknas stå klart i början av 2013. Omfattningen för VBK är ca 4 manmånader.</p>','2014-08-18','0000-00-00'),
	(44,1,'Studieresor','studieresor','Under hösten åker vi på studieresor för att se intressanta byggnader, studera tekniska bygglösningar och få möjlighet att lyssna på spännande föreläsningar.','<p>Under hösten åker vi på studieresor för att se intressanta byggnader, studera tekniska bygglösningar och få möjlighet att lyssna på spännande föreläsningar.</p><p>Vi kommer också att umgås och ha trevligt tillsammans och få möjlighet att bekanta oss med en ny stad. Våra resor i år går till Dubai resp. St. Petersburg.</p>','2014-08-18','0000-00-00'),
	(45,1,'En kväll enligt Eurocode','en-kvall-enligt-eurocode','Tänk att Eurocode kan vara så intressant!','<p>Tänk att Eurocode kan vara så intressant!</p><p>210 personer samlades igår, tisdagen den 30 okt, på Clarion Hotel Post för att lära sig lite mer om hur den gällande normen för bärande konstruktioner, Eurocode, påverkar byggbranschens olika aktörer. Vi fick följa med på en normresa från BABS 46 via SBN och BKR till EKS8 som trädde i kraft den 2 maj 2011. Säkerhet lades i begynnelsen helt på materialet medan den nu, i princip, läggs helt på lasten. Vissa avslöjande vid föredragningen förvånade också en del. Som t ex att betong blivit 4 % tyngre och att trä är en tredjedel sämre i Sverige än i Danmark och Finland. Man kan i alla fall glädjas åt att ståldimensionerna blir de samma nu som före införandet av Eurocode.</p><p>Efter Eurocode-showen och ett glas bubbel var det så dags för den verkliga showen när imitatören och sångerskan Maria Möller tog över scenen.  Vi bjöds på sång och snack i en härlig blandning där kvällens höjdpunkt utan tvekan var Marias Carola-imitation.</p><p>Framåt halv nio var det tid att ta plats i restaurangen för intagandet av kvällen måltid. Lax och oxfilé med därtill hörande både vitt och rött serverades samt självklart också ett alkoholfritt alternativ för den som så önskade. Ännu en lyckad kundkväll avslutades för de flesta vid 23-tiden, men några blev nog hängande i baren på Hotel Post ytterligare ett tag.</p><p><img width=\"687\" height=\"515\" alt=\"\" src=\"/uploads/images/bild01.JPG\"><img width=\"687\" height=\"515\" alt=\"\" src=\"/uploads/images/bild02.JPG\"><img width=\"687\" height=\"515\" alt=\"\" src=\"/uploads/images/bild03.JPG\"><img width=\"687\" height=\"515\" alt=\"\" src=\"/uploads/images/bild04.JPG\"><img width=\"687\" height=\"515\" alt=\"\" src=\"/uploads/images/bild05.JPG\"><img width=\"687\" height=\"515\" alt=\"\" src=\"/uploads/images/bild06.JPG\"><img width=\"687\" height=\"515\" alt=\"\" src=\"/uploads/images/bild07.JPG\"><img width=\"687\" height=\"515\" alt=\"\" src=\"/uploads/images/bild08.JPG\"><img width=\"687\" height=\"515\" alt=\"\" src=\"/uploads/images/bild09.JPG\"><img width=\"687\" height=\"515\" alt=\"\" src=\"/uploads/images/bild10.JPG\"><img width=\"687\" height=\"515\" alt=\"\" src=\"/uploads/images/bild11.JPG\"><img width=\"687\" height=\"515\" alt=\"\" src=\"/uploads/images/bild12.JPG\"><img width=\"687\" height=\"515\" alt=\"\" src=\"/uploads/images/bild13.JPG\"><img width=\"687\" height=\"515\" alt=\"\" src=\"/uploads/images/bild14.JPG\"><img width=\"687\" height=\"515\" alt=\"\" src=\"/uploads/images/bild15.JPG\"><img width=\"687\" height=\"515\" alt=\"\" src=\"/uploads/images/bild16.JPG\"><img width=\"687\" height=\"515\" alt=\"\" src=\"/uploads/images/bild17.JPG\"><img width=\"687\" height=\"515\" alt=\"\" src=\"/uploads/images/bild18.JPG\"></p>','2014-08-18','0000-00-00'),
	(46,1,'Vinnare VBK-tävling, VARM-dagen','vinnare-vbk-tavling-varm-dagen','Stort grattis till Ashna Zangana!','<p>Stort grattis till Ashna Zangana!</p><p>Under VARM-dagen på Chalmers, den 13 november 2012, anordnade VBK en beräkningstävling.</p><p>Frågan löd:</p><p>En punktlast på 1 ton ska placeras på en 12,5 m lång fritt upplagd balk av den här profilen. Var på balken kan lasten placeras utan att den går till brott?</p><p>Förutsättningar:</p><p>1. Stålsort: S355J2H</p><p>2. Lasten kan antas vara egentyngd</p><p>Profilen som nämns i frågan var en VKR 100x100x10 som fanns i VBKs monter under dagen. Svaret på frågan är 4232 mm från stöd.</p><p>Den person som kom närmast var Ashna Zangana, som i vinst kommer att få 5 stycken luncher i kårrestaurangen på Chalmers.</p>','2014-08-18','0000-00-00'),
	(47,1,'Nyanställda','nyanstallda','Vi hälsar Åsa Boberg, Per Stagler, Frida Berntsson och Sofia Hakner välkomna till VBK!','<p>Vi hälsar Åsa Boberg, Per Stagler, Frida Berntsson och Sofia Hakner välkomna till VBK!</p><p><img width=\"687\" height=\"515\" alt=\"\" src=\"/uploads/images/Nyanstallda_201210.jpg\"></p>','2014-08-18','0000-00-00'),
	(48,1,'Julstängt','julstangt','Vi önskar er alla GOD JUL och GOTT NYTT ÅR','<p>Vi tar ledigt för att fira jul och nyår. Vår växel och reception stänger kl. 11.45 den 21/12 och öppnar som vanligt igen den 7/1.</p><p>Vi önskar er alla GOD JUL och GOTT NYTT ÅR</p>','2014-08-18','0000-00-00'),
	(50,1,'Nya uppdrag','nya-uppdrag','VBK tar sig ann nya uppdrag','<p>GO+, tillbyggnad av GöteborgsOperan och Kvillebäcken Lott O2</p><p>GO+, tillbyggnad av GöteborgsOperan</p><p>GöteborgsOperan har för avsikt att väster om befintligt operahus uppföra en ny byggnad innehållande ny scen och salong. Ytan är ca 4.000 kvm och den totala kostnaden är beräknad till ungefär 150 miljoner kronor. Tengbom har utsetts till arkitekt för tillbyggnaden och VBK har så här långt, av GöteborgsOperan, fått uppdraget att upprätta en enkel systemhandling som skall ligga till grund för den kalkyl som skall upprättas. Omfattningen på vårt arbete är ca 2 manmånader. Projektet skall drivas som ett partneringprojekt och entreprenadformen är totalentreprenad. NCC har fått tilldelningsbeslut och skall i detta skede ta fram en kalkyl som skall ligga till grund för projektets riktpris. Projektledare är Higab. </p><p>Kvillebäcken Lott O2</p><p>Veidekke drar nu igång sitt andra bostadsprojekt i den nya stadsdelen Kvillebäcken. Byggstart av de 81 lägenheterna är planerad till januari 2014. VBK har fått uppdraget att som byggnadskonstruktör upprätta system- och bygghandlingar för projektet. Projekteringen startar under mars månad och pågår till oktober. Omfattningen för vår del är 6-7 manmånader. Totalytan är 7.600 kvm BTA och arkitekt är, precis som för Veidekkes första hus, Semrén &amp; Månsson.</p>','2014-08-18','0000-00-00'),
	(52,1,'En morgon enligt Eurocode','en-morgon-enligt-eurocode','Onsdag morgon den 10 april vid halv åtta samlades drygt femtio personer på hotell Scandic Billingen för att intaga EuroCode till frukost','<p>Onsdag morgon den 10 april vid halv åtta samlades drygt femtio personer på hotell Scandic Billingen för att intaga EuroCode till frukost. Innan den nu gällande normen för bärande konstruktioner, EuroCode, serverades, intogs dock en rejäl hotellfrukost. Frukosten innehöll allt man kan önska d v s såväl bacon och äggröra som färskt bröd, mängder av pålägg och fräsch frukt.</p><p>När frukosten var avslutad fick vi följa med på en normresa från BABS 46 via SBN och BKR till EKS8 som trädde i kraft den 2 maj 2011. Säkerhet lades i begynnelsen helt på materialet medan den nu, i princip, läggs helt på lasten. Vissa avslöjande vid föredragningen förvånade också en del. Som t ex att betong blivit 4% tyngre och att trä är en tredjedel sämre i Sverige än i Danmark och Finland. Man kan i alla fall glädjas åt att ståldimensionerna blir de samma nu som före införandet av Eurocode. I betongkonstruktioner får vi dock räkna med att det ofta kommer att behövas lite mer armering än tidigare.</p><p>Strax efter klockan nio på förmiddagen avslutades en lyckad kundträff och de flesta gav sig nöjda av för att ta sig an dagens utmaningar. Några stannade kvar för att skaffa sig ytterligare lite kunskap om den nya normen genom att ställa frågor till dagens föredragshållare.</p><p><img alt=\"\" width=\"687\" height=\"458\" src=\"/uploads/images/bild01.JPG\"><img alt=\"\" width=\"687\" height=\"458\" src=\"/uploads/images/bild02b.JPG\"><img alt=\"\" width=\"687\" height=\"458\" src=\"/uploads/images/bild03.JPG\"><img width=\"687\" height=\"458\" alt=\"\" src=\"/uploads/images/bild04.JPG\"><img width=\"687\" height=\"458\" alt=\"\" src=\"/uploads/images/bild06.JPG\"><img width=\"687\" height=\"458\" alt=\"\" src=\"/uploads/images/bild07.JPG\"><img width=\"687\" height=\"458\" alt=\"\" src=\"/uploads/images/bild08.JPG\"><img width=\"687\" height=\"458\" alt=\"\" src=\"/uploads/images/bild09.JPG\"><img width=\"687\" height=\"458\" alt=\"\" src=\"/uploads/images/bild10.JPG\"></p>','2014-08-18','0000-00-00'),
	(53,1,'VBK omorganiserar','vbk-omorganiserar','I nästan trettio år har VBK haft en platt organisation helt utan avdelningar och grupper. Vi har arbetat helt projektanpassat.','<p>I nästan trettio år har VBK haft en platt organisation helt utan avdelningar och grupper. Vi har arbetat helt projektanpassat.</p><p>När vi nu börjar närma oss nittio anställda i företaget har vi dock känt att det är dags för en mer uppdelad organisation.</p><p>De främsta skälen för detta har varit svårigheten att arbetsplanera för en så stor grupp och avsaknaden av en närmaste chef.</p><p>Så fr o m den 3 juni består VBK av fem avdelningar.</p><p>På vårt Göteborgskontor har vi två projekteringsavdelningar (BP1 och BP2) och en projektadministrativ avdelning (PA). På Skövdekontoret har vi en projekteringsavdelning (BP3). Den femte avdelningen är vår internadministration (IA) och den är placerad i Göteborg.</p><p>Med nya avdelningar följer också nya chefer.</p><p>Ola Kjellman basar för BP1 och chef för BP2 är Jan Bergstrand. Björn Brunander tar hand om den projektadministrativa avdelningen och i Skövde är det Jan-Eje Andersson som är avdelningschef. Anette Vall fortsätter som chef för intern-administrationen, men nu med tillägget att hon även är HR-chef.</p>','2014-08-18','0000-00-00'),
	(54,1,'Nya uppdrag','nya-uppdrag','Balkonginventering, Stombesiktningar och Nybyggnad av kontorshus','<p>Balkonginventering, Stombesiktningar och Nybyggnad av kontorshus</p><p><strong>Balkonginventering, AB Eidar Trollhättans bostadsbolag</strong></p><p>Under hösten 2013 genomför vi underhållsinventering av ca 2 000 stycken balkonger för Trollhättans kommunala bostadsföretag, AB Eidar Trollhättans bostadsbolag.</p><p><strong>Stombesiktningar, Volvo Powertrain AB</strong></p><p>Vi kommer att genomföra konditionsbesiktning av 8 stycken av Volvo Powertrain´s fabriker i Skövde, bl.a. A- och F-fabrikerna. Arbetet pågår under hösten 2013.</p><p><strong>Nybyggnad av kontorshus, House of Ports</strong></p><p>För Ports Real Estate AB upprättar vi program- system och bygghandling för nybyggnad av kontorshus på 1 200 kvm i två plan med möjlighet till framtida påbyggnad med indraget plan. Byggnaden är cirkulär med vertikal stomme av stålpelare och betongväggar. Bjälklaget utförs i betong och ytterväggar i prefab sandwich i nedre plan och av glas i övre plan. Arkitekt är Nadén Arkitekter.&nbsp;</p>','2014-08-18','0000-00-00'),
	(55,1,'Nya uppdrag','nya-uppdrag','Påbyggnad av Viktoriagatan 13, Öckerö skatepark','<p>Påbyggnad av Viktoriagatan 13, Öckerö skatepark</p><p><strong>Påbyggnad av Viktoriagatan 13</strong></p><p>För Akademiska Hus Väst AB upprättar VBK systemhandling för en påbyggnad av ett våningsplan på fastigheten Viktoriagatan 13. Lokalerna skall användas av Universitet för administrativa ändamål. I vårt uppdrag ingår projektering av befintlig stomme och eventuella förstärkningsåtgärder samt nytt takbjälklag. Arkitekt är Rådmark Arkitekter</p><p><strong>Öckerö skatepark</strong></p><p>Öckerökommun skall anlägga en skatepark vid Prästängen idrottsplats. På uppdrag av Adwice AB skall VBK upprätta förfrågningsunderlag för betongkonstruktionen till anläggningen.</p>','2014-08-18','0000-00-00'),
	(56,1,'Nytt uppdrag','nytt-uppdrag','Wilbur building project - Mölnlycke Health Care','<p>Wilbur building project - Mölnlycke Health Care</p><p>Mölnlycke Health Care, planerar utbyggnad av befintligt produktionsanläggning alternativt byggnation av en ny separat anläggning strax norr om Boston i Maine, USA. Vi har fått i uppdrag att vara byggrådgivare och ansvarar för att granska ritningar etc. så att de uppfyller Mölnlycke Health Care´s krav på produktionsanläggning. Arkitekt är Harriman.</p><p><img width=\"687\" height=\"350\" alt=\"\" src=\"/uploads/images/Wilbur.jpg\"></p>','2014-08-18','0000-00-00'),
	(57,1,'Nya uppdrag','nya-uppdrag','Svenska Mässan, Kvarteret Orren, Förskolor i Mölndal och Bilia däckhotell','<p>Svenska Mässan, Kvarteret Orren, Förskolor i Mölndal och Bilia däckhotell</p><p><strong>Underhållsplan Svenska Mässan, Höghuset och Kongressen</strong></p><p>På uppdrag av Bengt Dahlgren utför vi besiktning av invändiga ytor, fasader och yttertak för att fastställa befintlig status. Utifrån vår besiktning föreslår vi underhållsintervaller för byggnadsdelarna och underhållskostnader så att underhållsplanen kan uppdateras.</p><p><strong>Kvarteret Orren, bostadshus</strong></p><p>Skanska uppför åt Kungälvsbostäder ett bostadshus för trygghetsboende samt boende med särskilt stöd (BmSS) i Komarken, Kungälv.  Byggnaden består av ett halvt suterrängplan och fyra våningar ovan mark varav den översta våningen är indragen. VBK ansvarar för grundläggning och utvändiga stomkompletteringar (tak, tätskikt, isolering) gentemot Skanska och för prefab betongstomme mot Finja. Projektet är ett Partnering mellan Skanska och Kungälvsbostäder. Arkitekt är Liljewall Arkitekter AB.</p><p><strong>Förskolor i Mölndal, om- och tillbyggnad</strong></p><p>VBK har fått i uppdrag av Mölndals Stad att upprätta konstruktionsritningar för ombyggnad av befintlig ventilationsanläggning på ett antal förskolor i Mölndal. Arkitekt är D´office.</p><p><strong>Bilia däckhotell - ombyggnad</strong></p><p>Bilia däckhotell bygger om sitt däcklager genom en förhöjning med 2 m. Arkitekt är Westelius arkitekter och på deras uppdrag upprättar VBK konstruktionshandlingar för systemhandlingar.</p>','2014-08-18','0000-00-00'),
	(58,1,'Nyanställda','nyanstallda','Vi hälsar Emma , Elina, Osborn, Emmelie, Olivia, Frida, Per och Alexander välkomna till VBK!','<p>Vi hälsar Emma , Elina, Osborn, Emmelie, Olivia, Frida, Per och Alexander välkomna till VBK!</p><p><img alt=\"\" width=\"687\" height=\"570\" src=\"/uploads/images/Nyanstallda_201308.jpg\"></p><p>Vi hälsar Emma Thorén, Elina Björk, Osborn Andersson, Emmelie Eneland, Olivia Szmidt, Frida Sewén, Per Eriksson och Alexander Strand välkomna till VBK!</p><p><img src=\"https://www.uc.se/ucsigill2/sigill?org=5566086103&amp;language=swe&amp;product=lsa&amp;fontcolor=b\"></p>','2014-08-18','0000-00-00'),
	(60,1,'Nya uppdrag','nya-uppdrag','Förskola i Henån, Bagarevägen och Volvo Powertrain, renovering av kupolugnsstativ','<p>Förskola i Henån, Bagarevägen och Volvo Powertrain, renovering av kupolugnsstativ</p><p><strong>Förskola i Henån, Bagarevägen</strong></p><p>Orust kommun bygger ny förskola för 4-5 avdelningar på ca. 1200 kvm i Henån. VBK har fått i uppdrag att upprätta en ramhandling för upphandling av totalentreprenör. VBK är generalkonsult och har underkonsulter på A, V och E. Arkitekt är D´Office.</p><p>Vår projektadministrativa avdelning har fått i uppdrag att ansvara för projekt-, projekterings- och byggledning för projektet.</p><p><strong>Volvo Powertrain, renovering av kupolugnsstativ</strong></p><p>På uppdrag av Volvo Powertrain upprättar VBK bygghandling för utbyte av stålkonstruktion i kupolugnsstativ G1, samt ansvarar för uppföljning under byggtiden.</p>','2014-08-18','0000-00-00'),
	(61,1,'Frukost på hotell om hotell','frukost-pa-hotell-om-hotell','Tidigt på fredag morgon den 25 oktober samlades 140 personer i Svenska Mässans Expohall för att intaga en härlig hotellfrukost. Frukosten innehöll allt man kan önska sig d v s såväl bacon och prinskorv som äggröra, färskt bröd, mängder av pålägg och naturligtvis kaffe, the och juice.','<p>Tidigt på fredag morgon den 25 oktober samlades 140 personer i Svenska Mässans Expohall för att intaga en härlig hotellfrukost. Frukosten innehöll allt man kan önska sig d v s såväl bacon och prinskorv som äggröra, färskt bröd, mängder av pålägg och naturligtvis kaffe, the och juice.</p><p>När frukosten var avslutad var det dags för dagens huvudattraktion, ett föredrag i tre akter om miljardprojektet Gothia Triple Towers.</p><p>Först ut var beställarens representant, projektledaren Patrik Albertsson från Svenska Mässan.</p><p>När kraven för det nya hotellet, som totalt har 1200 rum, skulle formas var det \"Gästen i centrum\" som gällde. Alla krav formades runt gästen, affärsgästen, konferensgästen och den gästande familjen. </p><p>Det är viktigt för ett hotell att ha något som sticker ut, något som är unikt, en så kallad USP (Unique Selling Point). Broarna mellan tornen, drygt femtio meter över marken och med glasgolv, är Triple Towers egen USP.</p><p>Restaurangen är av absolut toppklass och har redan fått höga betyg i Dagens Industri och GP. </p><p>Hotellet kommer att miljöcertifieras enligt det engelska systemet BREEAM.</p><p>Efter Patrik A var det så dags för entreprenören Peab att genom projektchef Patrik Svens ge sin syn på projektet. Att bygga på ett hotell som samtidigt skall vara i full drift ställer stora krav på byggproduktionen. Gästerna vill inte vakna av ett borrljud som fortplantar sig i stommen. </p><p>En annan stor utmaning när man bygger höga hus är självklart de vertikala transporterna. För påbyggnaden fick det bli en fristående kran eftersom befintligt hus inte klarade kranlasterna. För det nybyggda East Tower ställdes en kran på toppen av den glidformsgjutna hisskärnan.</p><p>Så var det då dags för sista akten i denna presentation av Gothia Triple Towers. Konstruktör Erik Samuelsson från VBK hade en mycket pedagogisk genomgång om hur man stabiliserar höga hus av denna typ. Vi fick också en förklaring till hur det kan vara möjligt att bygga på ett befintligt hus med sex våningar. Välj ett hus som är byggt i platsgjuten betong och som är byggt rakt!</p><p>Strax efter halv tio på förmiddagen avslutades en lyckad kundträff och alla gav sig nöjda iväg för att ta sig an dagens utmaningar.</p><p><img alt=\"\" width=\"687\" height=\"515\" src=\"/uploads/images/kundtraff2013/bild_01.jpg\"><img alt=\"\" width=\"687\" height=\"515\" src=\"/uploads/images/kundtraff2013/bild_02.jpg\"><img alt=\"\" width=\"515\" height=\"687\" src=\"/uploads/images/kundtraff2013/bild_03.jpg\"><img alt=\"\" width=\"687\" height=\"515\" src=\"/uploads/images/kundtraff2013/bild_04.jpg\"><img alt=\"\" width=\"687\" height=\"515\" src=\"/uploads/images/kundtraff2013/bild_05.jpg\"><img alt=\"\" width=\"515\" height=\"687\" src=\"/uploads/images/kundtraff2013/bild_06.jpg\"><img alt=\"\" width=\"687\" height=\"515\" src=\"/uploads/images/kundtraff2013/bild_07.jpg\"><img alt=\"\" width=\"687\" height=\"515\" src=\"/uploads/images/kundtraff2013/bild_08.jpg\"><img alt=\"\" width=\"687\" height=\"515\" src=\"/uploads/images/kundtraff2013/bild_09.jpg\"><img alt=\"\" width=\"687\" height=\"515\" src=\"/uploads/images/kundtraff2013/bild_10.jpg\"><img alt=\"\" width=\"687\" height=\"515\" src=\"/uploads/images/kundtraff2013/bild_11.jpg\"><img alt=\"\" width=\"687\" height=\"515\" src=\"/uploads/images/kundtraff2013/bild_12.jpg\"></p>','2014-08-18','0000-00-00'),
	(62,1,'Nya uppdrag','nya-uppdrag','Hamngatan LSS-boende, om- och tillbyggnad, Göteborgs Auktionsverk, ombyggnad och Boulogner 2014','<p>Hamngatan LSS-boende, om- och tillbyggnad, Göteborgs Auktionsverk, ombyggnad och Boulogner 2014</p><p><strong>Hamngatan LSS-boende, om- och tillbyggnad</strong></p><p>Leifab gör en om- och tillbyggnad av en tidigare förskola till boende med särskild service i Lilla Edet. LSS-boendet kommer att uppgå till 525 kvm varav 150 kvm avser nybyggnadsdelen. VBKs uppdrag omfattar projektledning, projekteringsledning, upprätta A-handlingar till förfrågningsunderlag, byggledning, kontrollansvarig enligt PBL, BAS P samt entreprenadbesiktning. </p><p>Arkitekt är VBK</p><p><strong>Göteborgs Auktionsverk, ombyggnad</strong></p><p>För Sjövalla Byggservice AB upprättar vi bygghandlingar för ombyggnad av befintliga lokaler för Göteborgs Auktionsverk. Ombyggnad sker med ny godsmottagning såsom lastkajer, skärmtak, lyftbord, större fasadöppningar mm. samt nya öppningar med vindfång och skärmtak för utlastning.</p><p>Arkitekt är Liljewall Arkitekter</p><p><strong>Boulogner 2014</strong></p><p>Skövde kommun skapar badsjö mitt i stan när Boulognersjön i Skövde rustas upp. VBK har fått i uppdrag att upprätta bygghandlingar för bryggor mm. för badplats. Förhoppningsvis finns möjlighet att bada här redan till sommaren 2014.</p><p>Arkitekt är Sweco.</p><p><img width=\"480\" height=\"240\" alt=\"\" src=\"/uploads/images/Boulogner_2014.jpg\"></p>','2014-08-18','0000-00-00'),
	(63,1,'Nya uppdrag','nya-uppdrag','Cementa mottagningsstation, Kilsgården, Ljungskile och Askims kyrka','<p>2013-12-16</p><p>Cementa mottagningsstation, Kilsgården, Ljungskile och Askims kyrka</p><p><strong>Cementa mottagningsstation</strong></p><p>Cementa anlitar oss för upprättande av konstruktions- och bygglovshandlingar när de bygger ny mottagningsstation.</p><p><strong>Kilsgården, Ljungskile</strong></p><p>Kilsgården är en 3-vånings byggnad som ingår i Ljungskile folkhögskola. Byggnaden är från mitten av 1930-talet och är ett s.k. plankhus där bärande fasader och hjärtväggar är av stående plank.</p><p>Byggnaden har tidigare rymt både lägenheter och lärosalar men skall nu byggas om till 8 stycken bostadsrättslägenheter. Det skall bl.a. inrymmas ett hisschakt, nya inglasade balkonger och stora takkupor för vindslägenheter skall byggas. De bärande invändiga plankväggarna skall ersättas av pelare och balkar i stål. VBK har fått i uppdrag av Peab Byggservice Väst AB att ansvara för byggnadskonstruktionen.</p><p>Arkitekt: Joaquim Terrasó Arkitekter<img width=\"610\" height=\"225\" alt=\"\" src=\"/uploads/images/nya_uppdrag_1312_1.jpg\"></p><p><strong>Askims kyrka</strong></p><p>Vi har fått i uppdrag av Svenska kyrkan att upprätta förfrågningsunderlag för fönster, yttedörrar samt</p><p>mindre fasad- och takarbeten</p><p>.<img alt=\"\" width=\"225\" height=\"300\" src=\"/uploads/images/nya_uppdrag_1312_2.jpg\"></p>','2014-08-18','0000-00-00'),
	(64,1,'Julstängt','julstangt','Vi tar ledigt för att fira jul och nyår.','<p>Vi tar ledigt för att fira jul och nyår.</p><p>Vår växel och reception stänger kl. 11.45 den 20/12 </p><p>och öppnar som vanligt igen den 7/1.</p><p>Vi önskar er alla GOD JUL och GOTT NYTT ÅR</p>','2014-08-18','0000-00-00'),
	(65,1,'UC Sigill','uc-sigill','Vi har av UC bedömts ha högsta kreditvärdighet. Klickar du på sigillet hamnar du på UCs hemsida och kan läsa om vad ratingen innebär.','<p>Vi har av UC bedömts ha högsta kreditvärdighet. Klickar du på sigillet hamnar du på UCs hemsida och kan läsa om vad ratingen innebär.</p><p><img src=\"https://www.uc.se/ucsigill2/sigill?org=5566086103&amp;language=swe&amp;product=lsa&amp;fontcolor=b\" alt=\"\"></p>','2014-08-18','0000-00-00'),
	(66,1,'Nya uppdrag','nya-uppdrag','Vindaloo, Ny entrébyggnad Hornborga, Volvo LE4 och Nya Nösnäs Ishall','<p>Vindaloo, Ny entrébyggnad Hornborga, Volvo LE4 och Nya Nösnäs Ishall</p><p><strong>Vindaloo</strong></p><p>SCA Hygiene Products fabrik i Pune, Indien är uppdelad i 3 faser. Fabriken ska tillverka \"personal care- and tissueprodukter\". Projektet omfattar produktion, kontor, omklädningsrum och råvarulager. Arkitekter och ingenjörer är från indiska företag. VBKs roll är projektledning innefattande upphandling, teknisk och ekonomisk kontroll.</p><p>Arkitekt: Archivista Engineering</p><p><strong>Ny  entrébyggnad Hornborga</strong></p><p>Länsstyrelsen Västra Götaland bygger ny entré- och servicebyggnad vid Hornborgarsjön. Byggnaden kommer att innehålla både café, toaletter och hörsal. VBK upprättar på uppdrag av Arkitekturum system- och bygghandlingar. </p><p>Arkitekt: Arkitekturum</p><p>Hornborgasjön</p><p><strong>Volvo LE4 - Prefab betong</strong></p><p>Volvo Tuve bygger ut befintlig industribyggnad LE4. VBK har i tidigare skede projekterat systemhandlingar samt bygghandlingar och upprättar nu tillverkningshandlingar av prefab betongstomme åt Strängbetong.</p><p><strong>Nya Nösnäs Ishall</strong></p><p>Stenungsunds kommun bygger ny ishall, omklädningsrum och foajé i anslutning till den befintliga Sundahallen i Stenungsund. I anslutning till ishallen och Sundahallen skall även två nya komplementbyggnader byggas. Projektet omfattar 3 etapper där ishall 1 ingår i den första etappen.  I kommande etapper ingår ny simhall, multiarena, ishall 2 och huvudentré med café/bistro. VBK upprättar bygghandlingar på uppdrag av Hansson &amp; Sönder Entreprenad AB. </p><p>Arkitekt: Pyramiden Arkitekter AB</p><p><img src=\"/uploads/images/Nosnas_ishall.png\" width=\"642\" height=\"331\" alt=\"\"></p>','2014-08-18','0000-00-00'),
	(67,1,'Nya uppdrag','nya-uppdrag','Brf Björkviken Hultet 1:11, Kungsportsavenyn 22, Kv Tyr 3, Sköve och Tillbyggnad av Kulturhuset Vingen','<p>Brf Björkviken Hultet 1:11, Kungsportsavenyn 22, Kv Tyr 3, Sköve och Tillbyggnad av Kulturhuset Vingen</p><p><strong>Brf Björkviken Hultet 1:11</strong></p><p>Derome hus uppför 4 st flerbostadshus med totalt 45 lägenheter med tillhörande carport-/förrådsbyggnad. Placerat i Öjersjö med fasad så nära som 5 meter från Stora Hålsjön. VBK upprättar bygghandlingar på uppdrag av Tuve Bygg. Fakta om projektet: </p><p>Pålad grund</p><p>Källare i vattentät betong</p><p>Betongbjälklag gjutna med plattbärlag</p><p>Lättytterväggar med stålpelare</p><p>Bärande hjärtväggar i betong</p><p>Tak reglat med 2st 45x220 på varandra s1200</p><p>Arkitekt: Liljewall Arkitekter<img src=\"/uploads/images/nya_uppdrag_1402.png\" width=\"687\" height=\"515\" alt=\"\"></p><p><strong>Kungsportsavenyn 22</strong></p><p>Vi har på uppdrag av Wallenstam fått i uppdrag att ansvara för byggledning gällande renovering av putsade och avfärgade gatu- och innegårdsfasader samt fönster och yttertak.</p><p><strong>Kv Tyr 3, Sköve</strong></p><p>Skövdebostäder river befintlig byggnad och uppför nytt bostadshus i fem våningar med 17 hyreslägenheter samt en lokal. VBK har fått i uppdrag av Calles Bygg att upprätta bygghandlingar. Preliminär inflyttning är våren 2015.</p><p>Arkitekt: Abako/CH Arkitekter</p><p><strong>Tillbyggnad av Kulturhuset Vingen</strong></p><p>Kulturhuset Vingen i Torslanda skall byggas ut med ett kontorsplan och ett bilioteksplan med en BTA på 1500 m². VBK skall ta fram bygghandlingar för stommen till Tuve Bygg som är totalentreprenör. Byggnaden utförs med pålad bottenplatta och platsgjutet plattbärlag på plan 2. En del av bilioteket skall utföras med 8 meters takhöjd och stora glasade ytor.</p><p>Arkitekt: Frenning arkitekter AB</p>','2014-08-18','0000-00-00'),
	(68,1,'Nya uppdrag','nya-uppdrag','Stombesiktningar 2014 för Volvo Powertrain, Skövde och GIS-ställverk i Borås','<p>Stombesiktningar 2014 för Volvo Powertrain, Skövde och GIS-ställverk i Borås</p><p><strong>Stombesiktningar 2014 för Volvo Powertrain, Skövde</strong></p><p>Vi utför konditionsbesiktningar av sju befintliga byggnader, i syfte att kartlägga skador på byggnadsstomme och stomkompletteringar för Volvo Powertrain i Skövde. I samband med detta uppdaterar vi Volvos fastighetsarkiv för dessa sju byggnader.</p><p><strong>GIS-ställverk i Borås</strong></p><p>För Siemens upprättar vi systemhandling gällande ny ställverksbyggnad för GIS-ställverk intill Varbergsvägen i Borås för Borås Elnät. Stor omsorg har lagts på arkitektonisk utformning då byggnader ligger intill infarten från Varbergsvägen.</p>','2014-08-18','0000-00-00'),
	(69,1,'Nya uppdrag','nya-uppdrag','Kv. Tyr 3 - Skövde, Eurostop - Halmstad och Mungseröd ? Vindkraftverksfundament','<p>Kv. Tyr 3 - Skövde, Eurostop - Halmstad och Mungseröd - Vindkraftverksfundament</p><p><strong>Kv. Tyr 3, Skövde - Prefabväggar</strong></p><p>Skövdebostäder uppför nybyggt bostadshus i 6 plan. På uppdrag av Calles Bygg AB uppför vi bygghandlingar bestående av dimensionering och uppritande av tillverkningshandlingar ör prefab-väggar i betong.</p><p>Arkitekt: CH Arkitekter</p><p><strong>Eurostop, Halmstad</strong></p><p>Vi upprättar på uppdrag av Eurocommercial Properties systemhandling gällande ny- och ombyggnad av köpcenter. Befintligt köpcentrum skall utvecklas genom att delvis byggas om och byggas ut med ca. 18.000 kvm (BTA). Hela nybyggnaden blir i ett plan frånsett teknikbyggnader på tak. För att få bättre entréer måste befintlig stomme rivas på några delar.</p><p>Arkitekt: Ågren Konsult AB</p><p><strong>Mungseröd - Vindkraftverksfundament</strong></p><p>Eoulus Vind AB skall uppföra 8 stycken nya vindkraftverk i Mungseröd i Tanums kommun. VBK hjälper Veidekke Entreprenad AB med att ta fram konstruktionshandlingar för fundamenten. Tornen är 150 meter höga inklusive rotor och fundamenten utförs som bergsförankrade fundament för att minimera betong- och armeringsmängder. Projekteringen görs i Tekla för att få en bra översikt över den relativt komplicerade armeringsföringen och med möjlighet att få fram armeringsspecifikationer.</p>','2014-08-18','0000-00-00'),
	(70,1,'Nya uppdrag - Maj','nya-uppdrag-maj','Halltec Uusikaupunki, Orrekulla kubiklager , Härryda tennisbanor samt Kv Högvakten','<p>Halltec Uusikaupunki, Orrekulla kubiklager , Härryda tennisbanor samt Kv Högvakten</p><p><strong>Halltec Uusikaupunki</strong></p><p>På uppdrag av Halltec upprättar VBK konstruktionsberäkningar för uppförande av industrihall i Uusikaupunki, Finland. </p><p><strong>Orrekulla kubiklager, besiktningar</strong></p><p>Skanska fastigheter uppför i Orrekulla industriområde ett kubiklager i egen regi. VBKs uppdrag är förbesiktningar och slutbesiktningar samt granskning av handlingar.</p><p>Arkitekt är Krook och Tjäder</p><p><strong>Härryda tennisbanor</strong></p><p>Härryda kommun anlägger 4 nya tennisbanor intill befintlig tennishall vid Vällsjövägen i Pixbo. VBK ansvarar för projektledning, projekteringsledning, upphandling, KA, byggledning samt besiktning. </p><p><img src=\"/uploads/images/nya_uppdrag_1405_1.jpg\" width=\"399\" height=\"282\" alt=\"\"></p><p><strong>Kv Högvakten</strong></p><p>Kv Högvakten består av fyra byggnader; Stadshuset, Wenngrenska huset, Strömska huset och Börsen. Stadshuset stod klart 1759 och det senast byggda, Börsen 90 år senare, 1849.  Börsen var på sin tid den dyraste byggnationen i Sverige. Samtliga byggnader förutom Strömska huset är klassade som byggnadsminne. Strömska har pga alla ombyggnader genom åren inte den klassningen, däremot har det kulturhistoriskt värde och hela området är klassat som riksintresse. WSP har tidigare projekterat ett FU för grundförstärkning av hela kvarteret. VBK har nu tagit över ansvaret för den handlingen, som nu skall färdigställas till bygghandling. Samtidigt skall det efter en förstudie nu projekteras en bygghandling för lokalanpassning av de fyra byggnaderna, vilket bl.a. inkluderar nya hisschakt, påbyggnader av Wenngrenska och Strömska huset och inglasning av Wenngrenska gården. Vilka de nya hyresgästerna blir vet man i dagsläget inte, mer än att kommunfullmäktige i fortsättningen också kommer att ha sina KF-möten i Börsenhuset och stadens prominenta gäster också i fortsättningen kommer att få spisa i någon av festivitetssalarna. Uppdragsgivare och byggherre är Higab.&nbsp;</p><p><img src=\"/uploads/images/nya_uppdrag_1405_2.jpg\" width=\"687\" height=\"344\" alt=\"\"></p>','2014-08-18','0000-00-00'),
	(71,1,'Nya uppdrag - Juni','nya-uppdrag-juni','Nytt kontorshus i Harbin, Kina och Ullevi terrasser','<p>Nytt kontorshus i Harbin, Kina och Ullevi terrasser</p><p><strong>Nytt kontorshus i Harbin, Kina</strong> </p><p>VBK, Liljewall Arkitekter och Wikströms VVS har samarbetat en tid för att skapa nya projekt i Kina. Detta har nu resulterat i ett avtal med Heilongjiang Chenergy Real Estate Development, gällande ett nytt kontorshus på 20 000 m2. Vi ska ta fram en enkel gemensam systemhandling och VBK jobbar som underkonsult till Liljewall som har skrivit avtalet med vår kinesiska beställare. Ambitionen är att bygga huset som ett passivhus och reglerna som vi kommer utgå ifrån är i nivå med svenska krav. </p><p><strong>Ullevi terrasser</strong></p><p>Skanska Fastigheter Göteborg AB uppför kontorshus om 10 våningar samt källare med parkering och teknikutrymmen. Byggnaden är belägen vid Gamla Ullevi i hörnet av Parkgatan och&nbsp;Smålandsgatan.</p><p>Takterasser finns på plan och på plan 11 (takplan). VBKs uppdrag är granskning av A-, och K- och Markhandlingar samt besiktningar med normerande förbesiktningar och slutbesiktning.</p><p>Arkitekt: Arkitektbyrån design AB</p>','2014-08-18','0000-00-00'),
	(73,1,'Nya uppdrag - Augusti','nya-uppdrag-augusti','Tomtegårdens äldreboende, Tomtegårdens äldreboende och Vindkraftverksfundament Brotorp','<p>Tomtegårdens äldreboende, Tomtegårdens äldreboende och Vindkraftverksfundament Brotorp</p><p><strong>Tomtegårdens äldreboende, köksombyggnad</strong></p><p>Vi upprättar bygghandling för om- och tillbyggnad av storkök i Tomtgårdens äldreboende, Ollenborrens förskola och Billingsdalsskolan.</p><p>Arkitekt: Norconsult </p><p><strong>Project Jumbo, Cairo site</strong></p><p>SCA Hygiene Products ska etablera en fabrik i Cairo, Egypten. Fabriken ska tillverka feminin produkter. Projektet omfattar produktion, kontor, omklädningsrum och råvarulager. VBKs roll är projektledning innefattande upphandling, teknisk och ekonomisk rådgivning.</p><p><strong>Vindkraftverksfundament Brotorp</strong></p><p>VBK har fått i uppdrag av Arise AB att projektera ett bergsinspänt fundament till sina 14 nya turbiner i Brotorp i Mönsterås kommun. Tornen är 128 meter höga till navet och vingdiametern är 126 m, vilket ger en effekt på 3,3 MW. VBK:s fundament kommer även att certifieras av Det Norske Veritas (DNV), vilket innebär en komplett granskning av valt system, ritningar och beräkningar.</p><p>Beställare: Arise AB</p><p>Entreprenör: Kanonaden Entreprenad AB</p>','2014-08-18','0000-00-00'),
	(77,1,'Frukostmingel i nya lokaler i Skövde','frukostmingel-i-nya-lokaler-i-skovde','Tidigt fredag morgon den 5 september samlades 40 personer i våra nya lokaler på Långgatan i Skövde.','<p>\n	Tidigt fredag morgon den 5 september samlades 40 personer i våra nya lokaler på Långgatan i Skövde.\n</p>\n<p>\n	En härlig frukost innehållande bland annat färska frallor och croissanter, melon och banan, mängder av pålägg och naturligtvis kaffe, the och juice stod uppdukad.\n</p>\n<p>\n	Strax före åtta var det dags för VD att genomföra bandklippning och formellt inviga vårt nya, mycket fina kontor.\n</p>\n<p>\n	Därefter drog morgonens två kortföredrag igång.\n</p>\n<p>\n	David Froh berättade om VBK-projektet Trädgårdsstadens förskola som är en av Sveriges först certifierade passivhusförskolor. Genomgående mycket låga U-värden och en extremt hög lufttäthet gör denna förskola mycket energieffektiv.\n</p>\n<p>\n	Cathrin Ansgar redogjorde för ett annat lyckat VBK-projekt, Ållebergsgymnasiet i Falköping. Denna byggnad har nominerats som ett av bidragen i den prestigefyllda tävlingen Årets Bygge.\n</p>\n<p>\n	Strax före nio på förmiddagen avslutades en lyckad kundträff och alla gav sig nöjda iväg för att ta sig an dagens utmaningar.\n</p>\n<p>\n	<img src=\"/uploads/images/61bfe425e7c5a4e53f734c7f82d74ee5.jpg\"><br>\n</p>\n<p>\n	<img src=\"/uploads/images/90f5dda65194e201473776bc3d494169.jpg\">\n</p>\n<p>\n	<img src=\"/uploads/images/5f1713693d15f1324a2e55affd9a513b.jpg\">\n</p>\n<p>\n	<img src=\"/uploads/images/e81cd409bd7d1ff5ec527921c88cd918.jpg\">\n</p>\n<p>\n	<img src=\"/uploads/images/4e585564456d521934214b4f6bcf4ffd.jpg\">\n</p>\n<p>\n	<img src=\"/uploads/images/35f8c87d739e5cc7fdb8cd22baf864cf.jpg\">\n</p>\n<p>\n	<img src=\"/uploads/images/bd1aea41bc7999a48f88b5740bdf368a.jpg\">\n</p>\n<p>\n	<img src=\"/uploads/images/7c7febff1a856f86d9a1c16741f43170.jpg\">\n</p>\n<p>\n	<img src=\"/uploads/images/f02220d04db91a30f9eafc17ced7d1bb.jpg\">\n</p>\n<p>\n	<img src=\"/uploads/images/09a06a4f847ff822a399d9c89a190426.jpg\">\n</p>\n<p>\n	<img src=\"/uploads/images/5af7afc70c6219195e64cbcc000af277.jpg\">\n</p>\n<p>\n	<img src=\"/uploads/images/b256ad090a79f3b24ef98ca7ef9d7094.jpg\">\n</p>','2014-09-05','2015-01-09'),
	(78,1,'Nyanställda','nyanstallda','Vi hälsar Andreas Lindelöf, Martin Nilsson, Andreas Andersson, Kuang Zhao, Johan Edström, Emma Hallberg och Jan Johansson välkomna till VBK.','<p>\n	Vi hälsar Andreas Lindelöf, Martin Nilsson, Andreas Andersson, Kuang Zhao, Johan Edström, Emma Hallberg och Jan Johansson välkomna till VBK.\n</p>\n<p>\n	<img src=\"/uploads/images/4acf862e19bd5903074a231687989f4a.jpg\">\n</p>','2014-09-17','2015-01-09'),
	(81,1,'Nya uppdrag','nya-uppdrag','Jackon och Willab Garden','<p>\n	<em>Jackon och Willab Garden</em>\n</p>\n<h3>Jackon, Underhållsplaner</h3>\n<p>\n	VBK har på uppdrag av Fastighetsbolaget Våmb AB fått förtroende att utföra konditionsbesiktning på byggnadsstommen samt upprätta underhållsplan för fastigheten inklusive markområde.\n</p>\n<h3>Willab Garden</h3>\n<p>\n	Willab Garden, som säljer uterum och vinterträdgårdar är ett familjeägt företag med huvudsäte i Båstad som nu etablerar sig i Munkedal. Man finns sedan tidigare även i Bromma och Köpenhamn. Peab har som totalentreprenör fått uppdraget att projektera och bygga en ny utställningshall på en äng i Munkedals kommun, mitt mellan Göteborg och Oslo. Hallen är på ca 2200 m² och inrymmer till större delen en utställningsyta, men även lager och kontor och ett teknikutrymme, placerat på ett entresolbjälklag. Stålstommen dimensioneras och levereras av Llentab AB, den fribärande glasfiberarmerade betongplattan utförs av AB Linotol och pålgrundläggningen projekteras av VBK. VBK är även Peabs huvudkonstruktör och ansvarar för samordningen av konstruktionerna.\n</p>\n<p>\n	<img src=\"/uploads/images/eacf0b7ec05e0aac5aeb2960f67daa97.jpg\">\n</p>','2014-10-02','0000-00-00'),
	(82,1,'Nya uppdrag - Oktober','nya-uppdrag-oktober','Brf. Bröndbo, Brf Fladen och Kv Glädjen','<p>	<em></em><em>Brf. Bröndbo, Brf Fladen och Kv Glädjen</em></p><h3>Brf. Bröndbo</h3><p>	HSB Uppsala bygger 54 st nya lägenheter i Kv Gimo i Uppsala. Inflyttning är beräknas till 2016. VBK har fått i uppdrag utföra de statiska beräkningarna samt leda projekteringen av Prefab åt Finja Prefab AB.</p><p>	<strong>Arkitekt:</strong> Fidjeland Arkitektkontor AB</p><h3>Brf Fladen</h3><p>	Veidekke Bostad bygger 63 stycken bostadsrätter med garage i källaren i stadsdelen Kolla Parkstad i Kungsbacka. Beräknad inflyttning är sommaren 2016. VBK har fått i uppdrag av Veidekke Bygg att upprätta bygghandlingar.</p><p>	<strong>Arkitekt:</strong> KUB arkitekter</p><h3>Kv Glädjen</h3><p>	Peab bygger 111 st lägenheter på Kungsholmen i Stockholm. Byggnaden är 7-10 våningar hög med garage i källarplan. VBK har fått i uppdrag av Strängbetong att ta fram tillverkningshandlingar och montageritningar för väggar, balkar, pelare, balkongplattor och bjälklag. Projekteringen utförs i senaste versionen av Tekla.</p><p>	<strong>Arkitekt:</strong> Brunnberg &amp; Forshed Arkitektkontor</p>','2014-01-09','0000-00-00'),
	(83,1,'Frukostmingel','frukostmingel','Frukostmingel i det om- och tillbyggda Stadsbiblioteket','<p>\n	<em></em><em>Frukostmingel i det om- och tillbyggda Stadsbiblioteket</em>\n</p>\n<p>\n	Tidigt morgonen den 13 november samlades nära 100 personer för att inta en härlig frukost i caféet i den tillbyggda delen av Göteborgs Stadsbibliotek.\n</p>\n<p>\n	Strax efter klockan åtta förflyttade vi oss till sittrappan i den öppna hörsalen. Efter några inledande ord av VD så tog dagens föredragshållare över.\n</p>\n<p>\n	Projektledare Jan-Åke Johansson från Higab redogjorde för den 14 år långa resan från de första idéerna till nyinvigningen av biblioteket på världsbokens dag den 23 april 2014.\n</p>\n<p>\n	Därefter tog arkitekt Martin Lafveskans över och delgav oss de tankar arkitekten haft kring husets utformning från det parallella uppdraget och framåt. På ett mycket pedagogiskt sätt fick vi ta del av hur huset design successivt förändrades tills det fick sitt slutliga utseende.\n</p>\n<p>\n	Stadsbibliotekets chef Christina Persson avslutade med att ge oss brukarens upplevelse av processen från att de första behoven formulerades till projekt stod helt färdigt. Till en början gällde det att lära sig springa i samma tempo som fastighetsägare och projektörer som krävde ständiga besked. Vi fick också veta att i ett bibliotek kan allt hända, bara den egna fantasin sätter gränser.\n</p>\n<p>\n	Vid halv tiotiden avslutades en lyckad kundträff och det var dags att, om inte traska hemåt, så i alla fall ta sig till sin arbetsplats för att sätta igång och beta av dagens mailskörd.\n</p>\n<p>\n	<img src=\"/uploads/images/713bf094f5de99d130a43b4d5cf02101.jpg\">\n</p>\n<p>\n	<img src=\"/uploads/images/d6037ef82a48140186c81f0fd6f72e53.jpg\">\n</p>\n<p>\n	<img src=\"/uploads/images/8ae86e2f2b537b49253a67dc2bd87d6a.jpg\">\n</p>\n<p>\n	<img src=\"/uploads/images/53cb4c4e153e09122a4f1eec879fdac4.jpg\">\n</p>\n<p>\n	<img src=\"/uploads/images/0f569c389bcbc4f8312263b45a78b5c0.jpg\">\n</p>\n<p>\n	<img src=\"/uploads/images/a17567abac49086f77d750a315beb232.jpg\">\n</p>\n<p>\n	<img src=\"/uploads/images/b967946c59922393f514846d4a5178c1.jpg\">\n</p>\n<p>\n	<img src=\"/uploads/images/44ac0d823886707371bb24b294ca7115.jpg\">\n</p>','2014-11-17','0000-00-00'),
	(84,1,'VARM 25 November','varm-25-november','Uppdaterad med lösning på konstruktionsuppgift. VBK deltager som alltid i Väg- och Vattenbyggnads arbetsmarknadsdag på Chalmers.','<p>\n	Uppdaterad med lösning på konstruktionsuppgift. VBK deltager som alltid i Väg- och Vattenbyggnads arbetsmarknadsdag på Chalmers.\n</p>\n<p>\n	På VARM får vi kontakt med unga och ambitiösa blivande ingenjörer som söker kontakt med företag för framtida exjobb, praktik eller anställning.<span></span>\n</p>\n<p>\n	<img src=\"/uploads/images/1432ef8401c4121ec439ff5e3d8b4cc3.jpg\">\'\n</p>\n<p>\n	<br>\n</p>\n<p>\n	<img src=\"/uploads/images/f57d6703015f238777c58cec13035cfe.jpg\">\n</p>','2014-11-26','0000-00-00'),
	(85,1,'Nya uppdrag - November','nya-uppdrag-november','Daloc kontor/utställning, Göteborgs konstmuseum, Åtgärdsbeskrivning för KAE','<p>	<em></em><em>Daloc kontor/utställning, Göteborgs konstmuseum, Åtgärdsbeskrivning för KAE</em></p><h3>Daloc kontor/utställning</h3><p>	Daloc Ståldörrar i Töreboda bygger till med kontorslokaler och ny entré/utställningshall. Beräknad inflyttning är under hösten 2015. VBK har fått i uppdrag att projektera konstruktionshandlingar vilket utförs i Revit.</p><p>	<strong>Arkitekt:</strong> Amnehärad Byggkonsult AB</p><p>	<img src=\"/uploads/images/nya_uppdrag_1411.png\" width=\"687\" height=\"313\" alt=\"\"></p><h3>Göteborgs konstmuseum, Förstudie om- och tillbyggnad</h3><p>	Göteborgs konstmuseum skall, som Sveriges näst största konstmuseum uppgradera och utöka sin service och tillgänglighet. VBK har fått i uppdrag av Higab AB att ta fram en förstudie som skall belysa möjligheter för en större om- och tillbyggnad av muséet.</p><p>	<strong>Arkitekt:</strong> Wingårdh Arkitektkontor AB</p><p>	<img src=\"/uploads/images/nya_uppdrag_1411_2.jpg\" width=\"687\" height=\"594\" alt=\"\"></p><p>	<em>Fotograf: Hans Wreting</em></p><h3>Åtgärdsbeskrivning för KAE (Kyrkoantikvarisk ersättning) </h3><p>	För Svenska Kyrkan i Göteborg upprättar VBK åtgärdsbeskrivningar med budgetbedömning som underlag för ansökan av KAE, kyrkoantikvarisk ersättning.</p>','2014-11-28','0000-00-00'),
	(86,1,'Julstängt','julstangt','Vi tar ledigt för att fira jul och nyår.','<p>\n	Vi tar ledigt för att fira jul och nyår.\n</p>\n<p>\n	<img src=\"/uploads/images/f1fbacda99ca2fadcb69cc7318240aa4.jpg\">\n</p>','2015-01-09','2015-01-25'),
	(87,1,'Nya uppdrag - December','nya-uppdrag-december','Skövde slakteri, tillbyggnad och Hamnplan, Lidköping','<p>\n	<em></em><em>Skövde slakteri, tillbyggnad och Hamnplan, Lidköping</em>\n</p>\n<h3>Skövde slakteri, tillbyggnad</h3>\n<p>\n	Skövde slakteri uppför bygger till med ny slaktline, chockkyl, utlastning mm. samt en kontorsbyggnad i två våningar. VBK har fått i uppdrag att projektera konstruktionshandlingar och upprätta arbetsmiljöplan.\n</p>\n<p>\n	<strong>Arkitekt:</strong> CH Arkitekter\n</p>\n<h3>Hamnplan, Lidköping</h3>\n<p>\n	Lidköpings kommun avser att förstärka kajkonstruktionen vid småbåtshamnen i Lidköping. VBK har dels fått i uppdrag att vara projektledare, vilket innebär både projekteringsledning och byggledning och dels som konstruktörer genom att upprätta bygghandlingar.\n</p>','2015-01-09','2015-01-25'),
	(88,1,'Nya uppdrag','nya-uppdrag','Kårhus på landet och Kongahälla kv 9:1 och 9:2','<p>\n	<em>Kårhus på landet och Kongahälla kv 9:1 och 9:2</em>\n</p>\n<h2>Kårhus på landet<br>\n</h2>\n<p>\n	Chalmers Studentkår äger sedan 1930-talet en fritidsanläggning belägen på en halvö invid Sandsjön i Härryda kommun. Idag består anläggningen av två stugor, C.S.-bastun, som rymmer 60 personer och Sportstugan som rymmer 30 personer. Området och byggnaderna är hårt utnyttjade och det är idag en sliten och eftersatt oas.\n</p>\n<p>\n	Kåren har beslutat att utveckla området. Dels skall befintliga byggnader rustas upp och dels skall ytterligare en byggnad uppföras, Storstugan. Se bild ovan. Den skall inrymma en festsal med plats för 120 gäster med kring- och mingelytor. Upprustningen finansieras av egna sparade medel och av önskade donationer.\n</p>\n<p>\n	VBK har anlitats som konstruktör, dels i den genomförda förstudien som har mynnat i en systemhandling och dels i den nu startade bygghandlingsprojekteringen.\n</p>\n<p>\n	<img src=\"/uploads/images/0da3c6fbf290971766ae6ec0190168ce.jpg\" unselectable=\"on\">\n</p>\n<h2>Kongahälla kv 9:1 och 9:2<br>\n</h2>\n<p>\n	Förbo AB och Kungälvsbostäder bygger 120 nya lägenheter, en förskola och ett gruppboende i sammanlagt tre huskroppar. Kungälvsbostäder är beställare för den ena huskroppen med ca. 60 lägenheter och ett gruppboende och Förbo AB är beställare för de andra två huskropparna med ca. 60 lägenheter och en förskola.\n</p>\n<p>\n	VBKs roll är projekteringsledning.\n</p>\n<p>\n	<strong>Arkitekt:</strong> Metro Arkitekter\n</p>','2015-01-25','2015-04-09'),
	(90,1,'Nya uppdrag','nya-uppdrag','Kv Muraren 7,\nTöreboda','<p>\n	<strong><span class=\"redactor-ie-paste\">Kv Muraren 7, Töreboda<br>\n	</span></strong>\n</p>\n<p>\n	Törebodabostäder AB gör en tillbyggnad av hyresbostäder i\n	<br>\n	tre våningsplan om totalt 13 stycken lägenheter samt en ombyggnad i tre\n	<br>\n	våningsplan för 3 stycken lägenheter.\n</p>\n<p>\n	VBK har fått i uppdrag av Calles Byggnads AB i Skövde AB att\n	<br>\n	ansvara för K-projektering och energibalansberäkning.\n</p>\n<p>\n	Arkitekt: CH Arkitekter\n</p>\n<p>\n	<br>\n</p>','2015-04-09','2015-04-09'),
	(91,1,'Nya uppdrag','nya-uppdrag','Göta Kanal Bostäder','<p>\n	<strong>Göta kanal Bostäder</strong>\n</p>\n<p>\n	Gamla livförsäkringsaktiebolaget Trygg liv gör en ombyggnad i\n	<br>\n	ett kvarter beläget i västra Nordstan. Det kommer att bli lokaler i källarplan,\n	<br>\n	företagsbostäder i plan 2 och bostäder (hyreslägenheter) plan 2-5 på en total\n	<br>\n	yta om 8872 kvm.\n</p>\n<p>\n	Vårt uppdrag är att upprätta ett förfrågningsunderlag för\n	<br>\n	totalentreprenad. Vi kommer att göra en rambeskrivning stomme.\n</p>\n<p>\n	Arkitekt: Semrén och Månsson\n</p>','2015-04-09','2015-04-09'),
	(92,1,'Nya uppdrag','nya-uppdrag','Konditionsbesiktning Tjörns kommun och rivning av Gasklockan','<p>\n	<strong>Konditionsbesiktning idrottsanläggningar Tjörns kommun</strong>\n</p>\n<p>\n	VBK har fått i uppdrag av Professional Management AB att göra en konditionsbesiktning av fem idrottsanläggningar i Tjörns kommun. Besiktningarna omfattar såväl byggnader som ridbanor och fotbollsplaner.\n</p>\n<p>\n	<strong>Rivning Gasklockan</strong>\n</p>\n<p>\n	Gasklockan i Göteborg skall rivas. Samtliga delar ovan mark skall tas ner. Då det finns farligt avfall och risk för nedsmutsning av miljön kommer rivningsarbetet ske i nära samarbete med Göteborgs kommun och miljöförvaltningen. VBK är kontrollansvarig på uppdrag av Göteborg Energi.\n</p>','2015-04-09','2015-04-09'),
	(95,1,'Välkommen till VBKs nya hemsida!','valkommen-till-vbks-nya-hemsida','Vi hoppas att du på ett enkelt sätt skall finna all information om oss som du söker och att du inspireras av våra projekt som vi stolt presenterar.','<p>Vi hoppas att du på ett enkelt sätt skall finna all information om oss som du söker och att du inspireras av våra projekt som vi stolt presenterar.\n</p>\n<p>Välkommen att följa oss på <a href=\"https://www.linkedin.com/company/vbk-konsulterande-ingenjörer-ab\">LinkedIn</a><br>\n</p>','2015-04-28','0000-00-00'),
	(96,1,'Test','test','atest','<p>test\n</p>\n<p><img src=\"/uploads/images/5e1b8dded5353ab23e617dc40e06d8be.jpg\">\n</p>\n<p><img src=\"/uploads/images/d88b1f642dc6294c186529a510d9a299.jpg\">\n</p>','2015-05-27','0000-00-00'),
	(97,1,'Nya uppdrag','nya-uppdrag','Mölnlycke Health Care planerar för ny produktionsanläggning i Havirov utanför Ostrava i Tjeckien och ombyggnad av Nääs Fabriker','<p><strong>Mölnlycke Health Care</strong><br>\n</p>\n<p>Mölnlycke Health Care planerar att bygga en ny produktionsanläggning i Havirov utanför Ostrava i Tjeckien, som tillägg till den befintliga anläggningen i Karvina. Byggnaden blir en komplett anläggning med råvarulager, renrumsproduktion, sterilisering, färdigvarulager samt kontorsbyggnad och tekniska utrymmen. VBKs roll är att agera byggrådgivare/projektledare och bistå i upphandling, designarbete, kostnads- och tidsstyrning samt sedermera byggledning och besiktning.<br>\n</p>\n<p><img src=\"/uploads/images/2ee4351a56aff7e22a81cd3072ba7d88.jpg\">\n</p>\n<p><strong>Nääs Fabriker</strong>\n</p>\n<p>Nääs Fabriker är beläget i Lerums kommun vid Tollered. Nääs Fabriker är ursprungligen ett fabriksområde med bl.a. en före detta textilindustri vid sjön Sävelången. Byggnaderna började byggas på 1830-talet och bland annat fanns i början här ett bomullsspinneri. Vårt uppdrag består i att upprätta förfrågningsunderlag för totalentreprenad för ombyggnad av olika delar i byggnaderna. Berörda byggnader är Pannhuset, Mellersta fabriken, Bomullsmagasinet och Gamla nedre fabriken.\n</p>\n<p>Arkitekt: Malmström Edström Arkitekter Ingenjörer AB<br>\n</p>','2015-05-27','2015-06-02'),
	(98,1,'Nya uppdrag','nya-uppdrag','Käpplunda gruva och Tjolöholm-Magasinet','<strong></strong><strong></strong><strong>Käpplunda gruva</strong><br>\n<p>Käpplunda gruva ska fyllas igen. Gruvans ingång försluts med en ny vägg och gruvans kylarschakt försluts men en betongplatta. Efteråt fyller man framför ingången som begravs under ca. 20 m uppfyllnad.\n</p>\n<p><strong>Tjolöholm - Magasinet<br></strong>VBK har fått i uppdrag av CA Consultadministration AB att upprätta teknisk beskrivning med underhållsförslag för byggnaden Magasinet vid Tjolöholms slott.\n</p>','2015-06-22','0000-00-00'),
	(99,1,'Test','test','test','<p><img src=\"/uploads/images/19fd518e6d1484264eec5512ca09e052.jpg\">\n</p>\n<p><img src=\"/uploads/images/5145a59203bd95373870df1927cefa24.jpg\">\n</p>\n<p><img src=\"/uploads/images/7329973da4c7dd97e91d2bcdbf9e27da.jpg\">\n</p>\n<p><img src=\"/uploads/images/c034b4bef66f7b6d12e7aae3e4c9061b.jpg\">\n</p>\n<p><img src=\"/uploads/images/456d55f99561b3a63b07c55d8a0fe0ac.jpg\">\n</p>\n<p><img src=\"/uploads/images/70a247b15fb1fc829cb81a10b3dcfc97.jpg\">\n</p>','2015-06-22','2015-06-22'),
	(100,1,'Arbetsplatsbesök på SCAs nya kontor i Mölndal.','arbetsplatsbesok-pa-scas-nya-kontor-i-molndal','SCAs nya kontor i Mölndal','<p class=\"MsoNormal\" style=\"margin: 0cm 0cm 0pt;\">I juni var vi cirka fyrtio VBK-medarbetare som fick chansen\natt få en guidad tur på arbetsplatsen för SCAs nya kontor i Mölndal.\n</p>\n<p class=\"MsoNormal\" style=\"margin: 0cm 0cm 0pt;\">Huset är\nformat som en cirkel och ligger delvis över vändhållplatsen för spårvagnen. Det\nkommer bli åtta våningar högt och innehålla kontor, laboratorier, verkstad,\nterrasser med mera. \n	<o:p></o:p>\n</p>\n<p>\n	Inflyttning beräknad till december 2016.<o:p></o:p>\n</p>\n<p>\n	Vi tackar platschef Peter Olsson för den guidade turen.<o:p></o:p>\n</p>\n<p><img src=\"/uploads/images/802a26c9677b338aabb164f693672636.jpg\" unselectable=\"on\">\n</p>\n<p><img src=\"/uploads/images/548c740bd4366cc4cf166f344af8f110.jpg\" unselectable=\"on\">\n</p>\n<p><img src=\"/uploads/images/94bdff73362f226d7d614a6008fc9e8d.jpg\" unselectable=\"on\">\n</p>\n<p><img src=\"/uploads/images/e6fd8a6ae5e37ee66951512a685aadfb.jpg\" unselectable=\"on\">\n</p>\n<p><img src=\"/uploads/images/eee80965401c1e29972e6601c43c1d47.jpg\" unselectable=\"on\">\n</p>\n<p><img src=\"/uploads/images/3ea998356e6ee2ab608cfa2a34cf3e9d.jpg\" unselectable=\"on\">\n</p>','2015-06-22','0000-00-00'),
	(101,1,'Kundenkät','kundenkat','Mycket bra betyg i årets kundenkät','<p><strong></strong>\n</p>\n<p><strong>Mycket bra betyg i årets kundenkät</strong>\n</p>\n<p>VBK har sedan början av 2000-talet ett kvalitets- och miljöledningssystem certifierat enligt ISO 9001 och ISO 14001.<br><br>Som ett led i vår strävan efter ständig förbättring genomför vi årligen, genom personliga intervjuer, en uppföljning av ett antal genomförda uppdrag. Kunderna har att sätta betyg på hur vi klarat vårt uppdrag inom sju olika kategorier.<br>\n</p>\n<p>Betygsskalan är 1-6, där 1 är mycket dåligt och 6 mycket bra. I år har vi blivit betygssatta av 17 olika kunder i 17 olika uppdrag.<br><br>Genomsnittsresultatet blev följande:\n</p>\n<p><img src=\"/uploads/images/2e0fd6df39261244539a626d3218d8a3.jpg\">\n</p>\n<p>De kunder som besvarat enkäten, d v s deltagit i intervjuerna, är från såväl den offentliga förvaltningen som det privata näringslivet i form av industriföretag,fastighetsägare och byggentreprenörer.<br><br>Exempel på kommentarer från de intervjuade kunderna:\n</p>\n<p><img src=\"/uploads/images/480fa0e2106965448123cd17114497e9.jpg\">\n</p>','2015-07-01','2015-07-01'),
	(102,1,'Semesterstängt','semesterstangt','Vi stänger våra kontor för semester.','<p><img src=\"/uploads/images/aab9c3d023cc92cdd14f53aace401af9.jpg\">\n</p>','2015-07-03','2015-07-03');

/*!40000 ALTER TABLE `articlecontent` ENABLE KEYS */;
UNLOCK TABLES;


# Tabelldump employee
# ------------------------------------------------------------

DROP TABLE IF EXISTS `employee`;

CREATE TABLE `employee` (
  `employeeId` int(11) NOT NULL AUTO_INCREMENT,
  `employeeName` varchar(500) NOT NULL,
  `employeeTitle` varchar(500) NOT NULL,
  `employeeGroup` varchar(500) NOT NULL,
  `employeePhone` varchar(100) NOT NULL,
  `employeeEmail` varchar(100) NOT NULL,
  `employeeImage` varchar(50) NOT NULL,
  `employeeCity` varchar(500) NOT NULL,
  `employeeVisible` int(1) NOT NULL DEFAULT '1',
  `visibility` int(1) NOT NULL DEFAULT '1',
  `deletedAt` date DEFAULT NULL,
  PRIMARY KEY (`employeeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;

INSERT INTO `employee` (`employeeId`, `employeeName`, `employeeTitle`, `employeeGroup`, `employeePhone`, `employeeEmail`, `employeeImage`, `employeeCity`, `employeeVisible`, `visibility`, `deletedAt`)
VALUES
	(1,'Mirello Media','','','076-099 99 45','info@mirello.se','mirellomedia.png','Borås',0,0,NULL),
	(10,'Niklas Alsterhem','','Byggprojektering','031-703 35 77','niklas.alsterhem@vbk.se','Niklas-Alsterheim-6487.jpg','Göteborg',1,1,NULL),
	(11,'Arne Andersen','','Byggprojektering','031-703 35 21','arne.andersen@vbk.se','Arne-Andersen-6381.jpg','Göteborg',1,1,NULL),
	(12,'Osborn Andersson','','Projektadministration','031-703 35 03','osborn.andersson@vbk.se','Osborn-Andersson-8116.jpg','Göteborg',1,1,NULL),
	(13,'Hans Anhede','','Projektadministration','031-703 35 10','hans.anhede@vbk.se','Hans-Anhede-6403.jpg','Göteborg',1,1,NULL),
	(14,'Kristina Bauner Rydén','','Byggprojektering','031-703 35 26','kristina.b-ryden@vbk.se','Kristina-B-Ryden-.jpg','Göteborg',1,1,NULL),
	(16,'Jens Bergbom','','Projektadministration','031-703 35 09','jens.bergbom@vbk.se','Jens-Bergbom-6664.jpg','Göteborg',1,1,NULL),
	(17,'Jan Bergstrand','Delägare','Ledningsgrupp | Byggprojektering','031-703 35 28','jan.bergstrand@vbk.se','Jan-Bergstrand-6431.jpg','Göteborg',1,1,NULL),
	(18,'Isabella Berner','','Byggprojektering','031-703 35 78','isabella.berner@vbk.se','Isabella-Berner-6442.jpg','Göteborg',1,1,NULL),
	(19,'Frida Berntsson','','Internadministration','031-703 35 82','frida.berntsson@vbk.se','Frida-Berntsson-6452.jpg','Göteborg',1,1,NULL),
	(21,'Elina Björk','','Internadministration','031-703 35 81','elina.bjork@vbk.se','Elina-Bjork-6467.jpg','Göteborg',1,1,NULL),
	(22,'Daniel Bragd','','Byggprojektering','031-703 35 61','daniel.bragd@vbk.se','Daniel-Bragd-6524.jpg','Göteborg',1,1,NULL),
	(23,'Anna Brandberg','','Byggprojektering','031-703 35 57','anna.brandberg@vbk.se','Anna-Brandberg-6895.jpg','Göteborg',1,1,NULL),
	(24,'Björn Brunander','Delägare','Ledningsgrupp | Projektadministration | Kontrollansvarig PBL','031-703 35 20','bjorn.brunander@vbk.se','Bjorn-Brunander-6591.jpg','Göteborg',1,1,NULL),
	(25,'Emanuel Dagefors','','Byggprojektering','031-703 35 08','emanuel.dagefors@vbk.se','Emanuel-Dagefors-6975.jpg','Göteborg',1,1,NULL),
	(26,'John Edström','','Byggprojektering','031-703 35 24','john.edstrom@vbk.se','John-Edstrom-6496.jpg','Göteborg',1,1,NULL),
	(27,'Emelie Eneland','','Byggprojektering','031-703 35 29','emelie.eneland@vbk.se','Emelie-Eneland-6399.jpg','Göteborg',1,1,NULL),
	(28,'Lars Engsfelt','','Byggprojektering | Projektadministration | Kontrollansvarig PBL | Skyddsrumssakkunnig','031-703 35 15','lars.engsfelt@vbk.se','Lars-Engfelt-6515.jpg','Göteborg',1,1,NULL),
	(29,'Jonas Eriksson','','Byggprojektering','031-703 35 07','jonas.eriksson@vbk.se','Jonas-Eriksson-6623.jpg','Göteborg',1,1,NULL),
	(30,'Per Eriksson','','Byggprojektering','031-703 35 02','per.eriksson@vbk.se','Per-Eriksson-6685.jpg','Göteborg',1,1,NULL),
	(31,'Kristian Erlandsson','','Byggprojektering | Projektadministration','031-703 35 13','kristian.erlandsson@vbk.se','Kristian-Erlandsson-6655.jpg','Göteborg',1,1,NULL),
	(32,'Sigurd Fridstedt','','Byggprojektering','031-703 35 42','sigurd.fridstedt@vbk.se','Sigurd-Fridstedt-6643.jpg','Göteborg',1,1,NULL),
	(33,'Martin Fritiofsson','','Byggprojektering','031-703 35 38','martin.fritiofsson@vbk.se','Martin-Fritiofsson-6703.jpg','Göteborg',1,1,NULL),
	(35,'Mikael Furu','','Byggprojektering','031-703 35 19','mikael.furu@vbk.se','Mikael-Furu-6570.jpg','Göteborg',1,1,NULL),
	(36,'Fredrik Gelander','','Byggprojektering','031-703 35 43','fredrik.gelander@vbk.se','Fredrik-Gelander-7018.jpg','Göteborg',1,1,NULL),
	(37,'Christoffer Grandin','','Byggprojektering','031-703 35 48','christoffer.grandin@vbk.se','Christoffer-Grandin-6746.jpg','Göteborg',1,1,NULL),
	(38,'Leif Gustafson','','Byggprojektering','031-703 35 30','leif.gustafson@vbk.se','Leif-Gustafsson_8128.jpg','Göteborg',1,1,NULL),
	(39,'Sofia Hakner','','Projektadministration','031-703 35 80','sofia.hakner@vbk.se','Sofia-Hakner-6809.jpg','Göteborg',1,0,'2015-03-12'),
	(40,'Anna Hallberg','','Byggprojektering','031-703 35 37','anna.hallberg@vbk.se','Anna-Hallberg-7412.jpg','Göteborg',1,1,NULL),
	(41,'Lars-Olof Hellgren','','Byggprojektering','031-703 35 65','lars-olof.hellgren@vbk.se','placeholder.jpg','Göteborg',1,1,NULL),
	(42,'Caroline Holmqvist','','Byggprojektering','031-703 35 31','caroline.holmqvist@vbk.se','Caroline-Holmqvist-6838.jpg','Göteborg',1,1,NULL),
	(43,'Lukas Jacobsson','','Byggprojektering | Kontrollansvarig PBL | Skyddsrumssakkunnig','031-703 35 23','lukas.jacobsson@vbk.se','Lukas-Jacobsson-6562.jpg','Göteborg',1,1,NULL),
	(44,'Dan Jarlén','','Byggprojektering','031-703 35 64','dan.jarlen@vbk.se','Dan-Jarlen-6874.jpg','Göteborg',1,1,NULL),
	(45,'Bengt Johansson','','Byggprojektering','031-703 35 35','bengt.johansson@vbk.se','Bengt-Johansson-6856.jpg','Göteborg',1,1,NULL),
	(46,'Thomas Johansson','','Byggprojektering','031-703 35 72','thomas.johansson@vbk.se','Thomas-Johansson-7181.jpg','Göteborg',1,1,NULL),
	(47,'Jan Jönsson','','Byggprojektering','031-703 35 73','jan.jonsson@vbk.se','Jan-Jonsson-6904.jpg','Göteborg',1,1,NULL),
	(48,'Ulf Kjellberg','VD / Delägare','Ledningsgrupp | Byggprojektering','031-703 35 59','ulf.kjellberg@vbk.se','Ulf-Kjellberg-7022.jpg','Göteborg',1,1,NULL),
	(49,'Ola Kjellman','Delägare','Ledningsgrupp | Byggprojektering','031-703 35 56','ola.kjellman@vbk.se','Ola-Kjellman-6542.jpg','Göteborg',1,1,NULL),
	(50,'Per-Ove Lennartzon','','Projektadministration | Kontrollansvarig PBL | Certifierad besiktningsman','031-703 35 22','per-ove.lennartzon@vbk.se','Per-Ove-Lennartzon-6732.jpg','Göteborg',1,1,NULL),
	(51,'Jacob Lilljegren','','Byggprojektering','031-703 35 54','jacob.lilljegren@vbk.se','Jacob-Lilljegren-7050.jpg','Göteborg',1,1,NULL),
	(52,'Jimmy Lovén','','Byggprojektering','031-703 35 53','jimmy.loven@vbk.se','Jimmy-Loven-7089.jpg','Göteborg',1,1,NULL),
	(53,'Terese Löfberg','','Byggprojektering','031-703 35 76','terese.lofberg@vbk.se','Terese-Lofberg-7065.jpg','Göteborg',1,1,NULL),
	(54,'Niklas Marberg','','Byggprojektering','031-703 35 62','niklas.marberg@vbk.se','Niklas-Marberg-7114.jpg','Göteborg',1,1,NULL),
	(55,'Margareta Mathiasson','','Internadministration','031-703 35 67','margareta.mathiasson@vbk.se','Margareta-Mathiasson-7131.jpg','Göteborg',1,1,NULL),
	(56,'Kristian Naess','','','031-703 35 75','kristian.naess@vbk.se','Kristian-Naess-6762.jpg','Göteborg',1,1,NULL),
	(57,'Rasmus Nilsson','','Byggprojektering','031-703 35 11','rasmus.nilsson@vbk.se','Rasmus-Nilsson-7166.jpg','Göteborg',1,1,NULL),
	(58,'Barbro Norrby','','Byggprojektering | Kontrollansvarig PBL | BAS P/BAS U','031-703 35 45','barbro.norrby@vbk.se','Barbro-Norrby-7225.jpg','Göteborg',1,1,NULL),
	(59,'Peter Norrby','Delägare','Ledningsgrupp | Projektadministration | Kontrollansvarig PBL | BAS P/BAS U','031-703 35 36','peter.norrby@vbk.se','Peter-Norrby-7235.jpg','Göteborg',1,1,NULL),
	(60,'Claes Olofsson','','Projektadministration | Kontrollansvarig PBL | BAS P/Bas U','031-703 35 63','claes.olofsson@vbk.se','Claes-Olofsson-7207.jpg','Göteborg',1,1,NULL),
	(61,'Bo Olsson','','Byggprojektering','031-703 35 14','bo.olsson@vbk.se','Bo-Olsson-7217.jpg','Göteborg',1,1,NULL),
	(62,'Gunilla Olsson','','Internadministration','031-703 35 55','gunilla.olsson@vbk.se','Gunilla-Olsson-8079.jpg','Göteborg',1,1,NULL),
	(63,'Katarina Olsson','','Projektadministration','031-703 35 69','katarina.olsson@vbk.se','Katarina-Olsson-7193.jpg','Göteborg',1,1,NULL),
	(64,'Andreas Pamp','','Byggprojektering','031-703 35 50','andreas.pamp@vbk.se','Andreas-Pamp-7269.jpg','Göteborg',1,1,NULL),
	(65,'Nadezda Peric','','Byggprojektering','031-703 35 46','nadezda.peric@vbk.se','Nadezda-Peric-7333.jpg','Göteborg',1,1,NULL),
	(66,'Juha Rauk','','Byggprojektering','031-703 35 49','juha.rauk@vbk.se','Juha-Rauk-7319.jpg','Göteborg',1,1,NULL),
	(67,'Erik Ryding','','Byggprojektering','031-703 35 51','erik.ryding@vbk.se','Erik-Ryding-7348.jpg','Göteborg',1,1,NULL),
	(69,'Frida Sewén','','Projektadministration','031-703 35 32','frida.sewen@vbk.se','Frida-Sewen-7244.jpg','Göteborg',1,0,'2015-03-12'),
	(70,'Erik Singstrand','','Projektadministration | Kontrollansvarig PBL','031-703 35 12','erik.singstrand@vbk.se','Erik-Singstrand-7291.jpg','Göteborg',1,1,NULL),
	(71,'Claes Sjöström','','Projektadministration | Kontrollansvarig PBL','031-703 35 47','claes.sjostrom@vbk.se','Claes-Sjostrom-7385.jpg','Göteborg',1,1,NULL),
	(72,'Louise Steén','','Byggprojektering','031-703 35 74','louise.steen@vbk.se','Louise-Steen-6922.jpg','Göteborg',1,1,NULL),
	(73,'Alexander Strand','','Byggprojektering','031-703 35 41','alexander.strand@vbk.se','alexander-strand-7396-1432198940-1432199030.jpg','Göteborg',1,1,NULL),
	(74,'Sören Svensson','','Byggprojektering','031-703 35 60','soren.svensson@vbk.se','Soren-Svensson-7499.jpg','Göteborg',1,1,NULL),
	(75,'Annika Svensson','','Projektadministration','031-703 35 39','annika.svensson@vbk.se','Annika-Svensson-7440.jpg','Göteborg',1,1,NULL),
	(76,'Gunnel Svensson','','Projektadministration','031-703 35 25','gunnel.svensson@vbk.se','Gunnel-Svensson-8107.jpg','Göteborg',1,1,NULL),
	(77,'Rasmus Sylvén','','Byggprojektering','031-703 35 18','rasmus.sylven@vbk.se','Rasmus-Sylven-7486.jpg','Göteborg',1,1,NULL),
	(78,'Olivia Szmidt','','Byggprojektering','031-703 35 06','olivia.szmidt@vbk.se','Olivia-Szmidt-7305.jpg','Göteborg',1,1,NULL),
	(79,'Marie Teike','','Projektadministration | Kontrollansvarig PBL | BAS P/BAS U','031-703 35 66','marie.teike@vbk.se','Maria-Teike-7509.jpg','Göteborg',1,1,NULL),
	(80,'Jan Tureson','','Byggprojektering','031-703 35 70','jan.tureson@vbk.se','Jan-Tureson_8088.jpg','Göteborg',1,1,NULL),
	(81,'Anette Vall','Ekonomi- och HR-chef','Ledningsgrupp | Internadministration','031-703 35 52','anette.vall@vbk.se','Anette-Vall-6781.jpg','Göteborg',1,1,NULL),
	(82,'Daniel Åhlgren','','Byggprojektering','031-703 35 58','daniel.ahlgren@vbk.se','Daniel-Ahlgren-7456.jpg','Göteborg',1,1,NULL),
	(83,'Jon Örn','Delägare','Ledningsgrupp | Byggprojektering | BAS P/BAS U','031-703 35 17','jon.orn@vbk.se','Jon-Orn-6961.jpg','Göteborg',1,1,NULL),
	(84,'Helena Anderson','','Byggprojektering','0500-44 45 71','helena.anderson@vbk.se','Helena-Andersson.jpg','Skövde',1,1,NULL),
	(85,'Jan-Eje Andersson','Delägare','Ledningsgrupp | Byggprojektering','0500-44 45 61','jan-eje.andersson@vbk.se','Jan-Eje-Andersson.jpg','Skövde',1,1,NULL),
	(86,'Ann-Christine Andersson','','Byggprojektering','0500-44 45 65','ann-christine.andersson@vbk.se','Ann-Christine-Andersson.jpg','Skövde',1,1,NULL),
	(87,'Cathrine Ansgar','','Byggprojektering','0500-44 45 62','cathrine.ansgar@vbk.se','Cathrine-Ansgar.jpg','Skövde',1,1,NULL),
	(88,'Katarina Apleryd','','Byggprojektering | BAS P/BAS U','0500-44 45 70','katarina.apleryd@vbk.se','Katarina-Apleryd.jpg','Skövde',1,1,NULL),
	(89,'Sinisa Buha','','Byggprojektering','0500-444567','sinisa.buha@vbk.se','Sinisa-Buha.jpg','Skövde',1,1,NULL),
	(90,'David Froh','','Byggprojektering','0500-444564','david.froh@vbk.se','David-Froh-2.jpg','Skövde',1,1,NULL),
	(91,'Malin Ingmarsdotter','','Byggprojektering','0500-44 45 66','malin.ingmarsdotter@vbk.se','Malin-Ingmarsdotter.jpg','Skövde',1,1,NULL),
	(93,'Axel Söderberg','','Byggprojektering','031-703 35 16','axel.soderberg@vbk.se','Axel-Soderberg.jpg','Göteborg',1,1,NULL),
	(94,'Emma Thorén','','Byggprojektering','0500-44 45 72','emma.thoren@vbk.se','Emma-Thoren.jpg','Skövde',1,1,NULL),
	(95,'Johanna Törnberg','','Byggprojektering','0500-44 45 63','johanna.tornberg@vbk.se','Johanna-Tornberg.jpg','Skövde',1,1,NULL),
	(96,'Joachim Wixtröm','','Byggprojektering','0500-44 45 69','joachim.wixtrom@vbk.se','Joachim-Wixtrom.jpg','Skövde',1,1,NULL),
	(97,'Andreas Andersson','','Byggprojektering','031-703 35 99','andreas.andersson@vbk.se','Andreas-Andersson-6385.jpg','Göteborg',1,1,NULL),
	(98,'Andreas Lindelöf','','Byggprojektering','031-703 35 27','andreas.lindelof@vbk.se','Andreas-Lindelof-6986.jpg','Göteborg',1,1,NULL),
	(99,'Emma Hallberg','','Byggprojektering','031-703 35 98','emma.hallberg@vbk.se','Emma-Hallberg-6776.jpg','Göteborg',1,1,NULL),
	(100,'Erik Beets','','Byggprojektering','031-703 35 68','erik.beets@vbk.se','Erik-Beets-7365.jpg','Göteborg',1,1,NULL),
	(101,'Jan Johansson','','Projektadministration','031-703 35 40','jan.johansson@vbk.se','Jan-Johansson-6819.jpg','Göteborg',1,1,NULL),
	(103,'Jonas Lyckebäck','','Byggprojektering','','jonas.lyckeback@vbk.se','Jonas-Lyckeback-6376.jpg','Göteborg',1,0,'2015-03-12'),
	(104,'Julia Folkesson','','Byggprojektering','031-703 35 04','julia.folkesson@vbk.se','Julia-Folkesson-7281.jpg','Göteborg',1,1,NULL),
	(105,'Kuang Zhao','','Byggprojektering','031-703 35 33','kuang.zhao@vbk.se','Kuang-Zhao-7463.jpg','Göteborg',1,1,NULL),
	(106,'Martin Nilsson','','Byggprojektering','031-703 35 34','martin.nilsson@vbk.se','Martin-Nilsson-6939.jpg','Göteborg',1,1,NULL),
	(111,'Amanda Metz','','','031-703 35 05','amanda.metz@vbk.se','amanda-metz-2287-1429865592.jpg','Göteborg',1,1,NULL),
	(116,'Tomas Eriksson','','','031-703 35 83','tomas.eriksson@vbk.se','tomas-eriksson-1429865485.jpg','Göteborg',1,1,NULL),
	(118,'Jonas Ekholm','','','031-703 35 32','jonas.ekholm@vbk.se','jonas-ekholm-1432102824.jpg','Göteborg',1,1,NULL);

/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;


# Tabelldump employee_employeegroup
# ------------------------------------------------------------

DROP TABLE IF EXISTS `employee_employeegroup`;

CREATE TABLE `employee_employeegroup` (
  `employeeId` int(11) unsigned NOT NULL,
  `employeegroupId` int(11) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `employee_employeegroup` WRITE;
/*!40000 ALTER TABLE `employee_employeegroup` DISABLE KEYS */;

INSERT INTO `employee_employeegroup` (`employeeId`, `employeegroupId`)
VALUES
	(12,8),
	(11,2),
	(10,2),
	(13,8),
	(14,2),
	(18,2),
	(16,8),
	(17,2),
	(17,1),
	(21,3),
	(19,3),
	(22,2),
	(23,2),
	(24,1),
	(24,8),
	(24,4),
	(26,2),
	(27,2),
	(29,2),
	(30,2),
	(32,2),
	(33,2),
	(35,2),
	(36,2),
	(37,2),
	(38,2),
	(39,8),
	(40,2),
	(41,2),
	(42,2),
	(43,2),
	(43,4),
	(43,6),
	(44,2),
	(45,2),
	(46,2),
	(47,2),
	(49,1),
	(49,2),
	(50,8),
	(50,4),
	(50,7),
	(51,2),
	(53,2),
	(54,2),
	(55,3),
	(57,2),
	(59,1),
	(59,8),
	(59,4),
	(59,5),
	(60,8),
	(60,4),
	(60,5),
	(61,2),
	(62,3),
	(64,2),
	(65,2),
	(66,2),
	(67,2),
	(69,8),
	(70,8),
	(70,4),
	(71,4),
	(71,8),
	(72,2),
	(74,2),
	(75,8),
	(77,2),
	(78,2),
	(80,2),
	(82,2),
	(83,1),
	(83,2),
	(83,5),
	(84,2),
	(85,1),
	(85,2),
	(86,2),
	(87,2),
	(88,2),
	(88,5),
	(89,2),
	(90,2),
	(91,2),
	(94,2),
	(103,2),
	(52,2),
	(56,2),
	(60,7),
	(28,2),
	(28,4),
	(28,6),
	(107,1),
	(108,6),
	(25,2),
	(110,3),
	(97,2),
	(98,2),
	(99,2),
	(100,2),
	(101,8),
	(104,2),
	(105,2),
	(106,2),
	(93,2),
	(31,2),
	(0,2),
	(0,2),
	(0,2),
	(112,1),
	(79,8),
	(79,4),
	(79,5),
	(96,2),
	(48,2),
	(48,1),
	(58,2),
	(58,4),
	(58,5),
	(76,3),
	(95,2),
	(119,8),
	(111,2),
	(118,2),
	(116,2),
	(73,2),
	(63,8),
	(81,1),
	(81,3);

/*!40000 ALTER TABLE `employee_employeegroup` ENABLE KEYS */;
UNLOCK TABLES;


# Tabelldump employeedata
# ------------------------------------------------------------

DROP TABLE IF EXISTS `employeedata`;

CREATE TABLE `employeedata` (
  `employeeId` int(11) NOT NULL,
  `employeePassword` varchar(255) NOT NULL,
  `employeeCreated` date NOT NULL,
  `employeeVerified` int(11) NOT NULL,
  UNIQUE KEY `employeeId` (`employeeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `employeedata` WRITE;
/*!40000 ALTER TABLE `employeedata` DISABLE KEYS */;

INSERT INTO `employeedata` (`employeeId`, `employeePassword`, `employeeCreated`, `employeeVerified`)
VALUES
	(1,'$2y$10$IyXYl7/3grA6YVbLI1conOMC1GJGoltrK2BqVlvhhieUyRbshy/qq','2014-07-18',1),
	(25,'$2y$10$m4sZ9hwCjLhoaT3N9jj5K.D39II/zBw9XRwBXSwGDbbBXmPbu2u22','0000-00-00',1),
	(48,'$2y$10$OaNMvGFSNWXixz1GyehdcuIXlgU4La2RSg8MjUMOP3R2gkwfTiqoS','0000-00-00',1),
	(58,'$2y$10$5eFXHyhM4hKik5DdL.maV.QMHUpeLUwfLMREPx3gjGZ.pcrQXbtr6','0000-00-00',1),
	(63,'$2y$10$Q/FxahEV/YSIkOqCJOyZve1qLm1w0k6HwwCrrCgvFjlnOHaB7IbSC','0000-00-00',1),
	(81,'$2y$10$mBBlSaxoCdz/ue0jtRdteeVkImh6iqaj1c/XYhS1HGFnBU5KjHd0u','0000-00-00',1);

/*!40000 ALTER TABLE `employeedata` ENABLE KEYS */;
UNLOCK TABLES;


# Tabelldump employeegroup
# ------------------------------------------------------------

DROP TABLE IF EXISTS `employeegroup`;

CREATE TABLE `employeegroup` (
  `employeegroupId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `employeegroupName` varchar(100) DEFAULT NULL,
  `employeegroupOrder` int(11) DEFAULT NULL,
  PRIMARY KEY (`employeegroupId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `employeegroup` WRITE;
/*!40000 ALTER TABLE `employeegroup` DISABLE KEYS */;

INSERT INTO `employeegroup` (`employeegroupId`, `employeegroupName`, `employeegroupOrder`)
VALUES
	(1,'Ledningsgrupp',1),
	(2,'Byggprojektering',2),
	(3,'Internadministration',4),
	(4,'Kontrollansvarig PBL',5),
	(5,'Bas P / Bas U',6),
	(6,'Skyddsrumssakkunnig',7),
	(7,'Certifierad besiktningsman',8),
	(8,'Projektadministration',3);

/*!40000 ALTER TABLE `employeegroup` ENABLE KEYS */;
UNLOCK TABLES;


# Tabelldump employeeNotices
# ------------------------------------------------------------

DROP TABLE IF EXISTS `employeeNotices`;

CREATE TABLE `employeeNotices` (
  `noticeId` int(11) NOT NULL AUTO_INCREMENT,
  `employeeId` int(11) NOT NULL,
  `noticeMessage` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `noticeLink` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `noticeType` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `noticeStatus` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`noticeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Tabelldump language
# ------------------------------------------------------------

DROP TABLE IF EXISTS `language`;

CREATE TABLE `language` (
  `languageId` int(11) NOT NULL AUTO_INCREMENT,
  `languageName` varchar(500) NOT NULL,
  `languageNamePrimary` varchar(255) NOT NULL,
  `languageCode` varchar(5) NOT NULL,
  PRIMARY KEY (`languageId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `language` WRITE;
/*!40000 ALTER TABLE `language` DISABLE KEYS */;

INSERT INTO `language` (`languageId`, `languageName`, `languageNamePrimary`, `languageCode`)
VALUES
	(1,'Svenska','Svenska','sv'),
	(2,'English','Engelska','en');

/*!40000 ALTER TABLE `language` ENABLE KEYS */;
UNLOCK TABLES;


# Tabelldump page
# ------------------------------------------------------------

DROP TABLE IF EXISTS `page`;

CREATE TABLE `page` (
  `pageId` int(11) NOT NULL AUTO_INCREMENT,
  `pageTemplate` int(11) NOT NULL,
  `pageParent` int(11) NOT NULL,
  `pageMenu` int(11) NOT NULL,
  `pageMenuSort` int(11) NOT NULL,
  `visibility` int(1) NOT NULL DEFAULT '1',
  `deletedAt` date DEFAULT NULL,
  `published` int(1) NOT NULL DEFAULT '1',
  `hidden` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pageId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `page` WRITE;
/*!40000 ALTER TABLE `page` DISABLE KEYS */;

INSERT INTO `page` (`pageId`, `pageTemplate`, `pageParent`, `pageMenu`, `pageMenuSort`, `visibility`, `deletedAt`, `published`, `hidden`)
VALUES
	(6,0,0,1,0,1,NULL,1,0),
	(7,1,0,1,0,1,NULL,1,0),
	(9,1,7,3,1,1,NULL,0,0),
	(10,1,7,3,0,1,NULL,1,0),
	(11,1,7,3,0,1,NULL,1,0),
	(12,1,7,3,0,1,NULL,0,0),
	(13,2,0,1,0,1,NULL,1,0),
	(14,1,0,1,0,1,NULL,1,0),
	(15,1,14,3,1,1,NULL,1,0),
	(16,1,14,3,0,1,NULL,1,0),
	(17,1,14,3,0,1,NULL,1,0),
	(18,1,14,3,0,1,NULL,1,0),
	(19,3,0,1,0,1,NULL,1,0),
	(20,1,0,2,0,1,NULL,1,0),
	(21,1,0,2,0,1,NULL,1,0),
	(22,1,0,2,0,1,NULL,1,0),
	(23,1,0,2,0,1,NULL,1,0),
	(24,6,23,3,0,1,NULL,1,0),
	(25,10,23,3,0,1,NULL,1,0),
	(26,10,23,3,0,1,NULL,1,0),
	(27,1,20,3,1,1,NULL,1,0),
	(28,1,21,3,1,1,NULL,0,0),
	(29,1,22,3,1,1,NULL,0,0),
	(30,1,20,3,0,1,NULL,1,0),
	(31,1,21,3,0,1,NULL,1,0),
	(32,1,22,3,0,1,NULL,1,0),
	(33,11,19,3,1,1,NULL,1,0),
	(34,12,19,3,1,1,NULL,1,0),
	(35,11,19,3,0,1,NULL,1,0),
	(36,1,7,3,0,1,NULL,1,1),
	(37,1,0,1,0,1,NULL,1,0),
	(38,1,0,1,0,1,NULL,1,0);

/*!40000 ALTER TABLE `page` ENABLE KEYS */;
UNLOCK TABLES;


# Tabelldump pagecontent
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pagecontent`;

CREATE TABLE `pagecontent` (
  `pageId` int(11) NOT NULL,
  `languageId` int(11) NOT NULL,
  `pageName` varchar(500) NOT NULL,
  `pageHeader` varchar(500) DEFAULT NULL,
  `pageLink` varchar(500) NOT NULL,
  `pageContent` text NOT NULL,
  `pageHeaderimage` varchar(500) NOT NULL,
  PRIMARY KEY (`pageId`,`languageId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `pagecontent` WRITE;
/*!40000 ALTER TABLE `pagecontent` DISABLE KEYS */;

INSERT INTO `pagecontent` (`pageId`, `languageId`, `pageName`, `pageHeader`, `pageLink`, `pageContent`, `pageHeaderimage`)
VALUES
	(6,1,'Start','hehehe','start','<p>skapar för framtiden\n</p>',''),
	(6,2,'Start',NULL,'start','<p>creates for the future\n</p>',''),
	(7,1,'Om VBK',NULL,'om-vbk','',''),
	(7,2,'About VBK',NULL,'about-vbk','',''),
	(9,1,'Företagspresentation',NULL,'foretagspresentation','<h2>VBK är ett konsulterande ingenjörsföretag inom byggbranschen</h2>\n<p>\n	<strong>Vi är verksamma inom:</strong>\n</p>\n<ul>\n	<li><strong>Byggprojektering</strong> innefattande <em>byggnadskonstruktion</em>, <em>byggnadsteknik</em> och <em>byggnadsfysik</em>.</li>\n	<li><strong>Projektadministration</strong> bestående av <em>projekt</em>-, <em>projekterings</em>- och <em>byggledning</em>, <em>entreprenadbesiktningar</em>, <em>kontrollansvarig PBL</em>, <em>Bas P</em> och <em>U</em> samt <em>skyddsrumssakkunniga</em>.</li>\n	<li><strong>Underhåll</strong> vari ingår genomförande av <em>underhålls</em>- och <em>konditionsbesiktningar</em> samt <em>skadeutredningar</em> och <em>upprättande av underhållsplaner</em> och <em>åtgärdsprogram</em>.</li>\n</ul>\n<p>\n	Sedan starten år 1958 har VBK haft förmånen att få arbeta med projekt som väcker uppmärksamhet, såväl i Västsverige som i landet i övrigt. Det ser vi som tecken på våra uppdragsgivares uppskattning av vårt kunnande, och förtroende för vårt oberoende. VBK ägs uteslutande av personer som är verksamma i företaget.\n</p>\n<p>\n	Som ett led i vår strävan att följa den snabba tekniska utvecklingen i vår bransch väljer vi i huvudsak våra nya medarbetare bland unga, nyexaminerade ingenjörer. Hos oss får de under sakkunnig och erfaren ledning applicera sina kunskaper på intressanta och kvalificerade projekt. Detta är en viktig del av en långsiktig plan för tillväxt och succession inom VBK.\n</p>\n<p>\n	För våra kunder, som finns inom såväl privat näringsliv som offentlig förvaltning, har vi uppdrag inom områdena industri- och husbyggnad samt anläggning.\n</p>\n<p>\n	Vår organisation kännetecknas av korta beslutsvägar och hög grad av decentralisering där det ställs stora krav på individens eget ansvarstagande.\n</p>\n<p>\n	Huvudkontoret ligger i Göteborg och vi har ett lokalkontor i Skövde.\n</p>\n<p>\n	Vi är idag uppdelade i sex avdelningar. På vårt Göteborgskontor har vi tre projekteringsavdelningar (BP1, BP2 och BP4) och en projektadministrativ avdelning (PA). På Skövdekontoret har vi en projekteringsavdelning (BP3). Den sjätte avdelningen är vår internadministration (IA) och den är placerad i Göteborg.\n</p>\n<p>\n	Idag består VBK Konsulterande ingenjörer AB av ett drygt nittiotal medarbetare av vilka sju är delägare i företaget. Dessa är: \n	<strong>Jan-Eje Andersson</strong>, <strong>Jan Bergstrand</strong>, <strong>Björn Brunander</strong>, <strong>Ulf Kjellberg</strong> (vd), <strong>Ola Kjellman</strong>, <strong>Peter Norrby</strong> och <strong>Jon Örn</strong>.\n</p>\n<p>\n	VBK är medlem i bransch- och arbetsgivarorganisationen \n	<em>STD Svenska Teknik- och Designföretagen</em> som är en del av <em>ALMEGA Tjänsteförbunden</em>.\n</p>','mektagonen.jpg'),
	(9,2,'Company Presentation',NULL,'company-presentation','<h2>VBK is a Consulting Engineering company in the Constructing Industry. </h2>\n<p>\n	<strong>We are active within:</strong>\n</p>\n<ul>\n	<li><strong>Building Design:</strong> including <em>Structural Design</em>, <em>Building Technology</em> and <em>Building Physics</em>.</li>\n	<li><strong>Project Administration:</strong> consisting of <em>Project Management</em>, <em>Construction Management</em> and <em>Inspections</em>. We also have certification for control according to the <em>Swedish Plan</em> and <em>Building Law (PBL)</em>.</li>\n	<li><strong>Maintenance:</strong> which covers <em>maintenance assessments</em>, <em>building condition assessments (BCA)</em> and <em>damage investigations</em>. We aslo establish <em>maintenance plans</em> and <em>action plans</em>.</li>\n</ul>\n<p>\n	As a way to fulfill our ambition to keep up with the quick technical development in our industry we mainly choose our new employees amongst young, newly graduated engineers. At our firm, they then get to apply their knowledge on interesting and qualified project, under skilled and experienced guidance. This is an important part of a long-term plan for growth and succession within VBK.\n</p>\n<p>\n	Our costumers come from the private industry as well as the public administration, and we have assignments within the areas; Industry Construction, House Construction and Plants.\n</p>\n<p>\n	Our organization is characterized by short decision paths and a high rate of decentralization, were there is high demands on every individual to take their own responsibility.\n</p>\n<p>\n	Our Head Office is placed in Gothenburg, but we also have a Local Office in Skövde.\n</p>\n<p>\n	We are today divided in six departments. In the Gothenburg office there are located three Design departments (BP1, BP2, BP4) and one Project Administration department (PA). In the Skövde office we have one Design department (BP3). The sixth department is our Internal Administration (IA), and that one is placed in Gothenburg too.\n</p>\n<p>\n	VBK konsulterande ingenjörer AB consists today of more then ninety employees, of which seven are partners of the firm. These are: \n	<strong>Jan-Eje Andersson</strong>, <strong>Jan Bergstrand</strong>, <strong>Björn Brunander</strong>, <strong>Ulf Kjellberg</strong> (CEO), <strong>Ola Kjellman</strong>, <strong>Peter Norrby</strong> and <strong>Jon Örn</strong>.\n</p>\n<p>\n	VBK is a member of the Swedish Federation of Consulting Engineers and Architects, which is a member of both FIDIC and EFCA, and part of the ALMEGA Business Service Association.\n</p>','mektagonen.jpg'),
	(10,1,'Historik',NULL,'historik','<p>\n	<strong>Västsvenska Byggkonsult AB startades 1958 av Valter Enocksson, Stig Halvorsson och Olof Wallenås, tidigare verksamma hos Birger Ludvigson Ingenjörsbyrå AB.</strong>\n</p>\n<p>\n	Företaget var till en början inriktat på \n	<strong>byggkonstruktion</strong>, i första hand för anläggningar och tung industri och växte i takt med industriuppbyggnaden i Västsverige. <br>\n	<br>\n	För att motsvara kundernas önskemål, samt dra nytta av de kunskaper och erfarenheter som under åren samlades i det växande företaget, breddades inriktningen mot husbyggnadskonstruktion för såväl privata som offentliga uppdragsgivare. \n	<br>\n	<br>\n	Den ursprungliga koncentrationen mot byggkonstruktion har över tiden vidgats till att även omfatta <strong>projektadministration</strong> samt underhållsrelaterade uppdrag såsom skadeutredningar, underhålls- och konditionsbesiktningar \n	mm.\n</p>\n<p>\n	Genom åren har företaget, företrädesvis med svenska uppdragsgivare, också medverkat i projekt i många andra länder, både inom och utanför Europa.\n</p>\n<p>\n	Namnet \n	<strong>Västsvenska Byggkonsult</strong> är sedan länge ersatt av kortnamnet <strong>VBK</strong> och vårt fullständiga namn är nu <strong>VBK Konsulterande ingenjörer AB</strong>.\n</p>\n<p>\n	<a href=\"/om-vbk/vbk-5-decennier\">Läs mer i boken \"VBK 5 decennier\"</a><br>\n</p>','grundarna.jpg'),
	(10,2,'History',NULL,'history','<p>\n	<strong></strong><strong>Västsvenska Byggkonsult AB was established in 1958 by Valter Enocksson, Stig Halvorsson and Olof Wallenås, all of them earlier active at Birger Ludvigson Ingenjörsbyrå AB.</strong>\n</p>\n<p>\n	The new company was at the beginning focused on \n	<strong>structural design</strong>, mainly for construction industry and heavy industry, and it grew as the industries expanded in the Western region of Sweden.\n</p>\n<p>\n	To meet our customers requirements, and at the same time put to use the knowledge and experience that in the passed years had been collected in the growing company, the focus was widened to include structural design of houses for both private customers and public clients.\n</p>\n<p>\n	The original focus on structural design has over time opened up to also include project administration and projects of maintenance character such as damage investigation, maintenance and conditioning inspections, etc.\n</p>\n<p>\n	Over the years the company has also, mainly with Swedish customers, participated in projects in many other countries, both within and outside of Europe.\n</p>\n<p>\n	Since long, the name \n	<strong>Västsvenska Byggkonsult AB</strong> is replaced with the shorter <strong>VBK</strong> and our full name is now <strong>VBK Konsulterande ingenjörer AB</strong>, which translates to <strong>VBK Consulting Engineers AB</strong>.\n</p>','grundarna.jpg'),
	(11,1,'Vision & affärsidé',NULL,'vision-affarside','<h2>Vision</h2>\n<p class=\"highlight-section\">	VBK skall vara <strong>det självklara valet</strong> för såväl den <strong>medvetne kunden</strong> som den <strong>kreative ingenjören</strong>.\n</p>\n<h2>Värdeord</h2>\n<p>	Värdeorden ger oss vägledning i hur vi skall tänka och agera i vårt förhållande externt mot kund såväl som internt mot övriga medarbetare, likväl som de speglar klimatet på företaget och företagets tradition och själ.\n</p>\n<ul class=\"core-values__words highlight-section\">\n	<li>Stabilitet</li>\n	<li>Ledande</li>\n	<li>Engagemang</li>\n	<li>Kompetens</li>\n	<li>Initiativ</li>\n	<li>Arbetsglädje - ha kul</li>\n	<li>Delaktighet</li>\n	<li>Decentraliserat ansvar</li>\n	<li>Service</li>\n	<li>Tillväxt</li>\n</ul>\n<h2>Affärsidé</h2>\n<p class=\"highlight-section\">	Vi skall skapa ett bestående värde för våra kunder genom <strong>engagerad</strong>, <strong>kvalificerad</strong> och <strong>oberoende</strong> rådgivning inom byggprojektering och projektadministration.\n</p>','compass.jpg'),
	(12,1,'Kvalitet och miljö',NULL,'kvalitet-och-miljo','<p>Kvalitet är förmågan att regelbundet motsvara eller överträffa människors förväntningar. Kvalitet kan systematiseras i dokument som <em>ISO 9000</em> men den kan bara skapas och upplevas av människor. Det är den insikten som formar och utvecklar vårt kvalitetsarbete. Vi anstränger oss för att motsvara och överträffa våra uppdragsgivares förväntningar.\n</p>\n<p>Sedan den 15 april 1999 är vårt kvalitets- och miljöledningssystem certifierat enligt <em>ISO 9001</em> och sedan den 5 december 2001 även enligt <em>ISO 14001</em>.<br>\n</p>\n<h2>Kvalitetspolicy<br></h2>\n<p class=\"highlight-section\">VBK skall uppfylla uttalade och underförstådda krav från kunder och samhället. Genom ständiga förbättringar skall <strong>korrekt</strong>, <strong>kvalificerad</strong> och <strong>oberoende</strong> <strong>rådgivning</strong> säkerställas. Företagets uttalade ambition är att <strong>varje uppdrag skall bli utfört så att det kan tjäna som referens.</strong>\n</p>\n<h2>Miljöpolicy<br></h2>\n<p class=\"highlight-section\">VBK skall genom ständiga förbättringar <strong>värna om en god miljö</strong>, arbeta för en <strong>hållbar utveckling</strong> samt uppfylla samhällets krav och våra kunders förväntningar. Vi skall verka för en <strong>resurssnål</strong>, <strong>kretsloppsanpassad</strong> byggprocess, byggandet av sunda hus samt ge alla medarbetare goda kunskaper om hur vi kan minska miljöbelastningen från vår egen verksamhet.<br>\n</p>','quality-environment-1427105578.jpg'),
	(13,1,'Nyheter',NULL,'nyheter','',''),
	(14,1,'Karriär',NULL,'karriar','',''),
	(15,1,'Framtid och karriär',NULL,'framtid-och-karriar','<h2>Engagemang och arbetsglädje<br>\n</h2>\n<p>\n	Att jobba på VBK innebär <em></em><strong>frihet under ansvar</strong><em></em>. Vi tror på ett decentraliserat ansvar så långt det är möjligt. Detta för att skapa största möjliga <strong>delaktighet</strong> hos våra medarbetare. <strong>Engagemang och initiativkraft</strong> är förmågor som är viktiga att besitta när man börja jobba hos oss, faktiskt viktigare än kunskap. Kunskap och kompetens förvärvar man successivt genom att arbeta på VBK.\n</p>\n<p>\n	Vår strävan är att skapa ett arbetsklimat där <strong>arbetsglädje</strong> värdesätts och där det helt enkelt är tillåtet att ha kul på jobbet. Vi uppmuntrar aktiviteter tillsammans med arbetskamrater vid sidan av jobbet.\n</p>\n<p>\n	Vi väljer till mycket stor del våra nya medarbetare bland unga, nyexaminerade byggtekniskt utbildade ingenjörer. Vi anställer såväl civilingenjörer som högskoleingenjörer. Hos oss får den unge ingenjören under sakkunnig och erfaren ledning applicera sina kunskaper på intressanta och kvalificerade projekt. Detta är en viktig del av en långsiktig plan för tillväxt och succession inom VBK.\n</p>\n<h2>Varje projekt är unikt</h2>\n<p>\n	Det unika med att arbeta i byggbranschen är att varje projekt är just unikt. Inget är det andra likt och det görs ofta bara ett av varje. Under en period kanske du arbetar med en nybyggnad av ett hotell för att några månader senare jobba med en ombyggnad av en bilfabrik. Detta ger en spännande variation i arbetet och tiden från de tidiga skisserna till färdig byggnad är överblickbar, för en större byggnad kanske ett par år.\n</p>\n<p>\n	När man börjar på VBK, oavsett om man kommer direkt från skolan eller har ett antal års arbetslivserfarenhet, får man alltid en <strong>fadder</strong>. Denna fadder ser till så att man så snabbt som möjligt kommer in i de rutiner som gäller på VBK. Faddern är även behjälplig med att svara på frågor och stötta i själva ingenjörsarbetet. Under det första halvåret på VBK får man också gå ett <strong></strong><strong>Introduktionsprogram för nyanställda</strong><em></em>, ett enklare trainee-program som vi utvecklat på VBK.\n</p>\n<p>\n	När man som ung <strong>Ingenjör</strong> börjar arbeta hos oss med byggprojektering har man sin huvudsysselsättning med att antingen CAD-rita med hjälp av moderna projekteringsverktyg eller upprätta tekniska beräkningar, självklart med bästa möjliga datorstöd. Inte sällan blir det dock en mix av dessa två arbetsuppgifter.\n</p>\n<p>\n	De första arbetsuppgifterna inom projektadministration brukar bestå i att biträda våra mer erfarna projektledare, byggledare och besiktningsmän.\n</p>\n<p>\n	Efter ett antal år är man mogen att ta nästa steg i karriären. Man blir då <strong>Handläggare</strong>. Att vara handläggare innebär att man blir spindeln i nätet i ett projekt. Man leder sin grupp i det dagliga arbetet på kontoret, man går på möten där man inhämtar och lämnar information. Man styr projektet kostnads- och tidsmässigt mot uppställda mål. Varje handläggare har i varje uppdrag en uppdragsansvarig som överordnad.\n</p>\n<p>\n	I arbetet som handläggare får man successivt fler och fler kundkontakter. När kundkontakterna är av det antal att man är mer eller mindre självförsörjande på uppdrag och man samtidigt besitter en hög teknisk kompetens är man redo att ta nästa kliv i karriären. Man blir då <strong>Uppdragsledare</strong>. Arbetssituationen för en uppdragsledare påminner mycket om den för en handläggare. Dock finns två väsentliga skillnader. Som uppdragsledare har man ett kontinuerligt ansvar för att upprätthålla och utveckla sina kundrelationer. Man har heller ingen överordnad i dina uppdrag.\n</p>\n<p>\n	När man har utvecklat sina kundrelationer så mycket att man kan sysselsätta ett större antal medarbetare, kanske ett tiotal, och man samtidigt skaffat sig en gedigen fackkunskap är det naturligt att man blir utsedd till <strong>Uppdragsansvarig</strong>. Som uppdragsansvarig är man ytterst ansvarig för ett flertal uppdrag, såväl tekniskt, som ekonomiskt och tidsmässigt. Till sin hjälp har man en handläggare i varje uppdrag.\n</p>\n<p>\n	<strong>Vill du ha mer information om vilka arbetsuppgifter och karriärmöjligheter vi kan erbjuda kontakta: <a href=\"mailto:ulf.k@vbk.se\">Ulf Kjellberg</a>.</strong>\n</p>','framtid.jpg'),
	(15,2,'Futures and Careers',NULL,'futures-and-careers','','framtid.jpg'),
	(16,1,'Kompetensutveckling',NULL,'kompetensutveckling','<h2>Utbildning och kompetensutveckling<br>\n</h2>\n<p>\n	<em></em>Utbildning och kompetensutveckling på VBK sker dels genom de kunskaper vi förvärvar genom medverkan i projekt, dels genom kurs- och seminarieverksamhet och dels genom att utnyttja VBK:s interna kompetensbanker.<em></em>\n</p>\n<p>\n	Vi prioriterar utbildningar där så många som möjligt kan delta. Detta sker främst genom <strong>interna </strong><strong>kurser</strong>, d v s kurser där kursledare i allmänhet utgörs av egna medarbetare. Vi deltar även i <strong>externa </strong><strong>kurser </strong>för att få impulser och idéer från personer utanför kontoret.\n</p>\n<p>\n	<strong></strong>Inom kontoret finns en omfattande<strong> </strong>kunskapsbank som utgörs av <strong>kompetensbanker</strong>. Varje kompetensbank utgörs av några medarbetare med god insikt inom respektive område, och deras uppgift är att säkerställa och utveckla hög teknisk- och projektadministrativ kompetens samt bevaka nyheter, men även redovisa traditionella, goda VBK-lösningar. Kompetensbankerna samlar även in erfarenheter från genomförda projekt och redogör för sitt arbete två gånger per år för hela kontoret vid s k <strong>TEMA-träffar</strong>.\n</p>\n<p>\n	För ingenjörer som i huvudsak arbetar med tekniska beräkningar finns <strong>beräkningsträffar</strong> där frågor av beräkningsnatur analyseras.\n</p>\n<p>\n	<strong>PA-träffar</strong> för medarbetare verksamma inom den projektadministrativa avdelningen ordnas en till två gånger varje månad. Normalt har någon förberedd en kortare information om ett aktuellt ämne till dessa träffar. Efter denna inledande föredragning tar en friare diskussion vid där huvudsyftet är att medarbetarna skall utbyta erfarenheter med varandra. Deltagarna får en möjlighet att ventilera uppkomna problemställningar i pågående uppdrag.\n</p>','forelasning.jpg'),
	(16,2,'Competence',NULL,'competence','','forelasning.jpg'),
	(17,1,'Student',NULL,'student','<h2>Ta kontakt med oss!</h2>\n<p>\n	För dig som just nu \n	<strong>studerar</strong>, eller <strong></strong>precis har avslutat dina studier<strong></strong>, till <strong>civilingenjör</strong> eller <strong>högskoleingenjör</strong> på en teknisk högskola kan det vara värt att ta en kontakt med oss på VBK. I första hand är vi nyfikna på dig med byggteknisk inriktning som har intresse för konstruktion eller projektledning.\n</p>\n<p>\n	Av rent geografiska skäl har vi självklart störst samarbete med <a href=\"http://www.chalmers.se/\">Chalmers Tekniska Högskola</a>, men vi har genom åren även haft kontakt med och anställt studenter från många andra högskolor såsom exempelvis Skövde, Borås, Halmstad och Jönköping. VBK finns varje år representerat på VARM-dagen (Väg- och Vattensektionens arbetsmarknadsdag) som anordnas på Chalmers i november samt emellanåt även på andra högskolors arbetsmarknadsdagar.\n</p>\n<p>\n	<strong></strong>Det finns möjlighet att göra<strong> exjobb </strong>på VBK.<strong></strong> Varje år kan vi ta emot ett par examensarbetare. Vad du bör känna till är att vi inte har ett antal färdiga exjobbsförslag att erbjuda dig. Du får själv komma med idéer som vi sedan kan hjälpa till att förfina. Vi kan erbjuda handledarkompetens inom såväl husbyggnadskonstruktörens som byggprojektledarens normala arbetsfält.\n</p>\n<p>\n	<strong></strong>Varje år tar vi emot <strong>sommarpraktikanter.</strong> Som praktikant söker vi dig som befinner dig inom den senare delen av din byggtekniska utbildning och studerar till civil- eller högskoleingenjör.\n</p>\n<h2>Studiebesök</h2>\n<p>\n	Är du nyfiken på hur en byggkonsult arbetar? Vill du veta lite mer om en byggnadskonstruktörs vardag? Då är du alltid välkommen på ett studiebesök, själv eller tillsammans med några av dina studiekamrater, för att se hur och med vilka verktyg vi arbetar samt se hur vårt kontor ser ut.\n</p>\n<p>\n	Går du på Chalmers finns alltid möjligheten att du redan har stött på eller framöver kommer att stöta på någon VBKare i någon kurs du läser. Då och då arbetar nämligen flera av våra medarbetare som kursassistenter/hjälplärare.\n</p>\n<p>\n	Centrum för Management i Byggsektorn, <strong>CMB</strong>, är Sveriges främsta forum för managementfrågor inom samhällsbyggnadssektorn och drivs av fyra institutioner på Chalmers tillsammans med ett femtiotal av marknadens aktörer, däribland VBK. Vid CMBs olika träffar får man möjlighet att nätverka med ett tvärsnitt av branschen i Västsverige. Såväl privata som offentliga beställare, entreprenörer, arkitekter och tekniska konsulter ingår som huvudmän i CMB. Även arkitekt- och ingenjörsstudenter samt forskare deltar frekvent i CMBs olika aktiviteter. Centrumet bildades 1998 på initiativ av industrin för att ta tillvara och vidareutveckla management inom alla delar av samhällsbyggnadssektorn.\n</p>','student.jpg'),
	(17,2,'Student',NULL,'student','','student.jpg'),
	(18,1,'Lediga tjänster',NULL,'lediga-tjanster','<p>\n	Vi har för närvarande inga lediga tjänster, men är alltid intresserade av att få kontakt med <strong>kreativa och engagerade ingenjörer med byggteknisk bakgrund</strong>. Om detta stämmer in på dig är du välkommen att höra av dig till VBK.\n</p>\n<p>\n	Ta kontakt med <a href=\"mailto:ulf.kjellberg@vbk.se\">Ulf Kjellberg</a>.<br>\n</p>','job.jpg'),
	(18,2,'Vacancies',NULL,'vacancies','<p>\n	VBK IS A CONSULTING ENGINEERING COMPANY IN CONSTRUCTION\n</p>\n<p>\n	VBK are active in:\n</p>\n<p>\n	Byggprojektering including building construction, civil engineering and building physics.\n</p>\n<p>\n	Project Management consisting of project and construction management, construction inspection and control responsible PBL and P Bass.\n</p>\n<p>\n	Maintenance which includes the implementation of maintenance and condition surveys, damage analysis and establishment of action.\n</p>\n<p>\n	Since its inception in 1958, the VBK had the privilege to work on projects that attracts attention, both in western Sweden as the country in general. We see this as a sign of our clients\' appreciation of our knowledge, and confidence in our independence. We believe that consulting firms must be free of outside interests. Therefore VBK owned exclusively by persons who are active in the company. Strong bonds and obligations shall have a consultant to their clients, and no others.\n</p>\n<p>\n	As part of our effort to follow the rapid technological developments in our industry, we select our new employees among young, newly graduated engineers. With us, the under skilled and experienced management to apply their knowledge in interesting and qualified projects. This is an important part of a long term plan for growth and succession in the VBK. A plan that also aims to preserve the traditional role of the consultant.\n</p>\n<p>\n	Our customers are in the private sector and public administration. Our assignments are in the areas of industrial and building construction and facility.\n</p>\n<p>\n	Today the VBK Consulting Engineers Ltd of eighty employees, seven of which are shareholders in the company. These are: Bjorn Brunander, Ulf Kjellberg (CEO), Ola Kjellman, Peter Norrby, Jan-Eje Andersson, Jan Berg Dental and Jon Eagle.\n</p>\n<p>\n	VBK is a member of the business and employers organization STD Swedish Engineering and Design Firms that are part of ALMEGA Tjänsteförbunden.\n</p>\n<p>\n	To support the education of future engineers in our sector is important to us. VBK is among others involved as one of the principals in the network cooperating with Chalmers become the joint CMB, Centre for Management of the Built Environment. VBK is also a member of the Design Centre at Chalmers.\n</p>','job.jpg'),
	(19,1,'Kontakt',NULL,'kontakt','<p>123123\n</p>',''),
	(19,2,'Contact',NULL,'contact','<p>123\n</p>',''),
	(20,1,'Byggprojektering',NULL,'byggprojektering','',''),
	(21,1,'Projektadministration',NULL,'projektadministration','',''),
	(22,1,'Underhåll',NULL,'underhall','',''),
	(23,1,'Projekt',NULL,'projekt','<p>Välj projekttyp för att se referensprojekt av den typen.</p>\n\n<ul class=\"reference-list cf\">\n  <li class=\"reference-item reference-item--empty\">\n    <a href=\"/projekt/byggprojektering\">\n      <div class=\"reference-overlay-text\">\n        <div class=\"vertical-center\">\n          <h3>Byggprojektering</h3>\n        </div>\n      </div>\n    </a>\n  </li>\n  \n  <li class=\"reference-item reference-item--empty\">\n    <a href=\"/projekt/projektadministration\">\n      <div class=\"reference-overlay-text\">\n        <div class=\"vertical-center\">\n          <h3>Projektadministration</h3>\n        </div>\n      </div>\n    </a>\n  </li>\n  \n  <li class=\"reference-item reference-item--empty\">\n    <a href=\"/projekt/underhall\">\n      <div class=\"reference-overlay-text\">\n        <div class=\"vertical-center\">\n          <h3>Underhåll</h3>\n        </div>\n      </div>\n    </a>\n  </li>\n</ul>',''),
	(23,2,'Projects',NULL,'projects','<header class=\"page-head skin\">Information about our projects is only available in Swedish.</header>\n\n<ul class=\"reference-list cf\">\n  <li class=\"reference-item reference-item--empty\">\n    <a href=\"/projekt/byggprojektering\">\n      <div class=\"reference-overlay-text\">\n        <div class=\"vertical-center\">\n          <h3>Construction design</h3>\n        </div>\n      </div>\n    </a>\n  </li>\n  \n  <li class=\"reference-item reference-item--empty\">\n    <a href=\"/projekt/projektadministration\">\n      <div class=\"reference-overlay-text\">\n        <div class=\"vertical-center\">\n          <h3>Project management</h3>\n        </div>\n      </div>\n    </a>\n  </li>\n  \n  <li class=\"reference-item reference-item--empty\">\n    <a href=\"/projekt/underhall\">\n      <div class=\"reference-overlay-text\">\n        <div class=\"vertical-center\">\n          <h3>Maintenance</h3>\n        </div>\n      </div>\n    </a>\n  </li>\n</ul>',''),
	(24,1,'Byggprojektering',NULL,'byggprojektering','',''),
	(24,2,'Construction design',NULL,'construction-design','',''),
	(25,1,'Projektadministration',NULL,'projektadministration','',''),
	(25,2,'Project management',NULL,'project-management','',''),
	(26,1,'Underhåll',NULL,'underhall','',''),
	(26,2,'Maintenance',NULL,'maintenance','',''),
	(27,1,'Information',NULL,'information','<h2>Ett kreativt samarbete</h2>\n<p><strong></strong>Basen för VBK:s verksamhet har allt sedan starten 1958 varit byggprojektering,<strong></strong> innefattande <em><strong></strong></em><strong>byggnadskonstruktion</strong><strong>, byggnadsteknik och byggnadsfysik</strong>.\n</p>\n<p>\n	Vi projekterar husbyggnader, industribyggnader samt anläggningar och arbetar i alla skeden från tidiga utredningar till upprättande av färdiga bygghandlingar. Inget projekt är för litet eller för stort för oss.\n</p>\n<p>\n	Som byggprojektör är vår uppgift att genom ett kreativt samarbete med arkitekt och övriga projektörer skapa en byggnad som på bästa sätt och under många år motsvarar såväl kundens som samhällets krav och förväntningar. Det är viktigt att i projekteringen hitta rätt kombination av känd, beprövad teknik och nya moderna metoder.\n</p>\n<p>\n	<strong>BIM </strong>och<strong> 3D-projektering </strong>är viktiga och självklara ingredienser i en modern projektering.<strong></strong> Vi ligger långt framme inom detta område och nyttjar alltid de senaste projekteringsverktygen. Även på beräkningssidan ser vi till att ha bästa möjliga datorstöd för att kunna optimera våra byggnader och utföra en effektiv projektering.\n</p>\n<p>\n	Hållbarhets- och miljöfrågor står högt på dagordningen när dagens komplexa byggnader skapas. Vi besitter erforderlig kunskap inom dessa områden och framhållas bör särskilt vår spetskompetens inom områdena fukt och energi.\n</p>\n<p>Kontaktpersoner:<a href=\"mailto:ola.kjellman@vbk.se\"><br>Ola Kjellman</a>, <a href=\"mailto:jan.bergstrand@vbk.se\">Jan Bergstrand</a>, <a href=\"mailto:jan-eje.andersson@vbk.se\">Jan-Eje Andersson</a> och <a href=\"mailto:jon.orn@vbk.se\">Jon Örn</a>\n</p>',''),
	(27,2,'Information',NULL,'information','',''),
	(28,1,'Information',NULL,'information','<h2>Från de första idéerna...</h2>\n<p>\n	Redan från de första idéerna och hela vägen till färdig byggnad arbetar våra projektledare i nära samarbete med kunden för att fastlägga och följa upp ramar för projektet avseende funktion, kvalitet, miljö, ekonomi och tider.\n</p>\n<p>\n	<strong></strong>Våra<strong> projektledare </strong>medverkar och råder kunden i valet av organisation och entreprenadform. De arbetar i både utförande- och totalentreprenader och är inte främmande för olika samarbetsformer som exempelvis partnering. Allt utgår ifrån vad som är lämpligast i det enskilda projektet.\n</p>\n<p>\n	De har ansvar för upphandlingar av såväl entreprenörer som konsulter och att projektet genomförs inom uppsatta ramar innefattande även myndighets- och miljökrav.\n</p>\n<p>\n	<strong>Projekteringsledaren</strong> ansvarar under projekteringsfasens olika skeden för att projekteringen utförs enligt uppsatta ramar och når uppsatta mål.\n</p>\n<p>\n	När ett byggprojekt befinner sig i produktionsskedet behövs en noggrann styrning och kontroll så att kontraktets villkor avseende tid, kvalitet, arbetsmiljö och kostnader uppfylls. Detta utförs av <strong>byggledaren.</strong> Med lång erfarenhet och modern teknik i bagaget har VBK möjlighet att utföra detta uppdrag för såväl stora som små projekt och i såväl ny- som om- och tillbyggnadsprojekt.\n</p>\n<p>\n	VBK kan erbjuda <em>projekt</em>- <em>projekterings</em>- och <em>byggledare</em> med rätt kompetens för såväl stora som små projekt inom husbyggnad, industribyggnad och anläggning.\n</p>\n<p>\n	<strong></strong>För att få en opartisk bedömning om entreprenaden är utförd enligt kontraktshandlingar och gällande byggregler utförs en <strong>entreprenadbesiktning.\n	</strong>\n</p>\n<p>\n	Med våra goda tekniska och entreprenadjuridiska kunskaper kan vi garantera att beställaren får det som är avtalat. Vi utför förbesiktningar, slutbesiktningar, efterbesiktningar och garantibesiktningar enligt AB 04, ABT 06 eller ABS 09. VBK kan tillhandahålla certifierade besiktningsmän.\n</p>\n<p>\n	Som en garanti för att byggherren har tillräcklig kunskap och erfarenhet för att genomföra aktuellt byggprojekt behövs en <strong>Kontrollansvarig (KA) </strong><strong>enligt PBL.</strong> Detta kräver byggnadsnämnden senast vid ansökan om bygglov i enlighet med gällande lag. VBK har ett antal certifierade kontrollansvarig PBL. Dessa kan hjälpa till som KA i alla förekommande uppdrag separat eller gärna i kombination med till exempel projekt- och/eller byggledning för uppdraget i fråga.\n</p>\n<p>\n	Den som låter utföra ett byggnads- eller anläggningsarbetet ska redan vid planering och projektering se till att arbetsmiljön särskilt under byggskedet men även under bruksskedet beaktas. För detta skall en <strong>Byggarbetsmiljösamordnare, Bas P,</strong> utses. Även för produktionsskedet måste en samordnare av arbetsmiljön utses, <strong>Bas U.</strong><strong></strong> <strong></strong>\n</p>\n<p>\n	En <strong>certifierad skyddsrumssakkunnig</strong> med kvalificerad behörighet är behörig att kontrollera befintliga skyddsrum, utfärda godkännandeintyg över utförda konstruktionsåtgärder i skyddsrum samt utfärda intyg i samband med produktion av skyddsrum.Vid om- och/eller tillbyggnad i anslutning till skyddsrum skall utredning ske avseende skyddsrummets funktioner. Det gäller även vid nybyggnad av skyddsrum. Detta får bara utföras av en person som är certifierad av MSB-myndigheten.\n</p>\n<p>\n	Kontaktperson: <a href=\"mailto:bjorn.brunander@vbk.se\">Björn Brunander</a>\n</p>',''),
	(28,2,'Information',NULL,'information','',''),
	(29,1,'Information',NULL,'information','<h2>En byggnad och dess kondition förändras löpande över tiden.<br>\n</h2>\n<p>\n	Byggnadens kondition är avgörande för livslängden. Ett bra sätt att få en sakkunnig bedömning av statusen på sin fastighet är att låta utföra en konditionsbesiktning. Vid besiktningen görs en okulär kontroll där byggdelarnas aktuella status noteras. Vid behov utförs också mätningar eller provtagningar med efterföljande analyser. Detta gäller t ex vid problem med fukt eller mikrobiell verksamhet. Ofta kompletteras rapporten med ett kostnadsbedömt åtgärdsförslag.\n</p>\n<p>\n	<strong>Konditionsbesiktningen</strong> kan tillsammans med <strong>mängdberäkning</strong>, <strong>prissättning</strong> och <strong>underhållsintervall</strong> ge de verktyg som behövs för en bra underhållsplanering.\n</p>\n<p>\n	En långsiktig underhållsplanering är nämligen det bästa sättet att förlänga livslängden och bibehålla värdet på en fastighet.\n</p>\n<p>\n	Lokala skador kan vid olyckliga tillfällen ge kraftiga följdverkningar på olika byggnadsdelar. Då detta inträffar bör en utredning utföras för att fastställa skadeorsaken.\n</p>\n<p>\n	Detta utförs genom en skadebesiktning, mätningar och provtagningar. Med hjälp av detta upprättas en skaderapport där analys av mätningar och provtagningar utförs och skadeorsaken fastställs. Därefter framtas även ett åtgärdsförslag och kopplat till detta en bedömd kostnad.\n</p>\n<p>\n	Fukt- och mögelskador i byggnader orsakar skador som kostar stora belopp årligen att åtgärda. Vi har under lång tid skaffat oss mycket god kunskap inom detta område genom att utföra <strong>skadebesiktningar,</strong> kontinuerligt följa kunskapsutvecklingen inom området samt medverkat i att ta fram bra projekteringshjälpmedel.\n</p>\n<p>\n	I en byggnads fasad är fönstren en väsentlig del av fasaden, avseende utseendet och funktionen. Fönstret är en komplicerad byggnadsdel då dess funktion är att skapa kontakt med uteklimatet samtidigt som det ska utestänga detsamma. Vi upprättar med hjälp av besiktning av fönstren ett <strong>renoveringsprogram</strong> avvägt till renoveringsbehovet. Dessutom kan vi komma med råd och anvisningar om möjligheterna att förbättra befintliga fönsters energivärden.\n</p>\n<p>\n	<strong></strong>VBK har stor erfarenhet av <strong>underhålls- och konditionsbesiktningar, upprättande av underhållsplaner, skadebesiktningar samt att föreslå åtgärdsprogram för skadade eller bristfälligt underhållna byggnadsdelar </strong>såsom exempelvis fönster, dörrar, balkonger, fasader och tak. Våra kunder är allt ifrån små bostadsrättsföreningar till stora kommersiella fastighetsägare. Såväl privata som offentliga kunder har använd VBKs kunnande inom underhållsområdet.\n</p>\n<p>Kontaktperson: Björn Brunander<br>\n</p>',''),
	(29,2,'Information',NULL,'information','',''),
	(30,1,'Projekt',NULL,'projekt/byggprojektering','\n',''),
	(30,2,'Projects',NULL,'projects','',''),
	(31,1,'Projekt',NULL,'projekt/projektadministration','',''),
	(31,2,'Projects',NULL,'projects','',''),
	(32,1,'Projekt',NULL,'projekt/underhall','',''),
	(32,2,'Projects',NULL,'projects','',''),
	(33,1,'Göteborg',NULL,'goteborg','<div class=\"contact-information\">	<h3>Vi finns på:</h3>	<p class=\"location\"> 		Falkenbergsgatan 3<br> 		412 85 Göteborg	</p><a href=\"tel:031-703 35 00\">031-703 35 00</a><a href=\"mailto:mail@vbk.se\">mail@vbk.se</a></div><div class=\"hitta-hit highlight\">	<h3>Hitta till VBK</h3>	<p>Vi finns i Mektagonen-huset med ingång Falkenbergsgatan 3. Välj hissar B, plan 6.	</p>	<p>Om du kommer med bil finns besöksparkering med infart från Ebbe Lieberathsgatang.	</p></div>',''),
	(33,2,'Gothenburg',NULL,'gothenburg','<div class=\"contact-information\">	<h3>Our location:</h3>	<p class=\"location\"> 		Falkenbergsgatan 3<br> 		412 85 Gothenburg	</p><a href=\"tel:031-703 35 00\">031-703 35 00</a><a href=\"mailto:mail@vbk.se\">mail@vbk.se</a></div><div class=\"hitta-hit highlight\">	<h3>Find us</h3>	<p>We\'re located in <em>Mektagonen</em>, entrance adress Falkenbergsgatan 3. Use elevator B, plane 6.	</p>	<p>If you arrive by car, use our visitors\' parking from the entrance ramp at Ebbe Lieberathsgatang.	</p></div>',''),
	(34,1,'Skövde',NULL,'skovde','<div class=\"contact-information\"> 	<h3>Vi finns på:</h3> 	<p class=\"location\"> 		Långgatan 13<br> 		541 30 Skövde 	</p> 	<a href=\"tel:0500-44 45 60\">0500-44 45 60</a> 	<a href=\"mailto:mail@vbk.se\">mail@vbk.se</a> </div>',''),
	(34,2,'Skövde',NULL,'skovde','<div class=\"contact-information\"> 	<h3>Our location:</h3> 	<p class=\"location\"> 		Långgatan 13<br> 		541 30 Skövde 	</p> 	<a href=\"tel:0500-44 45 60\">0500-44 45 60</a> 	<a href=\"mailto:mail@vbk.se\">mail@vbk.se</a> </div>',''),
	(35,1,'Leverantörsinformation',NULL,'leverantorsinformation','<p>\n	Vi vill att samtliga fakturor skickas till vårt kontor i Göteborg på följande adress:\n</p>\n<p>\n	<strong>VBK Konsulterande ingenjörer AB<br>\n	Falkenbergsgatan 3<br>\n	412 85 Göteborg</strong>\n</p>\n<hr>\n<p>\n	Vi tar också emot fakturor via e-post på följande adress: <a href=\"mailto:mail@vbk.se\">mail@vbk.se</a>\n</p>\n<p>\n	<strong>Vårt organisationsnummer:</strong> 556608-6103\n</p>',''),
	(35,2,'Information for distributors',NULL,'information-for-distributors','<p>All invoices should be sent to our office in Gothenburg at the following address:\r\n</p>\r\n<p>\r\n	<strong>VBK Konsulterande ingenjörer AB<br>\r\n	Falkenbergsgatan 3<br>\r\n	412 85 Gothenburg</strong>\r\n</p>\r\n<hr>\r\n<p>\r\n	We also accept invoices over e-mail to this e-mail address: <a href=\"mailto:mail@vbk.se\">mail@vbk.se</a>\r\n</p>\r\n<p>\r\n	<strong>Our registration number:</strong> 556608-6103\r\n</p>',''),
	(36,1,'VBK - 5 decennier',NULL,'vbk-5-decennier','<div class=\"flash-book__wrap\">  <embed src=\"/assets/pageflip/pageflip.swf\" bgcolor=\"#ffffff\" loop=\"True\" quality=\"High\" scale=\"showall\" salign=\"left\" wmode=\"Transparent\" height=\"630\" width=\"1100\"></div>','');

/*!40000 ALTER TABLE `pagecontent` ENABLE KEYS */;
UNLOCK TABLES;


# Tabelldump project
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project`;

CREATE TABLE `project` (
  `projectId` int(11) NOT NULL AUTO_INCREMENT,
  `projectType` int(11) NOT NULL,
  `projectCategory` int(11) NOT NULL,
  `projectAdded` date NOT NULL,
  `visibility` int(1) NOT NULL DEFAULT '1',
  `deletedAt` date DEFAULT NULL,
  PRIMARY KEY (`projectId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;

INSERT INTO `project` (`projectId`, `projectType`, `projectCategory`, `projectAdded`, `visibility`, `deletedAt`)
VALUES
	(1,1,8,'2014-08-19',1,NULL),
	(2,1,5,'2014-08-19',1,NULL),
	(3,1,8,'2014-08-19',1,NULL),
	(4,1,2,'2014-08-19',1,NULL),
	(5,1,8,'2014-08-19',1,NULL),
	(6,1,5,'2014-08-19',1,NULL),
	(7,1,3,'2014-08-19',1,NULL),
	(8,1,6,'2014-08-19',1,NULL),
	(9,1,2,'2014-08-19',1,NULL),
	(10,1,8,'2014-08-19',1,NULL),
	(11,1,7,'2014-08-19',1,NULL),
	(12,2,0,'2014-08-19',1,NULL),
	(13,1,8,'2014-08-19',1,NULL),
	(14,1,2,'2014-08-19',1,NULL),
	(15,1,2,'2014-08-19',1,NULL),
	(16,1,8,'2014-08-19',1,NULL),
	(17,1,3,'2014-08-19',1,NULL),
	(18,1,4,'2014-08-19',1,NULL),
	(19,1,7,'2014-08-19',1,NULL),
	(20,1,2,'2014-08-19',1,NULL),
	(21,1,8,'2014-08-19',1,NULL),
	(22,1,5,'2014-08-19',1,NULL),
	(23,1,8,'2014-08-19',1,NULL),
	(24,1,7,'2014-08-19',1,NULL),
	(25,1,2,'2014-08-19',1,NULL),
	(26,1,4,'2014-08-19',1,NULL),
	(27,1,5,'2014-08-19',1,NULL),
	(28,1,3,'2014-08-19',1,NULL),
	(29,1,5,'2014-08-19',1,NULL),
	(30,1,3,'2014-08-19',1,NULL),
	(31,1,3,'2014-08-19',1,NULL),
	(32,1,4,'2014-08-19',1,NULL),
	(33,1,5,'2014-08-19',1,NULL),
	(34,1,1,'2014-08-19',1,NULL),
	(35,1,8,'2014-08-19',1,NULL),
	(36,1,4,'2014-08-19',1,NULL),
	(37,1,4,'2014-08-19',1,NULL),
	(38,1,2,'2014-08-19',1,NULL),
	(39,1,5,'2014-08-19',1,NULL),
	(40,1,5,'2014-08-19',1,NULL),
	(41,1,5,'2014-08-19',1,NULL),
	(42,1,8,'2014-08-19',1,NULL),
	(43,1,8,'2014-08-19',1,NULL),
	(44,1,5,'2014-08-19',1,NULL),
	(45,1,1,'2014-08-19',1,NULL),
	(46,1,7,'2014-08-19',1,NULL),
	(47,1,1,'2014-08-19',1,NULL),
	(48,1,1,'2014-08-19',1,NULL),
	(49,1,6,'2014-08-19',1,NULL),
	(50,1,3,'2014-08-19',1,NULL),
	(51,1,7,'2014-08-19',1,NULL),
	(52,2,0,'2014-08-19',1,NULL),
	(53,2,0,'2014-08-19',1,NULL),
	(54,2,0,'2014-08-19',1,NULL),
	(55,2,0,'2014-08-19',1,NULL),
	(56,2,0,'2014-08-19',1,NULL),
	(57,2,0,'2014-08-19',1,NULL),
	(58,2,0,'2014-08-19',1,NULL),
	(59,2,0,'2014-08-19',1,NULL),
	(60,2,0,'2014-08-19',1,NULL),
	(61,3,0,'2014-08-19',1,NULL),
	(62,3,0,'2014-08-19',1,NULL),
	(63,3,0,'2014-08-19',1,NULL),
	(64,3,0,'2014-08-19',1,NULL),
	(65,3,0,'2014-08-19',1,NULL),
	(66,3,0,'2014-08-19',1,NULL),
	(67,3,0,'2014-08-19',1,NULL),
	(68,1,2,'2015-01-18',1,NULL),
	(69,1,2,'2015-01-18',1,NULL),
	(70,1,3,'2015-01-18',1,NULL),
	(71,1,7,'2015-01-18',1,NULL),
	(72,1,7,'2015-01-18',1,NULL),
	(73,1,1,'2015-01-18',1,NULL),
	(74,1,1,'2015-01-18',1,NULL),
	(75,1,5,'2015-01-18',1,NULL),
	(76,1,5,'2015-01-19',1,NULL),
	(77,1,5,'2015-01-19',1,NULL),
	(78,1,4,'2015-01-19',1,NULL),
	(79,1,4,'2015-01-19',1,NULL),
	(80,1,4,'2015-01-19',1,NULL),
	(81,1,4,'2015-01-19',1,NULL),
	(82,1,6,'2015-01-19',1,NULL),
	(83,1,6,'2015-01-19',1,NULL),
	(84,3,0,'2015-01-19',1,NULL);

/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;


# Tabelldump project_projectcategory
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project_projectcategory`;

CREATE TABLE `project_projectcategory` (
  `projectCategory` int(11) NOT NULL,
  `projectId` int(11) NOT NULL,
  `projectOrder` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `project_projectcategory` WRITE;
/*!40000 ALTER TABLE `project_projectcategory` DISABLE KEYS */;

INSERT INTO `project_projectcategory` (`projectCategory`, `projectId`, `projectOrder`)
VALUES
	(1,34,2),
	(1,45,1),
	(1,47,3),
	(1,48,4),
	(1,73,5),
	(1,74,6),
	(2,4,1),
	(2,9,2),
	(2,14,3),
	(2,15,4),
	(2,25,5),
	(2,38,6),
	(2,68,7),
	(2,69,8),
	(3,31,1),
	(3,17,2),
	(3,28,3),
	(3,30,4),
	(3,7,5),
	(3,70,7),
	(3,50,6),
	(4,18,1),
	(4,26,2),
	(4,32,3),
	(4,36,4),
	(4,78,5),
	(4,79,6),
	(4,80,7),
	(4,81,8),
	(5,2,1),
	(5,29,5),
	(5,33,6),
	(5,39,7),
	(5,40,8),
	(6,8,1),
	(6,49,2),
	(6,82,3),
	(6,83,4),
	(7,11,1),
	(7,19,2),
	(7,24,3),
	(7,46,4),
	(7,71,5),
	(7,72,6),
	(8,1,1),
	(8,3,2),
	(8,13,5),
	(8,16,6),
	(8,21,7),
	(8,23,8),
	(5,6,2),
	(5,77,3),
	(5,75,4),
	(5,76,5),
	(8,43,3),
	(8,10,4);

/*!40000 ALTER TABLE `project_projectcategory` ENABLE KEYS */;
UNLOCK TABLES;


# Tabelldump projectcategory
# ------------------------------------------------------------

DROP TABLE IF EXISTS `projectcategory`;

CREATE TABLE `projectcategory` (
  `projectcategoryId` int(11) NOT NULL AUTO_INCREMENT,
  `projectcategoryName` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `projectcategoryLink` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `projectcategoryImage` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`projectcategoryId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `projectcategory` WRITE;
/*!40000 ALTER TABLE `projectcategory` DISABLE KEYS */;

INSERT INTO `projectcategory` (`projectcategoryId`, `projectcategoryName`, `projectcategoryLink`, `projectcategoryImage`)
VALUES
	(0,'Alla','all',NULL),
	(1,'Industri','industri','shell.jpg'),
	(2,'Bostäder','bostader','SGS-lindholm.jpg'),
	(3,'Hotell','hotell','fotomontage1-copy.jpg'),
	(4,'Skolor','skolor','hogskolanboras.jpg'),
	(5,'Kontor','kontor','SVT-SR.JPG'),
	(6,'Vårdbyggnader','vardbyggnader','vardbyggnad.jpg'),
	(7,'Idrottsanläggningar','idrottsanlaggningar','Ullevi.JPG'),
	(8,'Övrigt','ovrigt','OPERAN.JPG');

/*!40000 ALTER TABLE `projectcategory` ENABLE KEYS */;
UNLOCK TABLES;


# Tabelldump projectcontent
# ------------------------------------------------------------

DROP TABLE IF EXISTS `projectcontent`;

CREATE TABLE `projectcontent` (
  `projectId` int(11) NOT NULL,
  `languageId` int(11) NOT NULL,
  `projectName` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `projectLink` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `projectShortDesc` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `projectLongDesc` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`projectId`,`languageId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `projectcontent` WRITE;
/*!40000 ALTER TABLE `projectcontent` DISABLE KEYS */;

INSERT INTO `projectcontent` (`projectId`, `languageId`, `projectName`, `projectLink`, `projectShortDesc`, `projectLongDesc`)
VALUES
	(1,1,'Vindkraftverk','vindkraftverk','VBK har medverkat till att ta fram och optimera konstruktionen på fundamenten till ca 80 vindkraftverkstorn.','<p> Tornen har varit upp till 130 meter i navhöjd och fundamenten har utformats både som gravitationsfundament och bergsinfästa fundament.\n</p>\n<p>I projektet Korpfjället i Dalarna certifierades även vår konstruktion av DNV (Det Norske Veritas).\n</p>\n<p>\n	<strong>Beställare:</strong> Svevia<br><strong>Konstruktör:</strong> VBK<br>\n	<strong>Ort:</strong> Hallhult, Askersund, Korpfjället, Lemnhult, Fallåsberget samt Glötesvålen<br>\n	<strong>Färdigställt:</strong> 2009-2013\n</p>'),
	(2,1,'Kv Hermod Etapp 1','kv-hermod-etapp-1','Om- och tillbyggnad av Kv Hermod 7.','<p>Tillbyggnaden har en total yta av ca 2000 m2 och innehåller butik, kontor, hotell m.m.\n</p>\n<p>Källarplanet har utökats med nio nya parkeringsplatser. Denna del är helt fristående från tillbyggnaden i övrigt.\n</p>\n<p>\n	<strong>Byggherre:</strong> Rapp Fastigheter AB<br>\n	<strong>Beställare:</strong> NCC<br>\n	<strong>Konstruktör:</strong> VBK<br>\n	<strong>Ort:</strong> Skövde<br>\n	<strong>Färdigställt:</strong> 2012\n</p>'),
	(3,1,'Göteborgs moské','goteborgs-moske','Nybyggnad av moské.','<p>\n	<strong>Beställare:</strong> Saudiarabien / Sveriges Muslimska Stiftelse<br>\n	<strong>Arkitekt:</strong> ArkitektStudion AB / Björn Sahlqvist<br>\n	<strong>Konstruktör:</strong> VBK<br>\n	<strong>Ort:</strong> Göteborg<br>\n	<strong>Färdigställt:</strong> 2011\n</p>'),
	(4,1,'Porslinsfabriken','porslinsfabriken','Porslinsfabriken är ett nytt bostadsområde ute vid Kvillebäcken.','<p> Området består av 14st hus med 4-10 våningar samt ett höghus med 17 våningar \"Fajansen\". Totalt ca 500 lägenheter i området.\n</p>\n<p>\n	<strong>Beställare:</strong> PEAB<br>\n	<strong>Arkitekt:</strong> Arkitektbyrån / Semrén &amp; Månsson<br>\n	<strong>Konstruktör:</strong> VBK<br>\n	<strong>Ort:</strong> Göteborg<br>\n	<strong>Färdigställt:</strong> 2012 / 2013\n</p>'),
	(5,1,'Lisebergshjulet','lisebergshjulet','Fundament för Lisebergshjulet.','<p>Fundament för Lisebergshjulet. Det 60 meter höga hjulet var först placerat vid Lilla Bommen, där det var grundlagt med sulor ovanpå Götatunneln.</p>\n<p>Nu är det placerat med inspända betong- fundament på Liseberget. De 8 fundamenten har vardera ett tvärsnitt på 2,5 x 2,5 meter.</p>\n\n<p>\n  <strong>Beställare:</strong> Liseberg AB<br>\n  <strong>Konsult:</strong> VBK<br>\n  <strong>Ort:</strong> Göteborg<br>\n  <strong>Färdigställt:</strong> 2012\n</p>'),
	(6,1,'Lindholmspiren 3','lindholmspiren-3','Tillbyggnad av Lindholmen Science Park','<p>2003 uppfördes Navet på Lindholmspiren som centrumbyggnad för Lindholmen Science Park.\n</p>\n<p>Projektet Lindholmspiren 3 är en expansion av verksamheten genom sex våningar sammanbyggda och integrerade med Navet via det gemensamma inbyggda gatustråket mellan byggnaderna.\n</p>\n<p>De två bottenvåningarna är avsedda för publika verksamheter som konferens, restaurang, kafé samt Lindholmen Science Park Open Arena på plan 2.\n</p>\n<p>Plan 3 till 6 är avsedd för kontorsverksamhet.\n</p>\n<p>\n	<strong>Byggherre:</strong> NCC<br>\n	<strong>Beställare:</strong> Älvstranden Utveckling AB<br>\n	<strong>Arkitekt:</strong> White arkitekter AB<br>\n	<strong>Konstruktör:</strong> VBK<br>\n	<strong>Färdigställt:</strong> 2010\n</p>'),
	(7,1,'Hotel Post','hotel-post','Centralposthuset vid Drottningtorget har förvandlats till ett förstaklasshotell, Clarion Hotel Post.','<p>Byggnaden är ifrån 1920-talet och 1994 blev byggnaden klassad som statligt byggnadsminne.\n</p>\n<p>Hotellet omfattar ca 34 000 kvm fördelat på ca 550 hotellrum, konferensanläggning, spa-anläggning, exklusiva butiker och restauranger.\n</p>\n<p>\n	<strong>Beställare:</strong> Home Properties / Peab<br>\n	<strong>Arkitekt:</strong> Semrén &amp; Månsson Arkitektkontor<br>\n	<strong>Konstruktör:</strong> VBK<br>\n	<strong>Ort:</strong> Göteborg<br>\n	<strong>Färdigställt:</strong> 2012\n</p>'),
	(8,1,'Rättspsyk','rattspyk','En ny rättspsykiatrisk vårdbyggnad har byggts i Gunnilse innehållande 96 vårdplatser. BTA: 23.300 m2 + 6.800 m2.','<p>\n	<strong>Beställare:</strong> Västfastigheter, distrikt Göteborg<br>\n	<strong>Arkitekt:</strong> White arkitekter AB<br>\n	<strong>Konstruktör:</strong> VBK<br>\n	<strong>Ort:</strong> Göteborg<br>\n	<strong>Färdigställt:</strong> 2013\n</p>'),
	(9,1,'Kv Venus','kv-venus','Nytt bostadskvarter med parkering i källarplan under hela kvartersytan.','<p>På bilden syns hörnhuset som var tänkt att bli Göteborgs första skyskrapa med bostäder. Det stannade vid ett högt hus.\n</p>\n<p>\n	<strong>Beställare:</strong> Poseidon<br>\n	<strong>Arkitekt:</strong> Contekton Arkitekter<br>\n	<strong>Konstruktör:</strong> VBK<br>\n	<strong>Ort:</strong> Göteborg<br>\n	<strong>Färdigställt:</strong> 2012\n</p>'),
	(10,1,'Frölunda Torg - etapp 2','frolunda-torg-etapp-2','Om- och tillbyggnad av Frölunda Torg.','<p> Etappen 2, som VBK medverkat i, omfattade Hus H, en tillbyggnad för handel i två plan, Hus D, en ombyggnad av befintliga handelsytor i samband med tillbyggnaden, samt ett P-hus i 5 plan med plats för ca 760 fordon.\n</p>\n<p>Både Hus H och P-hus är grundlagda via pålar till berg med en längd som varierar mellan 3 m till ca 15 m. Byggnadens stomme består av förtillverkade betongpelare på ett c/c om 7,2 m resp. 16,6 m. Dessa pelare bär sedan infällda stålbalkar inne i huset liksom i fasaden. Den stora spännvidden (16,6 m) gör att HDf-plattor med tjocklek 400 har använts.\n</p>\n<p>Centralt i byggnaden finns en stor elliptisk taklanternin placerad. Denna är konstruktivt buren av stålramar, c/c 3 m, med en spännvidd på ca 14,5 m. Den lilla bilden nedan visar hur lanterninen med sina fönsterpartier i två nivåer kommer att se ut. I översiktsbilden ses P-huset till vänster i bild och Hus H till höger om detta. Bakom syns det höga, befintliga Frölunda Sjukhus.\n</p>\n<p>\n	<strong>Byggherre:</strong> Diligentia AB<br>\n	<strong>Beställare:</strong> Diligentia AB / Skanska Sverige AB<br>\n	<strong></strong><strong>Arkitekt:</strong> Thorbjörnsson+Edgren Arkitektkontor AB<br>\n	<strong>Konstruktör:</strong> VBK<br>\n	<strong>Färdigställt: </strong>2012<br>\n	<strong></strong>\n</p>'),
	(11,1,'Åby Gästpaddock','aby-gastpaddock','Nybyggnad av paddock för ÅbyTravet.','<p><u>Paddock 1:</u>\n</p>\n<p><strong>Plan 1:</strong> Paddock med ca 90 boxplatser, inskrivning, omklädningsrum, stallcafé,  A- och B-tränarfik. Ca 3200m².<br>\n	<strong>Plan 2:</strong> Uteterass på 1500m², klubbrum och totohall. Fläktrum och teknikutrymmen. Samlingslokal för 120 personer samt kiosk och servering. Båda med insyn i paddocken på plan 1.<br>\n	<strong>Plan 3-5:</strong> Banveterinärrum samt 24 stycken loger för Åbytränarna och deras kunder.<br>\n	2 trapphus som fungerar som fundament för de drygt 20m höga belysningsmaster som placeras på dem.<br>\n</p>\n<p><u>Paddock 2:</u>\n</p>\n<p><strong>Plan1:</strong> Paddock med ca 90 boxplatser, provtagningsrum, tvättplatser. Ca 2300m²<br>\n	<strong>Terminal:</strong> I och urlastningsplats för ankommande hästar.\n</p>\n<p>Paddock 1 utförs med en bärande stomme av stål och bjälklag av hdf och plattbärlag. Taket görs som lätt plåttak med undantag av den del som samtidigt utgör publik läktare som utförs som hdf-bjälklag. Trapphusen utförs platsgjutna mht till de krafter som förs ner i dem från belysningsmasterna.\n</p>\n<p>Paddock 2 är en 1-plansbyggnad som utförs med en pelar/balk-stomme av stål med lätt plåttak.\nTerminalen utförs med stålpelare och limträbalkar.\n</p>\n<p>\n	<strong>Byggherre:</strong> ÅbyTravet<br>\n	<strong>Beställare:</strong> NCC<br>\n	<strong></strong><strong>Arkitekt: </strong>Pyramiden<strong><br>Arkitekt (Åby):</strong> Palmeby Arkitekter<strong><br>Konstruktör:</strong> VBK<br><strong>Färdigställt:</strong> 2012<br><br>\n	<strong></strong><strong></strong>\n</p>'),
	(12,1,'Shell - Driftcentrum','shell-driftcentrum','Shell Raffinaderi i Göteborg bygger ett nytt drifcentrum som ersätter det gamla kontrollrummet.','<p> Detta skapar en bättre och säkrare miljö för raffinaderiets driftanställda samt mättar behovet av nya kontorsutrymmen.\n</p>\n<p>Byggnaden uppförs i två plan med en totalyta på ca 2500m2 och våningshöjder på vardera 5m. I bottenplan kommer kontrollrummet med tillhörande ytor för driftpersonalen att lokaliseras. I det övre planet (plan 1) kommer bl.a. konferensrum, kontor samt fläktrum att placeras.\n</p>\n<p>Då hårda säkerhetskrav föreligger kommer byggnaden utföras i betong (bottenplatta, bjälklag och ytterväggar). Ytterväggarna kommer till största del att kläs i isolerade plåtkassetter, typ Paroc, för att få en lättare känsla.\n</p>\n<p>Bygget påbörjades i februari 2009 och var färdigställt våren 2010.\n</p>\n<p>\n	<strong>Byggherre:</strong> Shell Raffinaderi AB<br>\n	<strong>Projekteringsledning:</strong> VBK<br>\n	<strong></strong><strong>Arkitekt:</strong> VBK<br>\n	<strong>Konstruktör:</strong> VBK <br>\n	<strong>KP:</strong> VBK<br>\n	<strong>VVS:</strong> Bengt Dahlgren <br>\n	<strong>El:</strong> Rejlers\n</p>'),
	(13,1,'Eriksbergsdockan','eriksbergsdockan','Nybyggnad av träbryggor, vågbrytare samt inklädnad av kajer.','<p>\n	<strong>Byggherre:</strong> Eriksbergs Förvaltning AB<br>\n	<strong>Beställare:</strong> Älvstranden Utveckling AB<br>\n	<strong>Arkitekt:</strong> Nyréns Arkitektkontor AB<br>\n	<strong>Konstruktör: </strong>VBK<br>\n	<strong>Ort:</strong> Göteborg<br>\n	<strong>Färdigställt:</strong> 2009\n</p>'),
	(14,1,'Eriksberg','eriksberg','Nybyggnad av bostäder längs kajen','<p>\n	<strong>Beställare:</strong> Älvstranden Utveckling AB<br>\n	<strong>Arkitekt:</strong> Arkitekturkompaniet AB<br>\n	<strong>Konstruktör:</strong> VBK<br>\n	<strong>Ort:</strong> Göteborg<br>\n	<strong>Färdigställt:</strong> 1990-talet\n</p>'),
	(15,1,'Växelmyntsgatan','vaxelmyntsgatan','Ombyggnader av bostäder','<p>\n	<strong>Beställare:</strong> Bostads AB Poseidon / Siab / Skanska / GEAB<br><strong>Konstruktör:</strong> VBK<br>\n	<strong>Ort:</strong> Göteborg<br>\n	<strong>Färdigställt:</strong> 1994 - 2000\n</p>'),
	(16,1,'GöteborgsOperan','goteborgsoperan','Nybyggnad av opera, totalt 28.700 m2.','<p>\n	<strong>Byggherre:</strong> Konsortiet Operatörerna Siab / SKANSKA<br>\n	<strong>Arkitekt:</strong> Lund &amp; Valentin arkitekter AB <br>\n	<strong>Konstruktör:</strong> VBK<br>\n	<strong>Färdigställt:</strong> 1994\n</p>'),
	(17,1,'Hotell 11','hotell-11','Nybyggnad av hotell, totalt 26.000 m2','<p>\n	<strong>Beställare:</strong> Eriksbergs Förvaltnings AB<br>\n	<strong>Arkitekt:</strong> White arkitekter AB<br>\n	<strong>Konstruktör:</strong> VBK<br>\n	<strong>Ort:</strong> Eriksberg, Göteborg<br>\n	<strong>Färdigställt:</strong> 1992\n</p>'),
	(18,1,'Högskolan i Borås','hogskolan-i-boras','Nybyggnad av utbildnings-, biblioteks- och kontorslokaler, totalt 15.800 m2','<p>\n	<strong>Beställare:</strong> Akademiska Hus i Göteborg AB<br>\n	<strong>Arkitekt:</strong> Semrén &amp; Månsson Arkitektkontor AB och MA Arkitekter AB<br>\n	<strong>Konstruktör:</strong> VBK<br>\n	<strong>Ort:</strong> Kv Sandgärdet, Borås<br>\n	<strong>Färdigställt:</strong> 2004\n</p>'),
	(19,1,'Falkhallen - Falkenberg','falkhallen-falkenberg','Nybyggnad av evenemangshall som innehåller en multihall, idrottshall, danssal,  teaterundervisning, inspelningsstudio, café, omklädning, foajé, garderober mm.','<p>\n	<strong>Beställare:</strong> Falkenbergs Kommun<br>\n	<strong>Arkitekt:</strong> White arkitekter AB, Halmstad<br>\n	<strong>Konstruktör:</strong> VBK<br>\n	<strong>Ort:</strong> Falkenberg<br>\n	<strong>Färdigställt:</strong> 2010\n</p>'),
	(20,1,'Kv Paradiset - Stockholm','kv-paradiset-stockholm','Kv. Paradiset skall förvandlas till ett volymhandelscentrum.','<p>Kv.Paradiset 29, f.d. Skogaholmsbageriet, beläget på nordvästra Kungsholmen i Stockholm skall förvandlas till ett volymhandelscentrum.</p>\n\n<p>Kv. Paradiset 29 består av två sammanbyggda hus ifrån olika tidsperioder. Industrihuset, 1940-talet i puts samt 1980-talets bageribyggnad med fasad av rasterstruktur och murad sten.</p>\n\n<p>Den nya byggnaden innehåller parkering i de 4 nedre planen, butikslokaler i plan 5-7 samt kontorslokaler i plan 8-11.</p>\n\n<p>Delar av de gamla husen behålls eller byggs om, i princip behålls källarvåningarna (skalet under mark). Rivningsarbetet kommer att vara komplicerat pga de närliggande byggnaderna och gatorna.</p>\n\n<p>De nedre parkeringsvåningarna är under mark men plan 3 är ovan mark och fasaderna är delvis transparanta med ett metallraster framför. Butikslokalerna och entréerna är uppglasade mot Lindhagensgatan, i övrigt utgör fasaderna av mörkgrå betongelement. Kontorslokalerna har långsträckta bandfönster i olika storlekar och övrigt fasadmaterial skall upplevas lätt tex metall.</p>\n\n<p>\n  <strong>Byggherre:</strong> Peab / Fabege<br>\n  <strong>Konsulter:</strong><br>\n  <strong>Arkitekt:</strong> Scheiwiller Svensson Arkitektkontor<br>\n  <strong>Konstruktör:</strong> VBK / Contiga<br>\n  <strong>VVS:</strong> Creanova<br>\n  <strong>El:</strong> Rejlers<br>\n  <strong>Brand:</strong> Brandkonsulten AB<br>\n  <strong>Akustik:</strong> Akustikbyrån<br>\n  <strong>Färdigställt:</strong> 2009\n</p>'),
	(21,1,'Livräddartornet - Tylösand','livraddartornet-tylosand','Nybyggnad av ett lutande betongtorn, 67 graders lutning.','<p>\n	<strong>Beställare:</strong> Kommunala Industristaden<br>\n	<strong>Arkitekt:</strong> White arkitektkontor AB<br>\n	<strong>Konstruktör:</strong> VBK<br>\n	<strong>Ort:</strong> Halmstad<br>\n	<strong>Färdigställt:</strong> 2008\n</p>'),
	(22,1,'NAP - Landvetter','nap-landvetter','Nybyggnad av fraktterminal inkl kontor för TNT, totalt 4.800 m2.','<p>Nybyggnad av fraktterminal inkl kontor för TNT, totalt 4.800m2.</p>\n<p>Nybyggnad av fraktterminal inkl underliggande frakttunnel för Spirit, totalt 5.400 m2.</p>\n<p>\n  <strong>Byggherre:</strong> NAP Nordic Airport Properties AB<br>\n  <strong>Beställare:</strong> PEAB Sverige AB<br>\n  <strong>Arkitekt:</strong> Contecton Arkitekter Fyrstad AB<br>\n  <strong>Vår roll:</strong> Byggnadskonstruktör och KA<br>\n  <strong>Ort:</strong> Göteborg<br>\n  <strong>Färdigställt:</strong> 2008, 2009\n</p>'),
	(23,1,'Mobilia Parkeringshus','mobilia-parkeringshus','Nybyggnad av parkeringshus.','<p>\n	VBK har projekterat den förtillverkade stommen dvs upprättat montagehandlingar, elementritningar för tillverkning mm.<strong></strong>\n</p>\n<p><strong>Beställare:</strong> Strängbetong AB<br>\n	<strong>Arkitekt:</strong> Henrik Jais-Nielsen &amp; Mats White Arkitekter AB<br>\n	<strong>Konstruktör:</strong> VBK<br>\n	<strong>Ort:</strong> Malmö<br>\n	<strong>Färdigställt:</strong> 2011\n</p>'),
	(24,1,'Idrottsparken - Norrköping','idrottsparken-norrkoping','Nybyggnad av idrottsarena','<p>\n	VBK har projekterat den förtillverkade stommen dvs upprättat montagehandlingar, elementritningar för tillverkning mm.<strong></strong>\n</p>\n<p><strong>Beställare:</strong> Strängbetong AB<br>\n	<strong>Arkitekt:</strong> Fredriksson arkitektkontor AB<br>\n	<strong>Konstruktör: </strong>VBK<br>\n	<strong>Ort:</strong> Norrköping<br>\n	<strong>Färdigställt:</strong> 2009\n</p>'),
	(25,1,'Östberget - Uddevalla','ostberget-uddevalla','Veidekke Bostad AB tänker bygga 5 st punkthus i 7 våningar med 28 lägenheter i varje.','<p>Östberget ligger mycket vackert beläget ca 800m från centrala Uddevalla. Detaljplanen medger uppförande av 5 punkthus i 6-7 våningar med en BYA per hus om 400 m2.\n</p>\n<p>Veidekke Bostad AB tänker bygga 5 st punkthus i 7 våningar med 28 lägenheter i varje hus, 1 rum o kokvrå, 2 rum o kök resp. 3 rum o kök. Samtliga lägenheter har stora uteplatser eller balkonger längs hela fasaden.  Lägenhetsförråd, soprum och cykelförråd finns vid parkeringen.\n</p>\n<p>Byggnaden utförs med platta på mark, bärande lägenhetsskiljande väggar av betong (skalväggar), stålstomme i fasad, bjälklag av betong typ plattbärlag.  Väggkonstruktionen består av träreglar med gips på insidan och skivbeklädnad på utsidan. Takkonstruktionen är av trä med ytskikt av papp, låglutande.\n</p>\n<p>\n	<strong>Beställare:</strong> Veidekke Bostad AB<br>\n	<strong>Entreprenör:</strong> Veidekke Entreprenad AB<br>\n	<strong></strong><strong>Arkitekt:</strong> Ditrix AB<br>\n	<strong>Konstruktör:</strong> VBK<br>\n	<strong>VVS:</strong> Rörkonsult i Uddevalla<br>\n	<strong>El:</strong> Elektrokonsult Leif Olsson AB\n</p>'),
	(26,1,'NTC-huset - Falkenberg','hus-12-ntc-huset-falkenberg','Nybyggnad av naturtekniskt centrum för Falkenbergs gymnasium.','<p>En byggnad med mycket miljötänk, innovation och flexibilitet. Utgångskriterierna är passivhus och att det även skall finnas möjlighet att producera egen energi.\n</p>\n<p>För att underlätta kraven för passivhus valdes en tung stomme bestående av en platsgjuten bottenvåning i suterrängnivå samt bjälklag, yttervägger och stomstabiliserande väggar av prefabricerad betong. Huset stabiliseras av ett flertal installationsschakt som även delar av undervisningslokalerna mot korridoren.\n</p>\n<p>Byggnaden kan producera egen energi med hjälp av de solceller som finns placerade på solavskärmningen samt av vindkraftverket placerat på taket.\n</p>\n<p>Byggnaden är anpassad för lätt påbyggnad i en våning.\n</p>\n<p>\n	<strong>Byggherre:</strong> Falkenbergs kommun<br>\n	<strong></strong><strong>Arkitekt:</strong> Liljewall Arkitekter<br>\n	<strong>Konstruktör:</strong> VBK<br>\n	<strong>Färdigställt: </strong>2011<br>\n	<strong>Yta:</strong> ca 3600 m² <br><br>\n</p>'),
	(27,1,'ABB-huset','abb-huset','Nybyggnad för kontors- och verkstadslokaler, totalt 14.000 m2','<p>Nybyggnad för kontors- och verkstadslokaler, totalt 14.000 m2</p>\n<p>\n  <strong>Beställare:</strong> Selmer Bygg i Göteborg AB<br>\n  <strong>Arkitekt:</strong> Archus Arosia Arkitekter AB<br>\n  <strong>Vår roll:</strong> Byggnadskonstruktör<br>\n  <strong>Ort:</strong> Mölndal<br>\n  <strong>Färdigställt:</strong> 1999\n</p>'),
	(28,1,'Avalon Hotel','avalon-hotel','Avalon Hotell är beläget vid Kungsportsplatsen mellan Vallgatan och Södra Larmgatan i centrala Göteborg.','<p> Hotellet har totalt 100 rum, bar, restaurang, vinkällare och konferensrum.\n</p>\n<p>De två källarvåningarna rymmer parkering för 24 bilar. På taket finns en swimmingpool som läckert hänger ut över kanten.\n</p>\n<p>\n	<strong>Byggherre:</strong> Bygg-Göta Göteborg AB<br>\n	<strong>Brukare:</strong> Svenska Stadshotell AB, Borås<br>\n	<strong>Arkitekt:</strong> Semren &amp; Månsson Arkitektkontor <br>\n	<strong>Konstruktör:</strong> VBK\n</p>'),
	(29,1,'Mektagonen','mektagonen','Om- och tillbyggnad av kv Hällungen.','<p>Fastigheten har moderniserats och byggts om, för att möta framtida marknadskrav på moderna, effektiva lokaler.\n</p>\n<p>\"Visionen var ett attraktivt Hällungen som ett landmärke vid entrén till<br> staden och en attraktiv adress för hyresgäster\", Mektagonen.\n</p>\n<ul>\n</ul>\n<p>\n	<strong>Byggherre:</strong> Alecta pensions-försäkring ömsesidigt<br>\n	<strong>Arkitekt:</strong> Krook &amp; Tjäder AB<br>\n	<strong>Konstruktör:</strong> VBK\n	<br><strong>Färdigställande:</strong> 2010<br>\n</p>'),
	(30,1,'Hotell Nötholmen, Strömstad','hotel-notholmen-stromstad','Ett nytt spa- och livsstilshotell har byggts på Nötholmen i Strömstad.','<p>Det nya hotellet i 5-7 plan har totalt 232 rum fördelade på 116 sviter och 116 gästrum, barer, restaurang, konferensrum för 450 personer, spa-avdelning med träningslokaler, simbassäng, inomhuspooler, utomhuspool mm.\n</p>\n<p>Intill hotellet har byggts ett parkeringshus i 3 plan med vidbyggd del med teknikutrymmen. På denna byggnad har 4 st bostadshus i 3-4 plan att byggts.\n</p>\n<p>En fristående byggnad i 5 plan för hotellgäster är också byggd.\n</p>\n<p>Vid sidan av hotellet byggs sjöbodar och parhus i 2 våningar med en ny kanal och badstrand.\n</p>\n<p>\n	<strong>Byggherre:</strong> Nordic Resort AS<br>\n	<strong>Brukare:</strong> Choice Hotels Scandinavia<br>\n	<strong>Arkitekt:</strong>  Krook &amp; Tjäder AB och Halvorsen &amp; Reine A/S<br>\n	<strong>Konstruktör:</strong> VBK\n</p>'),
	(31,1,'Hotell Opalen','hotel-opalen','Scandic Opalen kännetecknas av sin karakteristiska bågformade huskropp.','<p>Scandic Opalen är beläget i korsningen av Engelbrektsgatan och Skånegatan, mittemot Bergakungens salar, och kännetecknas av sin karakteristiska bågformade huskropp. Hotellet byggdes i början på 60-talet och har i dagsläget 242 rum, samt ett ansenligt antal konferensrum. Totalt reser sig byggnaden 11 våningar ovan mark, varav två är konferens- och restaurangplan, åtta våningar hotellrum och överst finns en installationsvåning.\n</p>\n<p>Den ökande efterfrågan på hotellrum i Göteborg har föranlett projektet, vars resultat kommer att vara en påbyggnad av hotellet med 5 hotellvåningar med totalt 132 nya rum, plus en tillbyggnad av befintlig installationsvåning. Påbyggnaden kommer att följa den befintliga huskroppens form, men med en modernare fasadbeklädnad i glas.\n</p>\n<p>Det befintliga huset är utfört av platsgjuten betong, medan den nya stommen kommer att bestå av en betydligt lättare stålstomme med håldäcksbjälklag. Den extra tyngden från de nya våningarna och den högre höjdens större påverkan från vindlaster kräver förstärkningsarbeten utefter befintlig byggnads gavlar samt viss extra pålning.\n</p>\n<p>\n	<strong>Byggherre:</strong> KB Opalus<br>\n	<strong>Brukare:</strong> Scandic<br>\n	<strong>Arkitekt:</strong> Arkitekterna Krook &amp; Tjäder<br>\n	<strong>Konstruktör:</strong> VBK\n</p>'),
	(32,1,'Pedagogen','pedagogen','Ny- och ombyggnad av lärarhögskola för Göteborgs Universitet i centrala Göteborg.','<p>De gamla och kulturhistoriskt värdefulla byggnaderna Sociala Huset och Gamla Latin byggdes om och kompletteras med nya byggnader.\n</p>\n<p>Det ena är ett hus för undervisning i fem plan.\n</p>\n<p>Under denna byggnad finns ett tvåvånings underjordiskt garage. De 200 parkeringsplatserna med in- och utfart på Sahlgrensgatan är lättillgängliga för allmänheten.\n</p>\n<p>Det andra nytillskottet är en tillbyggnad på norra sidan av Sociala Huset. Glasbyggnaden med sin inbjudande entré innehåller utbildningsvetenskapliga fakultetens gemensamma delar: café, administration och studerandeservice.\n</p>\n<p>I de här kvarteren finns under mark rester av Göteborgs tidiga historia. En del av den gamla befästningsmuren Carolus Dux är framtagen och synlig för allmänheten från garagets bottenplan.\n</p>\n<p>\n	<strong>Beställare:</strong> Higabgruppen<br>\n	<strong>Arkitekt:</strong> Nyréns Arkitektkontor, Stockholm3 x Nielsen A/S, Århus, Danmark, Arkitektlaget, Göteborg\n	<strong><br>Konstruktör:</strong> VBK\n	<br><strong>Färdigställt:</strong> 2006<br>\n</p>'),
	(33,1,'SVT/SR-huset','svt-rs-huset','','<p>Hasselblad uppförde ursprungligen denna byggnad i Lundbystrand på Hisingen.<br>\n</p>\n<p>Byggnaden har byggts om för SVT, SR och UR.\n</p>\n<p>Två stora studios, en för SVT och en för SR, innebar omfattande ingrepp i befintlig byggnad.\n</p>\n<p>\n	<strong>Byggherre:</strong> SRF och Älvstranden Utveckling<br>\n	<strong>Arkitekt:</strong> Krook &amp; Tjäder AB <br>\n	<strong>Konstruktör:</strong> VBK\n	<br><strong>Färdigställt för Hasselblad:</strong> 2002<br><strong>Färdigställt för SVT/SR:</strong> 2007<br><br>\n</p>'),
	(34,1,'Elanders Infomediahus','elanders-infomediahus','Nybyggnad av kontor och tryckerilokaler, totalt 18 000m²','<br>\n<p>\n	<strong>Brukare:</strong> Elanders<br>\n	<strong>Arkitekt:</strong> Arkotek Arkitekter AB<br>\n	<strong>Konstruktör:</strong> VBK<br>\n	<strong>Färdigställt:</strong> 2004\n</p>'),
	(35,1,'Stena Line','stena-line','Danmarksterminalen - anpassningar av ramper, matargångar, landgångar för HSS- och RoPax-färjor samt gångbro till P-hus.','<p>Danmarksterminalen - anpassningar av ramper, matargångar, landgångar för HSS- och RoPax-färjor samt gångbro till P-hus.</p>\n<p>\n  <strong>Beställare:</strong> Stena Line Scandinavia AB<br>\n  <strong>Konstruktör:</strong> VBK<br>\n  <strong>Färdigställt:</strong> 1997, 2005\n</p>'),
	(36,1,'Chalmers Matematiska Vetenskaper','chalmers-matematiska-vetenskaper','Om-, till- och påbyggnad av gamla skeppsbyggnad och fysik, totalt ca 8000 m2.','<p>\n	<strong>Byggherre:</strong> Akademiska Hus i Göteborg AB<br>\n	<strong>Arkitekt:</strong> Sweco FFNS<br>\n	<strong>Konstruktör:</strong> VBK<br>\n	<strong>Färdigställt:</strong> 2006\n</p>'),
	(37,1,'Chalmers Mikroelektronik','chalmers-mikroelektronik','Tillbyggnad för institutioner för mikroelektronik, totalt 23.000 m2.','<p>Tillbyggnad för institutioner för mikroelektronik, totalt 23.000 m2.</p>\n\n<p>\n  <strong>Beställare:</strong> Akademiska Hus i Göteborg AB<br>\n  <strong>Arkitekt:</strong> EFS Arkitekter AB<br>\n  <strong>Konstruktör:</strong> VBK<br>\n  <strong>Färdigställt:</strong> 2000\n</p>'),
	(38,1,'SGS Lindholmsallén','sgs-lidaholmsallen','Nybyggnad av 385 st studentlägenheter, 1 rum o kokvrå, 2 rum o kokvrå resp. 3 rum o kök.','<p>\n	<strong>Byggherre:</strong> SGS Studentbostäder<br>\n	<strong>Arkitekt:</strong> Arkitektbyrån AB<br>\n	<strong>Konstruktör:</strong> VBK<br>\n	<strong>Färdigställt:</strong> 2008\n</p>'),
	(39,1,'M2 Lundbystrand','m2-lundbystrand','Ombyggnad av Götaverkens maskinhall, M2, till parkeringshus,\n\nkontor och lätt industri, totalt ca 32.000 m2.','<p>\n	<strong>Byggherre:</strong> Älvstranden Utveckling AB<br>\n	<strong>Arkitekt:</strong> White Arkitekter AB<br>\n	<strong>Konstruktör:</strong> VBK<br>\n	<strong>Färdigställt:</strong> 2006\n</p>'),
	(40,1,'Ericsson, Lindholmspiren','ericsson-lindholmspiren','Nybyggnad av kontor, totalt 52.000 m2.','<p>\n	<strong>Byggherre:</strong> SKANSKA Sverige AB<br>\n	<strong>Arkitekt:</strong> Liljewall arkitekter ab<br>\n	<strong>Konstruktör:</strong> VBK<br>\n	<strong>Färdigställt:</strong> 2002\n</p>'),
	(41,1,'Ericsson - EMW','ericsson-emw','Nybyggnad av kontors- och produktionslokaler, totalt 40.000 m2.','<p>\n	Nybyggnad av kontors- och produktionslokaler, totalt 40.000 m2.\n</p>\n<p>\n	<strong>Byggherre:</strong> Tuve Bygg AB<br>\n	<strong>Arkitekt:</strong> Arkitekterna Krook &amp; Tjäder AB<br>\n	<strong>Konstruktör:</strong> VBK<br>\n	<strong>Färdigställt:</strong> 2000/2001\n</p>'),
	(42,1,'Svenska Mässan','svenska-massan','Bland annat A-hallen och Kongresshallen.','<p>Bland annat A-hallen och Kongresshallen.</p>\n<p>\n  <strong>Byggherre:</strong> Svenska Mässan<br>\n  <strong>Arkitekt:</strong> -<br>\n  <strong>Konstruktör:</strong> VBK\n</p>'),
	(43,1,'Liseberg','liseberg','Nöjesparken Liseberg bygger ständigt om, köper nya attraktioner, flyttar gamla attraktioner eller hittar på något annat som roar oss.','<p>VBKs uppdrag har varit allt från enkel husbyggnadsprojektering till avancerad anläggningsprojektering som Kållerado, Balder, Kanonen, Spaceport,  Helix, Kaninlandet, Göteborgshjulet mm.\n</p>\n<p>\n	<strong>Byggherre:</strong> Liseberg AB<br>\n	<strong>Konstruktör:</strong> VBK<br>\n	<strong>Färdigställt:</strong> 1995 -\n</p>'),
	(44,1,'Biotech-huset','bio-tech-huset','Nybyggnad av kontors- och lablokaler, bl.a. för nobelpristagaren Arvid Carlssons forskning. Total yta 16.000 m2.','<p>Nybyggnad av kontors- och lablokaler, bl.a. för nobelpristagaren Arvid Carlssons forskning. Total yta 16.000 m2.</p>\n\n<p>\n  <strong>Byggherre:</strong> Higabgruppen<br>\n  <strong>Arkitekt:</strong> Liljewall arkitekter ab<br>\n  <strong>Konstruktör:</strong> VBK<br>\n  <strong>Färdigställt:</strong> 2003\n</p>'),
	(45,1,'St1','st1','VBK har medverkat vid projektering och projekt- / byggledning av de flesta utbyggnader av raffinaderiet sedan 1958.','<p>\n	<strong>Byggherre:</strong> Shell Raffinaderi AB<br>\n	<strong>Konstruktör, projekt- och byggledare:</strong> VBK<br>\n	<strong>Färdigställt:</strong> 1958 -\n</p>'),
	(46,1,'Ullevi - ny läktare','ullevi-ny-laktare','Nybyggnad av läktare för ca 9300 sittplatser.','<p>\n	<strong>Byggherre:</strong> PEAB<br>\n	<strong>Brukare:</strong> Got Event AB<br>\n	<strong>Arkitekt:</strong> Sten Samuelsson - Curt Lignell<br>\n	<strong>Konstruktör:</strong> VBK<br>\n	<strong>Färdigställt:</strong> 1995\n</p>'),
	(47,1,'Göteborg Energi','goteborg-energi','VBK har medverkat vid om- till- och nybyggnader av ett flertal anläggningar för Göteborg Energi.','<p> Bland annat vid Rosenlunds-, och Sävenäsverket samt Rya Kraftvärmeverk och Hetvattencentral.\n</p>\n<p>\n	<strong>Byggherre:</strong> Göteborg Energi AB<br>\n	<strong>Arkitekt:</strong> <br>\n	<strong>Konstruktör:</strong> VBK\n</p>'),
	(48,1,'Volvo Lundby, EDL','volvo-lundby-edl','Nybyggnation av motorlaboratorium, totalt 10 000m².','<p>\n	<strong>Byggherre:</strong> Volvo Lastvagnar AB<br>\n	<strong>Arkitekt:</strong> Arkitekt Gunnar Werner HB och Arkotek Arkitekter AB<br>\n	<strong>Konstruktör:</strong> VBK\n</p>'),
	(49,1,'Sahlgrenska, vårdbyggnad','sahlgrenska-vardbyggnad','Uppförande av ny vårdbyggnad som är placerad vid Bruna stråket och som öppnar sig med sina flyglar mot Botaniska Trädgården.','<p>Det ökande behovet av ytterligare vårdplatser inom regionen har legat till grund för uppförandet av en ny vårdbyggnad, fördelat på källarplan och 6 våningsplan, fläktrumsplan samt hissmaskinrum i plan 8.\n</p>\n<p>Källarvåningen inrymmer teknikutrymmen, omklädningsrum, förråd mm. I objektet ingår även en ny kulvertförbindelse, samt förbindelsegång i 2 plan norr om byggnaden.\n</p>\n<p>Huvudfunktionen är primärt tänkt som vårdlokaler för både slutenvård och dagvård, om totalt 336 vårdplatser inom Medicin, Allmänkirurgi, Transplantationscentrum, Njurmediciniskt centrum och Hematologi.\n</p>\n<p>Varje våningsplan är planerat för två vårdavdelningar. Varje vårdavdelning består av 28 vårdplatser i fyra grupper, ett 3-patientrum, ett 2-patientrum och två 1-patientrum. Alla 1-patientrum kan enkelt förändras till isoleringsrum.\n</p>\n<p>Det finns också lokaler för sjukgymnastik, arbetsterapi samt för undervisning.\n</p>\n<ul>\n</ul>\n<p>\n	<strong>Byggherre:</strong> Västfastigheter, distrikt Göteborg<br>\n	<strong>Beställare:</strong> Peab Sverige AB<br>\n	<strong>Arkitekt:</strong> Abako Arkitektkontor AB<br>\n	<strong>Konstruktör:</strong> VBK<br>\n	<strong>Färdigställt:</strong> 2009\n	<br><strong>Yta:</strong> BTA 27000 kvm<br>\n</p>'),
	(50,1,'Scandic Hotell','scandic-hotel','Nybyggnad av hotell, totalt 9.000 m2','<p>\n	<strong>Beställare:</strong> Scandic Hotel<br>\n	<strong>Arkitekt:</strong> Lund &amp; Valentin arkitekter AB<br>\n	<strong>Konstruktör:</strong> VBK<br>\n	<strong>Ort:</strong> Mölndal<br>\n	<strong>Färdigställt:</strong> 2001\n</p>'),
	(51,1,'Torslanda Brandstation','torslanda-brandstation','Nybyggnad av brandstation och idrottshall, totalt 2.700 m2','<p>Nybyggnad av brandstation och idrottshall, totalt 2.700 m2</p>\n\n<p>\n  <strong>Beställare:</strong> Göteborgs Stad, Fastighetskontoret<br>\n  <strong>Arkitekt:</strong> Lund & Valentin arkitekter AB<br>\n  <strong>Konstruktör:</strong> VBK<br>\n  <strong>Ort:</strong> Torslanda, Göteborg<br>\n  <strong>Färdigställt:</strong> 2005\n</p>'),
	(52,1,'Kajer - broar','kajer-broar','Nybyggnad av kajer','<p>\n	<strong>Beställare:</strong> Norra Älvstranden Utveckling AB<br>\n	<strong>Byggledning och besiktning: </strong>VBK<strong><br>Konstruktör: </strong>VBK<br><strong>Ort:</strong> Lundby-, Lindholms- och Sannegårdshamnen<br>\n	<strong>Färdigställt:</strong> 2003-2005\n</p>'),
	(54,1,'SCA Hygiene Products - Ryssland','sca-hygiene-products-ryssland','Nybyggnad av fabrik för personliga hygienprodukter','<p>\n	<strong>Beställare:</strong> SCA Hygiene Products<br><strong>Projektledning och teknisk kontroll</strong>: VBK<br>\n	<strong>Ort:</strong> Veniov - Ryssland<br>\n	<strong>Färdigställt:</strong> 2011<br>\n	<strong></strong> <br>\n</p>'),
	(55,1,'SCA Hygiene Products - Polen','sca-hygiene-products-polen','Färdigvarulager har byggts i tidigare etapper och omfattar idag 16 200 m2.','<p>SCA Hygiene Products fabrik i Olawa, Polen har sedan år 2005 byggts till och om i flera etapper. I fabriken tillverkas det blöjor. Under år 2009 / 2010 kommer nuvarande projekt att slutföras.\n</p>\n<p>Projektet omfattar tillbyggnader av:\n</p>\n<ul>\n	<li>Produktion  10 400 m2</li>\n	<li>Kontor / omklädningsrum  1 200 m2</li>\n	<li>Råvarulager  5 000 m2</li>\n	<li>ombyggnad av bef kontorsdelar 1 400 m2</li>\n</ul>\n<p>Färdigvarulager har byggts i tidigare etapper och omfattar idag 16 200 m2. Totala byggnadsytan kommer då att vara ca 51 000 m2\n</p>\n<p>Byggarbetena drivs på entreprenad med polska konstruktörer, byggledare och entreprenörer.\n</p>\n<p>VBKs roll är projektledning innefattande upphandling, teknisk och ekonomisk kontroll. För installationskontroll biträdes vi av Bengt Dahlgren AB och Rejlers Ingenjörer AB.\n</p>\n<p>Parallellt med detta projekt deltager vi arbetet med framtagande av Masterplan för eventuella framtida utbyggnader.\n</p>'),
	(56,1,'SGS Kronhusgatan','sgs-kronhusgatna','Nybyggnad av 56 studentlägenheter, totalt 3.700 m2.','<p>\n	<strong>Byggherre:</strong> SGS Studenbostäder<br>\n	<strong>Arkitekt:</strong> Semren &amp; Månsson Arkitektkontor<br>\n	<strong>Projekt- och byggledare, </strong><strong>Kvalitetsansvarig PBL:</strong> VBK<br>\n	<strong>Färdigställt:</strong> 2004\n</p>'),
	(57,1,'Hornbach','hornbach','Hornbach är ett familjeägt företag nu i 5:e generationen, som efter att ha etablerat överallt i hemlandet Tyskland numera etablerar varuhus enligt sitt koncept inom hela EU.','<p>Totalt finns 122 Hornbachvaruhus i Europa. Det blir några fler varje år. I Sverige finns 3 varuhus byggda och fler är planerade.\n</p>\n<p>Göteborg blev den första orten att få ett varuhus här. Sedan kom Malmö och det senaste tillskottet finns i Botkyrka i södra Stockholm, som öppnades i juni 2008.\n</p>\n<p>Ett Hornbachvaruhus består av tre huvuddelar; byggmarknad, drive in (för grovbyggvaror) och trädgårdsmarknad. Sedan finns naturligtvis även personaldel, in- och utlastning, parkerings-anläggning mm.\n</p>\n<p>Varuhusen byggs likartat med typiska igenkänningstecken som kulör och skyltar. Anpassning sker till tomten i varje enskilt fall. Exempelvis kan parkering av personbilar förläggas till taket. Det utförandet finns bland annat i Malmö och München.\n</p>\n<p>\n	<strong>Byggherre:</strong> Hornbach<br>\n	<strong>Totalentreprenör:</strong> W.Markgraf GmbH &amp; Co KG Bauunternehmung från Bayern i Tyskland\n</p>\n<p>VBK:s roll vid byggandet av varuhuset i Göteborg har varit på entreprenörsidan. Ett uppdrag med VBK-insats under mer än fem år.\n</p>'),
	(58,1,'Boviva','boviva','Nybyggnad av lägenheter med utökad service och trygghet.','<h3>Boviva Göteborg, ett stenkast från Eriksbergskranen</h3>\n<p>\n	<strong>Projektledning:</strong> VBK<br>\n	<strong>Arkitekter:</strong> Fredblads arkitekter\n</p>\n<h3>Boviva Stockholm, nära vattnet i Norra Hammarbyhamnen</h3>\n<p>\n	<strong>Projektledning:</strong> VBK<br>\n	<strong>Arkitekter:</strong> Stenberg Lindberg arkitekter\n</p>\n<p>SEB Tryggliv har utformat ett boendekoncept för sina sparare under namnet BoViva.\n</p>\n<p>BoViva vänder sig till de över 55 år som vill ha ett bekvämt boende med gemenskap. Därför finns det t ex aktivitetsrum, bibliotekshörna, IT-rum, matsal och relaxavdelning, där hyresgästerna har möjlighet att träffa och umgås med sina grannar.\n</p>\n<p>I husets lobby arbetar en värdinna som arrangerar utflykter och aktiviteter samt hjälper hyresgästerna med olika saker.\n</p>\n<p>Boviva har uppförts i Göteborg, Stockholm och Halmstad.\n</p>'),
	(59,1,'GREFAB, Björlandakile','refab-bjrolandakile','Småbåtshamnar.','<p>\n	<strong>Byggherre:</strong> GREFAB<br>\n	<strong>Projektledare:</strong> VBK\n</p>'),
	(61,1,'Brännögatan - Bostads AB Poseidon','brannogatan-bostads-ab-poseidon','Upprättat program för fasadrenovering och underhållsåtgärder yttertak.','<p>Teknisk och ekonomisk kontroll under produktionsskedet.\n</p>\n<p>\n	<strong>Beställare:</strong> Bostads AB Poseidon<br>\n	<strong></strong> <strong>Projektering, byggledning och besiktning:</strong> VBK<br>\n	<strong>Ort:</strong> Göteborg<br>\n	<strong>Färdigställt:</strong> 2008\n</p>'),
	(62,1,'Hagabadet','hagabadet','Upprättande av renoveringsprogram för fönster, invändiga glaspartier och innerdörrar.','<p>\n	Under produktionstiden teknisk kontroll av fönsterrenovering.<br><strong></strong>\n</p>\n<p><strong>Beställare:</strong> Kigab<br>\n	<strong></strong> <strong>Projektering:</strong> VBK<br>\n	<strong>Ort:</strong> Göteborg<br>\n	<strong>Färdigställt:</strong> 1997\n</p>'),
	(63,1,'BRF Vite Knut - Göteborg','brf-vite-knut-goteborg','Upprättat program för trapphus- och balkongupprustning.','<p>\n	Teknisk och ekonomisk kontroll under produktionsskedet.<br><strong></strong>\n</p>\n<p><strong>Beställare:</strong> Brf Vite Knut<br>\n	<strong></strong><strong>Projektering och byggledning:</strong> VBK<br>\n	<strong>Ort:</strong> Göteborg<br>\n	<strong>Färdigställt:</strong> 2007\n</p>\n<p><br>\n</p>'),
	(64,1,'BRF Vasaparken 2, Göteborg','brf-vasaparken-2-goteborg','In- och utvändig fastighetsbesiktning samt upprättande av underhållsplan.','<p><strong>Beställare: </strong>Brf Vasaparken 2<strong><br>Besiktning och underhåll: </strong>VBK<strong><br></strong><strong><br></strong><br>\n	<br>\n</p>'),
	(65,1,'Kyrkor','kyrkor','Statusinventering av klimatskärmen med budgeterande åtgärdsförslag.','<p>\n	Upprättande av restaureringsprogram samt byggledning och kontroll under produktionsskedet.<br><strong></strong>\n</p>\n<p><strong>Beställare:</strong> Mölndals Kyrkonämnd / Svenska Kyrkan i Göteborg<br>\n	<strong></strong><strong>Projekt- och byggledare:</strong> VBK<br>\n	<strong>Ort:</strong> Mölndal / Göteborg<br>\n	<strong>Färdigställt:</strong> Fortlöpande\n</p>'),
	(66,1,'BRF Göteborgshus nr 34, Göteborg','brf-goteborgshuset-nr-34-goteborg','Upprättat program för fasad- och balkongupprustning.','<p>\n	<strong></strong>Teknisk och ekonomisk kontroll under produktionsskedet.<strong></strong>\n</p>\n<p><strong>Beställare:</strong> Brf Göteborgshus 34<strong></strong><strong></strong><strong><br>Projektering och Byggledning:</strong> VBK<br>\n	<br>\n</p>'),
	(67,1,'BRF Göteborgshus nr 31, Göteborg','brf-goteborgshus-nr-31-goteborg','Upprättat program för balkongupprustning.','<p>\n	Teknisk och ekonomisk kontroll under produktionsskedet.<br><strong></strong>\n</p>\n<p><strong></strong><strong>Beställare:</strong> Brf Göteborgshus 31<strong><br>Projektering, upphandling och Byggledning:</strong> VBK<br>\n	<br>\n</p>\n<p><br>\n</p>'),
	(68,1,'Rönnängs Brygga - Tjörn','ronnangs-brygga-tjorn','Varvsområdet V & T Löfbergs slip och båtvarv köptes av Saltholmsgruppen, som nu har byggt bostäder på området.','<p>Totalt har här byggts 2 st 4-våningshus med lägenheter (2-, 3- och 4 rok), 8 st 2-våningshus (115 resp 135 m2), 3 st 2-våningshus med lägenheter samt lite förrådsbyggnader garage, sophus och bryggor. Alla hus står på ett pålat betongdäck.\n</p>\n<p>Totalt är det ca 34 st lägenheter och 14 st hus.\n</p>\n<p><strong>Byggherre:</strong> Saltholmsgruppen Väst AB <strong><br>Beställare:</strong> Hansson &amp; Söner Entreprenad AB <strong><br>Arkitekt:</strong> Mats &amp; Arne Arkitektkontor AB och Arkitektbyrån Design Olsson Lyckefors AB <br><strong>Konstruktör:</strong> VBK<br><strong>Ort:</strong> Rönnäng <br><strong>Färdigställt:</strong> 2014<br>\n</p>'),
	(69,1,'Brf Cyklisten, Kvillebäcken','brf-cyklisten-kvillebacken','Bakom Backaplan rinner Kvillebäcken och i detta område bygger sju olika bolag en helt ny stadsdel med bostadshus.','<p>\n	<strong></strong>En ny stadsdel byggs i Kvillebäcken på Hisingen.  Veidekke Bostad byggde här i en första etapp bostadsrättsföreningen \"Cyklisten\" som innehåller ett hus med 75 lägenheter och tillhörande garage under gård och intilliggande hus.<strong><br></strong>\n</p>\n<p><strong></strong><strong>Byggherre: </strong>Veidekke Bostad<br>\n	<strong>Beställare:</strong> Veidekke Entreprenad, Region Väst<br>\n	<strong>Arkitekt:</strong> Semrén &amp; Månsson<br><strong>Konstruktör: </strong>VBK<br>\n	<strong>Ort:</strong> Göteborg<br>\n	<strong>Färdigställt:</strong> 2012\n</p>\n<p><br>\n</p>\n<p><br>\n</p>'),
	(70,1,'Gothia Triple Towers','gothia-triple-towers','Ny- och påbyggnad av hotell med ca 500 rum.','<p>\n	<strong>Beställare:</strong> Peab<br>\n	<strong>Byggherre:</strong>  Svenska Mässan Stiftelse<br>\n	<strong>Arkitekt:</strong> White arkitekter AB<br><strong>Konstruktör: </strong>VBK<strong></strong><br>\n	<strong>Ort:</strong>  Göteborg<br>\n	<strong>Färdigställt:</strong> 2015\n</p>\n<p><br>\n</p>'),
	(71,1,'Stadium Arena - Norrköping','stadium-arena-norrkoping','Nybyggnad av inomhusarena.','<p>\n	<strong></strong>VBK har projekterat den förtillverkade stommen i betong och stål dvs. upprättat montagehandlingar, elementritningar för tillverkning mm.<strong></strong>\n</p>\n<p><strong>Beställare:</strong> Strängbetong AB<br>\n	<strong>Arkitekt:</strong> ÅWL Arkitekter AB<br><strong>Konstruktör:</strong> VBK<br>\n	<strong>Ort:</strong> Norrköping<br>\n	<strong>Färdigställt:</strong> 2009\n</p>\n<p><br>\n</p>'),
	(72,1,'Skövde Arena','skovde-arena','Tillbyggnad av befintligt badhus med ny 25-meters bassäng samt nytt äventyrsbad med vågbassäng, utomhuspool, vildfors, rutschkaneanläggning mm.','<p>\n	<strong></strong>Om- och tillbyggnad för ny relaxavdelning med bla. inom- och utomhuspooler, fyra olika bastur samt relaxrum.<br><strong></strong>\n</p>\n<p><strong>Byggherre:</strong> Skövde Kommun<br>\n	<strong>Beställare:</strong> NCC<br>\n	<strong>Arkitekt:</strong> PP Arkitekter<br><strong>Konstruktör:</strong> VBK (i f d PIABs regi)<br>\n	<strong>Ort:</strong> Skövde<br>\n	<strong>Färdigställt:</strong> 2009\n</p>'),
	(73,1,'Arla - Vimmerby','arla-vimmerby','Nybyggnad av mjölkpulverfabrik.','<p>\n	<strong></strong><strong>Byggherre:</strong> Arla Foods AB<br>\n	<strong></strong><strong>Arkitekt:</strong> MA Projekt A/S<br><strong>Konstruktör:</strong> VBK<br>\n	<strong>Ort:</strong> Vimmerby<br>\n	<strong>Färdigställt:</strong> 2005<br>\n</p>\n<p><br>\n</p>'),
	(74,1,'Volvo Powertrain/PV','volvo-powertrain-pv','VBK har medverkat vid projektering och projektledning av ett flertal om-, till- och nybyggnader av motorfabriken sedan 1966.','<p>\n	<strong></strong>F.d. PIAB har medverkat vid projektering av ett flertal om-, till- och nybyggnader sedan 1984.<br><strong></strong>\n</p>\n<p><strong>Beställare:</strong> Volvo Powertrain och Volvo Personvagnar AB<br><strong>Konstruktör:</strong> VBK<br><strong>Projektledare:</strong> VBK<br>\n	<strong>Ort:</strong> Skövde<br>\n	<strong>Färdigställt:</strong> 1966 -\n</p>\n<p> <br>\n</p>'),
	(75,1,'Akzo Nobel','akzo-nobel','Nybyggnad av kontorshus ca 10 000 m2.','<p>\n	<strong></strong><strong>Beställare:</strong> Nösnäs Strand Fastighets AB / Tuve Bygg AB<br>\n	<strong>Arkitekt:</strong> Mats &amp; Arne Arkitektkontor AB<br><strong>Konstruktör:</strong> VBK (i f d PIABs regi)<br>\n	<strong>Ort:</strong> Stenungsund<br>\n	<strong>Färdigställt:</strong> 2005\n</p>\n<p><br>\n</p>'),
	(76,1,'BDAB - Göteborg','bdab-goteborg','Nybyggnad av kontorshus på ca 4200 m2 BTA.','<p>\n	<strong></strong><strong>Beställare:</strong> Husvärden<br>\n	<strong>Arkitekt:</strong> Wingårdh Arkitekter<br><strong>Konstruktör:</strong> VBK (i f d PIABs regi)<br>\n	<strong>Ort:</strong>  Mölndal<br>\n	<strong>Färdigställt:</strong> 2010\n</p>'),
	(77,1,'Pellviks','pellviks','Vid södra infarten till Falköping har företaget Pellviks AB uppfört kontor och lager.','<p>\n	<strong></strong>Kontoret är i två våningar och inrymmer även matsal och rekreationslokaler. Lagret har en yta av drygt 10 000 m2.<br><strong></strong>\n</p>\n<p><strong>Byggherre:</strong> Pellviks AB <br>\n	<strong>Beställare:</strong> Asplunds bygg AB<br>\n	<strong>Arkitekt:</strong> Mats Dahlström arkitektkontor<br><strong>Konstruktör:</strong> VBK<br>\n	<strong>Ort:</strong> Falköping<br>\n	<strong>Färdigställt:</strong> 2012\n</p>\n<p><br>\n</p>'),
	(78,1,'Ållebergsgymnasiet','allebergsgymnasiet','Falköpings kommun har i en partneringentreprenad tillsammans med Skanska uppfört en ny huvudbyggnad för Ållebergsgymnasiet.','<p> Inspirationen till byggnadens form kommer från en 1500 år gammal guldkrage som hittats vid Ålleberg.\n</p>\n<p>Ållebergsgymnasiet nominerades till \"Årets bygge 2014\".\n</p>\n<p><strong></strong><strong>Byggherre:</strong> Falköpings kommun <br>\n	<strong>Beställare:</strong> Skanska<br>\n	<strong>Arkitekt:</strong> Sweco<br><strong>Konstruktör:</strong> VBK<br>\n	<strong>Ort:</strong> Falköping<br>\n	<strong>Färdigställt:</strong> 2012\n</p><br>'),
	(79,1,'Herrgårdsskolan','herrgardsskolan','Nybyggnad av förskola, totalt 800 m2.','<p>\n	<strong></strong><strong>Beställare:</strong> Lokalsekretariatet<br>\n	<strong>Arkitekt:</strong> Carlsson Fernberg Arkitektkontor AB<br><strong>Konstruktör:</strong> VBK<br>\n	<strong>Ort:</strong> Göteborg - Lindholmen<br>\n	<strong>Färdigställt:</strong> 2008\n</p>\n<p><br>\n</p>'),
	(80,1,'Jättestenskolan','jattestenskolan','Om- och tillbyggnad av Jättestensskolan har möjliggjort en utökning av skolans verksamhet från F-6 till F-9.','<p>\n	<strong></strong>Skolan består av tre byggnader varav en gymnastikhall. Skolan har av Göteborgs Stad klassats som kulturhistoriskt värdefull bebyggelse vilket har ställt krav på en varsam om- och tillbyggnad.<br>\n	<br><strong></strong>\n</p>\n<p><strong>Byggherre:</strong> Göteborgs Stad, Lokalsekretariatet  <br>\n	<strong></strong><strong>Arkitekt:</strong> Liljewall arkitekter<br><strong>Konstruktör:</strong> VBK<br>\n	<strong>Ort:</strong> Göteborg<br>\n	<strong>Färdigställt:</strong> 2011\n</p>\n<p><br>\n</p>\n<p><br>\n</p>'),
	(81,1,'Trädgårdsstaden Förskola','tradgardsstaden-forskola','Skövde kommun har uppfört en förskola i området Trädgårdsstaden.','<p>Byggnaden har konstruerats som ett passivhus och har bl.a. två våningar med totalt fem hemvister, två torg, matsal samt balkonger runt hela huset.\n</p>\n<p>Detta blev den andra certifierade passivhusförskolan i Sverige. Den andra lufttätningsmätningen som utfördes efter byggnationen visade en täthet som motsvarar tio gånger bättre värden än de redan högt ställda kraven i passivhuskriterierna.\n</p>\n<p><strong>Byggherre:</strong> Skövde kommun<br>\n	<strong></strong><strong>Arkitekt:</strong> Glantz arkitektstudio AB<br><strong>Konstruktör:</strong> VBK<br>\n	<strong>Ort:</strong> Skövde<br>\n	<strong>Färdigställt:</strong> 2012\n</p><br>'),
	(82,1,'Sahlgrenska - Försörjningsbyggnad','forsorjningsbyggnad-sahlgrenska-sjukhuset','Ny försörjningsbyggnad på ca 8 800 m2 för godsmottagning, avfallshantering, central postservice, tryckeri, datahallar, IT-verksamhet mm.','<p>\n	<strong></strong><br>\n	<strong>Beställare:</strong> Västfastigheter Göteborg<br>\n	<strong>Arkitekt:</strong> CGC Arkitektkontor AB / White arkitekter AB<br><strong>Konstruktör:</strong> VBK (i f d PIABs regi)<br>\n	<strong>Ort:</strong> Göteborg<br>\n	<strong>Färdigställt:</strong> 2005\n</p>\n<p><br>\n</p>'),
	(83,1,'Sjukhus','sjukhus','VBK har medverkat vid projektering av ett flertal om- till- och nybyggnader av sjukhus bl a Strömstad, Lysekil, Uddevalla, Kungälv, Östra, Mölndal och Sahlgrenska.','<p>\n	<strong></strong><br>\n	<strong>Beställare:</strong> Bohuslandstinget, Västfastigheter<br><strong>Konstruktör:</strong> VBK<br>\n	<strong>Ort:</strong> Västra Götalands län<br>\n	<strong>Färdigställt:</strong> 1983 -\n</p>\n<p>\n	<br>\n</p>'),
	(84,1,'Haga Kyrkogata 6','haga-kyrkogata-6','VBK har tagit fram renoveringsprogram samt utfört byggledning och kontroll för fönsterbyte, fasad- och takrenovering.','<p>\n	<strong></strong><strong>Beställare:</strong> Egnahemsbolaget<br><strong>Byggledning:</strong> VBK<br>\n	<strong>Ort:</strong> Göteborg<br>\n	<strong>Färdigställt:</strong> 2000\n</p>');

/*!40000 ALTER TABLE `projectcontent` ENABLE KEYS */;
UNLOCK TABLES;


# Tabelldump projectimage
# ------------------------------------------------------------

DROP TABLE IF EXISTS `projectimage`;

CREATE TABLE `projectimage` (
  `projectId` int(11) NOT NULL,
  `projectimageId` int(11) NOT NULL AUTO_INCREMENT,
  `projectimageLink` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `projectimageOrder` int(11) NOT NULL,
  PRIMARY KEY (`projectimageId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `projectimage` WRITE;
/*!40000 ALTER TABLE `projectimage` DISABLE KEYS */;

INSERT INTO `projectimage` (`projectId`, `projectimageId`, `projectimageLink`, `projectimageOrder`)
VALUES
	(45,1,'shell.jpg',1),
	(45,2,'shl-003.jpg',2),
	(45,3,'VBK-026.jpg',3),
	(1,4,'vindkraftverk.jpg',1),
	(1,5,'dans-telefon-2011-06-12-086.jpg',2),
	(1,6,'dans-telefon-2011-06-12-089.jpg',3),
	(2,7,'P8310021.jpg',1),
	(2,8,'P8310015.jpg',2),
	(2,9,'P8310005.jpg',3),
	(3,10,'goteborgs-moske-01.jpg',1),
	(3,11,'goteborgs-moske-02.jpg',2),
	(3,12,'goteborgs-moske-03.jpg',3),
	(4,13,'DSC_0694.JPG',1),
	(4,14,'DSC_0693.JPG',2),
	(4,15,'DSC_0695.JPG',3),
	(5,16,'bild-027.jpg',1),
	(5,17,'bild-022.jpg',2),
	(6,18,'DSC_0690.JPG',1),
	(6,19,'DSC_0685.JPG',2),
	(6,20,'DSC_0687.JPG',3),
	(7,21,'DSC_0704.JPG',1),
	(7,22,'DSC_0702.JPG',2),
	(7,23,'DSC_0701.JPG',3),
	(8,24,'CIMG5802.JPG',1),
	(8,25,'CIMG5799.JPG',2),
	(8,26,'CIMG5816.JPG',3),
	(9,27,'Venus1.jpg',1),
	(10,28,'bild-008.jpg',1),
	(10,29,'bild-001.jpg',2),
	(10,30,'bild-003.jpg',3),
	(11,31,'DSC_1056.JPG',1),
	(11,32,'DSC_1055.JPG',2),
	(11,33,'DSC_1054.JPG',3),
	(12,34,'st1kontrollrum.jpg',1),
	(12,35,'Bild-145.jpg',2),
	(12,36,'Bild-144.jpg',3),
	(13,37,'eriksbergs107.jpg',1),
	(13,38,'eriksbergs108.jpg',2),
	(13,39,'eriksbergs112.jpg',3),
	(14,40,'Brf-Srhallsberget.jpg',1),
	(14,41,'DSC_0174.JPG',2),
	(14,42,'DSC_0182.JPG',3),
	(15,43,'Vaxelmynt.jpg',1),
	(15,44,'PICT0043.JPG',2),
	(15,45,'DSC_0149.JPG',3),
	(16,46,'OPERAN.JPG',1),
	(16,47,'DSC_0205.JPG',2),
	(16,48,'P1010019.JPG',3),
	(17,49,'Hotell11.JPG',1),
	(17,50,'kyrka.jpg',2),
	(18,51,'hogskolanboras.jpg',1),
	(18,52,'HiB_4.jpg',2),
	(18,53,'HiB_3.jpg',3),
	(18,54,'HiB_1.jpg',4),
	(19,55,'Falkhallen03.JPG',1),
	(19,56,'Falkhallen-kvall_forminskad.jpg',2),
	(19,57,'Bild-014.jpg',3),
	(20,58,'Projektbild_jpg.jpg',1),
	(20,59,'20080912-018.jpg',2),
	(21,60,'livraddartorn.jpg',1),
	(21,61,'DSC00001.JPG',2),
	(22,62,'Bild-177.jpg',1),
	(23,63,'DSCF2889.jpg',1),
	(23,64,'DSCF2906.JPG',2),
	(23,65,'DSCF2910.jpg',3),
	(24,66,'PICT9523.JPG',1),
	(24,67,'PICT9856.JPG',2),
	(24,68,'idrottsparken.JPG',3),
	(25,69,'ostberget.jpg',1),
	(25,70,'Picture-004.jpg',2),
	(25,71,'Picture-010.jpg',3),
	(26,72,'Hus-12.jpg',1),
	(26,73,'IMG_0980-2.jpg',2),
	(26,74,'IMG_1801-2.jpg',3),
	(27,75,'ABB-huset2.jpg',1),
	(27,76,'PICT0005.jpg',2),
	(27,77,'ABB-huset.jpg',3),
	(28,78,'071127_VBK_AVALON-002396.JPG',1),
	(28,79,'HotelAvalon.JPG',2),
	(28,80,'DSC_0066.JPG',3),
	(29,81,'krokslatt204_150.jpg',2),
	(29,82,'Mektagonen2014.jpg',1),
	(29,83,'perspektiv-natt-svart.jpg',3),
	(30,84,'Bilder-Hotell-Canon-070.jpg',1),
	(30,85,'Flyfoto-6.jpg',2),
	(30,86,'Bilder-Hotell-Canon-454.jpg',3),
	(31,87,'fotomontage1-copy.jpg',1),
	(31,88,'Julkort3.jpg',2),
	(31,89,'Opalen-011.jpg',3),
	(32,90,'Pedagogen.JPG',1),
	(32,91,'IMG_2656.jpg',2),
	(32,92,'CIMG3569.JPG',3),
	(33,93,'SVT-SR.JPG',1),
	(33,94,'hasselblad3.JPG',2),
	(33,95,'PICT0030.JPG',3),
	(34,96,'elanders.JPG',1),
	(35,97,'IMG_1341.JPG',1),
	(35,98,'Stena.JPG',2),
	(35,99,'PICT0001.JPG',3),
	(36,100,'CTH_AV_1.jpg',1),
	(36,101,'CTH_AV_2.jpg',2),
	(37,102,'microelectronic.jpg',1),
	(37,103,'PICT0002.jpg',2),
	(38,104,'SGS-lindholm.jpg',1),
	(38,105,'Picture-250.jpg',2),
	(39,106,'M2.JPG',1),
	(40,107,'PICT0007.JPG',1),
	(40,108,'PICT00432.JPG',2),
	(41,109,'EMW.jpg',1),
	(41,110,'PICT00072.JPG',2),
	(41,111,'Ericsson-EMW.JPG',3),
	(42,112,'svenskamassan.jpg',1),
	(42,113,'sv-massan5.jpg',2),
	(43,115,'liseberg.JPG',2),
	(43,116,'P5110120.JPG',3),
	(44,117,'biotech.JPG',1),
	(44,118,'PICT0025.JPG',2),
	(44,119,'PICT0027.JPG',3),
	(46,120,'Ullevi.JPG',1),
	(46,121,'VBK-015.JPG',2),
	(46,122,'ULLEVI-ljusare.jpg',3),
	(47,123,'ryabiobransle.jpg',1),
	(47,124,'DSC_0189.JPG',2),
	(47,125,'Rya-Kraftvarmeverk.jpg',3),
	(48,126,'02-819.jpg',1),
	(48,127,'02-820.jpg',2),
	(48,128,'EDL.jpg',3),
	(49,129,'vardbyggnad.jpg',1),
	(49,130,'Picture-255.jpg',2),
	(49,131,'Utv-entre.jpg',3),
	(50,132,'scandic.jpg',1),
	(50,133,'PICT0034.jpg',2),
	(50,134,'PICT0033.jpg',3),
	(51,135,'brandstation.JPG',1),
	(51,136,'CIMG2044.JPG',2),
	(51,137,'CIMG2045.JPG',3),
	(54,142,'SDC12384.JPG',1),
	(54,143,'DSC00071.JPG',2),
	(54,144,'SDC12483.JPG',3),
	(55,145,'Olawa-foto.jpg',1),
	(56,146,'SGS-kronhus.JPG',1),
	(56,147,'P1010012.JPG',2),
	(56,148,'P1010009.JPG',3),
	(57,149,'Picture-002.jpg',1),
	(57,150,'20080612_Botkyrka_NEU2_Front_PAN.jpg',2),
	(57,151,'Hornbach_malmo.jpg',3),
	(58,152,'Bovivagoteborg.jpg',1),
	(58,153,'L1040248.JPG',2),
	(58,154,'L1040218.JPG',3),
	(59,155,'bjorlandakile.jpg',1),
	(59,156,'VBK-045.JPG',3),
	(59,157,'061002-025.jpg',2),
	(61,158,'Poseidon_brannogatan.jpg',1),
	(62,159,'New-Image.jpg',1),
	(62,160,'hagabadet_02.jpg',2),
	(62,161,'hagabadetgget_108466225.jpg',3),
	(63,162,'Vite_Knut.jpg',1),
	(64,163,'Brf-Vasaparken-2.jpg',1),
	(65,164,'CIMG1451.JPG',1),
	(65,165,'kyrka2.jpg',2),
	(65,166,'DSCN1301.JPG',3),
	(66,167,'Brf-Goteborgshus-nr-34.jpg',1),
	(67,168,'Brf-Goteborgshus-nr-31.jpg',1),
	(68,169,'ronnang.jpg',1),
	(69,170,'Brf-CyklistenKvillebacken2.jpg',1),
	(69,171,'Brf-CyklistenKvillebacken1.jpg',2),
	(70,172,'DSC_1513.jpg',1),
	(70,173,'DSC_0683.jpg',2),
	(70,174,'DSC_1510.jpg',3),
	(71,175,'stadiumarena.jpg',1),
	(71,176,'SB_3.jpg',2),
	(72,177,'Vattenlandskap.jpg',1),
	(72,178,'Arena_1.jpg',2),
	(72,179,'DSC_0017.jpg',3),
	(73,180,'Arla-Vimmerby1.jpg',1),
	(73,181,'Arla-Vimmerby2.jpg',2),
	(74,182,'VBK-029.jpg',1),
	(75,183,'AkzoNobel.jpg',1),
	(75,184,'akzo2.jpg',2),
	(75,185,'DSC_0050.jpg',3),
	(76,186,'BDAB1.jpg',1),
	(76,187,'BDAB.jpg',2),
	(77,188,'IMG_3519.jpg',1),
	(77,189,'IMG_3520.jpg',2),
	(78,190,'allebergsgymnasium.jpg',1),
	(79,191,'herrgardsskolan1.jpg',1),
	(79,192,'herrgardsskolan2.jpg',2),
	(79,193,'herrgardsskolan3.jpg',3),
	(80,194,'DSC_1051.jpg',1),
	(80,195,'DSC_1047.jpg',2),
	(80,196,'DSC_1043.jpg',3),
	(81,197,'DSC_0520.jpg',1),
	(81,198,'DSC_0516.jpg',2),
	(82,199,'Forsorjningsbyggnad.jpg',1),
	(82,200,'bild-015.jpg',2),
	(83,201,'ostra.jpg',1),
	(83,202,'kungalv.jpg',2),
	(83,203,'Lysekil.jpg',3),
	(83,204,'molndal.jpg',4),
	(83,205,'stromstad.jpg',5),
	(84,207,'HagaKyrkogata.jpg',1),
	(83,212,'sahlgrenska1-1427104036.jpg',10),
	(43,213,'bild-027-1427208833.JPG',10),
	(43,214,'dsc_1533-1427208923.JPG',10),
	(52,217,'svt-srbeskuren-1432710221.JPG',1),
	(52,219,'pict0007beskuren-1432710332.jpg',10),
	(52,220,'cimg0537-1432710332.JPG',10),
	(52,221,'cimg0544-1432710368.JPG',10),
	(52,223,'pict0007beskuren-1433142275.jpg',10);

/*!40000 ALTER TABLE `projectimage` ENABLE KEYS */;
UNLOCK TABLES;


# Tabelldump slideshow
# ------------------------------------------------------------

DROP TABLE IF EXISTS `slideshow`;

CREATE TABLE `slideshow` (
  `slideId` int(11) NOT NULL AUTO_INCREMENT,
  `slideName` varchar(250) NOT NULL,
  `slideDescription` varchar(500) NOT NULL,
  `slideLink` varchar(100) NOT NULL,
  PRIMARY KEY (`slideId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `slideshow` WRITE;
/*!40000 ALTER TABLE `slideshow` DISABLE KEYS */;

INSERT INTO `slideshow` (`slideId`, `slideName`, `slideDescription`, `slideLink`)
VALUES
	(1,'Gothia Triple Towers','Ny- och påbyggnad av hotell med ca 500 rum.','gothia.jpg'),
	(2,'House of Ports','Projekteringen av nytt huvudkontor för House of Ports.','ports.jpg'),
	(3,'Liseberg Helix','VBK står för konstruktionen av både betongfundamenten som Mack Rides pelare vilar på och Helix stationsbyggnad.','helix.jpg'),
	(4,'NCC Triangeln Malmö','Nybyggnad av galleria som byggts samman med befintliga handelsytor.','triangeln.jpg'),
	(5,'Skanska Trollberget','Mellan centrala Halmstad och sandstranden Tylösand är ett nytt bostadsområde uppfört.','trollberget.jpg'),
	(6,'Rönnängs Brygga Tjörn','Varvsområdet V & T Löfbergs slip och båtvarv köptes av Saltholmsgruppen, som nu har byggt bostäder på området.','ronnang-hero.jpg');

/*!40000 ALTER TABLE `slideshow` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
