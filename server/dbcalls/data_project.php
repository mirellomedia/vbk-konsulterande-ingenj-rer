<?php

$data['thisproject'] = DB::select('         SELECT      pc.projectName,
                                                    pc.projectLink,
                                                    pc.projectId,
                                                    pc.projectShortDesc,
                                                    pc.projectLongDesc
                                        FROM        projectcontent pc
                                        INNER JOIN  project p
                                        ON          p.projectId = pc.projectId
                                        WHERE       pc.projectLink = ?
                                        AND         pc.languageId = ?
                                        LIMIT       1
                                    ', array($thisproject, $langInfo->languageId));
$data['thisproject'] = $data['thisproject'][0];
?>