<?php
$data['subnav'] = DB::select('  SELECT      pc.pageName, 
                                                    pc.pageLink
                                        FROM        pagecontent pc
                                        INNER JOIN  page p
                                        ON          p.pageId = pc.pageId
                                        WHERE       p.pageMenu = 2
                                        AND         pc.languageId = ?
                                        ORDER BY    p.pageMenuSort ASC
                                    ', array($langInfo->languageId));
?>