<?php
        $data['employees'] = DB::select('SELECT     e.employeeName,
                                                    e.employeePhone,
                                                    e.employeeEmail,
                                                    e.employeeImage,
                                                    e.employeeCity,
                                                    t.titleName
                                        FROM        employee e
                                        LEFT JOIN   employee_title et
                                        ON          e.employeeId = et.employeeId
                                        LEFT JOIN   title t
                                        ON          t.titleId = et.titleId
                                        ORDER BY    e.employeeName DESC
                                    ');
?>