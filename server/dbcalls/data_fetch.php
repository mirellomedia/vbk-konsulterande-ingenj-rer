<?php
 $data['fetch'] = DB::select('SELECT     p.pageTemplate,
                                                pc.pageName,
                                                pc.pageContent,
                                                p.pageId,
                                                pc.pageLink,
                                                pc.pageHeaderimage
                                    FROM        page p
                                    INNER JOIN  pagecontent pc
                                    ON          p.pageId = pc.pageId
                                    WHERE       pc.pageLink = ?
                                    AND         pc.languageId = ?
                                    AND         p.pageParent != 0
                                    LIMIT       1
                                 ', array($id, $langInfo->languageId));
        if(!$data['fetch']){
          return View::make('404');
          exit();
        }

?>