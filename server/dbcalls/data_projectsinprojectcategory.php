<?php

$data['projectsinprojectcategory'] = DB::select('   SELECT      pc.projectName,
                                                                pc.projectLink,
                                                                pc.projectId,
                                                                pc.projectShortDesc
                                                    FROM        projectcontent pc
                                                    INNER JOIN  project p
                                                    ON          p.projectId = pc.projectId
                                                    INNER JOIN  projectcategory pca
                                                    ON          pca.projectcategoryId = p.projectCategory
                                                    WHERE       pca.projectcategoryLink = ?
                                                    AND         pc.languageId = ?
                                                ', array($projektkategori, $langInfo->languageId));
?>