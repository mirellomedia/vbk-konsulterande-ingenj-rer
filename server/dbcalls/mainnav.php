<?php 
$data['mainnav'] = DB::select(' SELECT      pc.pageName,
                                                    pc.pageLink,
                                                    pc.pageContent,
                                                    pc.pageId
                                        FROM        pagecontent pc
                                        INNER JOIN  page p
                                        ON          p.pageId = pc.pageId
                                        WHERE       p.pageMenu = 1
                                        AND         pc.languageId = ?
                                        ORDER BY    p.pageMenuSort ASC
                                    ', array($langInfo->languageId));
        foreach($data['mainnav'] as $menuobject){
          if($menuobject->pageContent == ''){
            // Om innehållet i menyobjektet är tomt så kollar vi vilket första barnet är och tar dess länk.
            
            $test            = DB::select(' SELECT      pc.pageLink
                                            FROM        pagecontent pc
                                            INNER JOIN  page p
                                            ON          p.pageId = pc.pageId
                                            WHERE       p.pageParent = ?
                                            AND         p.pageMenuSort = ?
                                            AND         pc.languageId = ?
                                            LIMIT       1
                                          ', array($menuobject->pageId, '1', $langInfo->languageId));

            if($test){
              $menuobject->pageLink .= '/'.$test[0]->pageLink;
            }
          }
        }

?>