<?php

$data['projects'] = DB::select('            SELECT      pc.pageName, 
                                                        pc.pageLink
                                            FROM        pagecontent pc
                                            INNER JOIN  page p
                                            ON          p.pageId = pc.pageId
                                            WHERE       p.pageParent = ?
                                            AND         pc.languageId = ?
                                        ', array($parentId, $langInfo->languageId));

?>