<?php
$data['articles'] = DB::select('SELECT      ac.articleId,
                                                  ac.articleHeader,
                                                  ac.articleSum,
                                                  a.articleDate,
                                                  ac.articleLink
                                      FROM        articlecontent ac
                                      INNER JOIN  article a
                                      ON          a.articleId = ac.articleId
                                      WHERE       ac.languageId = ?
                                      ORDER BY    ac.articleDate DESC
                                      LIMIT       3
                                  ', array($langInfo->languageId));
?>