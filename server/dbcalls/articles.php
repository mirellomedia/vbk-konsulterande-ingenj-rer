<?php 
$articles = DB::select('    SELECT      ac.articleId,
                                                ac.articleHeader,
                                                a.articleDate,
                                                ac.articleLink
                                    FROM        articlecontent ac
                                    INNER JOIN  article a
                                    ON          a.articleId = ac.articleId
                                    ORDER BY    a.articleDate DESC
                                    ');

        $allArticles = array();

        foreach($articles as $article){
            $datePieces = explode("-",$article->articleDate);
            $year  = $datePieces[0];
                $month = $datePieces[1];
                $day   = $datePieces[2];

            $month = strftime("%B", mktime(0, 0, 0, $month, 10));

            // Skapar arrayer för år samt månad om de inte är satta
            if(!isset($allArticles[$year])){
                $allArticles[$year] = array();
            }  
            if(!isset($allArticles[$year][$month])){
                $allArticles[$year][$month] = array();
            }

            // Pushar in artikeln i det året samt månaden som den hör hemma
            array_push($allArticles[$year][$month], $article);
        }
        // echo '<pre>';
        // print_r($data['archive']);
        // echo '<pre>';

        $data['archive'] = $allArticles;

?>