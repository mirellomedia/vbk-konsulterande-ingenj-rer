<?php

  $languages = array('sv', 'en');
  $defaultlanguage = Config::get('app.locale');
  $locale = Request::segment(1); //fetches first URI segment

  if(in_array($locale, $languages) && $locale != $defaultlanguage) {
    App::setLocale($locale);
  }
  else {
    App::setLocale($defaultlanguage);
    $locale = '';
  }


Route::group(array('prefix' => $locale), function() {
    Route::get('/', array('as' => 'index', function() {
      $fetch   =  DB::select('  SELECT      l.languageId,
                                            l.languageName
                                FROM        language l
                                WHERE       l.languageCode = ?
                            ', array(App::getLocale()));
      $langInfo = $fetch[0];
      unset($fetch);

      $data['mainnav'] = DB::select(' SELECT      pc.pageName, 
                                                  pc.pageLink,
                                                  pc.pageContent,
                                                  p.pageId
                                      FROM        pagecontent pc
                                      INNER JOIN  page p
                                      ON          p.pageId = pc.pageId
                                      WHERE       p.pageMenu = 1
                                      AND         pc.languageId = ?
                                      ORDER BY    p.pageMenuSort ASC
                                  ', array($langInfo->languageId));

     foreach($data['mainnav'] as $menuobject){
          if($menuobject->pageContent == ''){
            // Om innehållet i menyobjektet är tomt så kollar vi vilket första barnet är och tar dess länk.
            
            $test            = DB::select(' SELECT      pc.pageLink
                                            FROM        pagecontent pc
                                            INNER JOIN  page p
                                            ON          p.pageId = pc.pageId
                                            WHERE       p.pageParent = ?
                                            AND         p.pageMenuSort = ?
                                            AND         pc.languageId = ?
                                            LIMIT       1
                                          ', array($menuobject->pageId, '1', $langInfo->languageId));
            if($test){
              $menuobject->pageLink .= '/'.$test[0]->pageLink;
            }
          }
        }

      $data['subnav'] = DB::select('  SELECT      pc.pageName, 
                                                  pc.pageLink,
                                                  pc.pageContent,
                                                  pc.pageId
                                      FROM        pagecontent pc
                                      INNER JOIN  page p
                                      ON          p.pageId = pc.pageId
                                      WHERE       p.pageMenu = 2
                                      AND         pc.languageId = ?
                                      ORDER BY    p.pageMenuSort ASC
                                  ', array($langInfo->languageId));

      foreach($data['subnav'] as $menuobject){
          if($menuobject->pageContent == ''){
            // Om innehållet i menyobjektet är tomt så kollar vi vilket första barnet är och tar dess länk.
            
            $test            = DB::select(' SELECT      pc.pageLink
                                            FROM        pagecontent pc
                                            INNER JOIN  page p
                                            ON          p.pageId = pc.pageId
                                            WHERE       p.pageParent = ?
                                            AND         p.pageMenuSort = ?
                                            AND         pc.languageId = ?
                                            LIMIT       1
                                          ', array($menuobject->pageId, '1', $langInfo->languageId));
            if($test){
              $menuobject->pageLink .= '/'.$test[0]->pageLink;
            }
          }
        }


      $data['articles'] = DB::select('SELECT      ac.articleId,
                                                  ac.articleHeader,
                                                  ac.articleSum,
                                                  a.articleDate,
                                                  ac.articleLink
                                      FROM        articlecontent ac
                                      INNER JOIN  article a
                                      ON          a.articleId = ac.articleId
                                      WHERE       ac.languageId = ?
                                      ORDER BY    ac.articleDate DESC
                                      LIMIT       3
                                  ', array($langInfo->languageId));


      return View::make('pages.index', $data);
    }));
  
    Route::get('/search', function() {
        $id = 'search';
        $searchstring = $_GET['search']; 
        $searchstringSQL = '%'.$_GET['search'].'%'; 
        
        $fetch   =       DB::select('   SELECT        l.languageId,
                                                      l.languageName
                                          FROM        language l
                                          WHERE       l.languageCode = ?
                                      ', array(App::getLocale()));
          $langInfo = $fetch[0];
          unset($fetch);

          $data['mainnav'] = DB::select(' SELECT      pc.pageName, 
                                                      pc.pageLink,
                                                      pc.pageContent,
                                                      pc.pageId
                                          FROM        pagecontent pc
                                          INNER JOIN  page p
                                          ON          p.pageId = pc.pageId
                                          WHERE       p.pageMenu = 1
                                          AND         pc.languageId = ?
                                          ORDER BY    p.pageMenuSort ASC
                                      ', array($langInfo->languageId));
          foreach($data['mainnav'] as $menuobject){
            if($menuobject->pageContent == ''){
              // Om innehållet i menyobjektet är tomt så kollar vi vilket första barnet är och tar dess länk.

              $test            = DB::select(' SELECT      pc.pageLink
                                              FROM        pagecontent pc
                                              INNER JOIN  page p
                                              ON          p.pageId = pc.pageId
                                              WHERE       p.pageParent = ?
                                              AND         p.pageMenuSort = ?
                                              AND         pc.languageId = ?
                                              LIMIT       1
                                            ', array($menuobject->pageId, '1', $langInfo->languageId));

              if($test){
                $menuobject->pageLink .= '/'.$test[0]->pageLink;
              }
            }
          }
          $data['subnav'] = DB::select('  SELECT      pc.pageName, 
                                                      pc.pageLink
                                          FROM        pagecontent pc
                                          INNER JOIN  page p
                                          ON          p.pageId = pc.pageId
                                          WHERE       p.pageMenu = 2
                                          AND         pc.languageId = ?
                                          ORDER BY    p.pageMenuSort ASC
                                      ', array($langInfo->languageId));

          $data['searchresultsarticles'] = DB::select(' SELECT        ac.articleHeader AS pageName, 
                                                                      ac.articleContent AS pageContent,
                                                                      ac.articleLink AS pageLink,
                                                                      ac.articleId
                                                          FROM        articlecontent ac
                                                          INNER JOIN  article a
                                                          ON          a.articleId = ac.articleId
                                                          AND         ac.languageId = ?
                                                          WHERE       ac.articleHeader LIKE ?
                                                          OR          ac.articleContent LIKE ?
                                                ', array($langInfo->languageId, $searchstringSQL, $searchstringSQL));

          foreach($data['searchresultsarticles'] as $searchresult){
            $searchresult->pageLink = 'nyheter/'.$searchresult->articleId . '/' . $searchresult->pageLink;
          }

          $data['searchresultspages'] = DB::select('SELECT      pc.pageName, 
                                                                pc.pageContent,
                                                                pc.pageLink,
                                                                pc.pageId
                                                    FROM        pagecontent pc
                                                    INNER JOIN  page p
                                                    ON          p.pageId = pc.pageId
                                                    AND         pc.languageId = ?
                                                    WHERE       pc.pageName LIKE ?
                                                    OR          pc.pageContent LIKE ?
                                                ', array($langInfo->languageId, $searchstringSQL, $searchstringSQL));

          $data['searchstring'] = $searchstring;
          $data['searchresults'] = array_merge($data['searchresultspages'], $data['searchresultsarticles']);
          $data['resultscount'] = count($data['searchresults']);
          foreach($data['searchresults'] as $searchresult){
            
            $searchresult->pageContent = strip_tags($searchresult->pageContent);
            $line=$searchresult->pageContent;
            if (preg_match('/^.{1,300}\b/s', $searchresult->pageContent, $match))
            {
                $searchresult->pageContent=$match[0];
            }
            $searchresult->pageContent = preg_replace("/\p{L}*?".preg_quote($searchstring)."\p{L}*/ui", "<b>$0</b>", $searchresult->pageContent);
          }

          return View::make('pages.5', $data);
    });

    Route::get('{id}', function($id) {
      $currentPath = dirname(__FILE__).'/dbcalls/';
      

          require_once($currentPath.'langinfo.php');
          require_once($currentPath.'data_pagecontent.php');

          $data['subpages'] = DB::select('SELECT      pc.pageName, 
                                                      pc.pageLink
                                          FROM        pagecontent pc
                                          INNER JOIN  page p
                                          ON          p.pageId = pc.pageId
                                          WHERE       p.pageParent = ?
                                          AND         pc.languageId = ?
                                      ', array($data['pagecontent']->pageId, $langInfo->languageId));
          $parentLink = $data['pagecontent']->pageLink;

          foreach($data['subpages'] as $subpage){
            $subpage->pageLink = $parentLink.'/'.$subpage->pageLink;
          }
          
          require_once($currentPath.'data_mainnav.php');
          require_once($currentPath.'data_subnav.php');



          $data['articles'] = DB::select('SELECT    ac.articleId,
                                                    ac.articleHeader,
                                                    ac.articleSum,
                                                    a.articleDate,
                                                    ac.articleLink
                                        FROM        articlecontent ac
                                        INNER JOIN  article a
                                        ON          a.articleId = ac.articleId
                                        WHERE       ac.languageId = ?
                                        ORDER BY    ac.articleDate DESC
                                        LIMIT       3
                                    ', array($langInfo->languageId));


          $data['employees'] = DB::select('SELECT     e.employeeName,
                                                      e.employeePhone,
                                                      e.employeeEmail,
                                                      e.employeeImage,
                                                      e.employeeCity,
                                                      t.titleName
                                          FROM        employee e
                                          LEFT JOIN   employee_title et
                                          ON          e.employeeId = et.employeeId
                                          LEFT JOIN   title t
                                          ON          t.titleId = et.titleId
                                          ORDER BY    e.employeeName DESC
                                      ');

          $articles = DB::select('    SELECT      ac.articleId,
                                                  ac.articleHeader,
                                                  a.articleDate,
                                                  ac.articleLink
                                      FROM        articlecontent ac
                                      INNER JOIN  article a
                                      ON          a.articleId = ac.articleId
                                      ORDER BY    a.articleDate DESC
                                      ');
          // echo '<pre>';
          // print_r($articles);
          // echo '</pre>';
          $allArticles = array();

          foreach($articles as $article){
              $datePieces = explode("-",$article->articleDate);
              $year  = $datePieces[0];
                  $month = $datePieces[1];
                  $day   = $datePieces[2];

              $month = strftime("%B", mktime(0, 0, 0, $month, 10));

              // Skapar arrayer för år samt månad om de inte är satta
              if(!isset($allArticles[$year])){
                  $allArticles[$year] = array();
              }  
              if(!isset($allArticles[$year][$month])){
                  $allArticles[$year][$month] = array();
              }

              // Pushar in artikeln i det året samt månaden som den hör hemma
              array_push($allArticles[$year][$month], $article);
          }
          // echo '<pre>';
          // print_r($data['archive']);
          // echo '<pre>';

          $data['archive'] = $allArticles;
          
          return View::make('pages.'.$pageTemplate, $data);
    });

    Route::get('{hej}/{id}', function($hej,$id) {
      $pageParent = $hej;

      $currentPath = dirname(__FILE__).'/dbcalls/';
        
      require_once($currentPath.'langinfo.php');
      require_once($currentPath.'data_pagecontent.php');
      require_once($currentPath.'data_mainnav.php');
      require_once($currentPath.'data_subnav.php');
      require_once($currentPath.'data_articles.php');
      require_once($currentPath.'data_employees.php');
      require_once($currentPath.'articles.php');
      require_once($currentPath.'parentpage.php');
      require_once($currentPath.'data_subpages.php');
      if($pageTemplate == 6){
        require_once($currentPath.'data_projects.php');
        require_once($currentPath.'data_projectcategory.php');
      }
      // echo '<pre>';
      // print_r($data);
      // echo '</pre>';
      return View::make('pages.'.$pageTemplate, $data);
    });

    Route::get('nyheter/{id}/{link}', function($id, $link) {

      $fetch   =          DB::select(' SELECT      l.languageId,
                                                   l.languageName
                                       FROM        language l
                                       WHERE       l.languageCode = ?
                                  ', array(App::getLocale()));
      $langInfo = $fetch[0];
      unset($fetch);

      require_once($currentPath.'data_mainnav.php');
      require_once($currentPath.'data_subnav.php');

      $articles = DB::select('        SELECT      ac.articleId,
                                                  ac.articleHeader,
                                                  a.articleDate,
                                                  ac.articleLink
                                      FROM        articlecontent ac
                                      INNER JOIN  article a
                                      ON          a.articleId = ac.articleId
                                      ORDER BY    a.articleDate DESC
                                  ');

      $allArticles = array();

      foreach($articles as $article){
          $datePieces = explode("-",$article->articleDate);
          $year  = $datePieces[0];
              $month = $datePieces[1];
              $day   = $datePieces[2];

          $month = strftime("%B", mktime(0, 0, 0, $month, 10));

          // Skapar arrayer för år samt månad om de inte är satta
          if(!isset($allArticles[$year])){
              $allArticles[$year] = array();
          }  
          if(!isset($allArticles[$year][$month])){
              $allArticles[$year][$month] = array();
          }

          // Pushar in artikeln i det året samt månaden som den hör hemma
          array_push($allArticles[$year][$month], $article);
      }
      // echo '<pre>';
      // print_r($data['archive']);
      // echo '<pre>';

      $data['archive'] = $allArticles;

      // echo '<pre>';
      // print_r($data['archive']);
      // echo '<pre>';
      
      $data['fetch'] = DB::select('SELECT         ac.articleId,
                                                  ac.articleHeader,
                                                  ac.articleSum,
                                                  ac.articleContent,
                                                  a.articleDate,
                                                  ac.articleLink
                                   FROM           articlecontent ac
                                   INNER JOIN     article a
                                   ON             a.articleId = ac.articleId
                                   WHERE          ac.articleId = ?
                                   AND            ac.articleLink = ?
                               ', array($id, $link));
      if(!$data['fetch']){
        return View::make('404');
        exit();
      }
      $data['pagecontent'] = $data['fetch'][0];
      unset($data['fetch']);  

      return View::make('pages.4', $data);
    });

    Route::get('projekt/{id}/', function($hej,$id) {
      $currentPath = dirname(__FILE__).'/dbcalls/';
      
      require_once($currentPath.'langinfo.php');
      require_once($currentPath.'data_pagecontent.php');
      require_once($currentPath.'data_mainnav.php');
      require_once($currentPath.'data_subnav.php');
      require_once($currentPath.'data_projectcategory.php');
      
      return View::make('pages.7', $data);
    });

    Route::get('{hej}/{id}/{projektkategori}', function($hej,$id,$projektkategori) {
      $currentPath = dirname(__FILE__).'/dbcalls/';

      require_once($currentPath.'langinfo.php');
      require_once($currentPath.'data_pagecontent.php');
      require_once($currentPath.'data_mainnav.php');
      require_once($currentPath.'data_subnav.php');
      $data['projektkategoriName'] = ucfirst($projektkategori);
      $data['projektkategoriLink'] = $projektkategori;
      require_once($currentPath.'data_projectcategory.php');
      require_once($currentPath.'data_projectsinprojectcategory.php');

      return View::make('pages.7', $data);
    });

    Route::get('{hej}/{id}/{projektkategori}/{projekt}', function($hej,$id,$projektkategori, $thisproject) {
      $currentPath = dirname(__FILE__).'/dbcalls/';
        
      require_once($currentPath.'langinfo.php');
      require_once($currentPath.'data_pagecontent.php');
      require_once($currentPath.'data_mainnav.php');
      require_once($currentPath.'data_subnav.php');
      $data['projektkategoriName'] = ucfirst($projektkategori);
      $data['projektkategoriLink'] = $projektkategori;
      require_once($currentPath.'data_projectcategory.php');
      require_once($currentPath.'data_project.php');

      return View::make('pages.8', $data);
    });

});