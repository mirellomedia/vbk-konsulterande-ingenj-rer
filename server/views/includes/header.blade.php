<?php
  $currentLanguage = App::getLocale();

  if($currentLanguage == 'sv'){
    $currentLanguage = '';
    $newLanguage = 'en';
    $languageName = 'English';
  }
  else{
    $currentLanguage = '/'.$currentLanguage;
    $newLanguage = '';
    $languageName = 'Svenska';
  }
?><header class="site-head cf">@include('includes.navigation')<div class=logo-bar><a href="/" class=logo-wrap><img class=logo src=/assets/img/logos/logo-vbk.png alt="VBK Konsulterande ingenjörers logotyp"></a><button type=button class=toggle-menu data-button-open=Meny data-button-close=Stäng></button></div></header>