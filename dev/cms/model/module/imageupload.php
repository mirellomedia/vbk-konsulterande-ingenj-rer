<?php date_default_timezone_set('Europe/Stockholm'); ?>
<?php

// files storage folder
$dir = '../../../uploads/images/';

$_FILES['file']['type'] = strtolower($_FILES['file']['type']);

if ($_FILES['file']['type'] == 'image/png'
|| $_FILES['file']['type'] == 'image/jpg'
|| $_FILES['file']['type'] == 'image/gif'
|| $_FILES['file']['type'] == 'image/jpeg'
|| $_FILES['file']['type'] == 'image/pjpeg')
{

      // setting file's mysterious name
    $file = md5(date('YmdHis')).'.jpg';

    // copying
    move_uploaded_file($_FILES['file']['tmp_name'], $dir.$file);

    $url = explode("cms.",$_SERVER['HTTP_HOST']);

    // displaying file
    $array = array(
        'filelink' => '/uploads/images/'.$file
    );

    echo stripslashes(json_encode($array));
}

?>