<?php

  $allData = array();

  $query = "  SELECT      e.employeegroupId
              ,           e.employeegroupName
              FROM        employeegroup e
           ";

  $this->stmt = $this->mysqli->stmt_init();
  $this->stmt->prepare($query);
  $this->stmt->execute();
  $this->stmt->bind_result(
    $employeegroupId,
    $employeegroupName
  );

  while($this->stmt->fetch()) {
    $thisData = (object) array(
      'employeegroupId'   => $employeegroupId,
      'employeegroupName' => $employeegroupName
    );
    array_push($allData, $thisData);
  }

  $this->stmt->close();

  $data = $allData;
