<?php

  $query = "UPDATE  employeegroup
            SET     employeegroupName   = ?
            WHERE   employeegroupId     = ?
  ";

  $this->stmt = $this->mysqli->stmt_init();
  $this->stmt->prepare($query);

  $this->stmt->bind_param("si",
    $data[0]->employeegroupName,
    $data[0]->employeegroupId
  );

  $this->stmt->execute();
  $this->stmt->close();
