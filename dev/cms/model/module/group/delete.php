<?php

  $groupId = intval($fetch);

  $query = "DELETE FROM employeegroup
            WHERE       employeegroupId = ?
  ";

  $this->stmt = $this->mysqli->stmt_init();
  $this->stmt->prepare($query);
  $this->stmt->bind_param("i",
    $groupId
  );

  $this->stmt->execute();

  $this->stmt->close();
