<?php

  $query = "INSERT INTO employeegroup (
                          employeegroupName,
                          employeegroupOrder
                        )
            VALUES      (?, 9999)
  ";

  $this->stmt = $this->mysqli->stmt_init();
  $this->stmt->prepare($query);

  $this->stmt->bind_param("s",
    $fetch->employeegroupName
  );

  $this->stmt->execute();
  $groupId = $this->stmt->insert_id;
  $this->stmt->close();

  $fetch->employeegroupId = $groupId;

  $data = $fetch;
