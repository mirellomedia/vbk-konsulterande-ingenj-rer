<?php date_default_timezone_set('Europe/Stockholm'); ?>
<?php
  // Api controller
  header("Cache-Control: no-cache, must-revalidate");
  header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");

  // Declare variables from URL
  $url = explode("/api/", $_SERVER['REQUEST_URI']);
  $urlData = explode("/", $url[1]);

  $endPoint = $urlData[0];
  $data   = urldecode(@$urlData[1]);

  require_once $endPoint.'/'.$endPoint.'.php';


  switch ($_SERVER['REQUEST_METHOD']) {

    case "GET":
      if (isset($data) && $data != null){
        $result = $dataClass->get($data);
      }
      else {
        $result = $dataClass->query();
      }
    break;


    case "POST":
      $fetch = json_decode(file_get_contents("php://input"), false);
      $result = $dataClass->insert($fetch);
    break;

    case "PUT":
      $fetch = json_decode(file_get_contents("php://input"), false);

      if ($endPoint != 'trash') {
        if (is_object($fetch) || is_array($fetch)) {
          $result = $dataClass->update($fetch);
        }

        else {
          $result = $dataClass->delete($fetch);
        }
      }

      else {
        if ((is_object($fetch) || is_array($fetch)) && (!isset($fetch->action) || $fetch->action == null)) {
          $result = $dataClass->update($fetch);
        }

        else {
          $result = $dataClass->delete($fetch);
        }
      }
    break;
  }


  $json = json_encode($result);
  echo $json;

  return;
?>
