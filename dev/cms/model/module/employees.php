<?php date_default_timezone_set('Europe/Stockholm'); ?>
<?php
  require_once("employeesClass.php");

  switch ($_SERVER['REQUEST_METHOD']) {

    case "GET":
      $id = explode("api/employees/", $_SERVER['REQUEST_URI']);

      if (isset($id[1])){
        $result = $employeesClass->getSingle($id[1]);

      }
      else {
        $result = $employeesClass->getAll();
      }
    break;


    case "POST":
      $fetch = json_decode(file_get_contents("php://input"), false);
      $result = $employeesClass->insert($fetch);
    break;

    case "PUT":
       $fetch = json_decode(file_get_contents("php://input"), false);
       if (isset($fetch->data)) {
        $result = $employeesClass->update($fetch);
       }

       else {
        $result = $employeesClass->delete($fetch->employeesId);
       }
    break;
  }


  // Return the data
  $json = json_encode($result);
  echo $json;

  return;
?>
