<?php date_default_timezone_set('Europe/Stockholm'); ?>
<?php 
 require_once("../core/function/dbConnect.php");
 
  class data {
    function __construct(mysqli $mysqli) {
      $this->mysqli       = $mysqli;
    }

    function getSingle($data){
      $test = array(
        "test"  =>  "1"
      );

      return $test;
    }


    function getAll(){
      $allData = array();
      $query = "  SELECT      l.languageId
                  ,           l.languageNamePrimary
                  ,           l.languageCode
                  FROM        language l
                  ORDER BY    l.languageId ASC
               ";

      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);
      $this->stmt->execute();
      $this->stmt->bind_result($languageId, $languageName, $languageCode);
      
      while($this->stmt->fetch()){
        $thisData = (object) array(
          'languageId'      => $languageId,
          'languageName'    => $languageName,
          'languageCode'    => $languageCode
        );

        $allData[$languageCode] = $thisData;
      }
      
      return $allData;
    }

     // END OF CLASS
  } $data = new data($mysqli);








  switch ($_SERVER['REQUEST_METHOD']) {

    case "GET":
      $id = explode("api/languages/", $_SERVER['REQUEST_URI']);

      if (isset($id[1])){
        $result = $data->getSingle($id[1]);
      }
      else {
        $result = $data->getAll();
      }
    break;


      case "POST":  
    break;


    case "PUT":
       $fetch = json_decode(file_get_contents("php://input"), false);
       $result = $data->save($fetch);
 
    break; 
 
  }


  // Return the data
  $json = json_encode($result);
  echo $json;

  return;

?>