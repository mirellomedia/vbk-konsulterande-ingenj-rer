<?php
  $articleId = intval($data[0]->id);

  if (isset($data[0]->newPublishDate)) {
    $time = $data[0]->newPublishDate;
    $timestamp = strtotime($time);
    $setDate = date('Y-m-d', $timestamp);
  } else {
    $setDate = date('Y-m-d');
  }

  // update "article":
  $query = "UPDATE article
            SET    articleDate = ?
            WHERE  articleId   = ?
  ";

  $this->stmt = $this->mysqli->stmt_init();
  $this->stmt->prepare($query);

  $this->stmt->bind_param("si",
    $setDate,
    $articleId
  );

  $this->stmt->execute();
  $this->stmt->close();


  // update "articlecontent":
  $query = "UPDATE  articlecontent
            SET     articleHeader   = ?,
                    articleLink     = ?,
                    articleSum      = ?,
                    articleContent  = ?,
                    articleEdit     = ?
            WHERE   articleId       = ?
            AND     languageId      = ?
  ";

  $this->stmt = $this->mysqli->stmt_init();
  $this->stmt->prepare($query);
  $timestamp = date('Y-m-d H:i:s');

  foreach ($data as $translation){
    $this->stmt->bind_param("sssssii",
      $translation->header,
      $translation->urlslug,
      $translation->sum,
      $translation->htmlcontent,
      $timestamp,
      $articleId,
      $translation->languageId
    );

    $this->stmt->execute();
  }

  $this->stmt->close();
