<?php
  $timestamp  = date('Y-m-d');
  $data       = explode("?", $data);
  $articleId  = intval($data[0]);
  $languageId = intval($data[1]);

  $query = "  SELECT      ac.articleHeader
              ,           ac.articleLink
              ,           a.articleDate
              ,           ac.articleId
              ,           ac.articleSum
              ,           ac.articleContent
              FROM        articlecontent ac
              INNER JOIN  article a
              ON          a.articleId = ac.articleId
              WHERE       a.articleId = ?
              AND         ac.languageId = ?
           ";

  $this->stmt = $this->mysqli->stmt_init();
  $this->stmt->prepare($query);
  $this->stmt->bind_param("ii",
                          $articleId,
                          $languageId);
  $this->stmt->execute();
  $this->stmt->bind_result($articleHeader, $articleLink, $articleDate, $articleId, $articleSum, $articleContent);
  $this->stmt->fetch();
  $thisData = (object) array(
      'header'      => $articleHeader,
      'urlslug'     => $articleLink,
      'publishDate' => $articleDate,
      'status'      => 'Publicerad',
      'id'          => $articleId,
      'sidtyp'      => 'Nyhet',
      'sum'         => $articleSum,
      'htmlcontent' => $articleContent,
      'currentDate' => $timestamp
  );

  $data = $thisData;
  $this->stmt->close();
