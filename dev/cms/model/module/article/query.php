<?php
  $allData = array();
  $query = "  SELECT      ac.articleHeader
              ,           ac.articleLink
              ,           a.articleDate
              ,           ac.articleEdit
              ,           ac.articleId
              FROM        articlecontent ac
              INNER JOIN  article a
              ON          a.articleId = ac.articleId
              WHERE       ac.languageId = 1
              AND         a.visibility = 1
              ORDER BY    ac.articleDate ASC
           ";

  $this->stmt = $this->mysqli->stmt_init();
  $this->stmt->prepare($query);
  $this->stmt->execute();
  $this->stmt->bind_result($articleHeader, $articleLink, $articleDateRaw, $articleEditRaw, $articleId);

  while($this->stmt->fetch()) {

        $articleDate = explode(" ", $articleDateRaw);
        $articleDate = $articleDate[0];
        if($articleDate == date('Y-m-d')) {
          $articleDate = 'Idag';
        }
        else{
          $articleDate = $articleDate;
        }

        $articleEdit = explode(" ", $articleEditRaw);
        $articleEdit = $articleEdit[0];
        if($articleEdit == date('Y-m-d')) {
          $articleEdit = 'Idag';
        }
        else {
          $articleEdit = $articleEdit;
        }

        if($articleEdit === '0000-00-00'){ $articleEdit = 'Aldrig ändrad'; }

    $thisData = (object) array(
        'namn'      => $articleHeader,
        'lank'      => $articleLink,
        'skapades'  => $articleDate,
        'andrades'  => $articleEdit,
        'id'        => $articleId,
        'sidtyp'    => 'Nyhet'
    );
    array_push($allData, $thisData);
  }
  $this->stmt->close();

  $data = $allData;
