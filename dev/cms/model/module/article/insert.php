<?php

  // table "article"
  if ($fetch->date == 'getcurrent'){
    $timestamp = date('Y-m-d');
  }

  else {
    $time = $fetch->date;
    $old_date_timestamp = strtotime($time);
    $timestamp = date('Y-m-d', $old_date_timestamp);
  }

  $query = "INSERT INTO   article
          (articleDate) VALUES (?)
  ";

  $this->stmt = $this->mysqli->stmt_init();
  $this->stmt->prepare($query);
  $this->stmt->bind_param("s", $timestamp);
  $this->stmt->execute();
  $articleId = $this->stmt->insert_id;
  $this->stmt->close();



  // table "articlecontent"
  $query = "INSERT INTO articlecontent (
                          articleId,
                          languageId,
                          articleHeader,
                          articleLink,
                          articleSum,
                          articleContent,
                          articleDate
                        )
            VALUES      (
                          ?,
                          ?,
                          ?,
                          ?,
                          ?,
                          ?,
                          ?
                        )
  ";

  $this->stmt = $this->mysqli->stmt_init();
  $this->stmt->prepare($query);

  $this->stmt->bind_param("iisssss",
    $articleId,
    $fetch->languageId,
    $fetch->header,
    $fetch->urlslug,
    $fetch->sum,
    $fetch->htmlcontent,
    $timestamp
  );

  $this->stmt->execute();
  $this->stmt->close();
