<?php
  $articleId = intval($fetch);
  $timestamp = date('Y-m-d');

  $query = "UPDATE  article
            SET     visibility = 0,
                    deletedAt = ?
            WHERE   articleId = ?
  ";

  $this->stmt = $this->mysqli->stmt_init();
  $this->stmt->prepare($query);
  $this->stmt->bind_param("si", $timestamp, $articleId);
  $this->stmt->execute();

  $this->stmt->close();
