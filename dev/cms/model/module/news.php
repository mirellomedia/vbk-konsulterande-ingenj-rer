<?php date_default_timezone_set('Europe/Stockholm'); ?>
<?php
 require_once("../core/function/dbConnect.php");
 
  class data {
    function __construct(mysqli $mysqli) {
      $this->mysqli       = $mysqli;
    }

    function getSingle($data){
      $query = "  SELECT      ac.articleHeader
                  ,           ac.articleLink
                  ,           a.articleDate
                  ,           ac.articleId
                  ,           ac.articleSum
                  ,           ac.articleContent
                  FROM        articlecontent ac
                  INNER JOIN  article a
                  ON          a.articleId = ac.articleId
                  WHERE       a.articleId = ?
               ";

      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);
      $this->stmt->bind_param("i", $data);
      $this->stmt->execute();
      $this->stmt->bind_result($articleHeader, $articleLink, $articleDate, $articleId, $articleSum, $articleContent);
      $this->stmt->fetch();
      $thisData = (object) array(
          'header'      => $articleHeader,
          'urlslug'     => $articleLink,
          'created'     => $articleDate,
          'status'      => 'Publicerad',
          'id'          => $articleId,
          'sidtyp'      => 'Nyhet',
          'sum'         => $articleSum,
          'htmlcontent' => $articleContent
      );
      return $thisData;
    }

    function insert($data){
      if(isset($data->date)){
        if($data->date == 'getcurrent'){
          $timestamp = date('Y-m-d');
        }
        else{
          $time = $data->date;
          $old_date_timestamp = strtotime($time);
          $timestamp = date('Y-m-d', $old_date_timestamp);
        }

         $query = "INSERT INTO   article
                (articleDate) VALUES (?)
        ";

        $this->stmt = $this->mysqli->stmt_init();
        $this->stmt->prepare($query);
        $this->stmt->bind_param("s", $timestamp);
        $this->stmt->execute();
        $articleId = $this->stmt->insert_id;
        $this->stmt->close();
      }
      else{
        $query = "select max(articleId) from article";
        
        $this->stmt = $this->mysqli->stmt_init();
        $this->stmt->prepare($query);
        $this->stmt->execute();
        $this->stmt->bind_result($articleId);
        $this->stmt->fetch();

        $timestamp = date('Y-m-d');

        $query = "INSERT INTO   articlecontent

                  (             articleId,
                                languageId,
                                articleHeader,
                                articleLink,
                                articleSum,
                                articleContent,
                                articleDate
                  )
                  VALUES
                  (
                                ?,
                                ?,
                                ?,
                                ?,
                                ?,
                                ?,
                                ?
                  )
        ";

        $this->stmt = $this->mysqli->stmt_init();
        $this->stmt->prepare($query);


        foreach ($data as $translation){

          $this->stmt->bind_param("iisssss",
                                $articleId,
                                $translation->languageId,
                                $translation->header,
                                $translation->urlslug,
                                $translation->summary,
                                $translation->htmlcontent,
                                $timestamp
                               );

          $this->stmt->execute();
        }
        $NewsId = $this->stmt->insert_id;
          if($NewsId !== ''){
            $status = 'success';  
          }
        $this->stmt->close();
      }
      $status = 'success';
      $response = array(
        'response' => $status,
        'data'     => $data
      );
      
      return $response;
    }


    function getAll(){
      $allData = array();
      $query = "  SELECT      ac.articleHeader
                  ,           ac.articleLink
                  ,           a.articleDate
                  ,           ac.articleEdit
                  ,           ac.articleId
                  FROM        articlecontent ac
                  INNER JOIN  article a
                  ON          a.articleId = ac.articleId
                  WHERE       ac.languageId = 1
                  AND         a.visibility = 1
                  ORDER BY    ac.articleDate ASC
               ";

      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);
      $this->stmt->execute();
      $this->stmt->bind_result($articleHeader, $articleLink, $articleDateRaw, $articleEditRaw, $articleId);
      
      while($this->stmt->fetch()){

        // Modiferar datumsträngar vid önskemål
        $articleDate = explode(" ", $articleDateRaw);
        $articleDate = $articleDate[0];
        if($articleDate == date('Y-m-d')) { 
          $articleDate = 'Idag'; 
        }
        else{
          $articleDate = $articleDate;
        }

        $articleEdit = explode(" ", $articleEditRaw);
        $articleEdit = $articleEdit[0];
        if($articleEdit == date('Y-m-d')) { 
          $articleEdit = 'Idag'; 
        }
        else {
          $articleEdit = $articleEdit;
        }

        if($articleEdit === '0000-00-00'){ $articleEdit = 'Aldrig ändrad'; }

        $thisData = (object) array(
            'namn'      => $articleHeader,
            'lank'      => $articleLink,
            'skapades'  => $articleDate,
            'andrades'  => $articleEdit,
            'status'    => 'Publicerad',
            'id'        => $articleId,
            'sidtyp'    => 'Nyhet'
        );
        $allData[$thisData->id] = $thisData;
      }
      if(!$allData == null){
        return $allData;  
      }
      else{
        return false;
      }
      
    } // End of getAll

    function delete($newsId){
      $articleId = intval($newsId);

      $query = "UPDATE  article
                SET     visibility = 0
                WHERE   articleId = ?
      ";
      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);
      $this->stmt->bind_param("i", $articleId);
      $this->stmt->execute();
      $this->stmt->close();

      // $query = "DELETE FROM articlecontent
      //           WHERE       articleId = ?
      // ";
      // $this->stmt = $this->mysqli->stmt_init();
      // $this->stmt->prepare($query);
      // $this->stmt->bind_param("i", $articleId);
      // $this->stmt->execute();
      // $this->stmt->close();

      $status = 'success';
      $response = array(
        'response' => $status,
        'data'     => ""
      );
      
      return $response;
    }

    function update($data){
      $query = "UPDATE  articlecontent
                SET     articleHeader   = ?,
                        articleLink     = ?,
                        articleSum      = ?,
                        articleContent  = ?,
                        articleEdit     = ?
                WHERE   articleId       = ?
                AND     languageId      = ?

      ";

      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);
      $timestamp = date('Y-m-d H:i:s');

      foreach ($data->data as $translation){

        $articleId = intval($data->newsId);
        $this->stmt->bind_param("sssssii",
                              $translation->header,
                              $translation->urlslug,
                              $translation->summary,
                              $translation->htmlcontent,
                              $timestamp,
                              $articleId,
                              $translation->languageId

                             );

        $this->stmt->execute();
      }
      $this->stmt->close();

      $status = 'success'; 
      $response = array(
        'response' => $status,
        'data'     => $data
      );
      
      return $response;
    } // End of update

     // END OF CLASS
  } $data = new data($mysqli);





  switch ($_SERVER['REQUEST_METHOD']) {

    case "GET":
      $id = explode("api/news/", $_SERVER['REQUEST_URI']);

      if (isset($id[1])){
        $result = $data->getSingle($id[1]);

      }
      else {
        $result = $data->getAll();
      }
    break;


      case "POST":  
      $fetch = json_decode(file_get_contents("php://input"), false);
      $result = $data->insert($fetch);
    break;


    case "PUT":
       $fetch = json_decode(file_get_contents("php://input"), false);
       if(isset($fetch->data)){
        $result = $data->update($fetch);
       }
       else{
        $result = $data->delete($fetch->newsId);  
       }
    break; 
 
  }


  // Return the data
  $json = json_encode($result);
  echo $json;

  return;

?>