<?php date_default_timezone_set('Europe/Stockholm'); ?>
<?php
  require_once("employeegroupClass.php");

  switch ($_SERVER['REQUEST_METHOD']) {

    case "GET":
      $id = explode("api/group/", $_SERVER['REQUEST_URI']);

      if (isset($id[1])){
        $result = $employeegroupClass->getSingle($id[1]);
      }

      else {
        $result = $employeegroupClass->getAll();
      }
    break;


    case "POST":
      $fetch = json_decode(file_get_contents("php://input"), false);
      $result = $employeegroupClass->insert($fetch);
    break;

    case "PUT":
       $fetch = json_decode(file_get_contents("php://input"), false);
       if(isset($fetch->data)){
        $result = $employeegroupClass->update($fetch);
       }
       else{
        $result = $employeegroupClass->delete($fetch->pageId);
       }
    break;
  }


  // Return the data
  $json = json_encode($result);
  echo $json;

  return;
?>
