<?php date_default_timezone_set('Europe/Stockholm'); ?>
<?php 
 require_once("../core/function/dbConnect.php");
 
  class data {
    function __construct(mysqli $mysqli) {
      $this->mysqli       = $mysqli;
    }

    function getSingle($data){  $data = explode("?", $data);
      $articleId = intval($data[0]);
      $languageId = intval($data[1]);

      $query = "  SELECT      ac.articleHeader
                  ,           ac.articleLink
                  ,           ac.articleDate
                  ,           ac.articleId
                  ,           ac.articleSum
                  ,           ac.articleContent
                  FROM        articlecontent ac
                  INNER JOIN  article a
                  ON          a.articleId = ac.articleId
                  WHERE       a.articleId = ?
                  AND         ac.languageId = ?
               ";

      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);
      $this->stmt->bind_param("ii", 
                              $articleId,
                              $languageId);
      $this->stmt->execute();
      $this->stmt->bind_result($articleHeader, $articleLink, $articleDate, $articleId, $articleSum, $articleContent);
      $this->stmt->fetch();
      $thisData = (object) array(
          'header'      => $articleHeader,
          'urlslug'     => $articleLink,
          'created'     => $articleDate,
          'status'      => 'Publicerad',
          'id'          => $articleId,
          'sidtyp'      => 'Nyhet',
          'sum'         => $articleSum,
          'htmlcontent' => $articleContent
      );
      return $thisData;
    }

     // END OF CLASS
  } $data = new data($mysqli);

    function getAll(){
      $allData = array();
      $query = "  SELECT      ac.articleHeader
                  ,           ac.articleLink
                  ,           a.articleDate
                  ,           ac.articleEdit
                  ,           ac.articleId
                  FROM        articlecontent ac
                  INNER JOIN  article a
                  ON          a.articleId = ac.articleId
                  WHERE       ac.languageId = 1
                  AND         a.visibility = 1
                  ORDER BY    ac.articleDate ASC
               ";

      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);
      $this->stmt->execute();
      $this->stmt->bind_result($articleHeader, $articleLink, $articleDateRaw, $articleEditRaw, $articleId);
      
      while($this->stmt->fetch()){

        // Modiferar datumsträngar vid önskemål
        $articleDate = explode(" ", $articleDateRaw);
        $articleDate = $articleDate[0];
        if($articleDate == date('Y-m-d')) { 
          $articleDate = 'Idag'; 
        }
        else{
          $articleDate = $articleDate;
        }

        $articleEdit = explode(" ", $articleEditRaw);
        $articleEdit = $articleEdit[0];
        if($articleEdit == date('Y-m-d')) { 
          $articleEdit = 'Idag'; 
        }
        else {
          $articleEdit = $articleEdit;
        }

        if($articleEdit === '0000-00-00'){ $articleEdit = 'Aldrig ändrad'; }

        $thisData = (object) array(
            'namn'      => $articleHeader,
            'lank'      => $articleLink,
            'skapades'  => $articleDate,
            'andrades'  => $articleEdit,
            'status'    => 'Publicerad',
            'id'        => $articleId,
            'sidtyp'    => 'Nyhet'
        );
        $allData[$thisData->id] = $thisData;
      }
      if(!$allData == null){
        return $allData;  
      }
      else{
        return false;
      }
      
    } // End of getAll

    function delete($newsId){
      $articleId = intval($newsId);

      $query = "UPDATE  article
                SET     visibility = 0
                WHERE   articleId = ?
      ";
      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);
      $this->stmt->bind_param("i", $articleId);
      $this->stmt->execute();
      $this->stmt->close();

      // $query = "DELETE FROM articlecontent
      //           WHERE       articleId = ?
      // ";
      // $this->stmt = $this->mysqli->stmt_init();
      // $this->stmt->prepare($query);
      // $this->stmt->bind_param("i", $articleId);
      // $this->stmt->execute();
      // $this->stmt->close();

      $status = 'success';
      $response = array(
        'response' => $status,
        'data'     => ""
      );
      
      return $response;
    }






  switch ($_SERVER['REQUEST_METHOD']) {

    case "GET":
      $id = explode("api/news_languages/", $_SERVER['REQUEST_URI']);
      $result = $data->getSingle($id[1]);
    break;
  }


  // Return the data
  $json = json_encode($result);
  print_r($json);

  return;

?>