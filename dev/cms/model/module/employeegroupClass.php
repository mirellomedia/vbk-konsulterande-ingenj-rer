<?php date_default_timezone_set('Europe/Stockholm'); ?>
<?php
require_once("../core/function/dbConnect.php");

  class employeegroupClass {
    function __construct(mysqli $mysqli) {
      $this->mysqli       = $mysqli;
    }

    function getAll() {
      $allData = array();
      $query = "  SELECT      e.employeegroupId
                  ,           e.employeegroupName
                  FROM        employeegroup e
               ";

      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);
      $this->stmt->execute();
      $this->stmt->bind_result($employeegroupId, $employeegroupName);

      while($this->stmt->fetch()){
        $thisData = (object) array(
            'employeegroupId'        => $employeegroupId,
            'employeegroupName'      => $employeegroupName
        );
        array_push($allData, $thisData);
      }

      return $allData;
    }

     // END OF CLASS
  } $employeegroupClass = new employeegroupClass($mysqli);
?>
