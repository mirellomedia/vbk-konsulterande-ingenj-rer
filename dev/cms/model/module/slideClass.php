<?php date_default_timezone_set('Europe/Stockholm'); ?>
<?php

require_once("../core/function/dbConnect.php");
require_once("helpers/nils.php");

  class slideClass {
    function __construct(mysqli $mysqli, helperClass $helperClass) {
      $this->mysqli       = $mysqli;
      $this->helper       = $helperClass;
    }

    function getSingle($data){

    }

    function insert($data) {

      if($data->slideImage->status == 'changed') {
        $data->slideImage->fileName = $this->helper->cleanImage('project', $data->slideImage->fileName);
      }

      $query =  " INSERT INTO slideshow
                  SET         slideName = ?
                  ,           slideDescription = ?
                  ,           slideLink = ?
                ";

      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);

      $this->stmt->bind_param("sss",
        $data->projectName,
        $data->projectSummary,
        $data->slideImage->fileName
      );

      $this->stmt->execute();
      $this->stmt->close();

      return $data;
    }

    function update($data){
      $slideId = intval($data->slideId);

      if($data->status == 'delete') {
        $query = "DELETE FROM slideshow
                  WHERE       slideId = ?
        ";

        $this->stmt = $this->mysqli->stmt_init();

        $this->stmt->prepare($query);
        $this->stmt->bind_param("i",
          $slideId
        );

        $this->stmt->execute();
        $this->stmt->close();

        return $data;
      }

      if($data->slideImage->status == 'changed') {
        $data->slideImage->fileName = $this->helper->cleanImage('project', $data->slideImage->fileName);
      }

      $query =  " UPDATE  slideshow
                  SET     slideName = ?,
                          slideDescription = ?,
                          slideLink = ?
                  WHERE   slideId = ?
                ";

      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);

      $this->stmt->bind_param("sssi",
        $data->projectName,
        $data->projectSummary,
        $data->slideImage->fileName,
        $slideId
      );

      $this->stmt->execute();
      $this->stmt->close();

      return $data;
    }

    function getAll(){
      $allData = array();
      $query = "  SELECT      s.slideId
                  ,           s.slideName
                  ,           s.slideDescription
                  ,           s.slideLink
                  FROM        slideshow s
               ";

      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);
      $this->stmt->execute();
      $this->stmt->bind_result($slideId, $slideName, $slideDescription, $slideLink);

      while($this->stmt->fetch()){

        $slideImage = (object) array("fileName" => $slideLink, 'status' => 'unchanged');
        $thisData = (object) array(
            'slideId'           => $slideId,
            'projectName'       => $slideName,
            'projectSummary'    => $slideDescription,
            'slideImage'         => $slideImage
        );
        array_push($allData, $thisData);
      }

      return $allData;
    } // End of getAll

     // END OF CLASS
  } $slideClass = new slideClass($mysqli, $helperClass);

?>
