<?php date_default_timezone_set('Europe/Stockholm'); ?>
<?php

  require_once("pagesClass.php");

  switch ($_SERVER['REQUEST_METHOD']) {

    case "GET":
      $id = explode("api/pages/", $_SERVER['REQUEST_URI']);

      if (isset($id[1])){
        $result = $pagesClass->getSingle($id[1]);

      }
      else {
        $result = $pagesClass->getAll();
      }
    break;

    case "POST":
      $fetch = json_decode(file_get_contents("php://input"), false);
      $result = $pagesClass->insert($fetch);
    break;

    case "PUT":
       $fetch = json_decode(file_get_contents("php://input"), false);
       if(isset($fetch->data)){
        $result = $pagesClass->update($fetch);
       }
       else{
        $result = $pagesClass->delete($fetch->dataId);
       }
    break;
  }


  // Return the data
  $json = json_encode($result);
  echo $json;

  return;

?>
