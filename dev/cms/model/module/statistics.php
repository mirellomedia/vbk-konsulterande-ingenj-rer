<?php date_default_timezone_set('Europe/Stockholm'); ?>
<?php
  $path = dirname(dirname(dirname(__FILE__)));
  $path = $path."/model/core/function/dbConnect.php";
  require_once($path);

  class statistics {

    function __construct(mysqli $mysqli) {
      $this->mysqli       = $mysqli;
      $this->getStatistics();
    }

    private function getStatistics(){
      $this->articleCount();
      $this->referenceCount();
      $this->employeeCount();
      $this->pageCount();
    }

    private function articleCount(){
      $query = "  SELECT      COUNT(*)
                  FROM        article
           ";

      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);
      $this->stmt->execute();
      $this->stmt->bind_result($this->articleCount);
      $this->stmt->fetch();
      $this->stmt->close();
    }

    private function referenceCount(){
      $query = "  SELECT      COUNT(*)
                  FROM        project
           ";

      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);
      $this->stmt->execute();
      $this->stmt->bind_result($this->referenceCount);
      $this->stmt->fetch();
      $this->stmt->close();
    }

    private function employeeCount(){
      $query = "  SELECT      COUNT(*)
                  FROM        employee
           ";

      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);
      $this->stmt->execute();
      $this->stmt->bind_result($this->employeeCount);
      $this->stmt->fetch();
      $this->stmt->close();
    }

    private function pageCount(){
      $query = "  SELECT      COUNT(*)
                  FROM        page
           ";

      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);
      $this->stmt->execute();
      $this->stmt->bind_result($this->pageCount);
      $this->stmt->fetch();
      $this->stmt->close();
    }

    function latestProject() {
      $query = "  SELECT      pc.projectName
                  ,           pc.projectLongDesc
                  ,           pc.projectLink
                  ,           pc.projectId
                  ,           pi.projectImageLink
                  FROM        projectcontent pc
                  LEFT JOIN   projectimage pi
                  ON          pc.projectId = pi.projectId
                  ORDER BY    pc.projectId DESC
                  LIMIT       1
              ";

      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);
      $this->stmt->execute();

      $this->stmt->bind_result(
        $projectName,
        $projectLongDesc,
        $projectLink,
        $projectId,
        $projectImage
      );

      $this->stmt->fetch();

      if ($projectImage == false) {
        $projectImage = 'placeholder.jpg';
      }

      $thisData = (object) array(
        'projectName'      => $projectName,
        'projectLongDesc'  => $projectLongDesc,
        'projectLink'      => $projectLink,
        'projectId'        => $projectId,
        'projectImage'     => $projectImage
      );

      $this->stmt->close();

      return $thisData;
    }

    function latestArticle() {
      $query = "  SELECT      ac.articleHeader
                  ,           ac.articleContent
                  ,           ac.articleLink
                  ,           ac.articleId
                  FROM        articlecontent ac
                  INNER JOIN  article a
                  ON          a.articleId = ac.articleId
                  WHERE       a.visibility = 1
                  ORDER BY    ac.articleId DESC
                  LIMIT       1
              ";

      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);
      $this->stmt->execute();

      $this->stmt->bind_result(
        $articleHeader,
        $articleContent,
        $articleLink,
        $articleId
      );

      $this->stmt->fetch();

      $thisData = (object) array(
        'articleHeader'   => $articleHeader,
        'articleContent'  => $articleContent,
        'articleLink'     => $articleLink,
        'articleId'       => $articleId
      );

      $this->stmt->close();

      return $thisData;
    }

    function latestEmployees() {
      $employees = array();

      $query = "  SELECT      e.employeeName
                  ,           e.employeePhone
                  ,           e.employeeEmail
                  ,           e.employeeImage
                  ,           e.employeeId
                  FROM        employee e
                  ORDER BY    e.employeeId DESC
                  LIMIT       3
              ";

      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);
      $this->stmt->execute();

      $this->stmt->bind_result(
        $employeeName,
        $employeePhone,
        $employeeEmail,
        $employeeImage,
        $employeeId
      );

      while($this->stmt->fetch()) {
        $employee = (object) array(
          'name'    => $employeeName,
          'phone'   => $employeePhone,
          'email'   => $employeeEmail,
          'image'   => $employeeImage,
          'id'      => $employeeId
        );


        array_push($employees, $employee);

      }

      $this->stmt->close();

      return $employees;
    }


     // END OF CLASS
  } $statistics = new statistics($mysqli);
?>
