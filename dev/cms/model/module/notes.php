<?php date_default_timezone_set('Europe/Stockholm'); ?>
<?php 


 require_once("../core/function/dbConnect.php");
 
  class data {
    function __construct(mysqli $mysqli) {
      $this->mysqli       = $mysqli;
    }

    function getSingle(){
      $query = "UPDATE      employeeNotices
                SET         noticeStatus = 1
      ";

      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);

      $this->stmt->execute();
      $this->stmt->close();

      return 'success';
    }

    function remove($data){
      $query = "UPDATE      employeeNotices
                SET         noticeStatus = 0
                WHERE       noticeId = ?
      ";

      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);

      $this->stmt->bind_param("i",
                              $data
                             );

      $this->stmt->execute();
      $this->stmt->close();

      return 'success';
    }

    function getAll(){
      $path = '../session';

      session_save_path($path);
      session_start();

      $allData = array();
      $query = "  SELECT      en.noticeMessage
                  ,           en.noticeLink
                  ,           en.noticeType
                  ,           en.noticeId
                  FROM        employeeNotices en
                  WHERE       en.employeeId = ?
                  AND         en.noticeStatus = 1
               ";

      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);
      $this->stmt->bind_param("i", $_SESSION['userid']);
      $this->stmt->execute();
      $this->stmt->bind_result($noticeMessage, $noticeLink, $noticeType, $noticeId);
      
      while($this->stmt->fetch()){
        $thisData = (object) array(
            'message'             => $noticeMessage,
            'link'                => $noticeLink,
            'status'              => $noticeType,
            'noticeId'            => $noticeId
        );
        $allData[$thisData->noticeId] = $thisData;
      }
      $this->stmt->close();

      
      // Om det inte finns några notiser så avbryter vi script för att inte skicka ut NULL (4 chars, 4 notiser).
      if($allData == null){
        exit();
      }
      else{
        return $allData;
      }
    } // End of getAll


     // END OF CLASS
  } $data = new data($mysqli);








  switch ($_SERVER['REQUEST_METHOD']) {

    case "GET":
      $id = explode("api/notes/", $_SERVER['REQUEST_URI']);

      if (isset($id[1])){
        $result = $data->getSingle($id[1]);

      }
      else {
        $result = $data->getAll();
      }
    break;


      case "POST":  
      $fetch = json_decode(file_get_contents("php://input"), false);
      $result = $data->remove($fetch);
    break;


    case "PUT":
       $fetch = json_decode(file_get_contents("php://input"), false);
       $result = $data->save($fetch);
 
    break; 
 
  }


  // Return the data
  $json = json_encode($result);
  echo $json;

  return;

?>