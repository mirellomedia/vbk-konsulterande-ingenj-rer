<?php date_default_timezone_set('Europe/Stockholm'); ?>
<?php
 require_once("../core/function/dbConnect.php");
 require_once("helpers/nils.php");

  class project {
    function __construct(mysqli $mysqli, helperClass $helperClass) {
      $this->mysqli = $mysqli;
      $this->helper = $helperClass;
    }

    public function get($data) {
      include 'get.php';
      return $data;
    }

    public function query() {
      include 'query.php';
      return $data;
    }

    public function insert($fetch) {
      include 'insert.php';
      return $data;
    }

    public function delete($fetch) {
      include 'delete.php';
      return $data;
    }

    public function update($data) {
      include 'update.php';
      return $data;
    }

  } $dataClass = new project($mysqli, $helperClass);
?>
