<?php

  // table "project"
  if ($fetch->projectType != 1) {
    $fetch->projectCategory = 0;
  }

  $timestamp = date('Y-m-d');

  $query = "INSERT INTO project
    (projectType, projectCategory, projectAdded) VALUES (?,?,?)
  ";

  $this->stmt = $this->mysqli->stmt_init();
  $this->stmt->prepare($query);
  $this->stmt->bind_param("iis", $fetch->projectType, $fetch->projectCategory, $timestamp);
  $this->stmt->execute();
  $projectId = $this->stmt->insert_id;
  $this->stmt->close();



  // table "projectcontent"
  $query = "INSERT INTO projectcontent (
                          projectId,
                          languageId,
                          projectName,
                          projectLink,
                          projectShortDesc,
                          projectLongDesc
                        )
            VALUES      (
                          ?,
                          ?,
                          ?,
                          ?,
                          ?,
                          ?
                        )
  ";

  $this->stmt = $this->mysqli->stmt_init();
  $this->stmt->prepare($query);

  $this->stmt->bind_param("iissss",
    $projectId,
    $fetch->languageId,
    $fetch->projectName,
    $fetch->urlslug,
    $fetch->projectShortDesc,
    $fetch->projectLongDesc
  );

  $this->stmt->execute();
  $this->stmt->close();


  // insert images
  $projectImages = $fetch->projectImages;

  foreach($projectImages as $key => $projectImage) {
    $projectImage->fileName = $this->helper->cleanImage('project', $projectImage->fileName);

    $query =  " INSERT INTO projectimage (
                              projectId,
                              projectimageLink,
                              projectimageOrder
                            )

                VALUES      (
                              ?,
                              ?,
                              10
                            )
              ";

    $this->stmt = $this->mysqli->stmt_init();
    $this->stmt->prepare($query);

    $this->stmt->bind_param("is",
      $projectId,
      $projectImage->fileName
    );

    $this->stmt->execute();
    $projectImageId = $this->stmt->insert_id;
    $this->stmt->close();
  }
