<?php
  $projectId = intval($fetch);
  $timestamp = date('Y-m-d');

  $query = "UPDATE  project
            SET     visibility = 0,
                    deletedAt = ?
            WHERE   projectId = ?
  ";

  $this->stmt = $this->mysqli->stmt_init();
  $this->stmt->prepare($query);
  $this->stmt->bind_param("si", $timestamp, $projectId);
  $this->stmt->execute();

  $this->stmt->close();
