<?php
  $projectId = intval($data[0]->projectId);
  $projectType = intval($data[0]->projectType);
  $projectCategory = intval($data[0]->projectCategory);

  // table "projectcontent"
  $query = " UPDATE  projectcontent
             SET     projectName      = ?,
                     projectLink      = ?,
                     projectShortDesc = ?,
                     projectLongDesc  = ?
             WHERE   projectId        = ?
             AND     languageId       = ?
  ";

  $this->stmt = $this->mysqli->stmt_init();
  $this->stmt->prepare($query);

  foreach ($data as $translation) {
    $this->stmt->bind_param("ssssii",
                          $translation->projectName,
                          $translation->projectLink,
                          $translation->projectShortDesc,
                          $translation->projectLongDesc,
                          $projectId,
                          $translation->languageId

                         );

    $this->stmt->execute();
  }

  $this->stmt->close();


  // table "project"
  $query = "UPDATE  project
            SET     projectType       = ?,
                    projectCategory   = ?
            WHERE   projectId         = ?
  ";

  $this->stmt = $this->mysqli->stmt_init();
  $this->stmt->prepare($query);

  foreach ($data as $translation) {
    $this->stmt->bind_param('iii',
      $projectType,
      $projectCategory,
      $projectId
    );

    $this->stmt->execute();
  }

  $this->stmt->close();

$projectImages = $data[0]->projectImages;

foreach($projectImages as $key => $projectImage) {
  switch($projectImage->status) {

    case "unchanged":
    break;

    // $projectImage->projectimageOrder

    case "changed":
      $query =  " UPDATE  projectimage
                  SET     projectimageOrder = ?
                  WHERE   projectimageId = ?
                ";

      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);

      $this->stmt->bind_param("ii", $projectImage->projectimageOrder, $projectImage->projectimageId);
      $this->stmt->execute();
      $this->stmt->close();
    break;

    case "new":
      $projectImage->fileName = $this->helper->cleanImage('project', $projectImage->fileName);

      $query =  " INSERT INTO   projectimage
                             (  projectId,
                                projectimageLink,
                                projectimageOrder
                             )
                  VALUES  (?,?,?)
                ";
      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);
      $this->stmt->bind_param("isi", $projectId, $projectImage->fileName, $projectImage->projectimageOrder);
      $this->stmt->execute();
      $projectImageId = $this->stmt->insert_id;
      $this->stmt->close();

    break;

    case "delete":
      unlink('../../../uploads/images/'.$projectImage->fileName);

      $query =  " DELETE FROM projectimage
                  WHERE       projectimageId = ?
                ";
      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);
      $this->stmt->bind_param("i", $projectImage->projectimageId);
      $this->stmt->execute();
      $this->stmt->close();
      unset($data[0]->projectImages[$key]);
    break;

  }


}
