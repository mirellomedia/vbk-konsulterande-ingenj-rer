<?php
  $data       = explode("?", $data);
  $projectId  = intval($data[0]);
  $languageId = intval($data[1]);

  $query = "  SELECT      pc.projectName
              ,           pc.projectLink
              ,           pc.projectShortDesc
              ,           pc.projectLongDesc
              ,           p.projectCategory
              ,           p.projectType
              ,           p.projectId
              FROM        projectcontent pc
              INNER JOIN  project p
              ON          p.projectId = pc.projectId
              WHERE       p.projectId = ?
              AND         pc.languageId = ?
           ";

  $this->stmt = $this->mysqli->stmt_init();
  $this->stmt->prepare($query);
  $this->stmt->bind_param("ii",
                          $projectId,
                          $languageId);
  $this->stmt->execute();
  $this->stmt->bind_result($projectName, $projectLink, $projectShortDesc, $projectLongDesc, $projectCategory, $projectType, $projectId);
  $this->stmt->fetch();

  if ($projectCategory === 0) {
    $projectCategory = null;
  }

  $thisData = (object) array(
      'projectName'           => $projectName,
      'projectLink'           => $projectLink,
      'projectShortDesc'      => $projectShortDesc,
      'projectLongDesc'       => $projectLongDesc,
      'projectCategory'       => $projectCategory,
      'projectType'           => $projectType,
      'projectId'             => $projectId,
      'projectImages'         => []
  );

  $query = "  SELECT      pi.projectimageLink
              ,           pi.projectimageOrder
              ,           pi.projectimageId
              FROM        projectimage pi
              WHERE       pi.projectId = ?
              ORDER BY    pi.projectimageOrder
           ";

  $this->stmt = $this->mysqli->stmt_init();
  $this->stmt->prepare($query);
  $this->stmt->bind_param("i", $projectId);
  $this->stmt->execute();
  $this->stmt->bind_result($projectimageLink, $projectimageOrder, $projectimageId);

  while($this->stmt->fetch()){
    $thisData->projectImages[] = (object) array(
        'fileName'              => $projectimageLink,
        'projectimageOrder'     => $projectimageOrder,
        'projectimageId'        => $projectimageId,
        'status'                => 'unchanged'
    );
  }

  $this->stmt->close();

  $data = $thisData;
