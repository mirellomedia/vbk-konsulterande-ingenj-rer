<?php
  $projects = array();
  $query = "  SELECT      p.projectId
              ,           p.projectAdded
              ,           pc.projectName
              ,           pc.projectLink
              FROM        project p
              INNER JOIN  projectcontent pc
              ON          p.projectId = pc.projectId
              WHERE       p.visibility = 1
              ORDER BY    p.projectId DESC
           ";

  $this->stmt = $this->mysqli->stmt_init();
  $this->stmt->prepare($query);
  $this->stmt->execute();
  $this->stmt->bind_result($projectId, $projectAdded, $projectName, $projectLink);

  while($this->stmt->fetch()) {
    $project = (object) array(
      'projectId'        => $projectId,
      'projectAdded'     => $projectAdded,
      'projectName'      => $projectName,
      'projectLink'      => $projectLink
    );

    array_push($projects, $project);
  }

  $this->stmt->close();

  $data = $projects;
