<?php date_default_timezone_set('Europe/Stockholm'); ?>
<?php

require_once("../core/function/dbConnect.php");

class projectClass {
    function __construct(mysqli $mysqli) {
      $this->mysqli       = $mysqli;
    }

    function getSingle($data){
      $data = explode("?", $data);
      $projectId = intval($data[0]);
      $languageId = intval($data[1]);

      $query = "  SELECT      pc.projectName
                  ,           pc.projectLink
                  ,           pc.projectShortDesc
                  ,           pc.projectLongDesc
                  ,           p.projectCategory
                  ,           p.projectType
                  FROM        projectcontent pc
                  INNER JOIN  project p
                  ON          p.projectId = pc.projectId
                  WHERE       p.projectId = ?
                  AND         pc.languageId = ?
               ";

      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);
      $this->stmt->bind_param("ii",
                              $projectId,
                              $languageId);
      $this->stmt->execute();
      $this->stmt->bind_result($projectName, $projectLink, $projectShortDesc, $projectLongDesc, $projectCategory, $projectType);
      $this->stmt->fetch();
      $thisData = (object) array(
          'projectName'           => $projectName,
          'projectLink'           => $projectLink,
          'projectShortDesc'      => $projectShortDesc,
          'projectLongDesc'       => $projectLongDesc,
          'projectCategory'       => $projectCategory,
          'projectType'           => $projectType
      );

      $allData = array();
      $query = "  SELECT      pi.projectimageLink
                  ,           pi.projectimageOrder
                  ,           pi.projectimageId
                  FROM        projectimage pi
                  WHERE       pi.projectId = ?
               ";

      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);
      $this->stmt->bind_param("i", $projectId);
      $this->stmt->execute();
      $this->stmt->bind_result($projectimageLink, $projectimageOrder, $projectimageId);

      while($this->stmt->fetch()){
        $imageData = (object) array(
            'projectimageLink'         => $projectimageLink,
            'projectimageOrder'        => $projectimageOrder,
            'projectimageId'           => $projectimageId
        );
        $allData[] = $imageData;
      }
      $thisData->images = $allData;

      return $thisData;
    }

    function clean($string) {
      $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

      return preg_replace('/[^A-Za-z0-9\-\_]/', '', $string); // Removes special chars.
    }

    function imageMove($filename){
      // Hanterar headerbilden
          $fileName = pathinfo($filename);

          $imageName = $fileName['filename'];
          $imageType  = $fileName['extension'];

          $new_filename = $this->clean($imageName);
          $new_filename = $new_filename.'.'.$imageType;

        $temp = '../../../uploads/tmp/';
        $dir  = '../../../uploads/images/';

        rename($temp.$filename, $dir.$new_filename);
      }






    function insert($data) {
      $getId = "select max(projectId) from project";

      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($getId);
      $this->stmt->execute();
      $this->stmt->bind_result($projectId);
      $this->stmt->fetch();

      // if (isset($data->data->projectCategory)) {

      //   if ($data->data->projectType !== 1) {
      //     $data->data->projectCategory = 0;
      //   }

        $query = "INSERT INTO project
                  (
                              projectId
                  )
                  VALUES
                  (
                              ?
                  )

        ";

        $this->stmt = $this->mysqli->stmt_init();
        $this->stmt->prepare($query);
        $this->stmt->bind_param("i",
                                $projectId
                               );

        $this->stmt->execute();
        $this->stmt->close();
      // }

      return $data;
    }






    function update($data){

      $projectId = intval($data->projectId);

      if(isset($data->data->projectCategory)){

        if($data->data->projectType !== 1){
          $data->data->projectCategory = 0;
        }

        $query = "UPDATE  project
                  SET     projectType        = ?,
                          projectCategory    = ?
                  WHERE   projectId          = ?

        ";

        $this->stmt = $this->mysqli->stmt_init();
        $this->stmt->prepare($query);
        $this->stmt->bind_param("iii",
                                $data->data->projectType,
                                $data->data->projectCategory,
                                $projectId
                               );

        $this->stmt->execute();
        $this->stmt->close();

      }
      else{

        $projectId = intval($data->projectId);
        $query = "UPDATE  projectcontent
                  SET     projectName        = ?,
                          projectLink        = ?,
                          projectShortDesc   = ?,
                          projectLongDesc    = ?
                  WHERE   projectId          = ?
                  AND     languageId         = ?

        ";

        $this->stmt = $this->mysqli->stmt_init();
        $this->stmt->prepare($query);
        foreach ($data->data as $translation){


          $this->stmt->bind_param("ssssii",
                                $translation->projectName,
                                $translation->projectLink,
                                $translation->projectShortDesc,
                                $translation->projectLongDesc,
                                $projectId,
                                $translation->languageId
                               );

          $this->stmt->execute();
        }

        $this->stmt->close();

        $query = "INSERT INTO projectimage
                  (
                              projectId,
                              projectImageLink
                  )
                  VALUES
                  (
                    ?,
                    ?
                  )

        ";

        $this->stmt = $this->mysqli->stmt_init();
        $this->stmt->prepare($query);

        foreach ($data->data[0]->image as $image){
          $this->stmt->bind_param("is",
                                $projectId,
                                $image
                               );

          $this->stmt->execute();
        }

        $this->stmt->close();

      }

      $status = 'success';
      $response = array(
        'response' => $status,
        'data'     => $data
      );

      return $response;
    }

    function getAll(){
      $projects = array();
      $query = "  SELECT      p.projectId
                  ,           p.projectAdded
                  ,           pc.projectName
                  ,           pc.projectLink
                  FROM        project p
                  INNER JOIN  projectcontent pc
                  ON          p.projectId = pc.projectId
                  ORDER BY    p.projectId DESC
               ";

      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);
      $this->stmt->execute();
      $this->stmt->bind_result($projectId, $projectAdded, $projectName, $projectLink);

      while($this->stmt->fetch()){
        $project = (object) array(
            'projectId'        => $projectId,
            'projectAdded'     => $projectAdded,
            'projectName'      => $projectName,
            'projectLink'      => $projectLink
        );


        array_push($projects, $project);
      }

      return $projects;
    }


     // END OF CLASS
  } $dataClass = new projectClass($mysqli);
?>
