<?php
  $data       = explode("?", $data);
  $pageId     = intval($data[0]);
  $languageId = intval($data[1]);

  $query = "  SELECT      pc.pageName
              ,           pc.pageLink
              ,           pc.pageId
              ,           pc.pageContent
              ,           pc.pageHeaderimage
              ,           p.pageTemplate
              ,           p.pageParent
              FROM        pagecontent pc
              INNER JOIN  page p
              ON          p.pageId = pc.pageId
              WHERE       p.pageId = ?
              AND         pc.languageId = ?
           ";

  $this->stmt = $this->mysqli->stmt_init();
  $this->stmt->prepare($query);
  $this->stmt->bind_param("ii",
                          $pageId,
                          $languageId);
  $this->stmt->execute();
  $this->stmt->bind_result(
    $pageName,
    $pageLink,
    $pageId,
    $pageContent,
    $pageHeaderimage,
    $pageTemplate,
    $pageParent
  );

  $this->stmt->fetch();
  $thisData = (object) array(
      'header'   => $pageName,
      'url'      => $pageLink,
      'id'       => $pageId,
      'body'     => $pageContent,
      'image'    => $pageHeaderimage,
      'template' => $pageTemplate,
      'parent'   => intval($pageParent)
  );

  return $thisData;
