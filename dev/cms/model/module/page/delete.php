<?php
  $pageId = intval($fetch);
  $timestamp = date('Y-m-d');

  $query = "UPDATE  page
            SET     visibility = 0,
                    deletedAt = ?
            WHERE   pageId = ?
  ";

  $this->stmt = $this->mysqli->stmt_init();
  $this->stmt->prepare($query);
  $this->stmt->bind_param("si", $timestamp, $pageId);
  $this->stmt->execute();

  $this->stmt->close();
