<?php
  $pages = array();
  $query = "SELECT      p.pageId
            ,           p.pageTemplate
            ,           p.pageParent
            ,           p.pageMenu
            ,           pc.pageName
            ,           pc.pageLink
            FROM        page p
            INNER JOIN  pagecontent pc
            ON          p.pageId = pc.pageId
            WHERE       pc.languageId = 1
            AND         p.visibility = 1
            ORDER BY    p.pageParent
           ";

  $this->stmt = $this->mysqli->stmt_init();
  $this->stmt->prepare($query);
  $this->stmt->execute();
  $this->stmt->bind_result($pageId, $pageTemplate, $pageParent, $pageMenu, $pageName, $pageLink);

  while($this->stmt->fetch()){
    $page = (object) array(
      'pageId'        => $pageId,
      'pageTemplate'  => $pageTemplate,
      'pageParent'    => intval($pageParent),
      'pageMenu'      => $pageMenu,
      'pagename'      => $pageName,
      'pageLink'      => $pageLink
    );

    if ($pageParent === 0) {
      // todo: fix these
      $page->sub = array();
      array_push($pages, $page);
    }

    else {
      array_push($pages[$pageParent]->sub, $page);
    }
  }

  $this->stmt->close();

  $data = $pages;
