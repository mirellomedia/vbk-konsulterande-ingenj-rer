<?php date_default_timezone_set('Europe/Stockholm'); ?>
<?php 
 require_once("projectCategoryClass.php");

// THE FETCHY PART
  
  switch ($_SERVER['REQUEST_METHOD']) {

    case "GET":
      $id = explode("api/projectcategories/", $_SERVER['REQUEST_URI']);

      if (isset($id[1])){
        $result = $projectCategoryClass->getSingle($id[1]);

      }
      else {
        $result = $projectCategoryClass->getAll();
      }
    break;


    case "POST":  
      $fetch = json_decode(file_get_contents("php://input"), false);
      $result = $projectCategoryClass->insert($fetch);
    break;


    case "PUT":
       $fetch = json_decode(file_get_contents("php://input"), false);
       $result = $projectCategoryClass->update($fetch);
 
    break; 
 
  }


  // Return the data
  $json = json_encode($result);
  echo $json;

  return;
?>