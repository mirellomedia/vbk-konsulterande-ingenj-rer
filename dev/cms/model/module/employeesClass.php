<?php date_default_timezone_set('Europe/Stockholm'); ?>
<?php
require_once("../core/function/dbConnect.php");
require_once("helpers/nils.php");

  class employeesClass {
    function __construct(mysqli $mysqli, helperClass $helperClass) {
      $this->mysqli       = $mysqli;
      $this->helper       = $helperClass;
    }

    function getSingle($data) {
      $query = "SELECT      e.employeeName
                ,           e.employeePhone
                ,           e.employeeEmail
                ,           e.employeeImage
                ,           e.employeeCity
                ,           e.employeeId
                ,           e.employeeTitle
                ,           ed.employeeId
                FROM        employee e
                LEFT JOIN   employeedata ed
                ON          ed.employeeId = e.employeeId
                WHERE       e.employeeId = ?
               ";

      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);
      $this->stmt->bind_param("i", $data);
      $this->stmt->execute();

      $this->stmt->bind_result(
        $employeeName,
        $employeePhone,
        $employeeEmail,
        $employeeImage,
        $employeeCity,
        $employeeId,
        $employeeTitle,
        $login
      );

      $this->stmt->fetch();

      $image = (object) array('fileName' => $employeeImage, 'status' => 'unchanged');

      if ($login) {
        $login = true;
      }

      $thisData = (object) array(
        'employeeName'       => $employeeName,
        'employeePhone'      => $employeePhone,
        'employeeEmail'      => $employeeEmail,
        'employeeImage'      => $image,
        'employeeCity'       => $employeeCity,
        'employeeId'         => $employeeId,
        'employeeTitle'      => $employeeTitle,
        'login'              => $login
      );

      $this->stmt->close();

      $query = "  SELECT      eeg.employeegroupId
                  FROM        employee_employeegroup eeg
                  WHERE       eeg.employeeId = ?
               ";

      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);
      $this->stmt->bind_param("i", $data);
      $this->stmt->execute();
      $this->stmt->bind_result($employeegroupId);

      while($this->stmt->fetch()){
        $thisData->employeeGroups[] = $employeegroupId;
      }


      return $thisData;
    }

    function clean($string) {
       $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

       return preg_replace('/[^A-Za-z0-9\-\_]/', '', $string); // Removes special chars.
    }

    function imageMove($filename){
      // Hanterar headerbilden
          $imageParts = explode('.', $filename);
          $imageType  = end($imageParts);
          array_pop($imageParts);

          $imageName    = implode($imageParts);
          $new_filename = $this->clean($imageName);
          $new_filename = $new_filename.'.'.$imageType;

        $temp = '../../../uploads/tmp/';
        $dir  = '../../../uploads/employees/';

        rename($temp.$filename, $dir.$new_filename);
    }

    function insert($data){

      // Säkerhet
      $data->id = intval($data->id); sleep(1);

      // Fixar länken till bilden
      if ($data->employeeImage->fileName) {
        $employeeLink = $this->clean($data->data->employeeName);
        $employeeImage = $employeeLink . '.jpg';
        $data->employeeImage->fileName = $data->employeeImage->fileName;
      }
      else {
        $data->employeeImage->fileName = 'placeholder.jpg';
      }

      // // Om namnet på personen är bytt så byter vi namn på bildfilen
      // if(isset($data->image) && $data->image != $employeeImage){
      //   $dir = '../../../uploads/employees/';

      //   rename($dir.$data->image, $dir.$employeeImage);
      //   $data->image2 = $data->image;
      //   $data->image = $employeeImage;
      // }

        //require_once(dirname(__DIR__).'/module/mailTemplates/orderconfirmation.php');
        $query = "  INSERT INTO   employee
                    (             employeeName,
                                  employeeTitle,
                                  employeePhone,
                                  employeeEmail,
                                  employeeImage,
                                  employeeCity
                    )
                    VALUES
                    (
                                  ?,
                                  ?,
                                  ?,
                                  ?,
                                  ?,
                                  ?
                    )
        ";

      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);
      $this->stmt->bind_param("ssssss",
                              $data->employeeName,
                              $data->employeeTitle,
                              $data->employeePhone,
                              $data->employeeEmail,
                              $data->employeeImage->fileName,
                              $data->employeeCity
                             );

      $this->stmt->execute();
      $employeeId = $this->stmt->insert_id;
      $this->stmt->close();

      $query =  " INSERT INTO employee_employeegroup
                  SET         employeeId = ?,
                              employeegroupId = ?
                ";
      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);

      foreach($data->employeeGroups as $group){
        $this->stmt->bind_param("ii", $employeeId, $group);
        $this->stmt->execute();
      }
      $this->stmt->close();


        if($data->login == true){
          $generatedPassword = substr(str_shuffle('0123456789abcdefghijklmnopqrstuvwxyz'), 0, 8);
          $generatedPasswordhashed = password_hash($generatedPassword, PASSWORD_DEFAULT);
          require_once(dirname(__DIR__).'/module/mailTemplates/orderconfirmation.php');

          $query = "INSERT INTO   employeedata
                    (             employeeId,
                                  employeePassword,
                                  employeeVerified
                    )
                    VALUES
                    (
                                  ?,
                                  ?,
                                  1
                    )
          ";

          $this->stmt = $this->mysqli->stmt_init();
          $this->stmt->prepare($query);
          $this->stmt->bind_param("is",
                                  $employeeId,
                                  $generatedPasswordhashed
                                 );

          $this->stmt->execute();
          $this->stmt->close();

          // $query = "INSERT INTO   employeeNotices
          //           (             employeeId,
          //                         noticeMessage,
          //                         noticeLink,
          //                         noticeType,
          //                         noticeStatus
          //           )
          //           VALUES
          //           (
          //                         ?,
          //                         ?,
          //                         ?,
          //                         ?,
          //                         ?
          //           )
          // ";

          // $link = 'installningar';
          // $type = 'warning';
          // $message = 'Eftersom att detta är din första inloggning uppmanar vid dig till att byta lösenord.';
          // $status = '1';

          // $this->stmt = $this->mysqli->stmt_init();
          // $this->stmt->prepare($query);
          // $this->stmt->bind_param("issss",
          //                         $employeeId,
          //                         $message,
          //                         $link,
          //                         $type,
          //                         $status
          //                        );

          // $this->stmt->execute();
          // $this->stmt->close();
        }

    return $data;

    }

    function update($data) {


      $query = "  SELECT      e.employeeName
                  FROM        employee e
                  WHERE       e.employeeId = ?
               ";

      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);
      $this->stmt->bind_param("i", $data->employeeId);
      $this->stmt->execute();
      $this->stmt->bind_result($employeeName);
      $this->stmt->fetch();
      $this->stmt->close();

      $employeeLink = $this->clean($data->data->employeeName);
      $employeeImage = $employeeLink . '.jpg';

      // Om namnet på personen är bytt så byter vi namn på bildfilen
      // if(isset($data->data->image) && $data->data->image != $employeeImage){
      //   $dir = '../../../uploads/employees/';

      //   rename($dir.$data->data->image, $dir.$employeeImage);
      //   $data->data->image = $employeeImage;
      // }

      if($data->data->employeeImage->status == 'changed'){
        $data->data->employeeImage->fileName = $this->helper->cleanImage('employee', $data->data->employeeImage->fileName);
      }
      $query = "UPDATE  employee
                SET     employeeName    = ?,
                        employeeTitle   = ?,
                        employeePhone   = ?,
                        employeeEmail   = ?,
                        employeeImage   = ?,
                        employeeCity    = ?
                WHERE   employeeId      = ?
      ";

      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);

      $this->stmt->bind_param("ssssssi",
                            $data->data->employeeName,
                            $data->data->employeeTitle,
                            $data->data->employeePhone,
                            $data->data->employeeEmail,
                            $data->data->employeeImage->fileName,
                            $data->data->employeeCity,
                            $data->employeeId
                           );

      $this->stmt->execute();

      $this->stmt->close();


      // Update for the employee groups

      $query = "DELETE  FROM  employee_employeegroup
                WHERE         employeeId = ?
      ";
      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);
      $this->stmt->bind_param("i", $data->employeeId);
      $this->stmt->execute();
      $this->stmt->close();


      $query =  " INSERT INTO employee_employeegroup
                  SET         employeeId = ?,
                              employeegroupId = ?
                ";
      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);

      foreach($data->data->employeeGroups as $group){
        $this->stmt->bind_param("ii", $data->employeeId, $group);
        $this->stmt->execute();
      }
      $this->stmt->close();

      if($data->data->sendEmail == true) {
        $data->employeeEmail = $data->data->employeeEmail;
        $data->employeeName  = $data->data->employeeName;

        $generatedPassword = substr(str_shuffle('0123456789abcdefghijklmnopqrstuvwxyz'), 0, 8);
        $generatedPasswordhashed = password_hash($generatedPassword, PASSWORD_DEFAULT);
        require_once(dirname(__DIR__).'/module/mailTemplates/orderconfirmation.php');

        $query = "INSERT INTO   employeedata
                  (             employeeId,
                                employeePassword,
                                employeeVerified
                  )
                  VALUES
                  (
                                ?,
                                ?,
                                1
                  )
        ";

        $this->stmt = $this->mysqli->stmt_init();
        $this->stmt->prepare($query);
        $this->stmt->bind_param("is",
          $data->employeeId,
          $generatedPasswordhashed
        );

        $this->stmt->execute();
        $this->stmt->close();
      }

      if ($data->data->removeLogin == true) {
        $query =  " DELETE FROM  employeedata
                    WHERE        employeeId = ?
                  ";

        $this->stmt = $this->mysqli->stmt_init();
        $this->stmt->prepare($query);
        $this->stmt->bind_param("i", $data->employeeId);
        $this->stmt->execute();
        $this->stmt->close();
      }

      $status = 'success';

      $response = array(
        'response' => $status,
        'data'     => $data
      );

      return $response;
    }

    function getAll() {
      $allData = array();
      $query = "  SELECT      e.employeeId
                  ,           e.employeeName
                  ,           e.employeePhone
                  ,           e.employeeEmail
                  ,           e.employeeImage
                  ,           e.employeeCity
                  FROM        employee e
                  WHERE       e.employeeVisible = 1
                  AND         e.visibility = 1
                  ORDER BY    employeeName ASC
               ";

      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);
      $this->stmt->execute();
      $this->stmt->bind_result($employeeId, $employeeName, $employeePhone, $employeeEmail, $employeeImage, $employeeCity);

      while($this->stmt->fetch()){
        if(!isset($employeeImage) || $employeeImage == '0'){
          $employeeImage = 'john-doe.jpg';
        }
        $thisData = (object) array(
            'employeeId'        => $employeeId,
            'employeeName'      => $employeeName,
            'employeePhone'     => $employeePhone,
            'employeeEmail'     => $employeeEmail,
            'employeeImage'     => $employeeImage,
            'employeeCity'      => $employeeCity
        );
        array_push($allData, $thisData);
      }

      return $allData;
    } // End of getAll



    function delete($data) {
      $employeeId = intval($data);
      $timestamp = date('Y-m-d');

      $query = "UPDATE  employee
                SET     visibility = 0,
                        deletedAt = ?
                WHERE   employeeId = ?
      ";
      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);
      $this->stmt->bind_param("si", $timestamp, $employeeId);
      $this->stmt->execute();

      $this->stmt->close();

      $status = 'success';
      $response = array(
        'response' => $status
      );

      return $response;
    }


     // END OF CLASS
  } $employeesClass = new employeesClass($mysqli, $helperClass);
?>
