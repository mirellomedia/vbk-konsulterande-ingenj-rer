<?php
  class helperClass {

    public function clean($string) {
       $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
       $string = strtolower($string);
       return preg_replace('/[^A-Za-z0-9\-\_]/', '', $string); // Removes special chars.
    }

    public function imageMove($imageType, $filename){
      // Hanterar headerbilden
        $fileName = pathinfo($filename);

        $imageName = $fileName['filename'];
        $imageExt  = $fileName['extension'];

        $new_filename = $this->clean($imageName);
        $new_filename = $new_filename.'.'.$imageExt;

        $temp = '../../../uploads/tmp/';

        if($imageType == 'employee'){
          $dir  = '../../../uploads/employees/';
        }
        else{
          $dir  = '../../../uploads/images/';
        }


        rename($temp.$filename, $dir.$new_filename);

        return $new_filename;
    }

    public function renameImage($imageType, $filename){

      if($imageType == 'employee'){
        $dir  = '../../../uploads/employees/';
      }
      else{
        $dir  = '../../../uploads/images/';
      }

      $randomString = $this->random_string();
      $fileName = pathinfo($filename);
        $imageName = $fileName['filename'];
        $imageExt  = $fileName['extension'];

      $imageName = $this->clean($imageName);

      $new_filename = $imageName .'-'.$randomString.'.'.$imageExt;

      rename($dir.$filename, $dir.$new_filename);

      return $new_filename;
    }

    public function random_string() {
      $mt = explode(' ', microtime());
      return $mt[1];
    }

    public function cleanImage($imageType, $fileName){
      $imgArray   = pathinfo($fileName);
      $imageName  = $imgArray['filename'];
      $imageExt   = $imgArray['extension'];

      $fileName   = $this->clean($imageName);
      $fileName   = $fileName . '.' .$imageExt;

      $newFileName   = $this->renameImage($imageType, $fileName);

      return $newFileName;
    }

  } $helperClass = new helperClass();

?>
