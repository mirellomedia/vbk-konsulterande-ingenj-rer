<?php date_default_timezone_set('Europe/Stockholm'); ?>
<?php
 require_once("../core/function/dbConnect.php");

  class data {
    function __construct(mysqli $mysqli) {
      $this->mysqli       = $mysqli;
    }

    function getSingle($data){
      $data       = explode("?", $data);
      $pageId     = intval($data[0]);
      $languageId = intval($data[1]);

      $query = "  SELECT      pc.pageName
                  ,           pc.pageLink
                  ,           pc.pageId
                  ,           pc.pageContent
                  ,           pc.pageHeaderimage
                  ,           p.pageTemplate
                  ,           p.pageParent
                  ,           p.published
                  ,           p.hidden
                  ,           pc.pageHeader
                  FROM        pagecontent pc
                  INNER JOIN  page p
                  ON          p.pageId = pc.pageId
                  WHERE       p.pageId = ?
                  AND         pc.languageId = ?
               ";

      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);
      $this->stmt->bind_param("ii",
        $pageId,
        $languageId
      );
      $this->stmt->execute();
      $this->stmt->bind_result(
        $pageName,
        $pageLink,
        $pageId,
        $pageContent,
        $pageHeaderimage,
        $pageTemplate,
        $pageParent,
        $published,
        $hidden,
        $pageHeader
      );



      $this->stmt->fetch();

      $image = (object) array('fileName' => $pageHeaderimage, 'status' => 'unchanged');

      $thisData = (object) array(
          'header'    => $pageName,
          'url'       => $pageLink,
          'id'        => $pageId,
          'body'      => $pageContent,
          'image'     => $image,
          'template'  => $pageTemplate,
          'parent'    => intval($pageParent),
          'published' => intval($published),
          'hidden'    => intval($hidden),
          'heading'   => $pageHeader
      );
      return $thisData;
    }

     // END OF CLASS
  } $data = new data($mysqli);








  switch ($_SERVER['REQUEST_METHOD']) {

    case "GET":
      $id = explode("api/pages_languages/", $_SERVER['REQUEST_URI']);
      $result = $data->getSingle($id[1]);
    break;
  }


  // Return the data
  $json = json_encode($result);
  print_r($json);

  return;

?>
