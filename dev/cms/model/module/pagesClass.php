<?php date_default_timezone_set('Europe/Stockholm'); ?>
<?php

require_once("../core/function/dbConnect.php");
require_once("helpers/nils.php");

class pagesClass {
    function __construct(mysqli $mysqli, helperClass $helperClass) {
      $this->mysqli       = $mysqli;
      $this->helper       = $helperClass;
    }

    function getSingle($data){
      $query = "  SELECT      p.pageId
                  ,           p.pageTemplate
                  ,           p.pageParent
                  ,           p.pageMenu
                  ,           pc.pageName
                  ,           pc.pageContent
                  ,           pc.pageHeaderimage
                  FROM        page p
                  INNER JOIN  pagecontent pc
                  ON          p.pageId = pc.pageId
                  WHERE       p.pageId = ?
                  LIMIT       1
               ";

      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);
      $this->stmt->bind_param("i", $data);
      $this->stmt->execute();
      $this->stmt->bind_result(
        $pageId,
        $pageTemplate,
        $pageParent,
        $pageMenu,
        $pageName,
        $pageContent,
        $pageHeaderimage
      );

      $image = (object) array('fileName' => $pageHeaderimage, 'status' => 'unchanged');
      $this->stmt->fetch();
      $page = (object) array(
        'pageId'          => $pageId,
        'pageTemplate'    => $pageTemplate,
        'pageParent'      => $pageParent,
        'pageMenu'        => $pageMenu,
        'pageName'        => $pageName,
        'pageContent'     => $pageContent,
        'pageHeaderimage' => $image
        );

      return $page;
    }

    function clean($string) {
       $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

       return preg_replace('/[^A-Za-z0-9\-\_]/', '', $string); // Removes special chars.
    }

    function imageMove($filename){
      // Hanterar headerbilden
          $imageParts = explode('.', $filename);
          $imageType  = end($imageParts);
          array_pop($imageParts);

          $imageName    = implode($imageParts);
          $new_filename = $this->clean($imageName);
          $new_filename = $new_filename.'.'.$imageType;

        $temp = '../../../uploads/tmp/';
        $dir  = '../../../uploads/images/';

        rename($temp.$filename, $dir.$new_filename);
    }

    function insert($data){
      $status = '';

      if  (strtolower($data[0]->options->parent) == 'mainnav'){
        $data[0]->options->parent    = 0;
        $data[0]->options->menu      = 1;
        $data[0]->options->menusort  = 0;
      }
      elseif(strtolower($data[0]->options->parent) == 'subnav'){
        $data[0]->options->parent    = 0;
        $data[0]->options->menu      = 2;
        $data[0]->options->menusort  = 0;
      }
      else  {
        $data[0]->options->menu      = 3;
        $data[0]->options->menusort  = 0;
      }
      // Om det är inställningarna

      $query =  " INSERT INTO     page
                  (
                                  pageTemplate,
                                  pageParent,
                                  pageMenu,
                                  pageMenuSort
                  )
                  VALUES
                  (
                                  ?,
                                  ?,
                                  ?,
                                  ?
                  )
                ";

      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);
      $this->stmt->bind_param("iiii",
                                $data[0]->options->template,
                                $data[0]->options->parent,
                                $data[0]->options->menu,
                                $data[0]->options->menusort
                              );
      $this->stmt->execute();
      $pageId = $this->stmt->insert_id;

      if($pageId !== ''){
        $status = 'success';
      }

      // Om det är en översättning
      $query = "select max(pageId) from page";

      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);
      $this->stmt->execute();
      $this->stmt->bind_result($pageId);
      $this->stmt->fetch();

      $query =  " INSERT INTO     pagecontent
                  (
                                  pageId,
                                  languageId,
                                  pageName,
                                  pageHeader,
                                  pageLink,
                                  pageContent,
                                  pageHeaderimage
                  )
                  VALUES
                  (
                                  ?,
                                  ?,
                                  ?,
                                  ?,
                                  ?,
                                  ?,
                                  ?
                  )
                ";

      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);


      if ($data[0]->image) {
        $new_filename = $this->helper->cleanImage('project', $data[0]->image);
      }
      else {
        $new_filename = '';
      }


      foreach ($data as $translation){
        if($translation->htmlcontent == null) {
          $translation->htmlcontent = '';
        }

        if($translation->heading == null) {
          $translation->heading = '';
        }

        $this->stmt->bind_param("issssss",
          $pageId,
          $translation->languageId,
          $translation->header,
          $translation->heading,
          $translation->urlslug,
          $translation->htmlcontent,
          $new_filename
        );

        $this->stmt->execute();
      }

      $insertId = $this->stmt->insert_id;

      if($insertId !== ''){
        $status = 'success';
      }


      else{
        $status = 'error';
      } // End of else

      $response = array(
        'response' => $status,
        'data'     => $data
      );

      return $response;
    }

    function delete($newsId){
      $articleId = intval($newsId);
      $timestamp = date('Y-m-d');

      $query = "UPDATE  page
                SET     visibility = 0,
                        deletedAt = ?
                WHERE   pageId = ?
      ";
      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);
      $this->stmt->bind_param("si", $timestamp, $articleId);
      $this->stmt->execute();
      $this->stmt->close();

      // $query = "DELETE FROM articlecontent
      //           WHERE       articleId = ?
      // ";
      // $this->stmt = $this->mysqli->stmt_init();
      // $this->stmt->prepare($query);
      // $this->stmt->bind_param("i", $articleId);
      // $this->stmt->execute();
      // $this->stmt->close();

      $status = 'success';
      $response = array(
        'response' => $status,
        'data'     => ""
      );

      return $response;
    }

    function update($data){
      // update "pagecontent":
      $query = "UPDATE  pagecontent
                SET     pageName        = ?,
                        pageHeader      = ?,
                        pageLink        = ?,
                        pageContent     = ?
                WHERE   pageId          = ?
                AND     languageId      = ?
      ";

      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);
      foreach ($data->data as $translation) {

        $pageId = intval($data->pagesId);
        $this->stmt->bind_param("ssssii",
                              $translation->header,
                              $translation->heading,
                              $translation->urlslug,
                              $translation->htmlcontent,
                              $pageId,
                              $translation->languageId
                             );

        $this->stmt->execute();
      }

      $this->stmt->close();


      // update "page":
      $query = "UPDATE  page
                SET     pageTemplate  = ?,
                        pageParent    = ?,
                        published     = ?,
                        hidden        = ?
                WHERE   pageId        = ?
      ";

      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);

      $pageId = intval($data->pagesId);
      $this->stmt->bind_param("iiiii",
        $data->data[0]->template,
        $data->data[0]->parent,
        $data->data[0]->publish,
        $data->data[0]->hidden,
        $pageId
      );

      $this->stmt->execute();
      $this->stmt->close();

      $projectImage = $data->data[0]->image;
        switch($projectImage->status) {

          case "unchanged":
          break;

          case "new":
            $projectImage->fileName = $this->helper->cleanImage('project', $projectImage->fileName);

            $query =  " UPDATE pagecontent
                        SET    pageHeaderimage = ?
                        WHERE  pageId = ?
                      ";
            $this->stmt = $this->mysqli->stmt_init();
            $this->stmt->prepare($query);
            $this->stmt->bind_param("si", $projectImage->fileName, $data->data[0]->pageId);
            $this->stmt->execute();
            $projectImageId = $this->stmt->insert_id;
            $this->stmt->close();

            $data->data[0]->image = $projectImage->fileName;


          break;

          case "delete":
            $query =  " UPDATE  pagecontent
                        SET     pageHeaderimage = ''
                        WHERE   pageId          = ?
                      ";
            $this->stmt = $this->mysqli->stmt_init();
            $this->stmt->prepare($query);

            $this->stmt->bind_param("i",
              $pageId
            );

            $this->stmt->execute();
            $this->stmt->close();
          break;

        }


      // remove translation if specified
      foreach ($data->data as $translation) {
        $pageId = intval($data->pagesId);

        if ($translation->status == 'DELETED') {
          $query =  " DELETE FROM pagecontent
                      WHERE       pageId = ?
                      AND         languageId = ?
                    ";

          $this->stmt = $this->mysqli->stmt_init();
          $this->stmt->prepare($query);

          $this->stmt->bind_param("ii",
            $pageId,
            $translation->languageId
          );

          $this->stmt->execute();
          $this->stmt->close();
        }

        if ($translation->status == 'NEW') {
          $query =  " INSERT INTO pagecontent (
                                    pageId,
                                    languageId,
                                    pageName,
                                    pageHeader,
                                    pageLink,
                                    pageContent
                                  )
                      VALUES      (
                                    ?,
                                    ?,
                                    ?,
                                    ?,
                                    ?,
                                    ?
                                  )
                    ";

          $this->stmt = $this->mysqli->stmt_init();
          $this->stmt->prepare($query);

          if($translation->htmlcontent == null) {
            $translation->htmlcontent = '';
          }

          if($translation->heading == null) {
            $translation->heading = '';
          }


          $this->stmt->bind_param("iisss",
            $pageId,
            $translation->languageId,
            $translation->header,
            $translation->heading,
            $translation->urlslug,
            $translation->htmlcontent
          );

          $this->stmt->execute();
        }
      }


      // return stuff:
      $status = 'success';

      $response = array(
        'response' => $status,
        'data'     => $data
      );

      return $response;
    }

    function getAll(){
      $pages = array();
      $query = "  SELECT      p.pageId
                  ,           p.pageTemplate
                  ,           p.pageParent
                  ,           p.pageMenu
                  ,           pc.pageName
                  ,           pc.pageLink
                  FROM        page p
                  INNER JOIN  pagecontent pc
                  ON          p.pageId = pc.pageId
                  WHERE       pc.languageId = 1
                  AND         p.visibility = 1
                  ORDER BY    p.pageParent
               ";

      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);
      $this->stmt->execute();
      $this->stmt->bind_result($pageId, $pageTemplate, $pageParent, $pageMenu, $pageName, $pageLink);

      while($this->stmt->fetch()){
        $page = (object) array(
            'pageId'        => $pageId,
            'pageTemplate'  => $pageTemplate,
            'pageParent'    => intval($pageParent),
            'pageMenu'      => $pageMenu,
            'pagename'      => $pageName,
            'pageLink'      => $pageLink
        );
          if($pageParent != '0'){
            array_push($pages[$pageParent]->sub, $page);
          }
          else{
          $pages[$pageId] = $page;
          $pages[$pageId]->sub = array();
          }

      }

      return $pages;
    }


     // END OF CLASS
  } $pagesClass = new pagesClass($mysqli, $helperClass);
?>
