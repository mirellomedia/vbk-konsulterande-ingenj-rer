<?php

  $itemId = intval($data->id);
  $this->stmt = $this->mysqli->stmt_init();

  if ($data->type == 'Nyhet') {
    $query = "UPDATE  article
              SET     visibility   = 1,
                      deletedAt    = null
              WHERE   articleId    = ?
    ";
  }

  elseif ($data->type == 'Sida') {
    $query = "UPDATE  page
              SET     visibility   = 1,
                      deletedAt    = null
              WHERE   pageId       = ?
    ";
  }

  elseif ($data->type == 'Projekt') {
    $query = "UPDATE  project
              SET     visibility   = 1,
                      deletedAt    = null
              WHERE   projectId    = ?
    ";
  }

  elseif ($data->type == 'Personal') {
    $query = "UPDATE  employee
              SET     visibility   = 1,
                      deletedAt    = null
              WHERE   employeeId   = ?
    ";
  }

  $this->stmt->prepare($query);
  $this->stmt->bind_param("i",
    $itemId
  );

  $this->stmt->execute();
  $success = $this->stmt->affected_rows;
  $this->stmt->close();

  $data = array($success);
