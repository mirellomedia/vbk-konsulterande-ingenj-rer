<?php
  $allData = array();


  // query articles:
  $query = "  SELECT      ac.articleHeader
              ,           ac.articleLink
              ,           ac.articleId
              ,           a.deletedAt
              FROM        articlecontent ac
              INNER JOIN  article a
              ON          a.articleId = ac.articleId
              WHERE       ac.languageId = 1
              AND         a.visibility = 0
           ";

  $this->stmt = $this->mysqli->stmt_init();
  $this->stmt->prepare($query);
  $this->stmt->execute();

  $this->stmt->bind_result(
    $articleHeader,
    $articleLink,
    $articleId,
    $deletedAt
  );

  while($this->stmt->fetch()) {
    $thisData = (object) array(
      'name'        => $articleHeader,
      'link'        => $articleLink,
      'id'          => $articleId,
      'deletedAt'   => $deletedAt,
      'type'        => 'Nyhet'
    );

    array_push($allData, $thisData);
  }

  $this->stmt->close();


  // query employees:
  $query = "  SELECT      e.employeeId
              ,           e.employeeName
              ,           e.deletedAt
              FROM        employee e
              WHERE       e.employeeVisible = 1
              AND         e.visibility = 0
              ORDER BY    employeeName ASC
           ";

  $this->stmt = $this->mysqli->stmt_init();
  $this->stmt->prepare($query);
  $this->stmt->execute();

  $this->stmt->bind_result(
    $employeeId,
    $employeeName,
    $deletedAt
  );

  while($this->stmt->fetch()) {
    $thisData = (object) array(
      'id'          => $employeeId,
      'name'        => $employeeName,
      'deletedAt'   => $deletedAt,
      'type'        => 'Personal'
    );

    array_push($allData, $thisData);
  }


  // query pages:
  $query = "  SELECT      p.pageId
              ,           pc.pageName
              ,           pc.pageLink
              ,           p.deletedAt
              FROM        page p
              INNER JOIN  pagecontent pc
              ON          p.pageId = pc.pageId
              WHERE       pc.languageId = 1
              AND         p.visibility = 0
              ORDER BY    pc.pageName
           ";

  $this->stmt = $this->mysqli->stmt_init();
  $this->stmt->prepare($query);
  $this->stmt->execute();
  $this->stmt->bind_result(
    $pageId,
    $pageName,
    $pageLink,
    $deletedAt
  );

  while($this->stmt->fetch()) {
    $thisData = (object) array(
      'id'        => $pageId,
      'name'      => $pageName,
      'link'      => $pageLink,
      'deletedAt' => $deletedAt,
      'type'      => 'Sida'
    );

    array_push($allData, $thisData);
  }

  $this->stmt->close();


  // query projects:
  $query = "  SELECT      p.projectId
              ,           pc.projectName
              ,           pc.projectLink
              ,           p.deletedAt
              FROM        project p
              INNER JOIN  projectcontent pc
              ON          p.projectId = pc.projectId
              WHERE       p.visibility = 0
              ORDER BY    pc.projectName ASC
           ";

  $this->stmt = $this->mysqli->stmt_init();
  $this->stmt->prepare($query);
  $this->stmt->execute();

  $this->stmt->bind_result(
    $projectId,
    $projectName,
    $projectLink,
    $deletedAt
  );

  while($this->stmt->fetch()) {
    $thisData = (object) array(
      'id'        => $projectId,
      'name'      => $projectName,
      'link'      => $projectLink,
      'deletedAt' => $deletedAt,
      'type'      => 'Projekt'
    );

    array_push($allData, $thisData);
  }

  $this->stmt->close();


  // return data:
  $data = $allData;
