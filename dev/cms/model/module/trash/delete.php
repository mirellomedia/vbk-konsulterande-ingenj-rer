<?php

  $itemId = intval($data->id);

  if ($data->type == 'Nyhet') {
    $query = "DELETE FROM article
              WHERE       articleId = ?
    ";

    $query2 ="DELETE FROM articlecontent
              WHERE       articleId = ?
    ";
  }

  else if ($data->type == 'Sida') {
    $query = "DELETE FROM page
              WHERE       pageId = ?
    ";

    $query2 ="DELETE FROM pagecontent
              WHERE       pageId = ?
    ";
  }

  else if ($data->type == 'Projekt') {
    $query = "DELETE FROM project
              WHERE       projectId = ?
    ";

    $query2 ="DELETE FROM projectcontent
              WHERE       projectId = ?
    ";
  }

  else if ($data->type == 'Personal') {
    $query = "DELETE FROM employee
              WHERE       employeeId = ?
    ";

    $query2 ="DELETE FROM employeedata
              WHERE       employeeId = ?
    ";
  }


  // query 1:
  $this->stmt = $this->mysqli->stmt_init();

  $this->stmt->prepare($query);
  $this->stmt->bind_param("i",
    $itemId
  );

  $this->stmt->execute();
  $success = $this->stmt->affected_rows;
  $this->stmt->close();


  // query2:
  if ($query2 != '') {
    $this->stmt = $this->mysqli->stmt_init();

    $this->stmt->prepare($query2);
    $this->stmt->bind_param("i",
      $itemId
    );

    $this->stmt->execute();
    $success = $this->stmt->affected_rows;
    $this->stmt->close();
  }


  $data = $success;
