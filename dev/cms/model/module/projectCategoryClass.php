<?php date_default_timezone_set('Europe/Stockholm'); ?>
<?php 
require_once("../core/function/dbConnect.php");
 
  class projectCategoryClass {
    function __construct(mysqli $mysqli) {
      $this->mysqli       = $mysqli;
    }

    function getAll(){
      $allData = array();
      $query = "  SELECT      pc.projectcategoryId
                  ,           pc.projectcategoryName
                  FROM        projectcategory pc
                  WHERE       pc.projectcategoryId != 0
               ";

      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);
      $this->stmt->execute();
      $this->stmt->bind_result(
        $id,
        $name
      );
      
      while($this->stmt->fetch()){
        $thisData = (object) array(
          'id'   => $id,
          'name' => $name
        );
        array_push($allData, $thisData);
      }

      return $allData;
    } // End of getAll


     // END OF CLASS
  } $projectCategoryClass = new projectCategoryClass($mysqli);
?>