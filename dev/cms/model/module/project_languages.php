<?php date_default_timezone_set('Europe/Stockholm'); ?>
<?php 
 require_once("../core/function/dbConnect.php");
 
  class data {
    function __construct(mysqli $mysqli) {
      $this->mysqli       = $mysqli;
    }

    function getSingle($data){  $data = explode("?", $data);
      $projectId = intval($data[0]);
      $languageId = intval($data[1]);

      $query = "  SELECT      pc.projectName
                  ,           pc.projectLink
                  ,           pc.projectShortDesc
                  ,           pc.projectLongDesc
                  ,           p.projectCategory
                  ,           p.projectType
                  FROM        projectcontent pc
                  INNER JOIN  project p
                  ON          p.projectId = pc.projectId
                  WHERE       p.projectId = ?
                  AND         pc.languageId = ?
               ";

      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);
      $this->stmt->bind_param("ii", 
                              $projectId,
                              $languageId);
      $this->stmt->execute();
      $this->stmt->bind_result($projectName, $projectLink, $projectShortDesc, $projectLongDesc, $projectCategory, $projectType);
      $this->stmt->fetch();
      $thisData = (object) array(
          'projectName'           => $projectName,
          'projectLink'           => $projectLink,
          'projectShortDesc'      => $projectShortDesc,
          'projectLongDesc'       => $projectLongDesc,
          'projectCategory'       => $projectCategory,
          'projectType'           => $projectType
      );

      $allData = array();
      $query = "  SELECT      pi.projectimageLink
                  ,           pi.projectimageOrder
                  ,           pi.projectimageId
                  FROM        projectimage pi
                  WHERE       pi.projectId = ?
               ";

      $this->stmt = $this->mysqli->stmt_init();
      $this->stmt->prepare($query);
      $this->stmt->bind_param("i", $projectId);
      $this->stmt->execute();
      $this->stmt->bind_result($projectimageLink, $projectimageOrder, $projectimageId);
      
      while($this->stmt->fetch()){
        $imageData = (object) array(
            'projectimageLink'         => $projectimageLink,
            'projectimageOrder'        => $projectimageOrder,
            'projectimageId'           => $projectimageId
        );
        $allData[] = $imageData;
      }
      $thisData->images = $allData;

      return $thisData;
    }

     // END OF CLASS
  } $data = new data($mysqli);








  switch ($_SERVER['REQUEST_METHOD']) {

    case "GET":
      $id = explode("api/project_languages/", $_SERVER['REQUEST_URI']);
      $result = $data->getSingle($id[1]);
    break;
  }


  // Return the data
  $json = json_encode($result);
  print_r($json);

  return;

?>