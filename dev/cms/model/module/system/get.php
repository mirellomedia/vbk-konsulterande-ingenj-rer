<?php
  $field = $data;

  $query = "  SELECT  s.fieldContent
              FROM    system s
              WHERE   s.id = ?
           ";

  $this->stmt = $this->mysqli->stmt_init();
  $this->stmt->prepare($query);
  $this->stmt->bind_param("s",
    $field
  );

  $this->stmt->execute();
  $this->stmt->bind_result($fieldContent);
  $this->stmt->fetch();
  $thisData = (object) array(
    'content' => $fieldContent,
    'id'      => $field
  );

  $data = $thisData;
  $this->stmt->close();

  return $data;
