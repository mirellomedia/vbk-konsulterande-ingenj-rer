<?php

  $id = intval($data[0]->id);

  $query = "UPDATE  system
            SET     fieldContent = ?
            WHERE   id           = ?
  ";

  $this->stmt = $this->mysqli->stmt_init();
  $this->stmt->prepare($query);

  $this->stmt->bind_param("si",
    $data[0]->content,
    $id
  );

  $this->stmt->execute();
  $this->stmt->close();
