//******************************//
//
//  "GET (id)"     - Hämtar ut en specifik rad
//  "QUERY"        - Hämtar ut alla rader
//  "UPDATE (id)"  - Uppdaterar specifik rad
//  "INSERT"       - Lägger till rad med specifik data
//******************************//


app.factory('Pages', ['$resource', function($resource, $route, $scope) {
  return $resource('/api/pages/:dataId',
    { dataId: '@dataId' }, {
      get: {
        method: 'GET',
        params: { pagesId: '@pagesId' },
        isArray: false
      },
      query: {
        method: 'GET',
        isArray: false
      },
      insert: {
        method: 'POST',
        isArray: false
      },
      update: {
        method: 'PUT'
      },
      delete: {
        method: 'PUT',
        params: { action: 'delete'}
      }
  });
}]);

  app.factory('Pages_Languages', ['$resource', function($resource, $route, $scope) {
    return $resource('/api/pages_languages/:pageId?:languageId',
      { pageId: '@pageId' }, {
        get: {
          method: 'GET',
          params: { pageId: '@pageId', languageId: '@languageId' },
          isArray: false
        }
    });
  }]);


// app.factory('Project', ['$resource', function($resource, $route, $scope) {
//   return $resource('/api/project/:projectId',
//     { projectId: '@projectId' }, {
//       get: {
//         method: 'GET',
//         params: { projectId: '@projectId' },
//         isArray: false
//       },
//       query: {
//         method: 'GET',
//         isArray: true
//       },
//       insert: {
//         method: 'POST',
//         isArray: false
//       },
//       update: {
//         method: 'PUT'
//       }
//   });
// }]);


app.factory('api', ['$resource', function($resource) {
  return $resource( '/api/:endPoint/:data',
    { data: '@data' }, {

      get: {
        method: 'GET'
      },
      insert: {
        method: 'POST'
      },
      update: {
        method: 'PUT',
        isArray: true
      },
      delete: {
        method: 'PUT'
      }
    });
}]);


app.factory('News', ['$resource', function($resource, $route, $scope) {
  return $resource('/api/news/:newsId',
    { newsId: '@newsId' }, {
      get: {
        method: 'GET',
        params: { newsId: '@newsId' },
        isArray: false
      },
      query: {
        method: 'GET',
        isArray: false
      },
      insert: {
        method: 'POST',
        isArray: false
      },
      update: {
        method: 'PUT'
      },
      delete: {
        method: 'PUT'
      }
  });
}]);

  app.factory('News_Languages', ['$resource', function($resource, $route, $scope) {
    return $resource('/api/news_languages/:newsId?:languageId',
      { newsId: '@newsId' }, {
        get: {
          method: 'GET',
          params: { newsId: '@newsId', languageId: '@languageId' },
          isArray: false
        }
    });
  }]);

  app.factory('Project_Languages', ['$resource', function($resource, $route, $scope) {
    return $resource('/api/project_languages/:projectId?:languageId',
      { projectId: '@projectId' }, {
        get: {
          method: 'GET',
          params: { projectId: '@projectId', languageId: '@languageId' },
          isArray: false
        }
    });
  }]);


app.factory('Employees', ['$resource', function($resource, $route, $scope) {
  return $resource('/api/employees/:employeesId',
    { employeesId: '@employeesId' }, {
      get: {
        method: 'GET',
        params: { employeesId: '@employeesId' },
        isArray: false
      },
      query: {
        method: 'GET',
        isArray: true
      },
      insert: {
        method: 'POST',
        isArray: false
      },
      update: {
        method: 'PUT'
      },
      delete: {
        method: 'PUT',
        params: {
          action: 'delete',
          employeesId: '@employeesId'
        }
      }
  });
}]);

app.factory('Languages', ['$resource', function($resource, $route, $scope) {
  return $resource('/api/languages/:languagesId',
    { languagesId: '@languagesId' }, {
      get: {
        method: 'GET',
        params: { languagesId: '@languagesId' },
        isArray: false
      },
      query: {
        method: 'GET',
        isArray: false
      },
      insert: {
        method: 'POST',
        isArray: true
      },
      update: {
        method: 'PUT'
      }
  });
}]);

app.factory('Notes', ['$resource', function($resource, $route, $scope) {
  return $resource('/api/notes/:notesId',
    { NotesId: '@NotesId' }, {
      get: {
        method: 'GET',
        params: { notesId: 1 },
        isArray: false
      },
      query: {
        method: 'GET',
        isArray: false
      },
      remove: {
        method: 'POST',
        isArray: false
      }
  });
}]);


app.factory('Groups', ['$resource', function($resource, $route, $scope) {
  return $resource('/api/group/:dataId',
    { dataId: '@dataId' }, {
      get: {
        method: 'GET',
        params: { groupId: '@groupId' },
        isArray: false
      },
      query: {
        method: 'GET',
        isArray: true
      },
      insert: {
        method: 'POST',
        isArray: false
      },
      update: {
        method: 'PUT'
      },
      delete: {
        method: 'PUT',
        params: { action: 'delete'}
      }
  });
}]);

app.factory('Slides', ['$resource', function($resource, $route, $scope) {
  return $resource('/api/slide/:dataId',
    { dataId: '@dataId' }, {
      get: {
        method: 'GET',
        params: { slideId: '@slideId' },
        isArray: false
      },
      query: {
        method: 'GET',
        isArray: true
      },
      insert: {
        method: 'POST'
      },
      update: {
        method: 'PUT'
      },
      delete: {
        method: 'PUT',
        params: { action: 'delete'}
      }
  });
}]);

app.factory('projectCategories', ['$resource', function($resource, $route, $scope) {
  return $resource('/api/projectcategories/:dataId',
    { dataId: '@dataId' }, {
      get: {
        method: 'GET',
        params: { categoryId: '@categoryId' },
        isArray: false
      },
      query: {
        method: 'GET',
        isArray: true
      },
      insert: {
        method: 'POST',
        isArray: false
      },
      update: {
        method: 'PUT'
      },
      delete: {
        method: 'PUT',
        params: { action: 'delete'}
      }
  });
}]);






// nodeModule.factory('NodeFirebase', ['$resource', function($resource) {
//   return {
//     Node:
//     $resource( FIREBASE_URL + 'nodes/:nodeId',
//       { nodeId: '@nodeId' }, {
//         add: {
//           method: 'POST',
//           params: {},
//           isArray: false },
//         update: {
//           method: 'PUT',
//           params: { nodeId: '@nodeId' },
//           isArray: false } }),
//     Stuff: $resource(
//       FIREBASE_URL + 'stuff/:stuffId')
//     }
//   } ]);
