app.factory('pageInformation', function() {
    var pages = {};
    var pageService = {};
    var page;

    pageService.init = function(data) {
      pages = data;
    };

    pageService.get = function(data){
      if(data !== undefined){

        angular.forEach(pages, function(value, key){
          if(data == value.pageId){ page = value; }
        });

        return page;
      }
      else if(data === undefined){
        return pages;
      }
      else {
        console.log("ERROR - Fel vid sidhämtning.");
      }
    };

    return pageService;
});