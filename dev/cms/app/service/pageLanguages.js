app.factory('pageLanguages', function() { var func = [];
  var languages           = [];
  var selectedLanguages   = [];
  var remainingLanguages  = [];
  var primaryLanguage     = [];

  func.getLanguages = function(){
    return selectedLanguages;
  };
  func.reset = function(){
    selectedLanguages = [];
    remainingLanguages = [];
    languages = [];
  };

  func.init = function(data){

    if(selectedLanguages.length === 0 && remainingLanguages.length === 0){
    // Kontrollerar så att det är första gången som det hämtas
      // Rensar bort all skit som hängt med i Ajax-pullen(?) och skapar en fin array
      $.each(data, function( index, value ) {
        if(value.languageId){
          languages.push(value);
        }
      });

      // languages kommer bara användas om något skall gå tillbaka till grundutförandet, annars används remainingLanguages som array
      remainingLanguages = languages.slice(0);


      // Loopar igenom länderna för att hitta det primära språket
      $.each(remainingLanguages, function( index, value ) {
        if(value.languageId == 1){
          selectedLanguages.push(value);
          remainingLanguages.splice(index, 1);
          return false;
        }
      });
    }

    // Skickar ut de valda språken
    return selectedLanguages;
  };

  func.remLanguages = function(){
    return remainingLanguages;
  };



  func.query = function(){
    return languages;
  };

  func.addTranslation = function(){
    $.each(remainingLanguages, function( index, value ) {
      if(remainingLanguages.length == 1){

        value.status = 'NEW';
        // Om det bara finns ett språk kvar bland de tillgängliga språken
        selectedLanguages.push(value);
        remainingLanguages.splice(index, 1);
        return false;
      }

      else{
        // Om det finns fler än ett språk kvar, gör någonting kul
      }
    });

    // Skickar ut de valda språken
    return selectedLanguages;
  };

  func.removeTranslation = function(languageCode){
    //console.log(languageCode);
    $.each(selectedLanguages, function( index, value ) {
      if(value.languageCode == languageCode){
        // Om det bara finns ett språk kvar bland de tillgängliga språken
        remainingLanguages.push(value);
        selectedLanguages.splice(index, 1);
        return false;
      }
    });

    // Skickar ut de valda språken
    return selectedLanguages;

  };
  // För varje språk som läggs till / ändras
  func.update = function(data){
    // Hämtar ut landskoden för att kunna uppdatera rätt del av arrayen
  };

  return func;
});
