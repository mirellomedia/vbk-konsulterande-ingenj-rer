app.config(['flowFactoryProvider', function (flowFactoryProvider) {
  flowFactoryProvider.defaults = {
    target: '/model/module/flow.php',
    permanentErrors: [404, 500, 501],
    maxChunkRetries: 4,
    chunkRetryInterval: 5000,
    simultaneousUploads: 4
  };



  flowFactoryProvider.on('catchAll', function (event) {
    //console.log(event);
  });
  // Can be used with different implementations of Flow.js
  // flowFactoryProvider.factory = fustyFlowFactory;
}]);
