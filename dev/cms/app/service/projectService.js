app.factory('projectInformation', function() {
    var projects = {};
    var projectService = {};
    var project;

    projectService.init = function(data) {
      projects = data;
    };

    projectService.get = function(data){
      if(data !== undefined){

        angular.forEach(projects, function(value, key){
          if(data == value.projectId){ project = value; }
        });

        return project;
      }
      else if(data === undefined){
        return projects;
      }
      else {
        console.log("ERROR - Fel vid sidhämtning.");
      }
    };

    return projectService;
});