app.factory('notification', function($rootScope, $timeout) {
  $rootScope.notifications = [];
  var array = $rootScope.notifications;

  var newNotification = function(message) {
    var notification = {
      status: 'success',
      message: message,
      noticeId: '999'
    };

    array.push(notification);

    $timeout(function() {
      array.splice(array.indexOf(notification), 1);
    }, 3000);
  };

  return newNotification;
});
