function onBlurDirective($document) {
  return {
    restrict: 'A',

    link: function($scope, $elem, $attrs) {
      $document.on('click', function(e) {
        if ($elem.find(e.target).length === 0) {
          $scope.$apply($attrs.onBlur);
        }
      });
    }
  };
}

angular.module('app')
  .directive('onBlur', ['$document', onBlurDirective]);
