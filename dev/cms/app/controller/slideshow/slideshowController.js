app.controller('slideshowController', function(
  $scope,
  $routeParams,
  $location,
  $http,
  $route,
  $rootScope,
  $timeout,
  Employees,
  Groups,
  filterFilter,
  Slides
) {
  Slides.query(function(data){
    $scope.slides = data;
    console.log(data);

    $.each(data, function(i, e){
      e.status = 'unchanged';
    });
  });


  function notification(message, status) {
    status = status || 'success';

    var newNotification = {
      status: status,
      message: message,
      noticeId: '999'
    };

    $scope.$parent.notifications = [newNotification];

    $timeout(function() {
      $scope.$parent.notifications = [];
    }, 4000);
  }


  $scope.addSlide = function() {
    var arr = $scope.slides,
        obj = {
          projectName: '',
          projectSummary: '',
          projectLink: '',
          status: 'new'
        };

    arr.push(obj);

    notification('Klicka på Spara för att spara den nya bilden');
  };

  $scope.removeSlide = function(slide) {
    if (slide.status === 'new') {
      var arr = $scope.slides;

      arr.splice(arr.indexOf(slide), 1);
    }

    else {
      slide.status = 'delete';

      notification('Slidern raderas permanent efter att du sparat dina ändringar.');
    }
  };

  $scope.slideChange = function(slide, $index) {
    if (slide.status === 'unchanged') {
      slide.status = 'changed';

      notification('Klicka på Spara för att spara dina ändringar.');
    }
  };

  $scope.submit = function(tempoptions) {
    var slides = $scope.slides;

    $.each(slides, function(i, e){
      e.flow.upload();
    });
  };

  $scope.success = function(slide){
    if(slide.flow.files.length > 0){
      var image = slide.flow.files[0].name;
      slide.slideImage = ({fileName: image, status: slide.status});
    }

    submitUpdate(slide);
  };

  function submitUpdate(slide){
    var thisSlide = angular.copy(slide);

    delete thisSlide.flow;
    switch(thisSlide.status) {

      case "unchanged":
      break;

      case "changed":
      case "delete":
        Slides.update(thisSlide, function(answer) {
          slide.slideImage.fileName = answer.slideImage.fileName;
          notification('Dina ändringar är sparade');
          $route.reload();
        });

      break;

      case "new":
        Slides.insert(thisSlide, function(answer) {
          // set status to unchanged to prevent double uploads if no page refresh
          slide.slideImage.status = 'unchanged';
          slide.status = 'unchanged';

          notification('Slidern har lagts till');
        });
      break;
    }
  }


});
