app.controller('systemController', function($scope,
  $timeout,
  $routeParams,
  $location,
  $http,
  $rootScope,
  api
) {

  function notification(message, status) {
    status = status || 'success';

    var newNotification = {
      status: status,
      message: message,
      noticeId: '999'
    };

    $scope.$parent.notifications = [newNotification];

    $timeout(function() {
      $scope.$parent.notifications = [];
    }, 4000);
  }

  $scope.data = {};


  // get groups:
  api.query({
    endPoint: 'group'
  }, function(data) {
    $scope.groups = data;
  });

  api.get({
    endPoint: 'system',
    data: 1 // frontHeroText
  }, function(data) {
    $scope.data.hero = data;
  });

  api.get({
    endPoint: 'system',
    data: 2 // frontWelcome
  }, function(data) {
    $scope.data.welcome = data;
  });


  $scope.save = function(newData) {
    $.each(newData, function(key, value) {
      var data = [];
      data.push(value);

      api.update({
        endPoint: 'system'
      }, data, function() {

      });
    });

    notification('Ändringarna sparade.');
  };


  $scope.toggleAddNewFunction = function() {
    $scope.toggleAddNew = !$scope.toggleAddNew;
  };

  $scope.toggleEditGroup = function() {
    $scope.toggleEdit = !$scope.toggleEdit;
  };

  $scope.removeGroup = function(group) {
    api.delete({
      endPoint: 'group',
    }, group.employeegroupId, function() {
      var arr = $scope.groups;

      arr.splice(arr.indexOf(group), 1);

      notification(group.employeegroupName + ' är nu borttagen.');
    });

  };

  $scope.createGroup = function(group, id) {
    if (group) {
      var data = {};

      data.employeegroupName = group;

      api.insert({
        endPoint: 'group'
      }, data, function(data) {

        $scope.groups.push(data);

        $scope.createNewGroup = '';
        $scope.toggleAddNewFunction();

        notification(data.employeegroupName + ' är nu tillagd.');
      });
    }
  };

  $scope.editGroup = function(group) {
    var data = [];
    data.push(group);

    if (group.employeegroupName.length) {
      api.update({
        endPoint: 'group'
      }, data, function(data) {
        notification('Namnbyte genomfört.');
      });
    }
  };

  $scope.$on('$routeChangeSuccess', function() {
    $scope.$parent.menuOpen = false;
  });

});
