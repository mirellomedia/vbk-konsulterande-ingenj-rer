app.config(function($routeProvider,
                    $locationProvider
                   )
{

  function templateURL(filename){
    var views = 'view/pages/';

    return views + filename + '.php';
  }


  $routeProvider

    // system:
    .when ('/', {
      templateUrl: templateURL('front'),
      controller: 'routeController',
      title: 'Dashboard'
    })

    .when ('/notiser', {
      templateUrl: templateURL('notiser'),
      controller: 'noticeController',
      title: 'Notiser'
    })

    .when ('/system', {
      templateUrl: templateURL('system/alla'),
      controller: 'systemController',
      title: 'System'
    })


    // nyheter:
    .when ('/nyheter/ny', {
      templateUrl: templateURL('nyhet/skapa-nyhet'),
      controller: 'add_newsController',
      title: 'Skriv nyhet'
    })

    .when ('/nyheter', {
      templateUrl: templateURL('nyhet/alla-nyheter'),
      controller: 'add_newsController',
      title: 'Nyheter'
    })

    .when ('/nyheter/:articleid?_:articlelink', {
      templateUrl: templateURL('nyhet/redigera-nyhet'),
      controller: 'edit_newsController',
      title: 'Redigera nyhet'
    })


    // undersidor:
    .when ('/sidor/ny', {
      templateUrl: templateURL('sida/skapa-sida'),
      controller: 'add_pageController',
      title: 'Skapa sida'
    })

    .when ('/sidor', {
      templateUrl: templateURL('sida/alla-sidor'),
      controller: 'all_pageController',
      title: 'Hantera sidor'
    })

    .when ('/sidor/:dataId', {
      templateUrl: templateURL('sida/redigera-sida'),
      controller: 'edit_pageController',
      title: 'Redigera sida',
      resolve: {
        pagesData: function(Pages) {
          return Pages.query().$promise.then(function(data) {
            return data;
          });
        },

        languagesData: function(Languages) {
          return Languages.query().$promise.then(function(data) {
            return data;
          });
        }
      }
    })


    // projekt:
    .when ('/projekt/ny', {
      templateUrl: templateURL('projekt/skapa-projekt'),
      controller: 'add_projectController',
      title: 'Skapa projekt'
    })

    .when ('/projekt', {
      templateUrl: templateURL('projekt/alla-projekt'),
      controller: 'projectController',
      title: 'Alla projekt'
    })

    .when ('/projekt/:projectId?_:projectLink', {
      templateUrl: templateURL('projekt/redigera-projekt'),
      controller: 'projectController',
      title: 'Redigera projekt'
    })


    // personal:
    .when ('/personal/ny', {
      templateUrl: templateURL('person/skapa-person'),
      controller: 'employeeController',
      title: 'Lägg till anställd'
    })

    .when ('/personal', {
      templateUrl: templateURL('person/all-personal'),
      controller: 'employeeController',
      title: 'All personal'
    })

    .when ('/personal/:employeeid', {
      templateUrl: templateURL('person/redigera-person'),
      controller: 'employeeController',
      title: 'Redigera anställd'
    })


    // slideshow:
    .when ('/bildspel', {
      templateUrl: templateURL('bildspel/redigera'),
      controller: 'slideshowController',
      title: 'Redigera bildspel'
    })


    // papperskorg:
    .when ('/trash', {
      templateUrl: templateURL('trash'),
      controller: 'trashController',
      title: 'Raderat material'
    })
  ;
});

app.factory('Page', function() {
  var title = 'default';
  return {
    title: function() { return title; },
    setTitle: function(newTitle) { title = newTitle; }
  };
});

app.controller('routeController', function($scope, $location, $http, Notes) {
  $scope.notifications = [];
  var notifications    = [];
  var temp             = [];

  $scope.activePath = null;

  $scope.$on('$routeChangeSuccess', function() {

    Notes.query(function(data){
      if(data){
        temp = [];
        // Rensar bort onödig data från arrayen
        $.each(data, function( index, value ) {
          if(value.noticeId){
            temp[value.noticeId] = value;
          }
        });

        // Rensar bort en jävla massa skit...
        temp = temp.filter(function(e){
          return e;
        });

        $.each(temp, function(index, value){
          if(value.noticeId){
            if(notifications[index]){
              // console.log("Den här finns redan!");
            }
            else{
              // console.log("Den här fanns inte innan, vi lägger till den.");
              notifications[index] = value;
            }
          }
        });
        // console.log(temp);
        $scope.notifications = notifications;
      }

      $scope.close = function(nils){
        $.each($scope.notifications, function(index, value) {
          if(value.noticeId == nils){

            if($scope.notifications.length >= 1) {
              $scope.notifications.splice(index, 1);
            }

            else {
              return false;
            }

            Notes.remove(nils, function(data) {
              console.log(data);
            });
          }
        });
      };
    });


    var currentFile = $location.path().substr(1);
    $scope.currentPath = currentFile;

    $scope.$parent.menuOpen = false;
  });
});

app.run(['$location', '$rootScope', function($location, $rootScope) {
  $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
    $rootScope.title = current.$$route.title;
  });
}]);
