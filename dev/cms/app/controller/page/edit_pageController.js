app.controller('edit_pageController', function(
  $scope,
  $filter,
  $timeout,
  $routeParams,
  $route,
  $location,
  $http,
  $rootScope,
  $q,
  pageLanguages,
  Pages,
  pageInformation,
  Languages,
  Pages_Languages,
  api,
  pagesData,
  languagesData
) {

  languages = pageLanguages.init(languagesData);
  var allLanguages = pageLanguages.query();

  function getPageLangages() {
    var pageLanguages = angular.copy(allLanguages);

    $.each(pageLanguages, function(i, value) {
      Pages_Languages.get({
        pageId: $routeParams.dataId,
        languageId: value.languageId
      }, function(data) {

        if (data.header) {
          value.url         = data.url;
          value.header      = data.header;
          value.heading     = data.heading;
          value.htmlcontent = data.body;
          value.image       = data.image;
          value.template    = data.template;
          value.parent      = data.parent;
          value.publish     = data.published;
          value.hidden      = data.hidden;
          value.pageId      = data.id;
        }
        else {
          pageLanguages.splice(i, 1);
        }

      });
    });

    setTimeout(function() {
      $scope.$apply(function() {
        $scope.languages        = pageLanguages;
        $scope.selectedLanguage = pageLanguages[0].languageCode;

        $scope.options = {
          // checkboxes need "true" or "false" to be checked
          'publish': pageLanguages[0].publish ? true : false,
          'hidden':  pageLanguages[0].hidden  ? true : false
        };
      });
    }, 250);
  }

  getPageLangages();


  // loop through the pagesData object and create an array of the relevant objects:
  $scope.allPages = [];

  for(var key in pagesData) {
    if(pagesData.hasOwnProperty(key) && pagesData[key].pageId) {
      $scope.allPages.push(pagesData[key]);
    }
  }

  if ($routeParams.dataId == 6) {
    $scope.isFrontPage = true;
  }
  else if ($routeParams.dataId == 13) {
    $scope.staticPage = true;
  }



  $scope.$watch('options', function(hej, ge){
    //console.log(hej + ge);
  });

  $scope.$on('flow::fileAdded', function (event, $flow, flowFile) {
    event.preventDefault();//prevent file from uploading
  });

  $scope.addTranslation = function(){
    var remaininglanguages    = pageLanguages.remLanguages();
    if(remaininglanguages.length > 0){
      var languages             = pageLanguages.addTranslation();
        $scope.selectedLanguage   = languages[languages.length -1].languageCode;
        $scope.languages          = languages;
        $scope.remainingtranslations = remaininglanguages.length;
    }
    remaininglanguages  = pageLanguages.remLanguages();
    if(remaininglanguages.length === 0) {
      $scope.remainingtranslations = 0;
    }
  };

  $scope.removeTranslation = function(language) {
    if (language.status === 'NEW') {
      $scope.languages.splice($scope.languages.indexOf(language), 1);
    }

    else {
      language.status = 'DELETED';
    }

    $scope.selectedLanguage = languages[0].languageCode;
  };

  $scope.toggleLanguage = function(languageCode){
    $scope.selectedLanguage = languageCode;
  };

  $scope.delete = function(){
    var data = $routeParams.dataId;

    Pages.delete({dataId: data}, function(answer) {
      if(answer.response == 'success') {
        $location.path('/sidor');
      }
    });
  };

  $scope.submit = function(projectImages, tempoptions, flow) {
    flow.upload();
    $scope.disableSubmit = true;
  };

  $scope.success = function(projectImages){
    var data = $scope.languages;
    $.each(projectImages.flow.files, function(index, value) {
      projectImages.flow.files.splice(projectImages.flow.files.indexOf(index), 1);
      $scope.languages[0].image = ({fileName: value.name, imageType: value.imageType, status: 'new'});
    });

    submitUpdate(data);
  };

  function submitUpdate(data) {

    // push the value of options into data:
    data[0].publish = $scope.options.publish ? 1 : 0;
    data[0].hidden  = $scope.options.hidden  ? 1 : 0;

    data.forEach(function(language) {
      language.status = language.status ? language.status : 'DEFAULT';
    });

    Pages.update({pagesId: $routeParams.dataId, data: data}, function(answer) {
      $scope.languages[0].image = ({fileName: answer.data.data[0].image, status: 'unchanged'});

      var notification = {
        status: 'success',
        message: 'Sidan har uppdaterats. Klicka här för att visa alla sidor',
        link: 'nyheter'
      };

      $scope.disableSubmit = false;
      $scope.$parent.notifications = [notification];

      $route.reload();

      $timeout(function() {
        if($scope.$parent.notifications.length == 1) {
          $scope.$parent.notifications = [];
        }

        else {
          $scope.$parent.notifications = $scope.$parent.notifications[$scope.$parent.notifications.length - 1];
        }
      }, 4000);
      // Om insättningen av översättningarna lyckades utan PHP-error

    });
  }

  if($routeParams.pageid){
    Pages.get({pagesId: $routeParams.pageid}, function(page) {

      $scope.header = page.pageName;
      $scope.body = page.pageContent;
      $scope.image = page.pageHeaderImage;

    });

    $scope.edit = function() {
      // var newHeader = $scope.pageheader;
      // var newContent = $scope.pagebody;

      var data = {
        pageName: $scope.header,
        pageContent: $scope.body,
        pageId: $routeParams.pageid
      };

      Pages.update(data, function(){
        // Händelse när datan sparas
      });
    };
  }
  else{
    var pages = Pages.query(function(){});
    $scope.data = pages;
  }

  $scope.templates = [
    {
      name: 'Vanlig undersida',
      value: 1
    },
    {
      name: 'Nyhetslistan',
      value: 2
    },
    {
      name: 'Kontakt',
      value: 3
    },
    {
      name: 'Specifik nyhet',
      value: 4
    },
    {
      name: 'Startsida',
      value: 'index'
    }
  ];

  $scope.options = [];
  $scope.pages = pagesData;

  $scope.removeImage = function(a){
    $scope.languages[0].image.status = 'delete';
  };

  $scope.$on('$routeChangeSuccess', function() {
    $scope.$parent.menuOpen = false;
  });
});
