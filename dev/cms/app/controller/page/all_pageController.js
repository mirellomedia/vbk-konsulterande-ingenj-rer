app.controller('all_pageController', function($scope, $timeout, pageLanguages, $routeParams, $location, $http, ngDialog, $rootScope, Pages, pageInformation, Languages) {

  var selectedLanguage = 'sv';

  // Hämtar ut array innehållande alla språk som finns tillgängliga
  Languages.query(function(data){
    // Hämtar alla länder, men visar bara tabbarna om en översättning är vald.
    //console.log("Nu hämtar vi lite språk");
    languages                 = pageLanguages.init(data);
    $scope.languages          = languages;

    Pages.query(function(data){
      pageInformation.init(data);
        $scope.pages = [];
        $scope.pages = pageInformation.get();
        $scope.selectedLanguage = $scope.languages[$scope.languages.length -1].languageCode;
      });
    });

    $scope.$watch('options', function(hej, ge){
      //console.log(hej + ge);
    });

    $scope.$on('flow::fileAdded', function (event, $flow, flowFile) {
      event.preventDefault();//prevent file from uploading
    });

    $scope.addTranslation = function(){
      var remaininglanguages    = pageLanguages.remLanguages();
      if(remaininglanguages.length > 0){
        var languages             = pageLanguages.addTranslation();
          $scope.selectedLanguage   = languages[languages.length -1].languageCode;
          $scope.languages          = languages;
          $scope.remainingtranslations = remaininglanguages.length;
      }

      if(remaininglanguages.length === 0){
        $scope.remainingtranslations = 0;
      }
    };

    $scope.removeTranslation = function(languageCode){
      var languages             = pageLanguages.removeTranslation(languageCode);
      $scope.selectedLanguage   = languages[languages.length -1].languageCode;
      $scope.languages          = languages;

      var remaininglanguages    = pageLanguages.remLanguages();
      if(remaininglanguages.length === 0){
        $scope.remainingtranslations = 0;
      }
      else{
        $scope.remainingtranslations = remaininglanguages.length;
      }
    };

    $scope.toggleLanguage = function(languageCode){
      $scope.selectedLanguage = languageCode;
    };

    $scope.submit = function(tempoptions, tempfile) {
      var options = tempoptions;
      var translations = $scope.languages;
      var file = tempfile;

      Pages.insert(options, function(answer) {
        // Börjar med att skapa en undersida med allt vad options har att erbjuda

        if(answer.response == 'success') {
          // Om skapandet av undersidan gick bra fortsätter inläggningen av översättningarna
          $.each(translations, function(index, value) {
            if(file.length > 0){
              value.image = file[0].name;
            }
          });
          Pages.insert(translations, function(answer) {

            if(answer.response == 'success') {
              var notification = {
                status: 'success',
                message: 'Undersidan sparades.',
                noticeId: '999'
              };
              $scope.$parent.notifications = [notification];

              $timeout(function(){
                  if($scope.$parent.notifications.length == 1){
                    $scope.$parent.notifications = [];
                  }
                  else{
                    $scope.$parent.notifications = $scope.$parent.notifications[$scope.$parent.notifications.length - 1];
                  }
              }, 3000);
              // Om insättningen av översättningarna lyckades utan PHP-error
            }
            else{
              // Om insättningen misslyckades utan PHP-error
            }

          }); // Pages.insert(translations)

        } // Pages.insert(options)

        else{
          // Om skapandet misslyckades utan PHP-error

        }
      });

    };

  if($routeParams.pageid){
    Pages.get({pagesId: $routeParams.pageid},function(page) {

      $scope.header = page.pageName;
      $scope.body = page.pageContent;
    });

    $scope.edit = function() {
      // var newHeader = $scope.pageheader;
      // var newContent = $scope.pagebody;

      var data = {
        pageName: $scope.header,
        pageContent: $scope.body,
        pageId: $routeParams.pageid
      };

      Pages.update(data, function(){
        // Händelse när datan sparas
      });
    };
  }
  else{
    var pages = Pages.query(function(){});
    $scope.data = pages;
  }

  $scope.$on('$routeChangeSuccess', function() {
    $scope.$parent.menuOpen = false;
  });



});
