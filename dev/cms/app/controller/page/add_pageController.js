app.controller('add_pageController', function($scope, $timeout, pageLanguages, $routeParams, $location, $http, ngDialog, $rootScope, Pages, pageInformation, Languages) {

  var selectedLanguage = 'sv';

  Languages.query(function(data) {
    languages        = pageLanguages.init(data);
    $scope.languages = languages;
  }).$promise;

  Pages.query(function(data) {
    pageInformation.init(data);
    $scope.pages = [];
    $scope.pages = pageInformation.get();
    $scope.selectedLanguage = $scope.languages[$scope.languages.length -1].languageCode;
  }).$promise;

  $scope.$on('flow::fileAdded', function (event, $flow, flowFile) {
    event.preventDefault();//prevent file from uploading
  });

  $scope.addTranslation = function(){
    var remaininglanguages    = pageLanguages.remLanguages();
    if(remaininglanguages.length > 0){
      var languages             = pageLanguages.addTranslation();
        $scope.selectedLanguage   = languages[languages.length -1].languageCode;
        $scope.languages          = languages;
        $scope.remainingtranslations = remaininglanguages.length;
    }

    if(remaininglanguages.length === 0){
      $scope.remainingtranslations = 0;
    }
  };

  $scope.removeTranslation = function(languageCode){
    var languages             = pageLanguages.removeTranslation(languageCode);
    $scope.selectedLanguage   = languages[languages.length -1].languageCode;
    $scope.languages          = languages;

    var remaininglanguages    = pageLanguages.remLanguages();
    if(remaininglanguages.length === 0){
      $scope.remainingtranslations = 0;
    }
    else{
      $scope.remainingtranslations = remaininglanguages.length;
    }
    //console.log($scope.languages);
  };

  $scope.toggleLanguage = function(languageCode){
    $scope.selectedLanguage = languageCode;
  };

  $scope.submit = function(Images, flow) {
    if (Images.flow.files.length) {
      flow.upload();
    }
    else {
      submitUpdate($scope.languages);
    }
  };

    $scope.success = function(Images){
      var data = $scope.languages;
      $.each(Images.flow.files, function(index, value) {
        Images.flow.files.splice(Images.flow.files.indexOf(index), 1);
        $scope.languages[0].image = value.name;
      });

      submitUpdate(data);
    };

  function submitUpdate(data){
    tempoptions = $scope.options;
    var translations = $scope.languages;
        translations[0].options = {
          parent: tempoptions.parent,
          template: tempoptions.template,
          publish: tempoptions.publish
        };

    console.log(translations);

        Pages.insert(translations, function(answer) {
          console.log(answer);
          if(answer.response == 'success') {
            $scope.languages[0].image = answer.data[0].image;
            //console.log(answer);
            //console.log("^ Översättningar sparade...");
            //console.log(" ");
            var notification = {
              status: 'success',
              message: 'Undersidan sparades.',
              noticeId: '999'
            };
            $scope.$parent.notifications = [notification];

            $timeout(function(){
                if($scope.$parent.notifications.length == 1){
                  $scope.$parent.notifications = [];
                }
                else{
                  $scope.$parent.notifications = $scope.$parent.notifications[$scope.$parent.notifications.length - 1];
                }
            }, 3000);
            // Om insättningen av översättningarna lyckades utan PHP-error
          }
          else {
            // Om insättningen misslyckades utan PHP-error
            console.log("Uh oooh, fel på translation");
          }

        }); // Pages.insert(translations)

  }

  $scope.templates = [
    {
      name: 'Vanlig undersida',
      value: 1
    },
    {
      name: 'Nyhetslistan',
      value: 2
    },
    {
      name: 'Kontakt',
      value: 3
    },
    {
      name: 'Specifik nyhet',
      value: 4
    },
    {
      name: 'Startsida',
      value: 'index'
    }
  ];

  $scope.options = [];
  $scope.options.template = $scope.templates[0].value;

  $scope.$on('$routeChangeSuccess', function() {
    $scope.$parent.menuOpen = false;
  });
});

