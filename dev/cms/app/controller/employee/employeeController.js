app.controller('employeeController', function(
  $scope,
  $routeParams,
  $location,
  $http,
  $timeout,
  $rootScope,
  Employees,
  Groups,
  filterFilter
) {


  if ($routeParams.employeeid) {
    Employees.get({employeesId: $routeParams.employeeid}, function(data) {
      $scope.employeeInitial = angular.copy(data);
      $scope.employee = data;

      $scope.employee.employeeGroups = data.employeeGroups || [];
    });

  }

  else {
    $scope.employee = {};
    $scope.employee.employeeGroups = [];

    Employees.query(function(data) {
      $scope.employees = data;
    });
  }


  Groups.query(function(groups) {
    $scope.groups = groups;
  });

  $scope.toggleGroup = function(group) {
    var array = $scope.employee.employeeGroups,
        id    = group.employeegroupId;

    if (array.indexOf(id) === -1) {
      array.push(id);
    }

    else {
      array.splice(array.indexOf(id), 1);
    }
  };

  $scope.submit = function(Images) {
    Images.flow.upload();
  };

  $scope.success = function(Images){
    $.each(Images.flow.files, function(index, value) {
      Images.flow.files.splice(Images.flow.files.indexOf(index), 1);
      $scope.employee.employeeImage = ({fileName: value.name, imageType: 'employee', status: 'changed'});
    });

    submitUpdate();
  };

  function submitUpdate() {
    var data    = $scope.employee;
        initial = $scope.employeeInitial;

    $scope.disableSubmit = true;

    if ($routeParams.employeeid) {
      if (data.login === true && data.login !== initial.login) {
        data.sendEmail = true;
      } else {
        data.sendEmail = false;
      }

      // todo: DELETE user from table "employeedata" when this is true
      if (data.login === false && data.login !== initial.login) {
        data.removeLogin = true;
      } else {
        data.removeLogin = false;
      }

      Employees.update({employeeId: $routeParams.employeeid, data: data}, function(answer) {
        $scope.employee.employeeImage.fileName = answer.data.data.employeeImage.fileName;
        // Todo: Implement the new notification system
        var notification = {
          status: 'success',
          message: answer.data.data.employeeName +'s uppgifter har ändrats.',
          link: ''
        };


        $scope.$parent.notifications = [notification];
        $scope.disableSubmit = false;

        $timeout(function() {
          if ($scope.$parent.notifications.length == 1) {
            $scope.$parent.notifications = [];
          }

          else {
            $scope.$parent.notifications = $scope.$parent.notifications[$scope.$parent.notifications.length - 1];
          }
        }, 4000);
      });
    }

    else {
      if (data.login === true) {
        data.sendEmail = true;
      } else {
        data.sendEmail = false;
      }

      Employees.insert(data, function(answer) {
        if (answer) {

          var notification = {
            status: 'success',
            message: $scope.employee.employeeName + ' har lagts till i systemet.',
            link: ''
          };

          $scope.resetForm();
          $scope.$parent.notifications = [notification];
          $scope.disableSubmit = false;

          $location.path('/personal');

          $timeout(function() {

            if ($scope.$parent.notifications.length == 1) {
              $scope.$parent.notifications = [];
            }

            else {
              $scope.$parent.notifications = $scope.$parent.notifications[$scope.$parent.notifications.length - 1];
            }
          }, 3000);
          // Om insättningen av översättningarna lyckades utan PHP-error

        }
      });
    }
  }


  $scope.delete = function() {
    var data = $routeParams.employeeid;

    Employees.delete({employeesId: data}, function(answer) {
      $location.path('/personal');
    });
  };


  $scope.cities = [
    { name: 'Göteborg', value: 'Göteborg' },
    { name: 'Skövde', value: 'Skövde' }
  ];

  $scope.resetForm = function() {
    $scope.employee = {
      "employeeCity": $scope.cities[0].value,
      "employeeGroups": [],
      "employeeName": "",
      "employeePhone": "",
      "employeeEmail": "",
      "employeeTitle": ""
    };
  };

  if ($scope.employee) {
    $scope.resetForm();
  }
});
