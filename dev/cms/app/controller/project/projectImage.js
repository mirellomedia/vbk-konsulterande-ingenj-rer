app.controller('editImage', function($scope, $sanitize, $timeout, $routeParams, $filter, $location, $http, ngDialog, $rootScope, Project){
  var projectId = parseInt($routeParams.projectId);
  console.log(projectId);

  Project.get({projectId: projectId}, function(data){
    $scope.project = data;
    console.log(data);

    angular.forEach($scope.project.projectImages, function(value, key){
      if(value.imageType == 2){
        $scope.product.projectImage = value;
      }
    });
  });

  $scope.submit = function(file, type){
    if(file[0].name.length > 0){
      $scope.project.image = file[0].name;
      console.log($scope.project.image);
    }

    $scope.project.imagetype = type;
    Project.update($scope.project, function(temp){
      console.log($scope.$parent.notifications);
    });
  };

}); // END OF CONTROLLER
