app.controller('add_projectController', function(
  $scope,
  $timeout,
  projectLanguages,
  $routeParams,
  $location,
  $http,
  ngDialog,
  $rootScope,
  projectInformation,
  projectCategories,
  Languages,
  Project_Languages,
  api
) {

  $scope.predicate    = 'skapades';
  $scope.reverse      = 'false';


  function resetLanguages() {
    // Hämtar ut array innehållande alla språk som finns tillgängliga
    Languages.query(function(data) {
      projectLanguages.reset();
      // Hämtar alla länder, men visar bara tabbarna om en översättning är vald.

      var languages             = projectLanguages.init(data);
      $scope.languages          = languages;
      $scope.selectedLanguage   = $scope.languages[$scope.languages.length -1].languageCode;
    });
  }

  resetLanguages();


    $scope.addTranslation = function(){
      var remaininglanguages    = projectLanguages.remLanguages();
      if(remaininglanguages.length > 0){
        var languages             = projectLanguages.addTranslation();
          $scope.selectedLanguage   = languages[languages.length -1].languageCode;
          $scope.languages          = languages;
          $scope.remainingtranslations = remaininglanguages.length;
      }
      remaininglanguages = projectLanguages.remLanguages();

      if(remaininglanguages.length === 0){
        $scope.remainingtranslations = 0;
      }
    };

    $scope.removeTranslation = function(languageCode){
      var languages             = projectLanguages.removeTranslation(languageCode);
      $scope.selectedLanguage   = languages[languages.length -1].languageCode;
      $scope.languages          = languages;

      var remaininglanguages    = projectLanguages.remLanguages();

      if(remaininglanguages.length === 0) {
        $scope.remainingtranslations = 0;
      }
      else{
        $scope.remainingtranslations = remaininglanguages.length;
      }
      //console.log($scope.languages);
    };

    $scope.toggleLanguage = function(languageCode){
      $scope.selectedLanguage = languageCode;
    };


    $scope.submit = function(projectImages, tempoptions, flow) {
      $scope.disableSubmit = true;
      flow.upload();
    };

    $scope.success = function(projectImages){
      var data = $scope.languages;

      var images = [];

      $.each(projectImages.flow.files, function(index, value) {
        projectImages.flow.files.splice(projectImages.flow.files.indexOf(index), 1);

        images.push({
          fileName: value.name,
          imageType: value.imageType,
          status: 'new'
        });
      });

      submitUpdate(images);
    };

    function submitUpdate(images) {
      // merge the objects:
      var key;
      var data = {};

      for (key in $scope.options) {
        data[key] = $scope.options[key];
      }

      for (key in $scope.languages[0]) {
        data[key] = $scope.languages[0][key];
      }

      data.projectImages = images;

      console.log(data);

      api.insert({
        endPoint: 'project',
      }, data, function(answer) {

        var notification = {
          status: 'success',
          message: 'Referensprojektet lades till.',
          noticeId: '999'
        };

        resetLanguages();
        $scope.options = {};

        $scope.$parent.notifications = [notification];
        $scope.disableSubmit = false;

        $timeout(function() {
          if($scope.$parent.notifications.length == 1) {
            $scope.$parent.notifications = [];
          }

          else {
            $scope.$parent.notifications = $scope.$parent.notifications[$scope.$parent.notifications.length - 1];
          }
        }, 4000);

      });
    }

    $scope.removeImage = function(i, a){
      a[i].status = 'delete';
    };

    $scope.projectCategories = projectCategories.query();
    $scope.options = {};
    $scope.projectTypes = [
      {
        name: 'Byggprojektering',
        id: 1
      },
      {
        name: 'Projektadministration',
        id: 2
      },
      {
        name: 'Underhåll',
        id: 3
      }
    ];

    $scope.$on('$routeChangeSuccess', function() {
      $scope.$parent.menuOpen = false;
    });


}); // END OF CONTROLLER


app.directive('restrict', function($parse) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, iElement, iAttrs, controller) {
            scope.$watch(iAttrs.ngModel, function(value) {
                if (!value) {
                    return;
                }
                $parse(iAttrs.ngModel).assign(scope, value.toLowerCase().replace(new RegExp(iAttrs.restrict, 'g'), '').replace(/\s+/g, '-'));
            });
        }
    }
});
