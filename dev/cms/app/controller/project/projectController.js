app.controller('projectController', function(
  $scope,
  $timeout,
  projectLanguages,
  $routeParams,
  $location,
  $http,
  $route,
  ngDialog,
  $rootScope,
  projectInformation,
  projectCategories,
  Languages,
  Project_Languages,
  api
) {

  $scope.predicate = 'projectAdded';
  $scope.reverse   = 'false';


  $scope.addTranslation = function(){
    var remaininglanguages    = projectLanguages.remLanguages();
    if(remaininglanguages.length > 0){
      var languages             = projectLanguages.addTranslation();
        $scope.selectedLanguage   = languages[languages.length -1].languageCode;
        $scope.languages          = languages;
        $scope.remainingtranslations = remaininglanguages.length;
    }
    remaininglanguages  = projectLanguages.remLanguages();
    if(remaininglanguages.length === 0){
      $scope.remainingtranslations = 0;
    }
  };

  $scope.submit = function(projectImages, tempoptions, flow) {
    flow.upload();
  };

  $scope.success = function(projectImages){
    var data = $scope.languages;

    $.each(projectImages.flow.files, function(index, value) {
      projectImages.flow.files.splice(projectImages.flow.files.indexOf(index), 1);
      $scope.languages[0].projectImages.push({
        fileName: value.name,
        imageType: value.imageType,
        status: 'new'
      });
    });

    submitUpdate(data);
  };

  function submitUpdate(data) {
    $scope.disableSubmit = true;

    if (!$scope.languages[0].projectImages) {
      $scope.languages[0].projectImages = [];
    }

    $.each($scope.languages[0].projectImages, function(index, obj) {
      obj.projectimageOrder = index + 1;
      obj.status = obj.status === "unchanged" ? "changed" : obj.status;
    });

    api.update({
      endPoint: 'project'
    }, data, function(answer) {
      $.each(answer[0].projectImages, function(index, value) {
        if(value.status == 'new'){
          value.status = 'unchanged';
          $scope.languages[0].projectImages.splice($scope.languages[0].projectImages.indexOf(index), 1);

          $scope.languages[0].projectImages.push(value);
        }
      });

      var notification = {
        status: 'success',
        message: 'Ändringarna sparades.',
        noticeId: '999'
      };

      $scope.$parent.notifications = [notification];
      $scope.disableSubmit = false;

      $timeout(function() {
        if($scope.$parent.notifications.length == 1) {
          $scope.$parent.notifications = [];
        }

        else {
          $scope.$parent.notifications = $scope.$parent.notifications[$scope.$parent.notifications.length - 1];
        }
      }, 4000);

      $route.reload();

    });
  }


  $scope.removeTranslation = function(languageCode){
    var languages             = projectLanguages.removeTranslation(languageCode);
    $scope.selectedLanguage   = languages[languages.length -1].languageCode;
    $scope.languages          = languages;

    var remaininglanguages    = projectLanguages.remLanguages();
    if(remaininglanguages.length === 0){
      $scope.remainingtranslations = 0;
    }
    else {
      $scope.remainingtranslations = remaininglanguages.length;
    }
  };

  $scope.toggleLanguage = function(languageCode){
    $scope.selectedLanguage = languageCode;
  };


  $scope.delete = function() {
    var data = $routeParams.projectId;

    api.delete({
      endPoint: 'project'
    }, data, function(answer) {
      $location.path('/projekt');
    });
  };


  if ($routeParams.projectId){
      Languages.query(function(data){
    // Hämtar alla länder, men visar bara tabbarna om en översättning är vald.
    projectLanguages.reset();
    var languages             = projectLanguages.init(data);

    var allLanguages          = projectLanguages.query();
    options = {};
    $scope.options = options;
      // Kontroll för artiklar och språk

    $.each(allLanguages, function( index, value ) {
        api.get(
            { endPoint: 'project',
              data: $routeParams.projectId + '?' + value.languageId
            },
            function(data){
              if(data){
                $.each(data, function(i, n){
                  value[i] = n;
                });
              }
              else{
                allLanguages.splice(index, 1);
              }
        });
      });


      $scope.languages          = allLanguages;
      $scope.selectedLanguage   = $scope.languages[0].languageCode;
    });

    $scope.projectCategories = projectCategories.query();

    $scope.projectTypes = [
      {
        name: 'Byggprojektering',
        id: 1
      },
      {
        name: 'Projektadministration',
        id: 2
      },
      {
        name: 'Underhåll',
        id: 3
      }
    ];

  }

  else {
    api.query({
      endPoint: 'project'
    }, function(data) {
      $scope.data = data;
    });
  }


  $scope.$on('$routeChangeSuccess', function() {
    $scope.$parent.menuOpen = false;
  });

  $scope.removeImage = function(i, a){
    a[i].status = 'delete';
  };

});
