app.controller('edit_newsController', function(
  $scope,
  $filter,
  $timeout,
  $routeParams,
  $filter,
  $location,
  $http,
  $rootScope,
  articleLanguages,
  News,
  Languages,
  api,
  News_Languages
) {

  $scope.options = {};

  // Hämtar ut array innehållande alla språk som finns tillgängliga
  Languages.query(function(data){

    // Hämtar alla länder, men visar bara tabbarna om en översättning är vald.
    articleLanguages.reset();

    var languages             = articleLanguages.init(data);
    var allLanguages          = articleLanguages.query();

      // Kontroll för artiklar och språk
      $.each(allLanguages, function( index, value ) {
        api.get({
          endPoint: 'article',
          data: $routeParams.articleid + '?' + value.languageId
        }, function(data){
          if (data) {
            $.each(data, function(i, n){
              value[i] = n;
            });

            var obj = $scope.languages[0];

            $scope.options.publish = obj.publishDate <= obj.currentDate ? true : false;
          }

          else {
            allLanguages.splice(index, 1);
          }
        });
      });

      $scope.languages          = allLanguages;
      $scope.selectedLanguage   = $scope.languages[0].languageCode;

      var watchThese = [
        'languages[0].header',
        'languages[0].sum',
        'languages[0].htmlcontent'
      ];

      $scope.$watchGroup(watchThese, function() {
        $scope.disableSubmit = ($scope.languages[0].header && $scope.languages[0].sum.length && $scope.languages[0].htmlcontent.length) ? false : true;
      });
    });

    $scope.addTranslation = function(){
      var remaininglanguages    = articleLanguages.remLanguages();
      if(remaininglanguages.length > 0){
        var languages             = articleLanguages.addTranslation();
          $scope.selectedLanguage   = languages[languages.length -1].languageCode;
          $scope.languages          = languages;
          $scope.remainingtranslations = remaininglanguages.length;
      }
      remaininglanguages    = articleLanguages.remLanguages();
      if(remaininglanguages.length == 0){
        $scope.remainingtranslations = 0;
      }
    }

    $scope.removeTranslation = function(languageCode){
      var languages             = articleLanguages.removeTranslation(languageCode);
      $scope.selectedLanguage   = languages[languages.length -1].languageCode;
      $scope.languages          = languages;

      var remaininglanguages    = articleLanguages.remLanguages();
      if(remaininglanguages.length == 0){
        $scope.remainingtranslations = 0;
      }
      else{
        $scope.remainingtranslations = remaininglanguages.length;
      }
      //console.log($scope.languages);
    }

    $scope.toggleLanguage = function(languageCode){
      $scope.selectedLanguage = languageCode;
    }

    $scope.submit = function() {
      var data = $scope.languages;

      $scope.disableSubmit = true;

      api.update({
        endPoint: 'article',
        data: $routeParams.articleid
      }, data, function(answer){

        var notification = {
          status: 'success',
          message: 'Ändringarna sparades.',
          link: ''
        };

        $scope.$parent.notifications = [notification];
        $scope.disableSubmit = false;

        $timeout(function() {
          if ($scope.$parent.notifications.length == 1) {
            $scope.$parent.notifications = [];
          }

          else {
            $scope.$parent.notifications = $scope.$parent.notifications[$scope.$parent.notifications.length - 1];
          }
        }, 4000);
      });
    };

    $scope.delete = function() {
      var data = $routeParams.articleid;

      api.delete({
        endPoint: 'article'
      }, data, function(answer) {
        $location.path('/nyheter');
      });

    };

    $scope.$on('$routeChangeSuccess', function() {
      $scope.$parent.menuOpen = false;
    });

}); // END OF CONTROLLER


app.directive('restrict', function($parse) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, iElement, iAttrs, controller) {
            scope.$watch(iAttrs.ngModel, function(value) {
                if (!value) {
                    return;
                }
                $parse(iAttrs.ngModel).assign(scope, value.toLowerCase().replace(new RegExp(iAttrs.restrict, 'g'), '').replace(/\s+/g, '-'));
            });
        }
    }
});
