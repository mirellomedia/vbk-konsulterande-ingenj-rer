app.controller('add_newsController', function(
  $scope,
  $filter,
  $timeout,
  $routeParams,
  $filter,
  $location,
  $http,
  ngDialog,
  $rootScope,
  Languages,
  articleLanguages,
  api
) {

  $scope.predicate    = 'skapades';
  $scope.reverse      = 'false';

  // Hämtar ut array innehållande alla språk som finns tillgängliga
  function resetLanguages() {
    // Hämtar ut array innehållande alla språk som finns tillgängliga
    Languages.query(function(data) {
      articleLanguages.reset();
      // Hämtar alla länder, men visar bara tabbarna om en översättning är vald.

      var languages             = articleLanguages.init(data);
      $scope.languages          = languages;
      $scope.selectedLanguage   = $scope.languages[$scope.languages.length -1].languageCode;

      var watchThese = [
        'languages[0].header',
        'languages[0].sum',
        'languages[0].htmlcontent'
      ];

      $scope.$watchGroup(watchThese, function() {
        $scope.disableSubmit = ($scope.languages[0].header && $scope.languages[0].sum.length && $scope.languages[0].htmlcontent.length) ? false : true;
      });
    });
  }

  resetLanguages();


    $scope.addTranslation = function(){
      var remaininglanguages    = articleLanguages.remLanguages();
      if (remaininglanguages.length > 0){
        var languages             = articleLanguages.addTranslation();
          $scope.selectedLanguage   = languages[languages.length -1].languageCode;
          $scope.languages          = languages;
          $scope.remainingtranslations = remaininglanguages.length;
      }
      var remaininglanguages    = articleLanguages.remLanguages();
      if (remaininglanguages.length == 0){
        $scope.remainingtranslations = 0;
      }
    }

    $scope.removeTranslation = function(languageCode){
      var languages             = articleLanguages.removeTranslation(languageCode);
      $scope.selectedLanguage   = languages[languages.length -1].languageCode;
      $scope.languages          = languages;

      var remaininglanguages    = articleLanguages.remLanguages();
      if (remaininglanguages.length == 0){
        $scope.remainingtranslations = 0;
      }
      else {
        $scope.remainingtranslations = remaininglanguages.length;
      }
    }

    $scope.toggleLanguage = function(languageCode){
      $scope.selectedLanguage = languageCode;
    }

    $scope.submit = function(tempoptions) {
      var options   = tempoptions,
          languages = $scope.languages,
          valid     = (languages[0].header      !== undefined &&
                       languages[0].htmlcontent !== undefined &&
                       languages[0].sum         !== undefined);

      if (valid) {
        if (options.publish == true){
          options = {date: 'getcurrent'};
        }

        // merge the objects:
        var data = {};

        for (var key in options) {
          data[key] = options[key];
        }

        for (var key in languages[0]) {
          data[key] = languages[0][key];
        }

        api.insert({
          endPoint: 'article'
        }, data, function(answer){


          var notification = {
            status: 'success',
            message: 'Nyheten skapades.',
            noticeId: '999'
          };

          resetLanguages();
          $scope.options = {};

          $scope.$parent.notifications = [notification];
          $scope.disableSubmit = false;

          $timeout(function() {
            if($scope.$parent.notifications.length == 1) {
              $scope.$parent.notifications = [];
            }

            else {
              $scope.$parent.notifications = $scope.$parent.notifications[$scope.$parent.notifications.length - 1];
            }
          }, 4000);

          $location.path('/nyheter');
        });
      }
    };

    $scope.$on('$routeChangeSuccess', function() {
      $scope.$parent.menuOpen = false;
    });

    api.query(
    { endPoint: 'article'
    },
    function(data){
      $scope.data = data;
    });

}); // END OF CONTROLLER


app.directive('restrict', function($parse) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, iElement, iAttrs, controller) {
            scope.$watch(iAttrs.ngModel, function(value) {
                if (!value) {
                    return;
                }
                $parse(iAttrs.ngModel).assign(scope, value.toLowerCase().replace(new RegExp(iAttrs.restrict, 'g'), '').replace(/\s+/g, '-'));
            });
        }
    }
});
