app.controller('trashController', function(
  $scope,
  $timeout,
  $routeParams,
  $location,
  $http,
  $rootScope,
  api,
  Languages,
  articleLanguages
) {

  $scope.predicate  = 'deletedAt';
  $scope.reverse    = 'false';


  function notification(message, status) {
    status = status || 'success';

    var newNotification = {
      status: status,
      message: message,
      noticeId: '999'
    };

    $scope.$parent.notifications = [newNotification];

    $timeout(function() {
      $scope.$parent.notifications = [];
    }, 4000);
  }


  // get all items in trashcan
  api.query({
    endPoint: 'trash'
  }, function(data) {
    $scope.data = data;
  });


  // function to restore item from the trashcan
  $scope.restore = function(item) {
    api.update({
      endPoint: 'trash',
      data: item.id
    }, item, function(data) {

      console.log(data);

      // for whatever reason, we can't declare a key to this value.
      // it holds the amount of rows affected.
      if (data[0] > 0) {
        var array = $scope.data;

        // splice the array on success
        array.splice(array.indexOf(item), 1);

        notification(item.name + ' lades tillbaka.');
      }

      else {
        notification(item.name + ' kan inte läggas tillbaka.', 'error');
      }

    });
  };


  // really delete items this time
  $scope.delete = function(item) {
    item["action"] = 'delete';

    api.delete({
      endPoint: 'trash',
      data: item.id
    }, item, function(data) {

      var array = $scope.data;

      // splice the array on success
      $scope.data.splice($scope.data.indexOf(item), 1);

      notification(item.name + ' raderades permanent.');

    });
  };
});
