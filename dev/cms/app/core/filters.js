angular.module('filters', [])

// formatera ett värde till bytes:
.filter('bytes', function() {
  return function(bytes, precision) {
    if (isNaN(parseFloat(bytes)) || !isFinite(bytes)) return '-';
    if (typeof precision === 'undefined') precision = 1;
    var units = ['bytes', 'kB', 'MB', 'GB', 'TB', 'PB'],
      number = Math.floor(Math.log(bytes) / Math.log(1024));
    
    return (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision) +  ' ' + units[number];
  }
})


// gör en textsträng url-vänlig:
.filter('urlConverter', function() {
  return function(text, separator, lowercase) {

    var text = text || '',
        output,
        q_separator,
        translation = {},
        tags,
        commentsAndPhpTags,
        key,
        replacement,
        leadingTrailingSeparators;

    // separera ord:
    if (!separator) {
      separator = '-';
    }

    // escapea alla regex characters
    q_separator = (separator + '').replace(new RegExp('[.\\\\+*?\\[\\^\\]$(){}=!<>|:\\-]', 'g'), '\\$&');

    // regex to replacement object
    translation['&.+?;']           = '';
    translation['[^a-z0-9 _-åäö]'] = '';  // tillåt endast dessa tecken
    translation['[åäáà]']          = 'a';
    translation['[öø]']            = 'o';

    translation['\\s+'] = separator; // konvertera whitespace till separator
    translation['(' + q_separator + ')+'] = separator;

    // ta bort html/php
    tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi;
    commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
    output = text.replace(commentsAndPhpTags, '').replace(tags, '');


    for(key in translation){
      replacement = translation[key];
      key = new RegExp(key, 'ig');
      output = output.replace(key, replacement);
    }

    output = output.toLowerCase();

    // lägg aldrig in dubbla separators och inled/avsluta aldrig med dem
    leadingTrailingSeparators = new RegExp('^' + q_separator + '+|' + q_separator + '+$');
    output = output.replace(leadingTrailingSeparators, '');

    // trimma whitespace (es5)
    output = output.trim();

    return output;
  }
})


// konvertera mysql-datetime till iso 8601:
.filter('dateToISO', function() {
  return function(input) {
    input = new Date(input).toISOString();

    return input;
  };
});