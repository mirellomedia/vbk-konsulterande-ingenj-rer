// Skapa applikation med namnet "app" och länka in bibliotek
var app = angular.module('app',
                        ['ngRoute',
                         'ngDialog',
                         'ngTouch',
                         'flow',
                         'ngAnimate',
                         'ngResource',
                         'monospaced.elastic',
                         'filters',
                         'slugifier',
                         'redactor',
                         'ngSanitize',
                         'as.sortable'
                        ]
);
