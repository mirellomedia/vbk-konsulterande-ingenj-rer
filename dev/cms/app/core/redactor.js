angular.module('redactor', [])

.directive('redactor', function ($timeout) {
  return {
    require: '?ngModel',
    link: function (scope, el, attrs, ngModel) {
      var updateModel, errorHandling;

      // Function to update model
      updateModel = function() {
        if(!scope.$$phase) {
          scope.$apply(ngModel.$setViewValue(el.redactor('code.get')));
        }
      };

      uploadErrorHandling = function(response) {
        alert("Error: " + response.error);
      };

      // Get the redactor element and call update model
      el.redactor({
        minHeight: 100,
        toolbarFixedTopOffset: 45,
        tabKey: false,
        callbacks: {
          keyup: updateModel,
          keydown: updateModel,
          change: updateModel
        },
        execCommand: updateModel,
        autosave: updateModel,
        imageUpload: '/model/module/imageupload.php',
        imageUploadError: uploadErrorHandling,
        imageGetJson: '/model/module/imageupload.php'
      });

      // Call to sync the redactor content
      ngModel.$render = function () {
        $timeout(function() {
          el.redactor('code.set', ngModel.$viewValue || '');
        });
      };
    }
  };
});

/*
buttons: ['formatting', '|', 'bold', 'italic', 'underline', 'html', 'deleted', '|',
        'unorderedlist', 'orderedlist', 'outdent', 'indent', '|',
        'image', 'video', 'file', 'table', 'link', '|', 'alignment', '|', 'horizontalrule'],
replaceDivs: false,
*/
