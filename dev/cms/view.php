<?php
  // Kontrollerar om sessionen är startad och innehåller ett userid. Om inte, skicka vidare användaren till loginsidan
  $root = dirname(__FILE__);
  $path = $root . '/model/session';

  session_save_path($path);
  session_start();

  if(!isset($_SESSION['userid'])){
    header('Location: /login');
    exit();
  }

  // Funktioner och inställningar:
  require_once("$root/view/template/setup.php");

  // get head
  htmlHead();
?>

<body class="has-sidebar has-mobile-menu"
      ng-controller="routeController"
      ng-class="{'menu-open': menuOpen}"

      ng-init=       "menuOpen = false"
      ng-swipe-left= "menuOpen = false"
      ng-swipe-right="menuOpen = true"
      ng-swipe-disable-mouse>

<?php require_once("$root/view/template/notifications.php"); ?>

<div class="site-container">
  <div class="mobile-menu--push">
    <!-- start MOBILE PUSH -->
    
    <div class="site-width site-body">
      <main class="site-main"
            ng-view
            ng-click="$parent.menuOpen = false"
            autoscroll="true"
            ng-cloak>

        <!-- MAIN CONTENT -->
      </main>

      <footer class="site-footer">
        <div class="site-footer__fade">
          <img src="/assets/img/logos/logo-vbk-g.png">
          
          <p>
            Copyright © <?php date_default_timezone_set('Europe/Stockholm'); echo date("Y") ?> | <a href="http://mirello.se" target="_blank">Mirello Media AB</a>
          </p>
          
          <p>
            System framtaget åt <a href="http://www.vbk.se" target="_blank">VBK Konsulterande ingenjörer AB</a>
          </p>
        </div>
      </footer>
    </div>

    <!-- end MOBILE PUSH -->
  </div>
</div>


<?php
  // Sidonavigation:
  require_once("$root/view/template/site-aside.php");

  // Top bar:
  require_once("$root/view/template/top-bar.php");

  // Scipts:
  globalScripts();
?>


</body>
</html>