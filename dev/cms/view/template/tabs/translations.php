<div class="tab-wrap">
  <div class="tab"
       ng-repeat="language in languages"
       ng-if="language.status !== 'DELETED'"
       ng-class="{current : selectedLanguage == '{{language.languageCode}}' || languages.length == 1,
                 'confirmation-required': removeTranslationConfirm }">

    <span class="tab__text"
          ng-click="toggleLanguage('{{language.languageCode}}')">{{language.languageName}}
    </span>

    <div class="confirmation-dialog__wrap" on-blur="removeTranslationConfirm = false" tabindex="-1">
      <div class="tab--remove"
           ng-click="removeTranslationConfirm = !removeTranslationConfirm"
           ng-hide="languages.length <= 1">
      </div>

      <div class="confirmation-dialog confirmation-dialog--left confirmation-dialog--bottom"
           ng-show="removeTranslationConfirm">

        <button type="button"
                class="button action-remove"
                ng-click="removeTranslation(language)">Radera
        </button>

        <div class="confirmation-dialog__text">
          <p>Vill du verkligen <strong>radera</strong> den {{language.languageName | lowercase}} versionen?</p>
        </div>
      </div>
    </div>
  </div>

  <button type="button"
          class="tab add-tab"
          ng-class="{'nothing-more-to-add' : remainingtranslations == 0}"
          ng-disabled="remainingtranslations == 0"
          title="Lägg till nytt språk"
          ng-click="addTranslation()">
  </button>
</div>
