<?php

function confirmRemoval($pagetype) {
  echo '<div class="confirmation-dialog__wrap" tabindex="-1" on-blur="toggleConfirmation = false" ng-hide="isFrontPage || staticPage">
          <button type="button"
                  class="button action-remove icon-only"
                  ng-click="toggleConfirmation = !toggleConfirmation"
                  ng-class="{\'confirmation-required\': toggleConfirmation}">
          </button>

          <div class="confirmation-dialog confirmation-dialog--right confirmation-dialog--bottom"
               ng-show="toggleConfirmation">

            <button type="button"
                    ng-init="toggleConfirmation = false"
                    ng-click="delete()"
                    class="button action-remove">Radera
            </button>

            <div class="confirmation-dialog__text">
              <p>Vill du verkligen <strong>radera</strong> ' . $pagetype . '?</p>
            </div>
          </div>
        </div>';
}

function publishDate() {
  echo '<div class="input-group__row">
          <div class="limit-width">
            <input id="publish-now"
                   type="checkbox"
                   ng-model="options.publish"
                   ng-init="options.publish=true">

            <label for="publish-now">Publicera direkt</label>

            <div class="explaination-wrap">
              <div class="explaination">
                <p>Bockar du ur så sparas nyheten som opublicerad.</p>
              </div>
            </div>
          </div>
        </div>

        <div class="input-group__row" ng-show="!options.publish">
          <div class="limit-width">
            <p>Välj publiceringsdatum:</p>
          </div>

          <input type="date"
                 ng-model="options.date"
                 class="datepicker">
        </div>';
}


function buttonSubmit($pagetype, $action) {
 echo '<button type="submit"
               class="button action-add"
               ng-click="submit(options)"
               ng-disabled="createPage.$pristine || disableSubmit">'. ucfirst($action) . ' ' . $pagetype . '
       </button>';
}


function buttonSubmitFlow($pagetype) {
 echo '<button type="submit"
               class="button action-add"
               disabled
               ng-click="$flow.upload(languages, hej); submit(options,$flow.files);"
               ng-disabled="createPage.$invalid">' . $pagetype . '
       </button>';
}


function input($type, $header, $other = null) {
  if ($type == 'summary') {
    echo '<div class="input-header">
            <h3>' . $header . '</h3>
          </div>

          <div class="input-field">
            <textarea id="create-page-summary"
                      name="summary"
                      ng-model="language.sum"
                      class="msd-elastic character-counter--display">
            </textarea>

            <span class="character-counter">{{language.summary.length}}</span>
          </div>';
  }

  if ($type == 'url') {
    echo '<div class="input-header">
            <h3 class="url--title">' . $header . '</h3>

            <slug from="language.header" to="language.urlslug"></slug>
          </div>

          <div id="url--wrap" class="input-field">
            <label class="url--bar">
              <div class="url--parent">' . ($other != null ? '/' . $other : null) . '/{{languages.languageCode}}</div>
              <input id="url--slug"
                     class="url--slug"
                     disabled
                     ng-model="language.urlslug">
            </label>
          </div>';
  }

  if ($type == 'body') {
    echo '<div class="input-header">
            <h3>' . $header . '</h3>
          </div>

          <div class="input-field">
            <textarea ng-model="language.htmlcontent"
                      redactor>
            </textarea>
          </div>';
  }

  if ($type == 'header') {
    echo '<div class="input-header">
            <h3>' . $header . '</h3>
          </div>

          <div class="input-field">
            <input id="create-page-header"
                   type="text"
                   name="header"
                   ng-model="language.header"
                   required
                   class="character-counter--display">

            <span class="character-counter"
                  ng-class="{\'full\' : language.header.length > 55}"
                  data-character-soft-limit="55">{{language.header.length}}</span>
          </div>';
  }

  if ($type == 'flow') {
    echo '<div class="input-header">
            <div class="limit-width">
              <h3>' . $header . '</h3>

              <div class="explaination-wrap">
                <div class="explaination">
                  <p>För bästa resultat, använd en bild med ca. 1000px i bredd av formatet JPG.</p>
                </div>
              </div>
            </div>
          </div>

          <div class="input-field">
            <div class="flow__wrap flex-parent" ng-repeat="file in $flow.files">
              <img class="flow-image" flow-img="$flow.files[0]">

              <ul class="flow__info">
                <li><strong>Namn: </strong>{{file.name}}</li>
                <li><strong>Storlek: </strong>{{file.size | bytes}}</li>
                <li class="flex-parent"><strong>Beskrivning:</strong> <input class="flow-image__description" type="text" name="image-alt"></li>
              </ul>
            </div>

            <input id="image-upload"
                   type="file"
                   ng-model="language.image"
                   flow-btn>

              <label for="image-upload"
                     class="button">
                {{$flow.files[0].name.length || language.image.fileName.length ? \'Byt bild\' : \'Lägg till bild\'}}
              </label>
          </div>';
  }

  if ($type == 'description') {
    echo '<div class="input-header">
            <div class="limit-width">
              <h3>' . $header . '</h3>

              <div class="explaination-wrap">
                <div class="explaination">
                  <p>Rekommenderad längd, ca. 50-200 tecken.</p>
                </div>
              </div>
            </div>
          </div>

          <div class="input-field">
            <textarea id="create-page-description"
                      name="description"
                      ng-model="language.description"
                      class="msd-elastic character-counter--display"
                      required>
            </textarea>

            <span class="character-counter" data-character-soft-limit="250">{{language.description.length}}</span>
          </div>';
  }
}

?>
