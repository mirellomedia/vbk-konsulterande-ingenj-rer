<?php
    $path = dirname(dirname(dirname(__FILE__)));
    $path = $path . '/model/session';

    session_save_path($path);
    session_start();

    $user = json_decode(file_get_contents('php://input'));

    function generalControl($username, $password){

    $dbPath = dirname(dirname(dirname(__FILE__)));
    $dbPath = $dbPath . '/model/core/function/dbConnect.php';


    require_once($dbPath);


    $query = "  SELECT      e.employeeName
                ,           e.employeeEmail
                ,           ed.employeePassword
                ,           ed.employeeVerified
                ,           ed.employeeId
                ,           e.employeeImage
                FROM        employee e
                INNER JOIN  employeedata ed
                ON          ed.employeeId = e.employeeId
                WHERE       e.employeeName = ?
                OR          e.employeeEmail = ?
                LIMIT 1
             ";


    $stmt = $mysqli->stmt_init();
    $stmt->prepare($query);
    $stmt->bind_param("ss", $username, $username);
    $stmt->execute();
    $stmt->bind_result($employeeName, $employeeEmail, $employeePassword, $employeeVerified, $employeeId, $employeeImage);
    $stmt->fetch();

    if (password_verify($password, $employeePassword)) {
      $employeeName = explode(" ", $employeeName);
      $_SESSION['userid']         = $employeeId;
      $_SESSION['firstname']      = $employeeName[0];
      $_SESSION['lastname']       = $employeeName[1];
      $_SESSION['employeeimage']  = $employeeImage;
      print 'nils';
    } else {
      print 'error';
    }

    }

    if($user->username && $user->password){

      generalControl($user->username, $user->password);

    }

?>
