<aside class=     "notification__wrap">
  <div class=     "notification"
       ng-repeat= "notification in notifications"
       ng-class=  "{'notification--{{notification.status}}' : notification.status}"
       ng-animate="'animate'"
       ng-cloak>

    <a ng-show=   "notification.link.length != 0" 
       ng-click=  "close({{notification.noticeId}})" 
       class=     "notification__body"
       href=      "/#{{notification.link}}">   
      
      <div class="notification__icon"></div>

      <div class="notification__message">
        <p>{{notification.message}}</p>
      </div>
    </a>

    <div ng-show="notification.link.length == 0" 
       ng-click= "close({{notification.noticeId}})" 
       class=    "notification__body">   
      
      <div class="notification__icon"></div>

      <div class="notification__message">
        <p>{{notification.message}}</p>
      </div>
    </div>
    
    <button type="button"
            class="notification__close"
            ng-click="close({{notification.noticeId}})">
    </button>
  </div>
</aside>