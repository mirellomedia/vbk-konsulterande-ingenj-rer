<?php date_default_timezone_set('Europe/Stockholm');

// variables:
$siteTitle = 'VBK CMS';

// biblioteksversioner, yao:
$angularVersion = '1.4.0-beta.6';
$jQueryVersion  = '2.1.3';


// html + head content:
function htmlHead() {

global $siteTitle;

echo '<!doctype html>
<html class="no-js" ng-app="app">
<head>
  <meta charset="utf-8">
  <title ng-bind="title + \' – ' . $siteTitle . '\'">' . $siteTitle . '</title>

  <meta name="viewport" content="width=device-width, initial-scale=1, minimal-ui">

  <meta name="mobile-web-app-capable"                content="yes">
  <meta name="apple-mobile-web-app-capable"          content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">

  <!-- favicons -->
  <link rel="shortcut icon" sizes="16x16 32x32 48x48 64x64" href="/assets/img/favicons/favicon.ico">
  <link rel="shortcut icon" type="image/x-icon"             href="/assets/img/favicons/favicon.ico">
  <!--[if IE]><link rel="shortcut icon"                     href="/assets/img/favicons/favicon.ico"><![endif]-->

  <link rel="apple-touch-icon"                 href="/assets/img/favicons/apple-touch-icon.png">
  <link rel="apple-touch-icon" sizes="57x57"   href="/assets/img/favicons/apple-touch-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="72x72"   href="/assets/img/favicons/apple-touch-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="114x114" href="/assets/img/favicons/apple-touch-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="144x144" href="/assets/img/favicons/apple-touch-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="57x57"   href="/assets/img/favicons/apple-touch-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72"   href="/assets/img/favicons/apple-touch-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="114x114" href="/assets/img/favicons/apple-touch-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="144x144" href="/assets/img/favicons/apple-touch-icon-152x152.png">

  <meta name="msapplication-TileColor"         content="#0050a0">
  <meta name="msapplication-TileImage"         content="/assets/img/favicons/mediumtile.png">
  <meta name="msapplication-square70x70logo"   content="/assets/img/favicons/smalltile.png">
  <meta name="msapplication-square150x150logo" content="/assets/img/favicons/mediumtile.png">
  <meta name="msapplication-wide310x150logo"   content="/assets/img/favicons/widetile.png">
  <meta name="msapplication-square310x310logo" content="/assets/img/favicons/largetile.png">

  <!-- static resources -->
  <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,700|Lato:300,400,400italic,700">
  <link rel="stylesheet" href="/assets/css/style.min.css?0.9.8">

  <script src="/assets/js/modernizr-custom.min.js"></script>
</head>';
}


// globala script:
function globalScripts($pagetype = null) {

  global $angularVersion;
  global $jQueryVersion;

  echo '
    <script src="//ajax.googleapis.com/ajax/libs/jquery/'    . $jQueryVersion  . '/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/' . $angularVersion . '/angular.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/' . $angularVersion . '/angular-route.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/' . $angularVersion . '/angular-animate.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/' . $angularVersion . '/angular-resource.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/' . $angularVersion . '/angular-touch.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/' . $angularVersion . '/angular-sanitize.min.js"></script>
  ';

  if ($pagetype != 'login') {
    echo '
      <script src="/assets/js/redactor.js"></script>
      <script src="//cdnjs.cloudflare.com/ajax/libs/ng-dialog/0.1.6/ng-dialog.min.js"></script>
      <script src="/assets/js/ng-flow-standalone.min.js"></script>
      <script src="/assets/js/ng-sortable.min.js"></script>
      <script src="//cdnjs.cloudflare.com/ajax/libs/ng-dialog/0.3.0/js/ngDialog.js"></script>

      <script src="/assets/js/app.js?0.9.8"></script>
      <script src="/assets/js/main.min.js?0.9.8"></script>
    ';
  }
}
?>
