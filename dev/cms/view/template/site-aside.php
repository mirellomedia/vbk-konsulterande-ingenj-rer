<aside class="site-aside flex-parent">
  <nav class="site-nav">
    <h3 class="site-aside__title">System</h3>
    <ul>
      <li>
        <a href="http://www.vbk.se"
           target="_blank"
           data-icon="home">VBK.se
        </a>
      </li>

      <li>
        <a href="/#/"
           ng-class="{'current' : currentPath == ''}"
           data-icon="dashboard">Dashboard
        </a>
      </li>

      <li>
        <a href="/#/system"
           ng-class="{'current' : currentPath.indexOf('system') === 0}"
           data-icon="cog">System
        </a>
      </li>
    </ul>

    <h3 class="site-aside__title">Webbplats</h3>
    <ul>
      <li>
        <span class="quicklink-trigger"></span>

        <a href="/#/nyheter/ny"
           class="quicklink"
           title="Skapa ny"
           tabindex="-1"
           data-icon="plus">
        </a>

        <a href="/#/nyheter"
           class="has-quicklink"
           ng-class="{'current' : currentPath.indexOf('nyheter') === 0}"
           data-icon="news">Nyheter
        </a>
      </li>

      <li>
        <span class="quicklink-trigger"></span>

        <a href="/#/sidor/ny"
           class="quicklink"
           title="Skapa ny"
           tabindex="-1"
           data-icon="plus">
        </a>

        <a href="/#/sidor"
           class="has-quicklink"
           ng-class="{'current' : currentPath.indexOf('sidor') === 0}"
           data-icon="pages">Alla sidor
        </a>
      </li>

      <li>
        <span class="quicklink-trigger"></span>

        <a href="/#/projekt/ny"
           class="quicklink"
           title="Skapa ny"
           tabindex="-1"
           data-icon="plus">
        </a>

        <a href="/#/projekt"
           class="has-quicklink"
          ng-class="{'current' : currentPath.indexOf('projekt') === 0}"
          data-icon="projects">Referensprojekt
        </a>
      </li>

      <li>
        <span class="quicklink-trigger"></span>

        <a href="/#/personal/ny"
           class="quicklink"
           title="Skapa ny"
           tabindex="-1"
           data-icon="plus">
        </a>

        <a href="/#/personal"
           class="has-quicklink"
           ng-class="{'current' : currentPath.indexOf('personal') === 0}"
           data-icon="users">Personal
        </a>
      </li>

      <li>
        <a href="/#/bildspel"
           ng-class="{'current' : currentPath.indexOf('bildspel') === 0}"
           data-icon="slideshow">Bildspel
        </a>
      </li>
    </ul>
  </nav>

  <section class="trash-aside__wrap">
    <a href="/#/trash"
       class="trash-aside__link"
       data-icon="trash">Papperskorg
    </a>
  </section>
</aside>
