<section class="sitemap-wrap">
  <h2>Sitemap:</h2>

  <ul class="sitemap">
    <li ng-repeat="page in data"
        ng-class="{'children-visible': isVisible}"
        ng-init="isVisible = false">

      <a href="#sida/{{page.pageId}}">{{page.pagename}}</a>

      <button ng-if="page.sub"
              ng-click="$parent.isVisible = !isVisible"
              class="toggle-children"
              ng-class="{'has-children' : page.sub.length > 0}"
              type="button">
      </button>

      <ul ng-if="page.sub"
          ng-show="isVisible"
          ng-animate="'animate'">

        <li ng-repeat="page in page.sub">

          <a href="#sida/{{page.pageId}}">{{page.pagename}}</a>
        </li>
      </ul>

    </li>
  </ul>
</section>