<aside class="top-bar__wrap">
  <div class="flex-parent">
    <button type="button"
            class="top-bar__button mobile-menu--toggle"
            ng-click="menuOpen = !menuOpen">

            <span class="hamburger-icon"></span>
    </button>

    <div class="top-bar__portrait">
      <img src="/uploads/employees/<?php echo $_SESSION['employeeimage']; ?>">
    </div>

    <h2 class="top-bar__name">
      <?php echo $_SESSION['firstname'] . " " . $_SESSION['lastname']; ?>
    </h2>

    <a href="/logout"
       class="top-bar__button top-bar__button--logout">
    </a>
  </div>
</aside>
