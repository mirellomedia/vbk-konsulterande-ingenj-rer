<?php require_once("../../../view/template/inputs/inputs.php"); ?>

<h1>Bildspel</h1>

<form name="createPage">

  <section class="main-field skin">
    <div class="block-padding create-page--components create-page--inputs active">
      <ul ng-repeat="slide in slides"
          flow-init="{singleFile:true, target: '/model/module/flow.php?type=slideshow'}"
          flow-file-added="!!{png:1,gif:1,jpg:1,jpeg:1}[$file.getExtension()]"
          flow-name="slide.flow"
          flow-complete="success(slide)"
          ng-class="{'status--delete' : slide.status === 'delete'}"
          class="input__group">

        <li>
          <h2>Projekt {{$index + 1}}</h2>

          <button type="button"
                  class="slide-remove"
                  ng-click="removeSlide(slide); createPage.$setDirty()">
          </button>
        </li>

        <li>
          <div class="input-header">
            <h3>Projektnamn</h3>
          </div>

          <div class="input-field">
            <input type="text"
                   name="projectName"
                   ng-change="slideChange(slide, $index);"
                   ng-model="slide.projectName">
          </div>
        </li>

        <li>
          <div class="input-header">
            <h3>Projektbeskrivning</h3>
          </div>

          <div class="input-field">
            <textarea name="projectSummary"
                      ng-model="slide.projectSummary"
                      ng-change="slideChange(slide, $index);"
                      class="msd-elastic character-counter--display">
            </textarea>

            <span class="character-counter"
                  ng-class="{'full' : slide.projectSummary.length > 250}"
                  data-character-soft-limit="250">{{slide.projectSummary.length}}</span>
          </div>
        </li>

        <li>
          <div class="input-header">
            <h3>Bild</h3>
          </div>

          <div class="image-upload">
            <div class="drop" flow-drop ng-class="dropClass" flow-btn class="thumbnail">
              <div class="cf">
                <img ng-show="!$flow.files[0] && (Images.flow.files || slide.slideImage.fileName.length)"
                     flow-img="slide.flow.files[0]"
                     class="slide-image line-component line-component--padded"
                     ng-src="/uploads/images/{{slide.slideImage.fileName}}">

                <img ng-show="$flow.files[0]"
                     flow-img="$flow.files[0]"
                     class="slide-image line-component line-component--padded"
                     ng-src="/uploads/images/{{slide.slideImage.fileName}}">
              </div>

              <button type="button"
                      class="button"
                      ng-click="createPage.$setDirty()"
                      ng-hide="slide.slideImage.fileName.length">Lägg till bild
              </button>

              <button type="button"
                      class="button"
                      ng-click="createPage.$setDirty()"
                      ng-show="slide.slideImage.fileName.length">Byt bild
              </button>
            </div>

            <span ng-model="$flow.files"></span>
          </div>
        </li>
      </ul>

      <div class="input__group">
        <button type="button"
                ng-click="addSlide()"
                class="button add-new">Lägg till projekt</button>
      </div>
    </div>

    <footer class="create-page--components create-page--footer">
      <div class="flex-parent">
        <button type="submit"
               class="button action-add"
               ng-click="submit(options);"
               ng-disabled="createPage.$invalid || createPage.$pristine">Spara ändringar
        </button>
      </div>
    </footer>
  </section>
</form>
