<?php
  $path = dirname(dirname(dirname(__FILE__)));
  $path = $path."/model/module/statistics.php";
  require_once($path);

  $latestProject    = $statistics->latestProject();
  $latestArticle    = $statistics->latestArticle();
  $latestEmployees  = $statistics->latestEmployees();
?>

<h1>Välkommen!</h1>

<section class="main-field dashboard">
  <div class="cf">
    <div class="dashboard__column">

      <section class="dashboard__box">
        <h2 class="dashboard__label skin">Systemet innehåller:</h2>

        <div class="counter-box__wrap flex-parent skin">
          <a class="counter-box" href="/#/nyheter">
            <span class="counter"><?php echo $statistics->articleCount; ?></span>
            <span>nyhetsinlägg</span>
          </a>

          <a class="counter-box" href="/#/projekt">
            <span class="counter"><?php echo $statistics->referenceCount; ?></span>
            <span>referensprojekt</span>
          </a>

          <a class="counter-box" href="/#/personal">
            <span class="counter"><?php echo $statistics->employeeCount; ?></span>
            <span>anställda</span>
          </a>

          <a class="counter-box" href="/#/sidor">
            <span class="counter"><?php echo $statistics->pageCount; ?></span>
            <span>publika sidor</span>
          </a>
        </div>
      </section>

      <section class="dashboard__box edit-object__area">
        <h2 class="dashboard__label skin">Senaste nyheten:</h2>

        <div class="skin">
          <a class="edit-object"
             href="#nyheter/<?php echo $latestArticle->articleId . '_' . $latestArticle->articleLink ?>">
          </a>

          <article class="block-padding">
            <h3><?php echo $latestArticle->articleHeader ?></h3>
            <?php echo $latestArticle->articleContent ?>
          </article>
        </div>
      </section>
    </div>

    <div class="dashboard__column">
      <section class="dashboard__box edit-object__area">
        <h2 class="dashboard__label skin">Senaste referensprojektet:</h2>

        <div class="skin">
          <a class="edit-object"
             href="/#/projekt/<?php echo $latestProject->projectId . '_' . $latestProject->projectLink ?>">
          </a>

          <header class="dashboard__header">
            <div class="dashboard__header--image-holder flex-parent">
              <img class="dashboard__header--image" src="/uploads/images/<?php echo $latestProject->projectImage; ?>">
            </div>

            <div class="dashboard__header--title-holder">
              <h3 class="dashboard__header--title">
                <?php echo $latestProject->projectName; ?>
              </h3>
            </div>
          </header>

          <div class="dashboard__reference block-padding">
            <?php echo $latestProject->projectLongDesc; ?>
          </div>
        </div>
      </section>

      <section class="dashboard__box">
        <h2 class="dashboard__label skin">Senast tillagda anställda:</h2>

        <div class="skin block-padding dashboard__employee">
          <h3>Välkomna till VBK:</h3>

          <?php
            foreach ($latestEmployees as $employee) {
              echo
              '<div class="dashboard__employee--holder skin--dark flex-parent edit-object__area">' .
                '<img class="dashboard__employee--image"
                      src="/uploads/employees/' . $employee->image .'">' .

                '<div class="dashboard__employee--information">'.
                  '<ul>' .
                    '<li><b>' . $employee->name . '</b></li>' .
                    '<li><b>Tel: </b>' . $employee->phone . '</li>' .
                    '<li><b>Email: </b>' . $employee->email . '</li>' .
                  '</ul>' .
                '</div>' .
                '<a href="/#/personal/' . $employee->id . '" class="edit-object"></a>' .
              '</div>';
            }
          ?>
        </div>
      </section>
    </div>
  </div>
</section>
