<h1>Alla nyheter</h1>

<section class="main-field skin">
  <div class="filter-list">
    <input type="text"
           placeholder="Skriv för att filtrera…"
           ng-model="filterNews">
  </div>

  <table class="page-list" ng-hide="data.length === 0">
    <thead>
      <tr>
        <th ng-click="predicate = 'namn'; reverse = !reverse"
            ng-class="{'sort-asc' : predicate === 'namn' && !reverse,
                       'sort-desc': predicate === 'namn' && reverse}">Namn:
        </th>

        <th ng-click="predicate = 'skapades'; reverse = !reverse"
            ng-class="{'sort-asc' : predicate === 'skapades' && !reverse,
                       'sort-desc': predicate === 'skapades' && reverse}">Publiceringsdatum:
        </th>

        <th ng-click="predicate = 'andrades'; reverse = !reverse"
            ng-class="{'sort-asc' : predicate === 'andrades' && !reverse,
                       'sort-desc': predicate === 'andrades' && reverse}">Ändrades:
        </th>
      </tr>
    </thead>

    <tr ng-repeat="news in data | orderBy:predicate:reverse | filter:filterNews">
      <td data-header="Namn" class="nyhet">
        <a href="#/nyheter/{{::news.id}}_{{::news.lank}}" ng-bind="::news.namn"></a>
      </td>

      <td data-header="Publiceringsdatum" ng-bind="::news.skapades"></td>

      <td data-header="Ändrades" ng-bind="::news.andrades"></td>
    </tr>
  </table>

  <div class="page-list--empty block-padding" ng-show="data.length === 0">
    <h2>Här var det tomt! :-(</h2>
    <p>Vill du skriva en nyhet?</p>
    <a href="#/nyheter/ny" class="button">Skriv nyhet</a>
  </div>

  <div class="button__bar">
    <a href="#/nyheter/ny" class="button slim add-new">Skapa ny</a>
  </div>
</section>
