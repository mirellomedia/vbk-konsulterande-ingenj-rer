<?php
  require_once("../../../view/template/inputs/inputs.php");
?>

<h1>Redigera nyhet</h1>

<form name="createPage">
  <section class="main-field skin">
    <div class="block-padding create-page--components create-page--inputs active"
         ng-repeat="language in languages"
         ng-show="selectedLanguage == '{{language.languageCode}}' || languages.length == 1"
         ng-animate="'animate'">

      <ul>
        <li><?php input('header',  'Rubrik');    ?></li>
        <li><?php input('body',    'Artikel');   ?></li>
        <li><?php input('summary', 'Summering'); ?></li>
        <li><?php input('url',     'URL');       ?></li>
      </ul>
    </div>

    <footer class="create-page--components create-page--footer">
      <ul>
        <li class="input-group">
          <div class="input-group__row">
            <div class="limit-width">
              <input id="publish-now"
                     type="checkbox"
                     ng-model="options.publish">

              <label for="publish-now">Publicerad</label>

              <div class="explaination-wrap">
                <div class="explaination">
                  <p>Bockar du ur så sparas nyheten som opublicerad.</p>
                </div>
              </div>
            </div>
          </div>

          <div class="row"
               ng-if="!options.publish && languages[0].publishDate > languages[0].currentDate">

            <b>Publiceras automatiskt den {{::languages[0].publishDate}}</b>
          </div>

          <div class="input-group__row" ng-if="!options.publish">
            <div class="limit-width">
              <p>Välj publiceringsdatum:</p>
            </div>

            <input type="date" required ng-model="languages[0].newPublishDate">
          </div>
        </li>
      </ul>

      <div class="flex-parent">
        <?php buttonSubmit('nyhet', 'redigera'); ?>

        <?php confirmRemoval('nyheten'); ?>
      </div>
    </footer>

    <div class="button__bar">
      <a href="#nyheter" class="button slim">Visa alla</a>
    </div>
  </section>
</form>
