<?php date_default_timezone_set('Europe/Stockholm'); ?>
<?php
  require_once("../../../view/template/inputs/inputs.php");
?>

<h1>Skriv nyhet</h1>


<form name="createPage"
      class="create-page"
      flow-init
      flow-file-added="!!{png:1,gif:1,jpg:1,jpeg:1}[$file.getExtension()]">

  <section class="main-field skin">
    <div class="block-padding create-page--components create-page--inputs active"
         ng-repeat="language in languages"
         ng-show="selectedLanguage == '{{language.languageCode}}' || languages.length == 1"
         ng-animate="'animate'">

      <ul>
        <li><?php input('header',  'Rubrik'); ?></li>
        <li><?php input('body',    'Artikel'); ?></li>
        <!-- <li><?php input('flow',    'Bild till nyhet'); ?></li> -->
        <li><?php input('summary', 'Summering'); ?></li>
        <li><?php input('url',     'URL', 'nyheter'); ?></li>
      </ul>

    </div>

    <footer class="create-page--components create-page--footer">
      <ul>
        <li class="input-group">
          <?php publishDate(); ?>
        </li>
      </ul>

      <div class="create-page__submit-bar flex-parent">
        <?php buttonSubmit('nyhet', 'skapa'); ?>
      </div>
    </footer>

    <div class="button__bar">
      <a href="#nyheter" class="button slim">Visa alla</a>
    </div>
  </section>
</form>
