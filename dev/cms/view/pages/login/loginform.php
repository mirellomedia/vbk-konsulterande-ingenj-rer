<?php
  
  date_default_timezone_set('Europe/Stockholm'); 
  $time = date("H");

  if      ($time <  "11")                 { $greeting = "God morgon";      }
  else if ($time >= "11" && $time < "18") { $greeting = "God eftermiddag"; }
  else if ($time >= "18" && $time < "21") { $greeting = "God kväll";       }
  else if ($time >= "21")                 { $greeting = "God natt";        }
?>

<div class="login-frame login-page-1">
  <header class="login-header">
    <h1><?php echo $greeting ?><span ng-bind="user"></span>!</h1>
    <p>Vänligen logga in genom att fylla i namn alt. email och lösenord.</p>
  </header>

  <section class="login-area" ng-class="status">
    <form id="login-form" name="loginform">
      <div class="login-inputs">
        <ul>
          <li>
            <label for="username">Användarnamn:</label>
            <input ng-model="name" id="username" type="text" name="name" autofocus required>
          </li>

          <li>
            <label for="password">Lösenord:</label>
            <input ng-model="password" id="password" type="password" name="password" ng-minlength="6" required>
          </li>
        </ul>
      </div>

      <button ng-disabled="loginform.$error.required" class="login-button" type="submit" ng-click="loginEvent()"></button>
    </form>
  </section>
</div>