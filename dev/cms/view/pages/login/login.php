<?php
  $ip = gethostbyname(trim('hostname'));
  $root = dirname(dirname(dirname(dirname(__FILE__))));

  if($ip == '192.168.1.118' || $ip == '192.168.1.3') {
    $path = $root . '/model/session';

    session_save_path($path);
    session_start();
    
    if(isset($_SESSION['userid'])) {
      header('Location: /');
      exit();
    }
  }

  // CO JAG KAN HA FÖRSTÖRT NÅGOT NÄR JAG BYTTE NAMN PÅ DIRNAME-PATHS TILL $ROOT? JAG VET INTE FÖR DET VERKAR FUNGERA
  $path = $root . '/model/session';
 
  session_save_path($path);
  session_start();

  if(@$_SESSION['userid']){
    header('Location: /');
    exit();
  }
  

  // Funktioner och inställningar:
  require_once("$root/view/template/setup.php");

  // get head
  htmlHead();
?>


<body class="login-body" ng-app="app">
  <main class="login-wrap">
    <section class="login-page {{ pageClass }}" ng-view ng-animate="'animate'">
    </section>
  </main>

  <aside class="login-warning" ng-show="error">
    <p>Något gick fel! Se över dina uppgifter och försök igen.</p>
  </aside>

  <footer class="login-copyright">
    <div class="login-copyright-text">
      <?php date_default_timezone_set('Europe/Stockholm'); ?>
      <p>Copyright © <?php echo date("Y"); ?> | <a href="http://mirello.se" target="_blank">Mirello Media AB</a></p>
      <p>System framtaget åt <a href="http://www.vbk.se" target="_blank">VBK Konsulterande ingenjörer AB</a></p>
    </div>
  </footer>

<?php
  // Angular + jQuery init:
  globalScripts('login');
?>


<script>
  var loginredirect = false;
  
  var app = angular.module('app',
                          ['ngRoute',
                           'ngAnimate',
                           'ngResource',
                           'ngSanitize'
                          ]
  );

  app.config(['$routeProvider', function($routeProvider) {
      $routeProvider.when('/', {templateUrl: '/view/pages/login/loginform.php', controller: 'loginController'});
      $routeProvider.otherwise({redirectTo: '/'});

    }]);

  'use strict';

  app.factory('loginService',function($http, $location) {
    return {
      login: function(data, $scope) {
        var $promise=$http.post('/view/pages/login/loginmodel.php', data);

        $promise.then(function(msg) {
          console.log(msg);
          if (msg.data=='nils') {
            $scope.status='Rätt information';


            loginredirect = true;
            window.location.href = '/';
          }

          else {
            $scope.status = 'error';
            $scope.$parent.error = true;

            setTimeout(function() {
              $scope.$apply(function () {
              $scope.status='';
              });
            }, 900);
          }
        });
      }
    }
  });

  app.controller('loginController', function($scope, $location, loginService){
    $scope.pageClass = 'login';
    // // TEST ONLY
    //   $scope.name = 'Jonas Almqvist';
    //   $scope.password = 'hejsan';
    // // TEST ONLY

    $scope.status='';
    $scope.loginEvent=function() {
      var data = {
        username: $scope.name,
        password: $scope.password
      }

      loginService.login(data,$scope); //call login service
    };
  });

  app.directive('loginDirective',function() {
    return {
      templateUrl:'/'
    }
  });
</script>
</body>
</html>