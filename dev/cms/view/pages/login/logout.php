<?php
   $path = dirname(dirname(dirname(dirname(__FILE__))));

  session_save_path( $path . '/model/session');
  session_start();

  unset($_SESSION['userid']);
  session_destroy();
  header('Location: /login');
  exit();
?>