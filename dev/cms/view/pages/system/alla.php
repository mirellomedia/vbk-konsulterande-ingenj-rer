<?php
  require_once("../../../view/template/inputs/inputs.php");
?>

<h1>Systeminställningar</h1>

<form name="systemInputs" class="create-page">
  <section class="main-field skin">
    <div class="block-padding create-page--components create-page--inputs active">
      <ul class="input__group">
        <li>
          <h2>Övrigt</h2>
        </li>

        <li>
          <div class="input-header">
            <h3>Arbetsgrupper</h3>
          </div>

          <div class="input-field">
            <ul class="cf tag__list">
              <li class="tag__item"
                  ng-class="{'active' : toggleEdit}"
                  ng-repeat="group in groups"
                  on-blur="toggleEdit = false">

                <span ng-show="!toggleEdit">{{group.employeegroupName}}</span>

                <input type="text"
                       class="tag__input"
                       ng-model="group.employeegroupName"
                       on-enter="editGroup(group)"
                       ng-show="toggleEdit">

                <button type="button"
                        ng-show="toggleEdit"
                        class="tag__edit"
                        ng-click="editGroup(group)">
                </button>

                <div class="cover-all"
                     ng-show="!toggleEdit"
                     ng-click="toggleEdit = !toggleEdit">
                </div>

                <div class="confirmation-dialog__wrap tag-remove__wrap"
                     tabindex="-1"
                     ng-show="!toggleEdit"
                     on-blur="tagToggleConfirmation = false">

                  <button type="button"
                          class="tag-remove__button"
                          ng-click="tagToggleConfirmation = !tagToggleConfirmation"
                          ng-class="{'confirmation-required': tagToggleConfirmation}">
                  </button>

                  <div class="confirmation-dialog confirmation-dialog--right confirmation-dialog--bottom"
                       ng-show="tagToggleConfirmation">

                    <button class="button action-remove"
                            ng-click="removeGroup(group)"
                            type="button">Radera
                    </button>

                    <div class="confirmation-dialog__text">
                      <p>Vill du verkligen radera <strong>{{group.employeegroupName}}</strong>? Detta kommer att radera gruppen permanent.</p>
                    </div>
                  </div>
                </div>
              </li>

              <li class="tag__item tag__item--add"
                  ng-class="{'active' : toggleAddNew}"
                  on-blur="toggleAddNew = false">

                  <span ng-show="!toggleAddNew">Lägg till…</span>

                  <input type="text"
                         class="tag__input"
                         id="createNewGroup"
                         ng-model="createNewGroup"
                         on-enter="createGroup(createNewGroup, groups.length)"
                         ng-show="toggleAddNew">

                  <button type="button"
                          ng-show="toggleAddNew"
                          class="tag__create"
                          ng-click="createGroup(createNewGroup, groups.length)">
                  </button>

                  <div class="cover-all"
                       ng-show="!toggleAddNew"
                       ng-click="toggleAddNewFunction()">
                  </div>
              </li>
            </ul>
         </div>
        </li>
      </ul>
    </div>

    <footer class="create-page--components create-page--footer">
      <div class="flex-parent">
        <button type="submit"
                class="button action-add"
                disabled
                ng-click="save(data)"
                ng-disabled="systemInputs.$invalid">Spara ändringar
        </button>
      </div>
    </footer>
  </section>
</form>
