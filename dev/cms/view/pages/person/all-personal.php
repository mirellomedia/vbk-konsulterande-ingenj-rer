<h1>Personal</h1>

<section class="main-field skin block-padding">
  <div ng-class="dataView"
       ng-init="dataView = 'display-grid'"
       class="toggle-view--wrapper">

    <div class="flex-parent">
      <button class="toggle-grid" type="button" ng-click="dataView='display-grid'"></button>
      <button class="toggle-list" type="button" ng-click="dataView='display-list'"></button>
    </div>

    <div class="flex-parent">
      <input class="search-field"
             type="text"
             placeholder="Skriv för att filtrera"
             ng-model="filterEmployees.$">
      <label class="search-label"></label>
    </div>

  </div>

  <ul class="personnel-list delay-children" ng-class="dataView">

    <li class="person vcard"
        ng-class="{active : isSelected(section)}"
        ng-repeat="data in employees | filter:filterEmployees track by data.employeeId">

        <div class="photo-holder">
          <img ng-src="/uploads/employees/{{::data.employeeImage}}" alt="Porträtt av {{::data.employeeName}}" class="photo">
          <a href="/#/personal/{{::data.employeeId}}" class="employee-settings">Redigera</a>
        </div>

      <div class="person-info-box">
        <h3  class="list-row-item fn"   >{{::data.employeeName}}</h3>
        <div class="list-row-item role" >{{::data.employeeTitle}}</div>
        <div class="list-row-item tel"  >{{::data.employeePhone}}</div>
        <div class="list-row-item email">{{::data.employeeEmail}}</div>
        <div class="list-row-item title">{{::data.employeeTitle}}</div>
        <div class="list-row-item city" style="display: none;">{{::data.employeeCity}}</div>
      </div>
    </li>
  </ul>

  <div class="button__bar">
    <a href="#personal/ny" class="button slim add-new">Skapa ny</a>
  </div>
</section>
