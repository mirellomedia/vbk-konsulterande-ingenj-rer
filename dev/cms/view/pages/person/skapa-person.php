<?php
  require_once("../../../view/template/inputs/inputs.php");
?>

<h1>Lägg till anställd</h1>

<form name="createEmployeeForm"
      flow-init="{target: '/model/module/flow.php?type=employee', singleFile: true}"
      flow-name="Images.flow"
      ng-class="{'disabled' : disableSubmit}"
      flow-file-added="!!{png:1,gif:1,jpg:1,jpeg:1}[$file.getExtension()]"
      flow-complete="success(Images);">

  <section class="main-field skin">
    <div class="block-padding  flex-parent">
      <!-- flow-files-submitted="$flow.upload()" -->
      <div class="image-upload">
        <div class="drop" flow-drop ng-class="dropClass" flow-btn class="thumbnail">
          <img flow-img="$flow.files[0]" src="/uploads/employees/placeholder.jpg" ng-src="/uploads/employees/{{employee.employeeImage.fileName}}">
          <h5>Lägg till bild</h5>
        </div>
      </div>

      <div class="employee-info__inputs">
        <ul>
          <li class="row">
            <h3>Namn</h3>
            <input id="employee-name"
                   type="text"
                   required
                   ng-model="employee.employeeName">
          </li>

          <li class="flex-parent">
            <div class="input-column">
              <h3>Telefon</h3>
              <input id="employee-phone"
                     type="text"
                     ng-model="employee.employeePhone">
            </div>

            <div class="input-column">
              <h3>Email</h3>
              <input id="employee-email"
                     type="text"
                     ng-required="employee.login"
                     ng-model="employee.employeeEmail">
            </div>
          </li>

          <li class="row">
            <h3>Titel</h3>
            <input id="employee-title"
                   type="text"
                   ng-model="employee.employeeTitle">
          </li>

          <li class="row">
            <h3 class="padded">Arbetsgrupper</h3>

            <ul class="cf">
              <li ng-repeat="group in groups" class="line-component line-component--padded">
                <button ng-click="toggleGroup(group)"
                        class="checkbox"
                        type="button"
                        ng-class="{'checked' : employee.employeeGroups.indexOf(group.employeegroupId) > -1}"
                        for="employee-group-{{$index}}">
                  {{group.employeegroupName}}
                </button>
              </li>
            </ul>
          </li>

          <li class="row">
            <h3>Stad</h3>
            <label for="employee-city" class="label-for__select">
              <select id="employee-city"
                      ng-model="employee.employeeCity"
                      required
                      ng-options="option.value as option.name for option in cities">
              </select>
            </label>
          </li>
        </ul>
      </div>
    </div>

    <footer class="create-page--footer  create-page--components">
      <div class="checkbox-row">
        <div class="limit-width">
          <input id="publish-now"
                 type="checkbox"
                 ng-model="employee.login">

          <label for="publish-now">Inloggningsrättigheter</label>

          <div class="explaination-wrap">
            <div class="explaination">
              <p>Om den anställde får tillgång till en inloggning kommer ett mail innehållande information och ett genererat lösenord att skickas till den ifyllda mailadressen.</p>
            </div>
          </div>
        </div>
      </div>

      <div class="create-page__submit-bar flex-parent">
        <button type="submit"
                class="button action-add"
                ng-disabled="createEmployeeForm.$invalid || createEmployeeForm.$pristine || disableSubmit"
                ng-click="submit(Images)">Skapa person
        </button>
      </div>
    </footer>

    <div class="button__bar">
      <a href="#personal" class="button slim">Visa alla</a>
    </div>
  </section>
</form>
