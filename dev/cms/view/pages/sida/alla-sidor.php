<h1>Alla undersidor</h1>

<section class="main-field skin">
  <div class="sitemap-wrap block-padding" ng-hide="data.length == 0">
    <h2>Sitemap:</h2>

    <ul class="sitemap">
      <li ng-repeat="page in data"
          ng-class="{'children-visible': isVisible}"
          ng-init="isVisible = false">

        <a href="#/sidor/{{::page.pageId}}" ng-bind="::page.pagename"></a>

        <button ng-if="page.sub.length"
                ng-click="$parent.isVisible = !isVisible"
                class="toggle-children"
                ng-class="{'has-children' : page.sub.length > 0}"
                type="button">
        </button>

        <ul ng-if="page.sub.length"
            ng-show="isVisible"
            ng-animate="'animate'">

          <li ng-repeat="page in page.sub">

            <a href="#/sidor/{{::page.pageId}}" ng-bind="::page.pagename"></a>
          </li>
        </ul>

      </li>
    </ul>
  </div>

  <div class="page-list--empty block-padding" ng-show="data.length == 0">
    <h2>Här var det tomt! :-(</h2>
    <p>Vill du skapa en undersida?</p>
    <a href="#/sidor/ny" class="button">Skapa undersida</a>
  </div>

  <div class="button__bar">
    <a href="#/sidor/ny" class="button slim add-new">Skapa ny</a>
  </div>
</section>
