<?php
  require_once("../../../view/template/inputs/inputs.php");
?>

<h1>Redigera undersida</h1>

<form name="createPage"
      id="createPage"
      flow-init="{target: '/model/module/flow.php?type=project', singleFile: true}"
      flow-name="projectImages.flow"
      flow-file-added="!!{png:1,gif:1,jpg:1,jpeg:1}[$file.getExtension()]"
      flow-complete="success(projectImages)">

  <?php // Translation tabs
    require_once("../../template/tabs/translations.php"); ?>

  <section class="main-field skin">
    <div class="active"
         ng-repeat="language in languages"
         ng-show="selectedLanguage == '{{language.languageCode}}' || languages.length === 1"
         ng-animate="'animate'">

      <div class="block-padding create-page--components create-page--inputs">
        <ul>
          <li><?php input('header', 'Sidtitel'); ?></li>

          <li ng-show="isFrontPage">
            <div class="input-header">
              <h3>Hero</h3>
            </div>

            <div class="input-field">
              <input id="create-page-heading"
                     type="text"
                     name="heading"
                     ng-model="language.heading">
            </div>
          </li>

          <li><?php input('body', 'Innehåll'); ?></li>

          <li ng-show="language.image && language.template !== 11 && language.template !== 12 && !isFrontPage && !staticPage">
            <div class="input-header">
              <h3>Bild</h3>
            </div>

            <div class="input-field">
              <div class="row"
                   ng-show="$flow.files[0].name.length || language.image.fileName.length && languages[0].image.status !== 'delete'">

                <div class="flex-parent">
                  <img class="flow-image"
                       flow-img="$flow.files[0]"
                       ng-src="/uploads/images/{{language.image.fileName}}">

                  <ul class="flow__info" ng-show="$flow.files[0]">
                    <li><strong>Namn: </strong>{{$flow.files[0].name}}</li>
                    <li><strong>Storlek: </strong>{{$flow.files[0].size | bytes}}</li>
                    <li class="flex-parent"><strong>Beskrivning:</strong> <input class="flow-image__description" type="text" name="image-alt"></li>
                  </ul>

                </div>

                <button class="remove-image row--small"
                        ng-show="language.image.fileName.length"
                        ng-click="removeImage(languages[0].image)"
                        type="button">Radera bild
                </button>

                <button class="remove-image"
                        ng-show="$flow.files[0].name.length"
                        ng-click="$flow.cancel()"
                        type="button">Radera bild
                </button>
              </div>

              <input id="image-upload"
                     type="file"
                     ng-model="language.image"
                     flow-btn>

              <label for="image-upload"
                     class="button">
                {{$flow.files[0].name.length || language.image.fileName.length ? 'Byt bild' : 'Lägg till bild'}}
              </label>
            </div>
          </li>

          <li ng-show="!language.image && language.template !== 11 && language.template !== 12 && !isFrontPage && !staticPage">
            <div class="input-header">
              <div class="limit-width">
                <h3>Bild</h3>

                <div class="explaination-wrap">
                  <div class="explaination">
                    <p>För bästa resultat, använd en bild med ca. 1000px i bredd av formatet JPG.</p>
                  </div>
                </div>
              </div>
            </div>

            <div class="input-field">
              <div class="flow__wrap flex-parent" ng-repeat="(key, file) in projectImages.flow.files">
                <img class="flow-image" flow-img="file">

                <ul class="flow__info">
                  <li><strong>Namn: </strong>{{file.name}}</li>
                  <li><strong>Storlek: </strong>{{file.size | bytes}}</li>
                  <li class="flex-parent"><strong>Beskrivning:</strong> <input class="flow-image__description" type="text" name="image-alt"></li>
                </ul>
              </div>

              <div class="input-header">
                <input id="image-upload"
                       class="button action-add icon-only"
                       type="file"
                       flow-btn>

                <label for="image-upload"
                       class="button">Ladda upp
                </label>
              </div>
            </div>
          </li>
          <li><?php input('url', 'URL'); ?></li>
        </ul>
      </div>
    </div>

    <footer class="create-page--components create-page--footer">
      <ul ng-hide="isFrontPage || staticPage || languages[0].parent == 0">
        <li class="input-group">
          <div class="input-group__row flex-parent">
            <div class="input-header">
              <h3>Visa under:</h3>
            </div>

            <div class="input-field">
              <label class="label-for__select"
                     for="optionsParent">

                <select ng-model="languages[0].parent"
                        id="optionsParent"
                        required
                        ng-options="page.pageId as page.pagename for page in allPages">
                </select>
              </label>

            </div>
          </div>

          <div class="input-group__row flex-parent">
            <div class="input-header">
              <h3>Template:</h3>
            </div>

            <div class="input-field">
              <label for="optionsTemplate" class="label-for__select">
                <select id="optionsTemplate"
                        ng-model="languages[0].template"
                        ng-options="template.value as template.name for template in templates"
                        required>
                </select>
              </label>
            </div>
          </div>
        </li>

        <li class="input-group">
          <div class="input-group__row">
            <div class="limit-width">
              <input id="pagePublished"
                     type="checkbox"
                     ng-model="options.publish">

              <label for="pagePublished">Publicerad</label>

              <div class="explaination-wrap">
                <div class="explaination">
                  <p>Bockar du ur så sparas nyheten som opublicerad. Inloggade administratörer kan se opublicerade nyheter på webbplatsen för förhandsgranksning innan publicering.</p>
                </div>
              </div>
            </div>
          </div>

          <div class="input-group__row">
            <div class="limit-width">
              <input id="pageHidden"
                     type="checkbox"
                     ng-model="options.hidden">

              <label for="pageHidden">Dölj från menyer</label>
            </div>
          </div>
        </li>

      </ul>

      <div class="flex-parent">
        <button type="submit"
                class="button action-add"
                ng-click="submit(projectImages, options, $flow);"
                ng-disabled="createPage.$invalid || disableSubmit">Spara ändringar
        </button>

        <?php confirmRemoval('sidan'); ?>
      </div>
    </footer>

    <div class="button__bar">
      <a href="#sidor" class="button slim">Visa alla</a>
    </div>
  </section>
</form>
