<?php
  require_once("../../../view/template/inputs/inputs.php");
?>

<h1>Skapa undersida</h1>

<form name="createPage"
      class="create-page"
      flow-init="{singleFile:true, target: '/model/module/flow.php?type=pages'}"
      flow-name="Images.flow"
      flow-file-added="!!{png:1,gif:1,jpg:1,jpeg:1}[$file.getExtension()]"
      flow-complete="success(Images)">

  <?php // Translation tabs
    require_once("../../template/tabs/translations.php");
  ?>

  <section class="main-field skin">

    <div class="block-padding create-page--components create-page--inputs active"
         ng-repeat="language in languages"
         ng-init="language.header = ''; language.htmlcontent = ''"
         ng-show="selectedLanguage == '{{language.languageCode}}' || languages.length == 1"
         ng-animate="'animate'">

      <ul>
        <li><?php input('header', 'Sidtitel'); ?></li>
        <li><?php input('body',   'Innehåll'); ?></li>
        <li><?php input('flow',   'Bild till sida'); ?></li>
        <li><?php input('url',    'URL'); ?></li>
      </ul>

    </div>

    <footer class="create-page--components create-page--footer">
      <ul>
        <li class="input-group">
          <div class="input-group__row flex-parent">
            <div class="input-header">
              <h3>Visa under:</h3>
            </div>

            <div class="input-field">
              <label class="label-for__select"
                     for="optionsParent">

                <select ng-model="options.parent"
                        id="optionsParent"
                        required>

                  <option ng-value="mainnav">Mainnav</option>
                  <option ng-value="subnav">Subnav</option>
                  <option ng-repeat="page in pages" ng-value="{{page.pageId}}">{{page.pagename}}
                  </option>
                </select>
              </label>

            </div>
          </div>

          <div class="input-group__row flex-parent">
            <div class="input-header">
              <h3>Template:</h3>
            </div>

            <div class="input-field">
              <label for="optionsTemplate" class="label-for__select">
                <select id="optionsTemplate"
                        ng-model="options.template"
                        ng-options="template.value as template.name for template in templates"
                        required>
                </select>
              </label>
            </div>
          </div>
        </li>

        <li class="input-group">
          <div class="input-group__row">
            <?php publishDate(); ?>
          </div>

          <div class="input-group__row">
            <div class="limit-width">
              <input id="pageHidden"
                     type="checkbox"
                     ng-model="options.hidden">

              <label for="pageHidden">Dölj från menyer</label>
            </div>
          </div>
        </li>
      </ul>

      <div class="create-page__submit-bar flex-parent">
        <button type="submit"
                class="button action-add"
                ng-click="submit(Images, $flow);"
                ng-disabled="createPage.$invalid || disableSubmit">Skapa sida
        </button>
      </div>

    </footer>

    <div class="button__bar">
      <a href="#sidor" class="button slim">Visa alla</a>
    </div>
  </section>
</form>
