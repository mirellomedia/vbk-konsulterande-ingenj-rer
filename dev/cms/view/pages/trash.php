<?php
  require_once("../../view/template/inputs/inputs.php");
?>

<h1>Papperskorg</h1>

<section class="main-field skin">
  <table class="page-list" ng-hide="!data.length">
    <thead>
      <tr>
        <th ng-click="predicate = 'namn'; reverse =! reverse"
            ng-class="{'sort-asc' : predicate == 'namn' && !reverse,
                       'sort-desc': predicate == 'namn' && reverse}">
          Namn:
        </th>

        <th ng-click="predicate = 'type'; reverse =! reverse"
            ng-class="{'sort-asc' : predicate == 'type' && !reverse,
                       'sort-desc': predicate == 'type' && reverse}">
          Typ:
        </th>

        <th ng-click="predicate = 'deletedAt'; reverse =! reverse"
            ng-class="{'sort-asc' : predicate == 'deletedAt' && !reverse,
                       'sort-desc': predicate == 'deletedAt' && reverse}">
          Raderades:
        </th>

        <th>
          Lägg tillbaka
        </th>

        <th>
          Radera
        </th>
      </tr>
    </thead>

    <tr ng-repeat="item in data | orderBy:predicate:reverse">
      <td data-header="Namn"
          class="{{item.type | lowercase}}">

        <span ng-bind="::item.name"></span>
      </td>

      <td data-header="Typ" ng-bind="::item.type"></td>

      <td data-header="Slängdes" ng-bind="::item.deletedAt"></td>

      <td data-header="Lägg tillbaka">
        <button type="button"
                class="text-button"
                title="Lägg tillbaka"
                ng-click="restore(item)">

          Lägg tillbaka
        </button>
      </td>

      <td data-header="Radera" class="centered-content">
        <div class="confirmation-dialog__wrap"
             tabindex="-1"
             on-blur="toggleConfirmation = false">

          <button type="button"
                  class="button action-remove icon-only"
                  ng-click="toggleConfirmation = !toggleConfirmation"
                  ng-class="{'confirmation-required': toggleConfirmation}">
          </button>

          <div class="confirmation-dialog confirmation-dialog--right confirmation-dialog--bottom"
               ng-show="toggleConfirmation">

            <button type="button"
                    ng-init="toggleConfirmation = false"
                    ng-click="delete(item)"
                    class="button action-remove">Radera
            </button>

            <div class="confirmation-dialog__text">
              <p>Vill du verkligen <strong>radera</strong> <em ng-bind="::item.name"></em>? Detta raderar föremålet permanent.</p>
            </div>
          </div>
        </div>
      </td>
    </tr>
  </table>

  <div class="page-list--empty block-padding" ng-show="!data.length">
    <h2>Papperskorgen är tom.</h2>
    <p>Är du inte lite sugen på att slänga någon av nyheterna?</p>
    <a href="#nyheter" class="button">Hitta nyhet att kasta</a>
  </div>
</section>
