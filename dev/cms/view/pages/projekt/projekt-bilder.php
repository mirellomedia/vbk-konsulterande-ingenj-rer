<section class="product-preview">
  <table>
    <tr ng-repeat="file in $flow.files">
      <td>{{$index+1}}</td>
      <td><img style="max-width:80px;" flow-img="file" /></td>
    </tr>
  </table>
</section>

<div class="input-header" style="float:left;">
  <input id="image-upload"
        class="button action-add icon-only"
         type="file"
         ng-model="language.image"
         
         flow-btn>

  <label for="image-upload" class="button">Ladda upp</label>
</div>

<button type="submit"
  class="button action-add icon-only"
  style="float:right;margin-top:0;"
  ng-click="$flow.upload(product); submit($flow.files, 3);">{{action}}
</button>