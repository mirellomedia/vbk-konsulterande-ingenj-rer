<?php
  require_once("../../../view/template/inputs/inputs.php");
?>

<h1>Redigera referensprojekt</h1>

<form name="createPage"
      class="create-page"
      ng-class="{'disabled' : disableSubmit}"
      flow-init="{target: '/model/module/flow.php?type=project'}"
      flow-name="projectImages.flow"
      flow-file-added="!!{png:1,gif:1,jpg:1,jpeg:1}[$file.getExtension()]"
      flow-complete="success(projectImages)">

  <section class="main-field skin">
    <div class="block-padding create-page--components create-page--inputs active"
         ng-animate="'animate'">

      <ul>
        <li>
          <div class="input-header">
            <h3>Sidtitel</h3>
          </div>

          <div class="input-field">
            <input id="create-page-header"
                   type="text"
                   name="header"
                   class="character-counter--display"
                   ng-model="languages[0].projectName"
                   required>

            <span class="character-counter"
                  ng-class="{'full' : languages[0].projectName.length > 55}"
                  data-character-soft-limit="55">{{languages[0].projectName.length}}</span>
           </div>
        </li>

        <li>
          <div class="input-header">
            <h3>Projektbeskrivning</h3>
          </div>

          <div class="input-field">
            <textarea ng-model="languages[0].projectLongDesc"
                      required
                      redactor>
            </textarea>
          </div>
        </li>

        <li>
          <div class="input-header">
            <h3>Summering</h3>
          </div>

          <div class="input-field">
            <textarea ng-model="languages[0].projectShortDesc"
                      required
                      class="msd-elastic character-counter--display">
            </textarea>

            <span class="character-counter"
                  ng-class="{'full' : languages[0].projectShortDesc.length > 250}"
                  data-character-soft-limit="250">{{languages[0].projectShortDesc.length}}</span>
          </div>
        </li>

        <li class="row">
          <div class="ugly-fix-to-work-around-global-flex-on-these-list-items-above">
            <div class="flex-parent row row--small">
              <div class="input-header">
                <h3>Projekttyp</h3>
              </div>

              <div class="input-field">
                <label for="project-type" class="label-for__select">
                  <select id="project-type"
                          ng-model="languages[0].projectType"
                          required="required"
                          ng-options="option.id as option.name for option in projectTypes">
                  </select>
                </label>
              </div>
            </div>

            <div class="flex-parent row row--small" ng-if="languages[0].projectType === 1">
              <div class="input-header">
                <h3>Projektkategori</h3>
              </div>

              <div class="input-field">
                <label for="project-category" class="label-for__select">
                  <select id="project-category"
                          ng-model="languages[0].projectCategory"
                          required
                          ng-options="option.id as option.name for option in projectCategories">
                  </select>
                </label>
              </div>
            </div>
          </div>
        </li>

        <li>
          <div class="input-header">
            <h3>Projektbilder</h3>
          </div>

          <div class="input-field">
            <ul class="product-preview cf"
                as-sortable="dragControlListeners"
                ng-model="languages[0].projectImages"
                ng-show="$flow.files.length || languages[0].projectImages">

              <!-- PRODUCT IMAGES -->
              <li class="flow-item"
                  as-sortable-item
                  ng-mousedown="createPage.$setDirty()"
                  ng-repeat="(key, image) in languages[0].projectImages"
                  ng-hide="image.status == 'delete'">

                <div class="product-preview__holder" as-sortable-item-handle>
                  <img ng-src="/uploads/images/{{image.fileName}}">
                </div>

                <button class="remove-image"
                        ng-click="removeImage(key, languages[0].projectImages)"
                        type="button">Radera bild
                </button>
              </li>


              <!-- FLOW IMAGES -->
              <li class="flow-item"
                ng-repeat="(key, file) in projectImages.flow.files">
                <div class="product-preview__holder">
                  <img flow-img="file">
                </div>

                <button class="remove-image"
                        ng-click="file.cancel()"
                        type="button">Radera bild
                </button>
              </li>
            </ul>

            <div class="input-header">
              <input id="image-upload"
                    class="button action-add icon-only"
                     type="file"
                     flow-btn>

              <label for="image-upload"
                     ng-click="createPage.$setDirty()"
                     class="button">Ladda upp
              </label>
            </div>
          </div>
        </li>
      </ul>
    </div>

    <footer class="create-page--components create-page--footer">
      <div class="flex-parent">
        <button type="submit"
                class="button action-add"
                ng-click="submit(projectImages, options, $flow);"
                ng-disabled="createPage.$invalid || createPage.$pristine || disableSubmit">Spara ändringar
        </button>

        <?php confirmRemoval('referensprojektet'); ?>
      </div>
    </footer>

    <div class="button__bar">
      <a href="#projekt" class="button slim">Visa alla</a>
    </div>
  </section>
</form>
