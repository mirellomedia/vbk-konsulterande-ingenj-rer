<h1>Alla referensprojekt</h1>

<section class="main-field skin">
  <div class="filter-list">
    <input type="text"
           placeholder="Skriv för att filtrera…"
           ng-model="filterProjects">
  </div>

  <table class="page-list" ng-hide="data.length === 0">
    <thead>
      <tr>
        <th ng-click="predicate = 'projectName'; reverse = !reverse"
            ng-class="{'sort-asc' : predicate === 'projectName' && !reverse,
                       'sort-desc': predicate === 'projectName' && reverse}">
          Namn:
        </th>

        <th ng-click="predicate = 'projectAdded'; reverse = !reverse"
            ng-class="{'sort-asc' : predicate === 'projectAdded' && !reverse,
                       'sort-desc': predicate === 'projectAdded' && reverse}">
          Skapades:
        </th>
      </tr>
    </thead>

    <tr ng-repeat="project in data | orderBy:predicate:reverse | filter:filterProjects">
      <td data-header="Namn">
        <a href="#projekt/{{::project.projectId}}_{{::project.projectLink}}">
          {{::project.projectName}}</a>
      </td>

      <td data-header="Ändrades">
        {{::project.projectAdded}}
      </td>
    </tr>
  </table>

  <div class="page-list--empty block-padding" ng-show="data.length === 0">
    <h2>Här var det tomt! :-(</h2>
    <p>Vill du skapa ett referensprojekt?</p>
    <a href="#/projekt/ny" class="button">Skapa referensprojekt</a>
  </div>

  <div class="button__bar">
    <a href="#/projekt/ny" class="button slim add-new">Skapa ny</a>
  </div>
</section>
