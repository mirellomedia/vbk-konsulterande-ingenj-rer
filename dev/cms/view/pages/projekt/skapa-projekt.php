<?php
  require_once("../../../view/template/inputs/inputs.php");
?>

<h1>Lägg till referensprojekt</h1>

<form name="createPage"
      class="create-page"
      ng-class="{'disabled' : disableSubmit}"
      flow-init="{target: '/model/module/flow.php?type=project'}"
      flow-name="projectImages.flow"
      flow-file-added="!!{png:1,gif:1,jpg:1,jpeg:1}[$file.getExtension()]"
      flow-complete="success(projectImages)">

  <section class="main-field skin">
    <div class="block-padding create-page--components create-page--inputs active"
         ng-repeat="language in languages"
         ng-show="selectedLanguage == '{{language.languageCode}}' || languages.length == 1"
         ng-animate="'animate'">

      <ul>
        <li>
          <div class="input-header">
            <h3>Sidtitel</h3>
          </div>

          <div class="input-field">
            <input id="create-page-header"
                   type="text"
                   name="header"
                   class="character-counter--display"
                   ng-model="language.projectName"
                   required>

            <span class="character-counter"
                  ng-class="{'full' : language.projectName.length > 55}"
                  data-character-soft-limit="55">{{language.projectName.length}}</span>
           </div>
        </li>

        <li>
          <div class="input-header">
            <h3>Projektbeskrivning</h3>
          </div>

          <div class="input-field">
            <textarea ng-model="language.projectLongDesc"
                      required
                      redactor>
            </textarea>
          </div>
        </li>

        <li>
          <div class="input-header">
            <h3>Summering</h3>
          </div>

          <div class="input-field">
            <textarea ng-model="language.projectShortDesc"
                      required
                      class="msd-elastic character-counter--display">
            </textarea>

            <span class="character-counter"
                  ng-class="{'full' : language.projectShortDesc.length > 250}"
                  data-character-soft-limit="250">{{language.projectShortDesc.length}}</span>
          </div>
        </li>

        <li class="row">
          <div class="ugly-fix-to-work-around-global-flex-on-these-list-items-above">
            <div class="flex-parent row row--small">
              <div class="input-header">
                <h3>Projekttyp</h3>
              </div>

              <div class="input-field">
                <label for="project-type" class="label-for__select">
                  <select id="project-type"
                          ng-model="options.projectType"
                          required
                          ng-options="option.id as option.name for option in projectTypes">
                  </select>
                </label>
              </div>
            </div>

            <div class="flex-parent row row--small" ng-if="options.projectType === 1">
              <div class="input-header">
                <h3>Projektkategori</h3>
              </div>

              <div class="input-field">
                <label for="project-category" class="label-for__select">
                  <select id="project-category"
                          ng-model="options.projectCategory"
                          ng-init="projectCategories[0]"
                          required
                          ng-options="option.id as option.name for option in projectCategories">
                  </select>
                </label>
              </div>
            </div>
          </div>
        </li>

        <li>
          <div class="input-header">
            <h3>Projektbilder</h3>
          </div>

          <div class="input-field">
            <ul class="product-preview"
                ng-show="$flow.files.length">

              <li class="flow-item"
                  ng-repeat="file in $flow.files">

                <div class="product-preview__holder">
                  <img flow-img="file">
                </div>

                <button class="remove-image"
                        ng-click="file.cancel()"
                        type="button">Radera bild
                </button>
              </li>
            </ul>

            <div class="input-header">
              <input id="image-upload"
                     class="button action-add icon-only"
                     type="file"
                     ng-model="language.image"
                     flow-btn>

              <label for="image-upload" class="button">Ladda upp</label>
            </div>
          </div>
        </li>

        <li>
          <div class="input-header">
            <h3 class="url--title">URL</h3>

            <slug from="language.projectName" to="language.urlslug"></slug>
          </div>

          <div id="url--wrap" class="input-field">
            <label class="url--bar">
              <div class="url--parent">/projekt/</div>
              <input id="url--slug"
                     class="url--slug"
                     disabled
                     ng-model="language.urlslug">
            </label>
          </div>
        </li>
      </ul>
    </div>

    <footer class="create-page--components create-page--footer">
      <div class="create-page__submit-bar flex-parent">
        <button type="submit"
                class="button action-add"
                disabled
                ng-click="submit(projectimages, options, $flow)"
                ng-disabled="createPage.$invalid || disableSubmit">Skapa projekt
        </button>
      </div>
    </footer>
  </section>
</form>
