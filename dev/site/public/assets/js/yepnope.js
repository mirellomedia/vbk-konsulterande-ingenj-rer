// =========
//  YEPNOPE
// =========

yepnope([

  { // css3 selectors:
    test: Modernizr.lastchild,
    nope: '//cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js'
  },

  { // media queries:
    test: Modernizr.mq("only all"),
    nope: '//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js'
  },

  { // ta bort klickfördröjningen på 300 ms från touchenheter:
    test: Modernizr.touch,
    yep : '//cdnjs.cloudflare.com/ajax/libs/fastclick/0.6.11/fastclick.min.js',
    complete: function() {
      if (Modernizr.touch) {
        FastClick.attach(document.body);
      }
    }
  }
]);