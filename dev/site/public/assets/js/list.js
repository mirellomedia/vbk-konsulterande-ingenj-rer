// ==============
//  LIST.JS INIT
// ==============

(function() {
  var listOptions = {
    valueNames: [ 'fn', 'tel', 'email', 'city', 'role' ]
  };

  var personnelList = new List('personnel-list', listOptions);

  var location  = document.location.pathname;

  function filterPersonnel(newFilter) {
    personnelList.filter(function(item) {
      if (!newFilter.dataValue) {
        return true;
      }

      else if (item.values()[newFilter.dataProperty].indexOf(newFilter.dataValue) !== -1) {
        return true;
      }
    });

    // yeah... some browsers jump to the href element if the target has doesn't exist.
    // it's stupid, but we have to fix it
    setTimeout(function() {
      window.scrollTo(0, 0);
    }, 10);
  }


  // only run the script on the contact pages
  if (location.indexOf("/kontakt") === 0 || location.indexOf("/en/contact") === 0) {
    // run filter on click
    $('.employees a[href*=#]').click(function(e) {
      var newFilter = {
        dataProperty: this.getAttribute('data-property'),
        dataValue:    this.getAttribute('data-value')
      };

      filterPersonnel(newFilter);
    });

    // run filter on page load
    $(document).ready(function() {
      var hash = window.location.hash.substring(1); // remove the #
      var prop = $('.employees a[data-value="' + hash + '"]')[0].getAttribute('data-property');

      var newFilter = {
        dataProperty: prop,
        dataValue: hash
      };

      filterPersonnel(newFilter);
    });
  }
}) ();
