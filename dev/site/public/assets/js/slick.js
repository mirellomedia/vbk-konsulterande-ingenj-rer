// ============
//  SLICK INIT
// ============

$('.slideshow__wrap').slick({
  arrows:        false,
  infinite:      true,
  autoplay:      true,
  autoplaySpeed: 7000,
  speed:         800
});