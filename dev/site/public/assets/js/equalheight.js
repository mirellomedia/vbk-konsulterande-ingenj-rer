// ==============
//  EQUAL HEIGHT
// ==============

// normalisera höjd på kolumner:
function equalheight(container) {
  var currentDiv = null,
      currentTallest = 0,
      currentRowStart = 0,
      rowDivs = [],
      $el,
      topPosition = 0;

  $(container).each(function() {
    $el = $(this);
    $($el).height('auto');
    topPosition = $el.position().top;

    if (currentRowStart !== topPosition) {
      for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
        rowDivs[currentDiv].height(currentTallest);
      }

      rowDivs.length = 0; // empty the array
      currentRowStart = topPosition;
      currentTallest = $el.height();
      rowDivs.push($el);
    } else {
      rowDivs.push($el);
      currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
    }
    
    for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
      rowDivs[currentDiv].height(currentTallest);
    }
  });
}

(function(){
  if (!Modernizr.flexbox) {
    var equalheightTargets = '.equal-height';

    $(window).resize(function() {
      windowResize(function() {
        equalheight(equalheightTargets);
      }, resizeDelay);
    });


    $('.personnel-list').imagesLoaded( function() {
      equalheight(equalheightTargets);
    });
  }
})();
