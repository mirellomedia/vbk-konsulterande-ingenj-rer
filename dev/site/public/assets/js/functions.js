// ==============
//  JS FUNCTIONS
// ==============

// Delay på resize-events
var resizeDelay  = 500,
    windowResize = (function() {
  
  var timers = {};

  return function(callback, ms, uniqueId) {
    if (!uniqueId) {
      uniqueId = "Don't call this twice without a uniqueId";
    }

    if (timers[uniqueId]) {
      clearTimeout (timers[uniqueId]);
    }

    timers[uniqueId] = setTimeout(callback, ms);
  };
})(); // Usage: windowResize(function() {}, resizeDelay);



// Existerar elementet?
$.fn.exists = function(callback) {
  var args = [].slice.call(arguments, 1);

  if (this.length) {
    callback.call(this, args);
  }

  return this;
};


// Animera width/height från auto
$.fn.animateAuto = function(prop, speed, callback) {
  var elem, height, width;
  return this.each(function(i, el) {
    el     = $(el), elem = el.clone().css({"height":"auto","width":"auto"}).appendTo("body");
    height = elem.css("height");
    width  = elem.css("width");
    elem.remove();
    
    if      (prop === "height") { el.animate({"height":height}, speed, callback); }
    else if (prop === "width")  { el.animate({"width":width}, speed, callback); }
    else if (prop === "both")   { el.animate({"width":width,"height":height}, speed, callback); }
  });
};