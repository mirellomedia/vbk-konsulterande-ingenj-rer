// =========
//  TOGGLES
// =========

// toggle navigation:
var menuParent    = $("body"),
    mobilePush    = $(".mobile-push"),
    menuAside     = $(".aside-mobile"),

    menuButton    = $(".toggle-menu"),
    menuOpen      = "menu-open",

    submenuButton = $(".toggle-submenu"),
    submenuOpen   = "submenu-open";


menuButton.click(function() {
  menuParent.toggleClass(menuOpen);
});


mobilePush.click(function() {
  if (menuParent.hasClass(menuOpen)) {
    menuParent.removeClass(menuOpen);
  }
}).find(menuButton).click(function() {
  return false;
});


submenuButton.click(function() {
  if (!menuParent.hasClass(submenuOpen)) {
    menuAside.css("height", "auto");
  } else {
    menuAside.css("height", "0");
  }

  menuParent.toggleClass(submenuOpen);
});

// var location = document.location.pathname;

// if (location.indexOf("/kontakt") === 0 || location.indexOf("/en/contact") === 0) {
  $('.employees a[href*=#]').click(function() {
    if (window.innerWidth < 790) {
      menuAside.css("height", "0");
      menuParent.removeClass(submenuOpen);
    }
  });
// }

