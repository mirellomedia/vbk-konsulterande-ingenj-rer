// =============
//  GOOGLE MAPS 
// =============

function googleMaps(city) {
  
  // Sätt koordinater
  var locations = [
    ['VBK Huvudkontor, Göteborg', 57.681142, 12.000975],
    ['VBK Lokalkontor, Skövde',   58.390351, 13.848066]
  ];

  // Inställningar för karta
  if (city === "Göteborg") {
    var mapOptions = {
      center: {lat: locations[0][1], lng: locations[0][2]},
      zoom: 15
    };
  }

  else if (city === "Skövde") {
    var mapOptions = {
      center: {lat: locations[1][1], lng: locations[1][2]},
      zoom: 15
    };
  }

  else { // defaulta till gbg
    var mapOptions = {
      center: {lat: locations[0][1], lng: locations[0][2]},
      zoom: 15
    };
  }

  // Skapa karta
  var offices = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);


  // Skapa inforuta
  var infowindow = new google.maps.InfoWindow;

  // Placera markörer
  var marker, i;

  for (i = 0; i < locations.length; i++) {  
    marker = new google.maps.Marker({
      position: new google.maps.LatLng(locations[i][1], locations[i][2]),
      animation: google.maps.Animation.DROP,
      map: offices
    });

    // Öppna inforuta vid klick på markör
    google.maps.event.addListener(marker, 'click', (function(marker, i) {
      return function() {
        infowindow.setContent('<h5>'+locations[i][0]+'</h5>');
        infowindow.open(offices, marker);
      }
    })(marker, i));
  }
}