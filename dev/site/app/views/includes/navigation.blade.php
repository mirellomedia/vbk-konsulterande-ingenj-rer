<nav class="site-nav">
  <div class="nav-bar-blue cf" data-navigation-type="<?php echo Lang::get('headers.company-information'); ?>">
    <ul class="right-aligned cf">
      @foreach($mainnav as $nav)
      <li>
        <a href="<?php echo $currentLanguage; ?>/{{{ $nav->pageLink === 'start' ? '' : $nav->pageLink }}}" data-nav-target="">{{{ $nav->pageName }}}</a>
      </li>
      @endforeach

      <li>
        <a href="/<?php echo $newLanguage; ?>" data-nav-target="language"><?php echo $languageName; ?></a>
      </li>

      <li class="search-wrap">
        <form method="get" action="<?php echo $currentLanguage; ?>/search">
          <input class="global-search"
                 type="search"
                 name="search"
                 placeholder="<?php echo Lang::get('headers.search-website'); ?>"
                 id="global-search">

          <label for="global-search"></label>
        </form>
      </li>
    </ul>
  </div>

  <div class="main-nav cf" data-navigation-type="<?php echo Lang::get('headers.main-menu'); ?>">
    <ul class="right-aligned cf">
      @foreach($subnav as $nav)
      <li>
        <a href="<?php echo $currentLanguage; ?>/{{{ $nav->pageLink }}}" data-nav-target="">{{{ $nav->pageName }}}</a>
      </li>
      @endforeach
    </ul>
  </div>
</nav>
