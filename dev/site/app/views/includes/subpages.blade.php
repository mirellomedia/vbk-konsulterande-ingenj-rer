<?php
  $currentLanguage = App::getLocale();

  if($currentLanguage == 'sv'){
    $currentLanguage = '';
    $newLanguage = 'en';
    $languageName = 'English';
  }
  else{
    $currentLanguage = '/'.$currentLanguage;
    $newLanguage = '';
    $languageName = 'Svenska';
  }
?>

<aside class="main-aside">
  <h2>{{ $pageParent }}</h2>
  <button type="button" class="toggle-submenu"><?php echo Lang::get('headers.read-more'); ?></button>

  <div class="aside-mobile">
    <nav class="aside-nav">
      <ul>
      @foreach($subpages as $page)
        <li><a href="<?php echo $currentLanguage; ?>/{{$page->pageLink}}">{{$page->pageName}}</a></li>
      @endforeach
      </ul>
    </nav>
  </div>
</aside>
