<?php
  $currentLanguage = App::getLocale();

  if ($currentLanguage == 'sv') {
    $currentLanguage = '';
    $newLanguage = 'en';
    $languageName = 'English';
    $contactUrl = '/' . Request::segment(1) . '#';
  }

  else {
    $currentLanguage = '/'.$currentLanguage;
    $newLanguage = '';
    $languageName = 'Svenska';
    $contactUrl = $currentLanguage . '/' . Request::segment(2) . '#';
  }
?>

<aside class="main-aside">
  <h2><?php echo Lang::get('headers.contact'); ?></h2>

  <button type="button" class="toggle-submenu"><?php echo Lang::get('headers.read-more'); ?></button>


  <div class="aside-mobile">
    <nav class="aside-nav">
      <ul>
        <li class="submenu-parent">
          <span><?php echo Lang::get('headers.employees'); ?></span>
          <ul class="employees">
            <li>
              <a href="#">
                <?php echo Lang::get('headers.all-employees'); ?>
              </a>
            </li>

            <li>
              <a data-property="city"
                 data-value="Göteborg"
                 id="<?php echo Lang::get('headers.gothenburg'); ?>"
                 href="<?php echo $contactUrl . Lang::get('headers.gothenburg'); ?>">

               <?php echo Lang::get('headers.gothenburg'); ?>
              </a>
            </li>

            <li>
              <a data-property="city"
                 data-value="Skövde"
                 id="<?php echo Lang::get('headers.skovde'); ?>"
                 href="<?php echo $contactUrl . Lang::get('headers.skovde'); ?>">
                 <?php echo Lang::get('headers.skovde'); ?>
              </a>
            </li>

            @foreach($employeegroups as $group)
            <li>
              <a data-property="role"
                 data-value="<?php echo $group->employeegroupName ?>"
                 href="<?php echo $contactUrl . $group->employeegroupName ?>"
                 id="<?php
                    if (Lang::has('headers.' . $group->employeegroupName)) {
                      echo Lang::get('headers.' . $group->employeegroupName);
                    }

                    else {
                      echo $group->employeegroupName;
                    }
                ?>">

                <?php
                  if (Lang::has('headers.' . $group->employeegroupName)) {
                    echo Lang::get('headers.' . $group->employeegroupName);
                  }

                  else {
                    echo $group->employeegroupName;
                  }
                ?>
              </a>
            </li>
            @endforeach
          </ul>
        </li>

        <li class="submenu-parent">
          <span><?php echo Lang::get('headers.other'); ?></span>
          <ul>
            @foreach($subpages as $page)
            <li>
              <a href="<?php echo $currentLanguage; ?>/{{$page->pageLink}}">
                {{$page->pageName}}
              </a>
            </li>
            @endforeach
          </ul>
        </li>
      </ul>
    </nav>
  </div>
</aside>
