<?php
  $currentLanguage = App::getLocale();

  if ($currentLanguage == 'sv') {
    $currentLanguage = '';
    $newLanguage = 'en';
    $languageName = 'English';
    $baseUrl = '/';
  }
  else {
    $currentLanguage = '/'.$currentLanguage;
    $newLanguage = '';
    $languageName = 'Svenska';
    $baseUrl = '/en/';
  }
?>

<header class="site-head cf">
  @include('includes.navigation')

  <div class="logo-bar">
    <a href="{{$baseUrl}}" class="logo-wrap">
      <img class="logo"
           src="/assets/img/logos/logo-vbk.png"
           alt="VBK Konsulterande ingenjörers logotyp">
    </a>

    <button type="button"
            class="toggle-menu"
            data-button-open="<?php echo Lang::get('headers.menu'); ?>"
            data-button-close="<?php echo Lang::get('headers.close'); ?>">
    </button>
  </div>
</header>
