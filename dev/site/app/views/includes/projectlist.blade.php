<section class="quicklinks-all-projects">
    <h5 class="quicklinks-title">Alla projekt</h5>
      <ul>
        @foreach($allprojects as $projectlist)
            <li><a href="/projekt/{{$projectlist->projectType}}/{{$projectlist->projectcategoryLink}}/{{$projectlist->projectLink}}">{{ $projectlist->projectName }}</a></li>
        @endforeach
      </ul>
</section>