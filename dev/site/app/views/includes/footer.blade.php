<footer class="section site-footer cf">
  <div class="footer-section-right">
    <div class="footer-section">
      <h5><?php echo Lang::get('companyinformation.social'); ?></h5>
      <ul>
        <li><a href="https://www.linkedin.com/company/vbk-konsulterande-ingenjörer-ab" target="_blank">LinkedIn</a></li>
      </ul>
    </div>

    <div class="footer-section contact-information">
      <h5><?php echo Lang::get('companyinformation.officegothenburg'); ?></h5>
      <span class="location">Falkenbergsgatan 3<br>412 85 Göteborg</span>
      <span><a href="tel:+46317033500">031-703 35 00</a></span>
      <span><a href="mailto:mail@vbk.se">mail@vbk.se</a></span>
    </div>

    <div class="footer-section contact-information">
      <h5><?php echo Lang::get('companyinformation.officeskovde'); ?></h5>
      <span class="location">Långgatan 13<br>541 30 Skövde</span>
      <span><a href="tel:+460500444560">0500-44 45 60</a></span>
      <span><a href="mailto:mail@vbk.se">mail@vbk.se</a></span>
    </div>
  </div>

  <div class="more-information">
    <p>Copyright © 2014</p>
    <p><?php echo Lang::get('companyinformation.companyname'); ?></p><br>
    <p><a href="http://www.std.se" target="_blank"><?php echo Lang::get('companyinformation.memberof'); ?></a></p>
    <p><?php echo Lang::get('companyinformation.iso'); ?></p>
  </div>
</footer>