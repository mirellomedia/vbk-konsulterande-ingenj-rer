<section class="section skin">
  <div class="wrap hide-en">
    <div class="news-wrap-header cf">
      <h2><?php echo Lang::get('headers.news'); ?></h2>
      <a href="nyheter" class="news-archive-link">Äldre inlägg</a>
    </div>

    <div class="news-feed-excerpt cf">
      @foreach($articles as $article)
        <article class="news-item">
          <header class="news-header">
            <h3 class="news-title">
              <a href="/nyheter/{{$article->articleId}}/{{$article->articleLink}}">{{ $article->articleHeader }}</a>
            </h3>
            <span class="news-date">{{ $article->articleDate }}</span>
          </header>

          <div class="news-body">
            <p>{{ $article->articleSum }}</p>
          </div>

          <footer class="news-footer">
            <a href="/nyheter/{{$article->articleId}}/{{$article->articleLink}}" class="read-full-article"><?php echo Lang::get('articles.read-more'); ?></a>
          </footer>
        </article>
      @endforeach
    </div>
  </div>
</section>
