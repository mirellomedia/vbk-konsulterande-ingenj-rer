<!--
//
// 1.blade.php används för UNDERSIDOR
//
-->

@include('includes.document-start')

<div class="site-container">
  @include('includes.header')

  <div class="mobile-push">
    <main class="main cityscape">
      <div class="sidebar-holder">
        @include('includes.staff')

        <section class="main-body">
          <header class="page-head">
            <h1>{{{ $pagecontent->pageName }}}</h1>
            @if($pagecontent->pageHeaderimage)
            <div class="header-image">
              <div class="constrain-image">
                <img src="/uploads/images/{{{ $pagecontent->pageHeaderimage }}}">
              </div>
            </div>
            @endif
          </header>

          {{ $pagecontent->pageContent }}
        </section>
      </div>
    </main>

    @include('includes.footer')
  </div>
</div>

@include('includes.document-end')
