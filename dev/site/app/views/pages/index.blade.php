<!--
//
// 4.blade.php används för INDEX
//
-->

@include('includes.document-start')

<div class="site-container sticky-header">
  @include('includes.header')

  <div class="mobile-push">
    <section class="hero">
      <div class="slideshow__wrap">
        @foreach($slideshow as $slide)
          <div class="slideshow__slide">
            <div class="slideshow__cover">
             <img src="/uploads/images/{{$slide->slideLink}}">
            </div>

            <div class="slideshow__description">
              <h4>{{$slide->slideName}}</h4>
              <p class="hide-en">{{$slide->slideDescription}}</p>
            </div>
          </div>
        @endforeach
      </div>
      <div class="hero__text">
        <h1 class="hero__text--jumbo">
          {{$pagecontent->pageHeader}}
        </h1>
      </div>
    </section>

    <main class="main cityscape main-front">
      <section class="section intro-text">
        {{$pagecontent->pageContent}}
      </section>
    </main>

    @include('includes.articles')
    @include('includes.footer')
  </div>
</div>

@include('includes.document-end')
