<!--
//
// 2.blade.php används för NYHETER
//
-->

@include('includes.document-start')

<div class="site-container">
  @include('includes.header')

  <div class="mobile-push">
    <main class="main cityscape">
      <div class="sidebar-holder  cf">
        @include('includes.archive')

        <section class="main-body">
          <header class="page-head">
            <h1><?php echo Lang::get('headers.news'); ?></h1>
          </header>

          <div class="news-feed">
            @foreach($articles as $article)
            <article class="news-item">
              <header class="news-header">
                <h3 class="news-title">
                  <a href="/nyheter/{{$article->articleId}}/{{$article->articleLink}}">{{ $article->articleHeader }}</a>
                </h3>
                <span class="news-date">{{ $article->articleDate }}</span>
              </header>

              <div class="news-article-body">
                {{ $article->articleSum }}
              </div>

              <footer class="news-footer">
                <a href="/nyheter/{{$article->articleId}}/{{$article->articleLink}}" class="read-full-article"><?php echo Lang::get('headers.read-more'); ?></a>
              </footer>
            </article>
            @endforeach
          </div>
        </section>
      </div>
    </main>

    @include('includes.footer')
  </div>
</div>

@include('includes.document-end')
