<!--
//
// 8.blade.php används för PROJEKTSIDAN
//
-->

@include('includes.document-start')

<div class="site-container">
  @include('includes.header')

  <div class="mobile-push">
    <main class="main cityscape">
      <div class="sidebar-holder">
        <aside class="main-aside">
          <h2>Projekt</h2>
          <button type="button" class="toggle-submenu"><?php echo Lang::get('headers.read-more'); ?></button>

          <div class="aside-mobile">
            <nav class="aside-nav">
              <ul>
                @foreach($projectcategory as $category)
                  <li>
                    <a href="/projekt/{{ $pagecontent->pageLink }}/{{ $category->projectcategoryLink }}">
                      {{ $category->projectcategoryName }}
                    </a>
                  </li>
                @endforeach
              </ul>
            </nav>

            @include('includes.projectlist')
          </div>
        </aside>

        <section class="main-body no-limit">
          <nav class="breadcrumbs">
            <ul>
              <li><a href="/projekt/{{ $pagecontent->pageLink }}">{{ $pagecontent->pageName }}</a></li>
              <li><a href="/projekt/{{ $pagecontent->pageLink }}/{{ $projektkategoriLink }}">{{ $projektkategoriName }}</a></li>
              <li><a href="/projekt/{{ $pagecontent->pageLink }}/{{ $projektkategoriLink }}/{{$thisproject->projectLink}}">{{ $thisproject->projectName }}</a></li>
            </ul>
          </nav>

          <div class="cf">
            <div class="project-page">
              <div class="limit-width">
                <header class="page-head">
                  <h1>{{ $thisproject->projectName }}</h1>
                  <p class="skin skin--padding">{{ $thisproject->projectShortDesc }}</p>
                </header>

                <div class="project-page-body">
                  {{ $thisproject->projectLongDesc }}
                </div>
              </div>
            </div>

            <div class="project-images-wrap">
              @foreach($thisproject->images as $image)
                <a href="/uploads/images/{{$image->projectimageLink}}" class="fancybox" rel="project">
                  <img src="/uploads/images/{{$image->projectimageLink}}">
                </a>
              @endforeach
            </div>
          </div>
        </section>
      </div>
    </main>

    @include('includes.footer')
  </div>
</div>

@include('includes.document-end')
