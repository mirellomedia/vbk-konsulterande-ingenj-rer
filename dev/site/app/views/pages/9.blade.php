<!-- 
//
// 3.blade.php används för KONTAKT
//
-->

@include('includes.document-start')

<div class="site-container">
  @include('includes.header')

  <div class="mobile-push">
    <main class="main cityscape"> 
      <div class="sidebar-holder">
        @include('includes.staff')
      
        <section class="main-body no-limit">
          hej
        </section>
      </div>
    </main>

    @include('includes.footer')
  </div>
</div>

@include('includes.document-end')