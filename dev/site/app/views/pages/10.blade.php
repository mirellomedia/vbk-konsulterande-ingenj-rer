<!--
//
// 2.blade.php används för INDEX
//
-->

@include('includes.document-start')

<div class="site-container">
  @include('includes.header')

  <div class="mobile-push">
    <main class="main cityscape">
      <div class="sidebar-holder">
        <aside class="main-aside">
          <h2>Projekt</h2>
          <button type="button" class="toggle-submenu"><?php echo Lang::get('headers.read-more'); ?></button>

          <div class="aside-mobile">
            <nav class="aside-nav">
              <ul>
              @foreach($projects as $project)
                <li>
                  <a href="/projekt/{{ $pagecontent->pageLink }}/all/{{ $project->projectLink }}">
                    {{ $project->projectName }}
                  </a>
                </li>
              @endforeach
              </ul>
            </nav>

            @include('includes.projectlist')
        </div>
      </aside>

      <section class="main-body">
        <nav class="breadcrumbs">
          <ul>
            <li><a href="/projekt/{{ $pagecontent->pageLink }}">{{ $pagecontent->pageName }}</a></li>
          </ul>
        </nav>

        <ul class="reference-list cf">
          <li class="reference-item reference-item--empty">
            <div class="reference-overlay-text">
              <div class="vertical-center">
                <p>Våra projekt varierar och vi är stolta över att kunna visa upp över 50 år lång erfarenhet i ett axplock av våra genomförda uppdrag</p>
              </div>
            </div>
          </li>

          @foreach($projects as $project)
            <li class="reference-item">
              <a href="/projekt/{{ $pagecontent->pageLink }}/all/{{ $project->projectLink }}">
                <img src="/uploads/images/{{$project->projectimageLink}}">

                <div class="reference-overlay">
                  <div class="reference-overlay-text">
                    <div class="vertical-center">
                      <h3>{{ $project->projectName }}</h3>
                    </div>
                  </div>

                  <div class="read-more">
                    <div class="vertical-center">
                      Gå till {{ $project->projectName }}
                    </div>
                  </div>
                </div>
              </a>
            </li>
          @endforeach
        </ul>
      </section>
      </div>
    </main>

    @include('includes.footer')
  </div>
</div>

@include('includes.document-end')
