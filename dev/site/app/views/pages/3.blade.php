<!--
//
// 3.blade.php används för KONTAKT
//
-->

@include('includes.document-start')

<div class="site-container">
  @include('includes.header')

  <div class="mobile-push">
    <main class="main cityscape">
      <div class="sidebar-holder">
        @include('includes.staff')

        <section class="main-body no-limit">
          <header class="page-head">
            <h1><?php echo Lang::get('headers.our-employees'); ?></h1>
          </header>

          <div id="personnel-list">
            <input class="search"
                   type="search"
                   placeholder="<?php echo Lang::get('headers.type-to-filter'); ?>…">

            <ul class="cf personnel-list list delay-children">
              @foreach($employees as $employee)
                <li class="person vcard  equal-height"
                    data-title="<?php echo $employee->employeeTitle; ?>"
                    data-role="<?php  echo $employee->employeeGroup; ?>"
                    data-city="<?php  echo $employee->employeeCity; ?>">

                  <img src="/uploads/employees/{{$employee->employeeImage}}" alt="{{$employee->employeeName}}" class="photo">

                  <div class="person__info">
                    <h3 class="fn">{{$employee->employeeName}}</h3>

                    <ul class="contact-personell">
                      <li><p class="title">{{$employee->employeeTitle}}</p></li>
                      <li><p class="tel">{{$employee->employeePhone}}</p></li>
                      <li><a href="mailto:{{$employee->employeeEmail}}" class="email">{{$employee->employeeEmail}}</a></li>

                      <li class="city" style="display: none;"><?php echo $employee->employeeCity; ?></li>
                      <li class="role" style="display: none;"><?php echo $employee->employeeGroup; ?></li>
                    </ul>
                  </div>
                </li>
              @endforeach
            </ul>
          </div>
        </section>
      </div>
    </main>

    @include('includes.footer')
  </div>
</div>

@include('includes.document-end')
