<!--
//
// 3.blade.php används för KONTAKT
//
-->

@include('includes.document-start')

<div class="site-container">
  @include('includes.header')

  <div class="mobile-push">
    <main class="main cityscape">
      <div class="sidebar-holder">
        @include('includes.staff')

        <section class="main-body no-limit">
          <header class="page-head">
            <h1>{{$pagecontent->pageName}}</h1>

            <div class="header-image">
              <div id="map-canvas"></div>
            </div>
          </header>

          {{$pagecontent->pageContent}}
        </section>
      </div>
    </main>

    @include('includes.footer')
  </div>
</div>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCrLbn1f_s9--Rf_JEI4iY7XKNt9v_ioUw&sensor=true"></script>
<script src="/assets/js/google-maps.min.js"></script>

<script>
  // Initialisera karta
  google.maps.event.addDomListener(window, "load", googleMaps("Skövde"));
</script>

@include('includes.document-end')
