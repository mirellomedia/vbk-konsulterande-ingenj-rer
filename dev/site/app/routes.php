<?php

  $languages = array('sv', 'en');
  $defaultlanguage = Config::get('app.locale');
  $locale = Request::segment(1); //fetches first URI segment

  if(in_array($locale, $languages) && $locale != $defaultlanguage) {
    App::setLocale($locale);
  }
  else {
    App::setLocale($defaultlanguage);
    $locale = '';
  }


Route::group(array('prefix' => $locale), function() {

    Route::get('/', array('as' => 'index', function() {

      $currentPath = dirname(__FILE__).'/dbcalls/';

      require_once($currentPath.'langinfo.php');
      require_once($currentPath.'data_mainnav.php');
      require_once($currentPath.'data_subnav.php');
      require_once($currentPath.'data_slideshow.php');

      require_once($currentPath.'data_articles.php');

      $id = 'start';
      require_once($currentPath.'data_pagecontent.php');

      return View::make('pages.index', $data);
    }));

    Route::get('/search', function() {
      $currentPath = dirname(__FILE__).'/dbcalls/';

      require_once($currentPath.'langinfo.php');
      require_once($currentPath.'data_mainnav.php');
      require_once($currentPath.'data_subnav.php');

      require_once($currentPath.'data_search.php');

      return View::make('pages.5', $data);
    });

    Route::get('{id}', function($id) {

      $currentPath = dirname(__FILE__).'/dbcalls/';


      require_once($currentPath.'langinfo.php');
      require_once($currentPath.'data_mainnav.php');
      require_once($currentPath.'data_subnav.php');
      require_once($currentPath.'data_pagecontent.php');


      $data['subpages'] = DB::select('SELECT      pc.pageName,
                                                  pc.pageLink
                                      FROM        pagecontent pc
                                      INNER JOIN  page p
                                      ON          p.pageId = pc.pageId
                                      WHERE       p.pageParent = ?
                                      AND         pc.languageId = ?
                                  ', array($data['pagecontent']->pageId, $langInfo->languageId));
      $parentLink = $data['pagecontent']->pageLink;

      foreach($data['subpages'] as $subpage){
        $subpageExplode = explode("/", $subpage->pageLink);

        if ($subpageExplode[0] == 'projekt') {
          $subpage->pageLink = '/'.$subpage->pageLink;
          continue;
        }

        $subpage->pageLink = $parentLink.'/'.$subpage->pageLink;
      }

      require_once($currentPath.'data_articles.php');
      require_once($currentPath.'data_employeegroups.php');
      require_once($currentPath.'data_employees.php');
      require_once($currentPath.'data_archive.php');
      require_once($currentPath.'data_allprojects.php');

      return View::make('pages.'.$pageTemplate, $data);
    });

    Route::get('{hej}/{id}', function($hej,$id) {
      $pageParent = $hej;


      if($id == 'byggprojektering'){
        $projectType = 1;
      }
      elseif($id == 'projektadministration'){
        $projectType = 2;
      }
      else{
        $projectType = 3;
      }

      $currentPath = dirname(__FILE__).'/dbcalls/';

      require_once($currentPath.'langinfo.php');
      require_once($currentPath.'data_pagecontent.php');
      require_once($currentPath.'data_mainnav.php');
      require_once($currentPath.'data_subnav.php');
      require_once($currentPath.'data_articles.php');
      require_once($currentPath.'data_employeegroups.php');
      require_once($currentPath.'data_employees.php');
      require_once($currentPath.'data_archive.php');
      require_once($currentPath.'parentpage.php');
      require_once($currentPath.'data_subpages.php');

      if($pageTemplate == 6){
        require_once($currentPath.'data_projects.php');
        require_once($currentPath.'data_projectcategory.php');
        require_once($currentPath.'data_allprojects.php');
      }
      elseif($pageTemplate == 10){
        require_once($currentPath.'data_allprojects.php');
        require_once($currentPath.'data_projects.php');

      }

      return View::make('pages.'.$pageTemplate, $data);

    });

    Route::get('nyheter/{id}/{link}', function($id, $link) {
      $currentPath = dirname(__FILE__).'/dbcalls/';

      require_once($currentPath.'langinfo.php');
      require_once($currentPath.'data_mainnav.php');
      require_once($currentPath.'data_subnav.php');

      require_once($currentPath.'data_archive.php');

      $data['fetch'] = DB::select('SELECT         ac.articleId,
                                                  ac.articleHeader,
                                                  ac.articleSum,
                                                  ac.articleContent,
                                                  a.articleDate,
                                                  ac.articleLink
                                   FROM           articlecontent ac
                                   INNER JOIN     article a
                                   ON             a.articleId = ac.articleId
                                   WHERE          ac.articleId = ?
                                   AND            ac.articleLink = ?
                               ', array($id, $link));
      if(!$data['fetch']){
        return View::make('404');
        exit();
      }
      $data['pagecontent'] = $data['fetch'][0];
      unset($data['fetch']);

      return View::make('pages.4', $data);
    });

    Route::get('projekt/{id}/', function($id) {

      $currentPath = dirname(__FILE__).'/dbcalls/';

      require_once($currentPath.'langinfo.php');
      require_once($currentPath.'data_pagecontent.php');
      require_once($currentPath.'data_mainnav.php');
      require_once($currentPath.'data_subnav.php');
      require_once($currentPath.'data_projectcategory.php');
      require_once($currentPath.'data_allprojects.php');

      return View::make('pages.7', $data);

    });

    Route::get('{hej}/{id}/{projektkategori}', function($hej,$id,$projektkategori) {
      if($projektkategori == 'all'){
        header('Location: /projekt/'.$id);
        exit();
      }
      $currentPath = dirname(__FILE__).'/dbcalls/';

      require_once($currentPath.'langinfo.php');
      require_once($currentPath.'data_pagecontent.php');
      require_once($currentPath.'data_mainnav.php');
      require_once($currentPath.'data_subnav.php');
      $data['projektkategoriName'] = ucfirst($projektkategori);
      $data['projektkategoriLink'] = $projektkategori;
      require_once($currentPath.'data_projectcategory.php');
      require_once($currentPath.'data_projectsinprojectcategory.php');
      require_once($currentPath.'data_allprojects.php');

      return View::make('pages.7', $data);
    });

    Route::get('{hej}/{id}/{projektkategori}/{projekt}', function($hej,$id,$projektkategori, $thisproject) {
      $currentPath = dirname(__FILE__).'/dbcalls/';


      if($id == 'byggprojektering'){
        $projectType = 1;
      }
      elseif($id == 'projektadministration'){
        $projectType = 2;
      }
      else{
        $projectType = 3;
      }

      require_once($currentPath.'langinfo.php');
      require_once($currentPath.'data_pagecontent.php');
      require_once($currentPath.'data_mainnav.php');
      require_once($currentPath.'data_subnav.php');
      $data['projektkategoriName'] = ucfirst($projektkategori);
      $data['projektkategoriLink'] = $projektkategori;
      require_once($currentPath.'data_projectcategory.php');
      require_once($currentPath.'data_project.php');
      require_once($currentPath.'data_allprojects.php');

      return View::make('pages.8', $data);
    });

});
