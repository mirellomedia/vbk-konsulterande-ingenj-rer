<?php 

	return array(
		'createsforthefuture' => 'skapar för framtiden',
		'welcometext' 				=> 'Välkommen till VBK. Vi är ett konsulterande ingenjörsföretag inom byggbranschen.',
		'companyname' 				=> 'VBK Konsulterande Ingenjörer AB',
		'memberof'						=> 'Medlem i Svenska Teknik & Designföretagen',
		'iso'									=> 'Kvalitets- och miljöcertifierade enligt ISO-9001 och ISO-14001',
		'social' 							=> 'Socialt',
		'officegothenburg' 		=> 'Kontor Göteborg',
		'officeskovde' 				=> 'Kontor Skövde',
		'gothenburg' 					=> 'Göteborg'
	);