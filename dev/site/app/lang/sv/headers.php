<?php 

return array(
  'news' => 'Nyheter',
  'about-vbk' => 'Om VBK',
  'read-more' => 'Läs mer',
  'our-employees' => 'Våra medarbetare',
  'contact' => 'Kontakt',
  'menu' => 'Meny',
  'close' => 'Stäng',
  'company-information' => 'Företagsinformation',
  'main-menu' => 'Huvudmeny',
  'search-website' => 'Sök på webbplats',
  'type-to-filter' => 'Skriv för att filtrera',
  'gothenburg' => 'Göteborg',
  'Göteborg' => 'Göteborg',
  'skovde' => 'Skövde',
  'office-gothenburg' => 'Göteborg, huvudkontor',
  'office-skovde' => 'Skövde, lokalkontor',
  'employees' => 'Personal',
  'all-employees' => 'Alla medarbetare',
  'Ledningsgrupp' => 'Ledningsgrupp',
  'Byggprojektering' => 'Byggprojektering',
  'Projektadministration' => 'Projektadministration',
  'Internadministration' => 'Internadministration',
  'Kontrollansvarig PBL' => 'Kontrollansvarig PBL',
  'Bas P / Bas U' => 'Bas P / Bas U',
  'Skyddsrumssakkunnig' => 'Skyddsrumssakkunnig',
  'Certifierad besiktningsman' => 'Certifierad besiktningsman',
  'our-location' => 'Här finns vi',
  'other' => 'Övrigt',
  'supplier-info' => 'Leverantörsinformation',
);