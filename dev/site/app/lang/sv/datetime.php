<?php 

	return array(

		// MONTHS
		'january' 	=> 'Januari',
		'february' 	=> 'Februari',
		'march'			=> 'Mars',
		'april'			=> 'April',
		'may'				=> 'Maj',
		'june'			=> 'Juni',
		'july'			=> 'Juli',
		'august'		=> 'Augusti',
		'september'	=> 'September',
		'october'		=> 'Oktober',
		'november'	=> 'November',
		'december' 	=> 'December',
		'monday'		=> 'Måndag',
		'tuesday'		=> 'Tisdag',
		'wednesday'	=> 'Onsdag',
		'thursday'	=> 'Torsdag',
		'friday'		=> 'Fredag',
		'saturday'	=> 'Lördag',
		'sunday'		=> 'Söndag'
	);