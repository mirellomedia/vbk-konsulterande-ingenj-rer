<?php

return array(
  'news' => 'News',
  'about-vbk' => 'About VBK',
  'read-more' => 'Read more',
  'our-employees' => 'Our employees',
  'contact' => 'Contact',
  'menu' => 'Menu',
  'close' => 'Close',
  'company-information' => 'Company information',
  'main-menu' => 'Main menu',
  'search-website' => 'Search website',
  'type-to-filter' => 'Type to filter',
  'gothenburg' => 'Gothenburg',
  'Göteborg' => 'Gothenburg',
  'skovde' => 'Skövde',
  'office-gothenburg' => 'Gothenburg, main office',
  'office-skovde' => 'Skövde, local office',
  'employees' => 'Employees',
  'all-employees' => 'All employees',
  'Ledningsgrupp' => 'Management',
  'Byggprojektering' => 'Construction design',
  'Projektadministration' => 'Project administration',
  'Internadministration' => 'Internal administration',
  'Kontrollansvarig PBL' => 'Control Manager PBL',
  'Bas P / Bas U' => 'Bas P / Bas U',
  'Skyddsrumssakkunnig' => 'Shelter expert',
  'Certifierad besiktningsman' => 'Certified inspector',
  'our-location' => 'Our location',
  'other' => 'Other',
  'supplier-info' => 'Information for suppliers',
);
