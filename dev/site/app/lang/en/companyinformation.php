<?php

	return array(
		'createsforthefuture' => 'Creates for the future',
		'welcometext' 				=> 'Welcome to VBK. We are an engineering consulting firm in the construction industry.',
		'companyname' 				=> 'VBK Konsulterande Ingenjörer AB',
		'memberof'						=> 'Medlem i Svenska Teknik & Designföretagen',
		'iso'									=> 'Kvalitets- och miljöcertifierade enligt ISO-9001 och ISO-14001',
		'social' 							=> 'Social',
		'officegothenburg' 		=> 'Office Gothenburg',
		'officeskovde' 				=> 'Office Skövde',
		'gothenburg' 					=> 'Gothenburg'
	);
