<?php
$data['allprojects'] = DB::select('         SELECT      pc.projectName, 
                                                        pc.projectLink,
                                                        p.projectType,
                                                        pcy.projectcategoryLink
                                            FROM        projectcontent pc
                                            INNER JOIN  project p
                                            ON          p.projectId = pc.projectId
                                            INNER JOIN  projectcategory pcy
                                            ON          pcy.projectcategoryId = p.projectCategory
                                            ORDER BY    pc.projectName ASC
                                        ');
    foreach($data['allprojects'] as $project){
        if($project->projectType == 1){
                $project->projectType = 'byggprojektering';
            }
            elseif($project->projectType == 2){
                $project->projectType = 'projektadministration';
            }
            else{
                $project->projectType = 'underhall';
            }
    }

?>