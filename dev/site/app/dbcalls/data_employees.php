<?php
        $data['employees'] = DB::select('SELECT     e.employeeName,
                                                    e.employeeId,
                                                    e.employeePhone,
                                                    e.employeeTitle,
                                                    e.employeeEmail,
                                                    e.employeeImage,
                                                    e.employeeCity
                                        FROM        employee e
                                        WHERE       e.visibility = 1
                                        ORDER BY    e.employeeName ASC
                                    ');

        foreach($data['employees'] as $employee){
            $employee->employeeGroup = '';
            $employeeGroups = DB::select('  SELECT      eg.employeegroupName
                                            FROM        employeegroup eg
                                            INNER JOIN  employee_employeegroup eeg
                                            ON          eeg.employeegroupId = eg.employeegroupId
                                            INNER JOIN  employee e
                                            ON          eeg.employeeId = e.employeeId
                                            WHERE       e.employeeId = ?
                                    ', array($employee->employeeId));

            foreach($employeeGroups as $group){
               $employee->employeeGroup .= $group->employeegroupName .' | ';
            }
        }
?>