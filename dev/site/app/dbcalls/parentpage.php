<?php

$parentPage   = DB::select('        SELECT      p.pageId,
                                                pc.pageLink
                                    FROM        page p
                                    INNER JOIN  pagecontent pc
                                    ON          p.pageId = pc.pageId
                                    WHERE       pc.pageLink = ?
                                    AND         pc.languageId = ?
                                    LIMIT       1
                                 ', array($hej, $langInfo->languageId));
$parentId   = $parentPage[0]->pageId;
$parentLink = $parentPage[0]->pageLink;
?>
