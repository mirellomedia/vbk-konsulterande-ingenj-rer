<?php

  $id = 'search';
  $searchstring = $_GET['search'];
  $searchstringSQL = '%'.$_GET['search'].'%';

  $data['searchresultsarticles'] = DB::select(
  ' SELECT      ac.articleHeader AS pageName,
                ac.articleContent AS pageContent,
                ac.articleLink AS pageLink,
                ac.articleId
    FROM        articlecontent ac
    INNER JOIN  article a
    ON          a.articleId = ac.articleId
    AND         ac.languageId = ?
    AND         a.visibility = 1
    AND         a.articleDate <= CURDATE()
    WHERE       ac.articleHeader LIKE ?
    OR          ac.articleContent LIKE ?
  ', array($langInfo->languageId, $searchstringSQL, $searchstringSQL));

  foreach($data['searchresultsarticles'] as $searchresult){
    $searchresult->pageLink = 'nyheter/'.$searchresult->articleId . '/' . $searchresult->pageLink;
  }



          $data['searchresultsprojects'] = DB::select(' SELECT        pc.projectName AS pageName,
                                                                      pc.projectLongDesc AS pageContent,
                                                                      pc.projectLink AS pageLink,
                                                                      p.projectType,
                                                                      pcy.projectcategoryLink
                                                          FROM        projectcontent pc
                                                          INNER JOIN  project p
                                                          ON          p.projectId = pc.projectId
                                                          INNER JOIN  projectcategory pcy
                                                          ON          pcy.projectcategoryId = p.projectCategory
                                                          WHERE       pc.languageId = ?
                                                          AND         (pc.projectName LIKE ?
                                                          OR          pc.projectShortDesc LIKE ?
                                                          OR          pc.projectLongDesc LIKE ?)
                                                          AND         p.visibility = 1
                                                ', array($langInfo->languageId, $searchstringSQL, $searchstringSQL, $searchstringSQL));

          foreach($data['searchresultsprojects'] as $searchresult){
            if($searchresult->projectType == 1){
                $searchresult->projectType = 'byggprojektering';
            }
            elseif($searchresult->projectType == 2){
                $searchresult->projectType = 'projektadministration';
            }
            else{
                $searchresult->projectType = 'underhall';
            }
            $searchresult->pageLink = '/projekt/' . $searchresult->projectType . '/' . $searchresult->projectcategoryLink . '/' . $searchresult->pageLink;
          }

          $data['searchresultspages'] = DB::select('SELECT      pc.pageName,
                                                                pc.pageContent,
                                                                pc.pageLink,
                                                                pc.pageId,
                                                                p.pageParent
                                                    FROM        pagecontent pc
                                                    INNER JOIN  page p
                                                    ON          p.pageId = pc.pageId
                                                    AND         pc.languageId = ?
                                                    WHERE       (pc.pageName LIKE ?
                                                    OR          pc.pageContent LIKE ?)
                                                    AND         p.visibility = 1
                                                ', array($langInfo->languageId, $searchstringSQL, $searchstringSQL));

          foreach($data['searchresultspages'] as $page){
            if($page->pageParent != 0){
              $pageParent                 = DB::select('SELECT      pc.pageLink
                                                        FROM        pagecontent pc
                                                        INNER JOIN  page p
                                                        ON          p.pageId = pc.pageId
                                                        AND         pc.languageId = ?
                                                        WHERE       p.pageId = ?
                                                    ', array($langInfo->languageId, $page->pageParent));
              $pageParent = $pageParent[0];
              $page->pageLink = $pageParent->pageLink . '/' . $page->pageLink;
            }
            else{
              if($page->pageContent == ''){

                $temp            = DB::select(' SELECT      pc.pageLink
                                                FROM        pagecontent pc
                                                INNER JOIN  page p
                                                ON          p.pageId = pc.pageId
                                                WHERE       p.pageParent = ?
                                                AND         p.pageMenuSort = ?
                                                AND         pc.languageId = ?
                                                LIMIT       1
                                              ', array($page->pageId, '1', $langInfo->languageId));

                if($temp){
                  $page->pageLink .= '/'.$temp[0]->pageLink;
                }
              }
            }
          }


          $data['searchstring'] = $searchstring;
          $data['searchresults'] = array_merge($data['searchresultspages'], $data['searchresultsarticles'], $data['searchresultsprojects']);
          $data['resultscount'] = count($data['searchresults']);
          foreach($data['searchresults'] as $searchresult){

            $searchresult->pageContent = strip_tags($searchresult->pageContent);
            $line=$searchresult->pageContent;
            if (preg_match('/^.{1,300}\b/s', $searchresult->pageContent, $match))
            {
                $searchresult->pageContent=$match[0];
            }
            $searchresult->pageContent = preg_replace("/\p{L}*?".preg_quote($searchstring)."\p{L}*/ui", "<b>$0</b>", $searchresult->pageContent);
          }

          ?>
