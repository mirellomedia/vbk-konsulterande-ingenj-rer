<?php

if($id == 'byggprojektering') {
    $id = 1;
}
elseif($id == 'projektadministration') {
    $id = 2;
}
else {
    $id = 3;
}

$data['projectsinprojectcategory'] = 
  DB::select('  SELECT      pc.projectName,
                            pc.projectLink,
                            pc.projectId,
                            pc.projectShortDesc,
                            pi.projectimageLink
                FROM        projectcontent pc
                INNER JOIN  project p
                ON          p.projectId = pc.projectId
                INNER JOIN  projectcategory pca
                ON          pca.projectcategoryId = p.projectCategory
                INNER JOIN  projectimage pi
                ON          pi.projectId = p.projectId
                INNER JOIN  project_projectcategory ppc
                ON          p.projectId = ppc.projectId
                WHERE       pca.projectcategoryLink = ?
                AND         p.projectType = ?
                AND         pc.languageId = ?
                AND         pi.projectimageOrder = 1
                LIMIT       9
  ', array($projektkategori, $id, $langInfo->languageId));

?>