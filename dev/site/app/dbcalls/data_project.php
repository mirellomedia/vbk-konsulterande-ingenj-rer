<?php
    $data['thisproject'] = DB::select('     SELECT      pc.projectName,
                                                        pc.projectLink,
                                                        pc.projectId,
                                                        pc.projectShortDesc,
                                                        pc.projectLongDesc
                                            FROM        projectcontent pc
                                            INNER JOIN  project p
                                            ON          p.projectId = pc.projectId
                                            WHERE       pc.projectLink = ?
                                            AND         pc.languageId = ?
                                            LIMIT       1
                                        ', array($thisproject, $langInfo->languageId));
    $data['thisproject'] = $data['thisproject'][0];

    $data['temp'] = DB::select('            SELECT      pi.projectimageLink
                                            FROM        projectimage pi
                                            WHERE       pi.projectId = ?
                                        ', array($data['thisproject']->projectId));
    
    $data['thisproject']->images = $data['temp'];
    unset($data['temp']);

?>