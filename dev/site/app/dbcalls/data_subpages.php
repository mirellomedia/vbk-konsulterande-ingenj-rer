<?php
  $data['subpages'] = DB::select(
  ' SELECT      pc.pageName,
                pc.pageLink
    FROM        pagecontent pc
    INNER JOIN  page p
    ON          p.pageId      = pc.pageId
    WHERE       p.pageParent  = ?
    AND         pc.languageId = ?
    AND         p.visibility  = 1
    AND         p.hidden     != 1
  ', array($parentId, $langInfo->languageId));

  foreach($data['subpages'] as $subpage) {
    $subpageExplode = explode("/", $subpage->pageLink);

    if($subpageExplode[0] == 'projekt') {
      $subpage->pageLink = ''.$subpage->pageLink;
      continue;
    }

    $subpage->pageLink = $parentLink.'/'.$subpage->pageLink;
  }
?>
