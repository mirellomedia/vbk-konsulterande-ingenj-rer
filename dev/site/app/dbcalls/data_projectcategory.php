<?php
    if(@$projektkategori == 'all'){
        $data['projectcategory'] = DB::select(' SELECT      pc.projectName as projectcategoryName,
                                                            pc.projectLink,
                                                            pcy.projectcategoryLink
                                                FROM        projectcontent pc
                                                INNER JOIN  project p
                                                ON          p.projectId = pc.projectId
                                                INNER JOIN  projectcategory pcy
                                                ON          pcy.projectcategoryId = p.projectCategory
                                                WHERE       p.projectType = ?
                                                ORDER BY    pc.projectName ASC
                                        ', array($projectType));

        foreach($data['projectcategory'] as $projectcategory){
            $projectcategory->projectcategoryLink = $projectcategory->projectcategoryLink.'/'.$projectcategory->projectLink;
          }
    }
    else{
        $data['projectcategory'] = DB::select(' SELECT      pc.projectcategoryName,
                                                            pc.projectcategoryLink,
                                                            pc.projectcategoryImage
                                                FROM        projectcategory pc
                                                WHERE       pc.projectcategoryid != 0
                                                ORDER BY    pc.projectcategoryid ASC
                                        ');
    }

?>