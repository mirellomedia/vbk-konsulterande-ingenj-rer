<?php
  $data['articles'] = DB::select(
  ' SELECT      ac.articleId,
                ac.articleHeader,
                ac.articleSum,
                a.articleDate,
                ac.articleLink
    FROM        articlecontent ac
    INNER JOIN  article a
    ON          a.articleId = ac.articleId
    WHERE       ac.languageId = ?
    AND         a.visibility = 1
    AND         a.articleDate <= CURDATE()
    ORDER BY    ac.articleDate DESC,
                a.articleId DESC
    LIMIT       3
', array($langInfo->languageId));
?>
