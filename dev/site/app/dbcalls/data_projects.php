<?php
$data['projects'] = DB::select('            SELECT      pc.projectName, 
                                                        pc.projectLink,
                                                        pi.projectimageLink
                                            FROM        projectcontent pc
                                            INNER JOIN  project p
                                            ON          p.projectId = pc.projectId
                                            AND         p.projectType = ?
                                            INNER JOIN  projectimage pi 
                                            ON          pi.projectId = p.projectId
                                            WHERE       pi.projectimageOrder = 1
                                            ORDER BY    pc.projectName ASC
                                        ', array($projectType));

?>