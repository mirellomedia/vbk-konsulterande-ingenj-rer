<?php

if(isset($pageParent) && $pageParent !== ''){
    $test = DB::select('        SELECT      p.pageId
                                ,           pc.pageName
                                FROM        page p
                                INNER JOIN  pagecontent pc
                                ON          p.pageId = pc.pageId
                                WHERE       pc.pageLink = ?
                                AND         pc.languageId = ?
                                LIMIT       1
                             ', array($pageParent, $langInfo->languageId));

    $pageParent = intval($test[0]->pageId);

    $data['pageParent'] = $test[0]->pageName;

    $data['fetch'] = DB::select('SELECT         p.pageTemplate,
                                                pc.pageName,
                                                pc.pageContent,
                                                p.pageId,
                                                pc.pageLink,
                                                pc.pageHeaderimage,
                                                pc.pageHeader
                                    FROM        page p
                                    INNER JOIN  pagecontent pc
                                    ON          p.pageId = pc.pageId
                                    WHERE       pc.pageLink = ?
                                    AND         pc.languageId = ?
                                    AND         p.pageParent = ?
                                    LIMIT       1
                                 ', array($id, $langInfo->languageId, $pageParent));
}

else{
    $data['fetch'] = DB::select('SELECT         p.pageTemplate,
                                                pc.pageName,
                                                pc.pageContent,
                                                p.pageId,
                                                pc.pageLink,
                                                pc.pageHeaderimage,
                                                pc.pageHeader
                                    FROM        page p
                                    INNER JOIN  pagecontent pc
                                    ON          p.pageId = pc.pageId
                                    WHERE       pc.pageLink = ?
                                    AND         pc.languageId = ?
                                    LIMIT       1
                                 ', array($id, $langInfo->languageId));

    if(!isset($data['pageParent'])){
        $data['pageParent'] = $data['fetch'][0]->pageName;
    }
}


    if(!$data['fetch']){
      return View::make('404');
      exit();
    }
$data['pagecontent'] = $data['fetch'][0];
unset($data['fetch']);

$pageTemplate = $data['pagecontent']->pageTemplate;
?>
