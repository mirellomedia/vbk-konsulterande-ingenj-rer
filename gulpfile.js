/*
  gulpfile.js
  ===========

  Author:         Mirello Media
  Author website: http://mirello.se
  Creation date:  2014-07-31
*/


// ==============
//  DEPENDENCIES
// ==============

var
  // generic:
  gulp          = require('gulp'),
  gutil         = require('gulp-util'),
  rename        = require('gulp-rename'),
  notify        = require('gulp-notify'),
  livereload    = require('gulp-livereload'),
  newer         = require('gulp-newer'),
  plumber       = require('gulp-plumber'),
  replace       = require('gulp-replace-task'),
  lazypipe      = require('lazypipe'),
  runSequence   = require('run-sequence'),

  // css:
  sass          = require('gulp-ruby-sass'),
  autoprefixer  = require('gulp-autoprefixer'),
  minifycss     = require('gulp-minify-css'),
  cmq           = require('gulp-combine-media-queries'),

  // javascript:
  jshint        = require('gulp-jshint'),

  // minify:
  minifyHTML    = require('gulp-minify-html'),
  uglify        = require('gulp-uglify'),
  concat        = require('gulp-concat'),
  ngAnnotate    = require('gulp-ng-annotate'),

  // image compression:
  imagemin      = require('gulp-imagemin'),
  pngcrush      = require('imagemin-pngcrush');


// ============
//  PATH SETUP
// ============

var dev = {
  cms:  'dev/cms/',
  site: 'dev/site/'
};

var dis = {
  cms:  'dist/cms/',
  site: 'dist/'
};

var assets = {
  dev: {
    cms: {
      img:      dev.cms  + 'assets/img/**/*',
      scripts:  dev.cms  + 'assets/js/**/*.js',
      vendor:   dev.cms  + 'assets/vendor/**/*.js',
      fonts:    dev.cms  + 'assets/fonts/**/*',
      scss:     dev.cms  + 'assets/scss/**/*',
      style:    dev.cms  + 'assets/scss/style.scss'
    },

    site: {
      img:      dev.site + 'public/assets/img/**/*',
      scripts:  dev.site + 'public/assets/js/**/*.js',
      vendor:   dev.site + 'public/assets/vendor/**/*.js',
      fonts:    dev.site + 'public/assets/fonts/**/*',
      scss:     dev.site + 'public/assets/scss/**/*',
      style:    dev.site + 'public/assets/scss/style.scss',
      pageflip: dev.site + 'public/assets/pageflip/**/*'
    }
  },

  dis: {
    cms: {
      img:      dis.cms  + 'assets/img/',
      scripts:  dis.cms  + 'assets/js/',
      fonts:    dis.cms  + 'assets/fonts/',
      css:      dis.cms  + 'assets/css/'
    },

    site: {
      img:      dis.site + 'public/assets/img/',
      scripts:  dis.site + 'public/assets/js/',
      fonts:    dis.site + 'public/assets/fonts/',
      css:      dis.site + 'public/assets/css/',
      pageflip: dis.site + 'public/assets/pageflip/'
    }
  }
};

var app = {
  dev: {
    build: [
      dev.cms + 'app/core/app.js',
      dev.cms + 'app/core/*',
      dev.cms + 'app/service/*',
      dev.cms + 'app/factory/*',
      dev.cms + 'app/controller/*',
      dev.cms + 'app/**/*.js'
    ],

    view: [
      dev.cms + '**/*',
      '!' + dev.cms + 'assets/**/*',
      '!' + dev.cms + 'app/**/*'
    ],

    dotfiles: [
      dev.cms + '**/.htaccess',
      '!' + dev.cms + '**/.DS_Store',
      '!' + dev.cms + '**/.keep',
      '!' + dev.cms + '**/.gitkeep'
    ]
  },

  dis: {
    angular: dis.cms + 'app/',
    root:    dis.cms
  }
};

var laravel = {
  dev: {
    view: dev.site + 'app/views/**/*.php',
    core: dev.site + 'vendor/**/*',

    files: [
      dev.site + '**/*',
      '!' + dev.site + 'public/assets/**/*',
      '!' + dev.site + 'app/views/**/*',
      '!' + dev.site + 'vendor/**/*'
    ],

    dotfiles: [
      dev.site + '**/.htaccess',
      '!' + dev.site + '**/.DS_Store',
      '!' + dev.site + '**/.keep',
      '!' + dev.site + '**/.gitkeep'
    ]
  },

  dis: {
    view:  dis.site + 'app/views/',
    core:  dis.site + 'vendor/',
    files: dis.site
  }
};


// ===========
//  FUNCTIONS
// ===========

var // timestamp:
  date  = new Date(),
  yyyy  = date.getFullYear().toString(),
  mm    = (date.getMonth() + 1).toString(),
  dd    = date.getDate().toString(),

  mmChars   = mm.split(''),
  ddChars   = dd.split(''),

  timestamp = yyyy + '-' + (mmChars[1]?mm:"0" + mmChars[0]) + '-' + (ddChars[1]?dd:"0" + ddChars[0]);


// ==========
//  SETTINGS
// ==========

var settings = {
  sass:       { precision: 2 },
  cmq:        { log: true },
  rename:     { suffix: '.min' },
  minifycss:  { keepSpecialComments: 1 },
  notify:     { message: 'Task complete.' },
  htmlmin:    { collapseWhitespace: false,
                removeComments: false
              },
  replace:    { patterns: [{
                  match: 'timestamp',
                  replacement: timestamp
                }]
              },

  appname:    'app.js',
  jsname:     'main.js',

  autoprefixer: [
    'last 2 version',
    'Explorer >= 8',
    'Firefox >= 16',
    'Chrome >= 21',
    'Opera >= 15',
    'Safari >= 6'
  ],

  imagemin: {
    optimizationLevel: 3,
    progressive: true,
    interlaced: true,
    use: [pngcrush()]
  },
};


// ===========
//  LAZYPIPES
// ===========

var tasks = {
  generic: {
    // triggra livereload och ge notifikation:
    finished: lazypipe()
      .pipe(livereload),
  },

  styles: {
    // kompilera sass, autoprefixa och kombinera media queries:
    compile: lazypipe()
      .pipe(sass)
      .pipe(autoprefixer, settings.autoprefixer)
      .pipe(cmq,          settings.cmq)
      .pipe(replace,      settings.replace),

    // minifiera css och döp om dokument:
    minify: lazypipe()
      .pipe(rename,    settings.rename)
      .pipe(minifycss, settings.minifycss),
  },

  scripts: {
    // minifiera js och döp om dokument:
    minify: lazypipe()
      .pipe(rename, settings.rename),
  },

  images: {
    // komprimera bilder:
    compress: lazypipe()
      .pipe(imagemin, settings.imagemin),
  },

  app: {
    // minifera angular och döp om dokument
    minify: lazypipe()
      .pipe(rename, settings.rename)
      .pipe(ngAnnotate)
      .pipe(uglify),
  },
};


// ===========
//  CMS TASKS
// ===========

// css:
gulp.task('cms-css', function() {
  var input  = assets.dev.cms.style,
      output = assets.dis.cms.css;

  return gulp.src(input)
    .pipe(plumber())

    .pipe(tasks.styles.compile())
    .pipe(gulp.dest(output))

    .pipe(tasks.styles.minify())
    .pipe(gulp.dest(output))

    .pipe(tasks.generic.finished());
});

// javascript:
gulp.task('cms-js', function() {
  var input  = assets.dev.cms.scripts,
      output = assets.dis.cms.scripts;

  return gulp.src(input)
    .pipe(plumber())

    .pipe(concat(settings.jsname))
    .pipe(gulp.dest(output))

    .pipe(tasks.scripts.minify())
    .pipe(gulp.dest(output))

    .pipe(tasks.generic.finished());
});

// vendor:
gulp.task('cms-vendor', function() {
  var input  = assets.dev.cms.vendor,
      output = assets.dis.cms.scripts;

  return gulp.src(input)
    .pipe(plumber())
    .pipe(gulp.dest(output))

    .pipe(tasks.scripts.minify())
    .pipe(gulp.dest(output))

    .pipe(tasks.generic.finished());
});

// fonts:
gulp.task('cms-fonts', function() {
  var input  = assets.dev.cms.fonts,
      output = assets.dis.cms.fonts;

  return gulp.src(input)
    .pipe(gulp.dest(output))

    .pipe(tasks.generic.finished());
});

// images:
gulp.task('cms-images', function() {
  var input  = assets.dev.cms.img,
      output = assets.dis.cms.img;

  return gulp.src(input)
    .pipe(newer(output))

    .pipe(tasks.images.compress())
    .pipe(gulp.dest(output))

    .pipe(tasks.generic.finished());
});

// angular:
gulp.task('cms-angular', function() {
  var input  = app.dev.build,
      output = assets.dis.cms.scripts;

  return gulp.src(input)
    .pipe(plumber())

    .pipe(concat(settings.appname))
    .pipe(gulp.dest(output))

    .pipe(rename(settings.rename))
    .pipe(ngAnnotate())
    .pipe(uglify())

    .pipe(gulp.dest(output))

    .pipe(tasks.generic.finished());
});

// view:
gulp.task('cms-view', function() {
  var input  = app.dev.view,
      output = app.dis.root;

  return gulp.src(input)
    .pipe(newer(output))

    // .pipe(ngAnnotate())
    // .pipe(minifyHTML())

    .pipe(gulp.dest(output))

    .pipe(tasks.generic.finished());
});

// .dotfiles:
gulp.task('cms-dotfiles', function() {
  var input  = app.dev.dotfiles,
      output = app.dis.root;

  return gulp.src(input)
    .pipe(newer(output))

    .pipe(gulp.dest(output))

    .pipe(tasks.generic.finished());
});


// ============
//  SITE TASKS
// ============

// css:
gulp.task('site-css', function() {
  var input  = assets.dev.site.style,
      output = assets.dis.site.css;

  return gulp.src(input)
    .pipe(plumber())

    .pipe(tasks.styles.compile())
    .pipe(gulp.dest(output))

    .pipe(tasks.styles.minify())
    .pipe(gulp.dest(output))

    .pipe(tasks.generic.finished());
});

// javascript:
gulp.task('site-js', function() {
  var input  = assets.dev.site.scripts,
      output = assets.dis.site.scripts;

  return gulp.src(input)
    .pipe(plumber())

    .pipe(concat(settings.jsname))
    .pipe(gulp.dest(output))

    .pipe(tasks.scripts.minify())
    .pipe(gulp.dest(output))

    .pipe(tasks.generic.finished());
});

// vendor:
gulp.task('site-vendor', function() {
  var input  = assets.dev.site.vendor,
      output = assets.dis.site.scripts;

  return gulp.src(input)
    .pipe(plumber())
    .pipe(gulp.dest(output))

    .pipe(tasks.scripts.minify())
    .pipe(gulp.dest(output))

    .pipe(tasks.generic.finished());
});

// fonts:
gulp.task('site-fonts', function() {
  var input  = assets.dev.site.fonts,
      output = assets.dis.site.fonts;

  return gulp.src(input)
    .pipe(gulp.dest(output))

    .pipe(tasks.generic.finished());
});

// pageflip:
gulp.task('site-pageflip', function() {
  var input  = assets.dev.site.pageflip,
      output = assets.dis.site.pageflip;

  return gulp.src(input)
    .pipe(gulp.dest(output))

    .pipe(tasks.generic.finished());
});

// images:
gulp.task('site-images', function() {
  var input  = assets.dev.site.img,
      output = assets.dis.site.img;

  return gulp.src(input)
    .pipe(newer(output))

    .pipe(tasks.images.compress())
    .pipe(gulp.dest(output))

    .pipe(tasks.generic.finished());
});

// view:
gulp.task('site-view', function() {
  var input  = laravel.dev.view,
      output = laravel.dis.view;

  return gulp.src(input)
    .pipe(plumber())
    .pipe(newer(output))

    .pipe(minifyHTML())
    .pipe(gulp.dest(output))

    .pipe(tasks.generic.finished());
});

// laravel:
gulp.task('site-laravel-core', function() {
  var input  = laravel.dev.core,
      output = laravel.dis.core;

  return gulp.src(input)
    .pipe(newer(output))
    .pipe(gulp.dest(output));
});

gulp.task('site-laravel-files', function() {
  var input  = laravel.dev.files,
      output = laravel.dis.files;

  return gulp.src(input)
    .pipe(newer(output))
    .pipe(gulp.dest(output));
});

// .dotfiles:
gulp.task('site-dotfiles', function() {
  var input  = laravel.dev.dotfiles,
      output = laravel.dis.files;

  return gulp.src(input)
    .pipe(newer(output))
    .pipe(gulp.dest(output));
});


// =============
//  CORE EVENTS
// =============

// watch:
gulp.task('watch', function() {

  // cms assets:
  gulp.watch(assets.dev.cms.scss,      ['cms-css']);
  gulp.watch(assets.dev.cms.js,        ['cms-js']);
  gulp.watch(assets.dev.cms.vendor,    ['cms-vendor']);
  gulp.watch(assets.dev.cms.fonts,     ['cms-fonts']);
  gulp.watch(assets.dev.cms.img,       ['cms-images']);

  // cms view:
  gulp.watch(app.dev.build,            ['cms-angular']);
  gulp.watch(app.dev.view,             ['cms-view']);
  gulp.watch(app.dev.dotfiles,         ['cms-dotfiles']);

  // site assets:
  gulp.watch(assets.dev.site.scss,     ['site-css']);
  gulp.watch(assets.dev.site.scripts,  ['site-js']);
  gulp.watch(assets.dev.site.vendor,   ['site-vendor']);
  gulp.watch(assets.dev.site.fonts,    ['site-fonts']);
  gulp.watch(assets.dev.site.img,      ['site-images']);
  gulp.watch(assets.dev.site.pageflip, ['site-pageflip']);

  // site view:
  gulp.watch(laravel.dev.dotfiles,     ['site-dotfiles']);
  gulp.watch(laravel.dev.view,         ['site-view']);
  // gulp.watch(laravel.dev.files,        ['site-laravel-files']);

});

// sequence:
gulp.task('sequence', function() {
  runSequence(

    // scaffold:
    ['cms-angular', 'cms-view'],
    ['site-view', 'site-laravel-core', 'site-laravel-files'],
    ['cms-dotfiles', 'site-dotfiles'],

    // text assets:
    ['cms-css', 'cms-js', 'cms-vendor', 'cms-fonts'],
    ['site-css', 'site-js', 'site-vendor', 'site-fonts'],

    // etc:
    ['watch', 'site-pageflip'],

    // images:
    ['cms-images', 'site-images']
  );
});

// default:
gulp.task('default', function() {
  gulp.start('sequence');
});
